//
//  UserProfiles+CoreDataProperties.swift
//  
//
//  Created by Pisal on 11/30/2561 BE.
//
//

import Foundation
import CoreData


extension UserProfiles {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserProfiles> {
        return NSFetchRequest<UserProfiles>(entityName: "UserProfiles")
    }

    @NSManaged public var email: String?
    @NSManaged public var facebookProvider: Bool
    @NSManaged public var imageData: NSData?
    @NSManaged public var username: String?

}
