// // // // // // // // // // // // // // // // // // // // //    READ ME    // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//                                                                  Welcome teammates (^_^). Welcome to this ReusableFunction Document.                  //
//                                                                                                                                                                                                           //
//                                                              This document is mainly focuses of the detail description of a reusable function              //
//                                                                      that you have created and wants to share for our team to use it.                               //
//                                                                                                                                                                                                           //
//       => NOTE: To use a reusable function, you must mention as like below sample :                                                                      //
//                        Ex: ChhayrithReusableFunction.shared.functionYouWantToUse()                                                                            //
//                                                                                                                                                                                                           //
//       => Below is a sample to describe your Reusable Function.                                                                                                      //
//                                                                                                                                                                                                           //
//        ====> Reusable  1 :  <===                                                                                                                                                       //  
//        func loadImageToCacheWithUrlString( urlString: String, cacheName: NSCache<AnyObject, AnyObject>) -> UIImage { }     //
//        -> This function return back UIImage and takes two parameters:                                                                                            //
//        + urlString : String  => Image download URL                                                                                                                           //
//        + cacheName: NSCache  => Name of a specific NSCache we want to save image to.                                                          //   
//        -> This function used to download image via urlString parameter and save that image to cache via cacheName provided. // 
//        -> Once the image is saved to cache, when the function called once again, it will load that image from cache.121233        // 
//                                                                                                                                                                                                           //   
//        ====> Reusable 2: <===                                                                                                                                                          //  
//                                                                                                                                                                                                          //
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 


========>  Chhayrith's Created Reusable Function  <================

====> Reusable  1 :  <===
func loadImageToCacheWithUrlString( urlString: String, cacheName: NSCache<AnyObject, AnyObject>, completion: @escaping (_ success: Bool, _ image: UIImage) -> Void) 
-> This function takes two parameters: 
    + urlString : String  => Image download URL
    + cacheName: NSCache  => Name of a specific NSCache we want to save image to.
-> This function provide a completion of success and UIImage
-> This function used to download image via urlString parameter and save that image to cache via cacheName provided.
-> Once the image is saved to cache, when the function called once again, it will load that image from cache.121233

====> Reusable 2: <===




==========>  End Chhayrith's Reusable Function Block  <=============



################################################################################################################################



==============>  Pisal Created Reusable Function  <================





==============>  End Pisal Reusable Function Block  <===============



################################################################################################################################



==============>  Senghak Created Reusable Function  <================





==============>  End Senghak Reusable Function Block  <===============
