//
//  CacheFile.swift
//  vKclub
//
//  Created by Chhayrith on 12/1/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import Foundation

let contactCache = NSCache<AnyObject, AnyObject>()
let contactProfileImgCache = NSCache<AnyObject, AnyObject>()
let phonebookCache = NSCache<AnyObject, AnyObject>()
let phonebookImageCache = NSCache<AnyObject, AnyObject>()
let mapDataCache = NSCache<AnyObject, AnyObject>()
let mapImageCache = NSCache<AnyObject, AnyObject>()
let vkPointQRImageCache = NSCache<AnyObject, AnyObject>()
