//
//  CacheKey.swift
//  vKclub
//
//  Created by Chhayrith on 12/3/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import Foundation

func phonebookCacheKey() -> String{
    return "phonebook"
}

func contactCacheKey(userId: String) -> String{
    return "contactCache+\(userId)"
}
func contactProfileImageCacheKey(contactId: String) -> String{
    return "contactProfileImage+\(contactId)"
}

