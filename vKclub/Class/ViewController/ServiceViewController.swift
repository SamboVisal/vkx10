//
//  ServiceViewController.swift
//  vKclub
//
//  Created by Heng Senghak on 11/12/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit

struct Headline {
    var id: Int
    var label: String
    var btn: String
    var icon: UIImage
}



class ServiceViewController: UIViewController {
    let height = UIScreen.main.bounds.height
    let width = UIScreen.main.bounds.width
    
}

extension ServiceViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.CustomNavigationBar(title: "Service", isShadow: true, isPresentVC: false)
        reservation()
        reception()
        activity()
        restaurant()
        information()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    // handle Reservation View
    func reservation() {
        let newView = UIView(frame: CGRect(x: 0, y: 0, width: width - 48.0, height: 180.0))
        let iconImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 110.0, height: 110.0))
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 150.0, height: 40.0))
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 150.0, height: 35.0))
        
        newView.backgroundColor = .white
        newView.layer.shadowColor = UIColor.lightGray.cgColor
        newView.layer.shadowRadius = 3.0
        newView.layer.shadowOpacity = 3.0
        newView.cornerRadius = 100.0
        newView.layer.shadowOffset = CGSize(width: 1, height: 1)
        newView.layer.masksToBounds = false
        newView.center = CGPoint(x: view.bounds.midX, y: 110)
        
        label.textAlignment = .center
        label.text = "Reservation"
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 25.0)
        label.center = CGPoint(x: newView.bounds.midX, y: 70.0)
        
        iconImage.set(image: #imageLiteral(resourceName: "reservation"), focusOnFaces: false)
        iconImage.alpha = 0.07
        iconImage.center = CGPoint(x: newView.bounds.midX, y: 70.0)
        
        btn.setTitle("Book Now", for: .normal)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 25.0)
        btn.setTitleColor(.white, for: .normal)
        btn.backgroundColor = UIColor(red:0.87, green:0.62, blue:0.00, alpha:1.0)
        btn.cornerRadius = 70.0
        btn.center = CGPoint(x: newView.bounds.midX, y: 140.0)
        
        btn.addTarget(self, action: #selector(handleReservation), for: .touchUpInside)
        view.addSubview(newView)
        newView.addSubview(label)
        newView.addSubview(iconImage)
        newView.addSubview(btn)
        
        newView.translatesAutoresizingMaskIntoConstraints = true
    }
    // end handling Reservation View
    
    // handle Reception View
    func reception() {
        let newView = UIView(frame: CGRect(x: 0, y: 0, width: 170.0, height: 180.0))
        let iconImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 110.0, height: 110.0))
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 150.0, height: 40.0))
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 150.0, height: 35.0))
        
        newView.backgroundColor = .white
        newView.layer.shadowColor = UIColor.lightGray.cgColor
        newView.layer.shadowRadius = 3.0
        newView.layer.shadowOpacity = 3.0
        newView.cornerRadius = 100.0
        newView.layer.shadowOffset = CGSize(width: 1, height: 1)
        newView.layer.masksToBounds = false
        newView.center = CGPoint(x: 110, y: 310)
        
        label.textAlignment = .center
        label.text = "Reception"
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 25.0)
        label.center = CGPoint(x: newView.bounds.midX, y: 70.0)
        
        iconImage.set(image: #imageLiteral(resourceName: "reception"), focusOnFaces: false)
        iconImage.alpha = 0.07
        iconImage.center = CGPoint(x: newView.bounds.midX, y: 70.0)
        
        btn.setTitle("Call Now", for: .normal)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 25.0)
        btn.setTitleColor(.white, for: .normal)
        btn.backgroundColor = UIColor(red:0.00, green:0.50, blue:0.04, alpha:1.0)
        btn.cornerRadius = 70.0
        btn.center = CGPoint(x: newView.bounds.midX, y: 150)
        
        btn.addTarget(self, action: #selector(defaultHandling), for: .touchUpInside)
        view.addSubview(newView)
        newView.addSubview(label)
        newView.addSubview(iconImage)
        newView.addSubview(btn)
        
        newView.translatesAutoresizingMaskIntoConstraints = true
    }
    //end handling Reception View
    
    // handle Activity View
    func activity() {
        let newView = UIView(frame: CGRect(x: 0, y: 0, width: 170.0, height: 180.0))
        let iconImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 110.0, height: 110.0))
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 150.0, height: 40.0))
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 150.0, height: 35.0))
        
        newView.backgroundColor = .white
        newView.layer.shadowColor = UIColor.lightGray.cgColor
        newView.layer.shadowRadius = 3.0
        newView.layer.shadowOpacity = 3.0
        newView.cornerRadius = 100.0
        newView.layer.shadowOffset = CGSize(width: 1, height: 1)
        newView.layer.masksToBounds = false
        newView.center = CGPoint(x: width-110, y: 310)
        
        label.textAlignment = .center
        label.text = "Activity"
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 25.0)
        label.center = CGPoint(x: newView.bounds.midX, y: 70.0)
        
        iconImage.set(image: #imageLiteral(resourceName: "activity"), focusOnFaces: false)
        iconImage.alpha = 0.07
        iconImage.center = CGPoint(x: newView.bounds.midX, y: 70.0)
        
        btn.setTitle("Call Now", for: .normal)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 25.0)
        btn.setTitleColor(.white, for: .normal)
        btn.backgroundColor = UIColor(red:0.00, green:0.50, blue:0.04, alpha:1.0)
        btn.cornerRadius = 70.0
        btn.center = CGPoint(x: newView.bounds.midX, y: 150)
        
        
        btn.addTarget(self, action: #selector(defaultHandling), for: .touchUpInside)
        view.addSubview(newView)
        newView.addSubview(label)
        newView.addSubview(iconImage)
        newView.addSubview(btn)
        
        newView.translatesAutoresizingMaskIntoConstraints = true
    }
    // end handling Activity View
    
    // handle Restaurant View
    func restaurant() {
        let newView = UIView(frame: CGRect(x: 0, y: 0, width: 170.0, height: 180.0))
        let iconImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 110.0, height: 110.0))
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 150.0, height: 40.0))
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 150.0, height: 35.0))
        
        newView.backgroundColor = .white
        newView.layer.shadowColor = UIColor.lightGray.cgColor
        newView.layer.shadowRadius = 3.0
        newView.layer.shadowOpacity = 3.0
        newView.cornerRadius = 100.0
        newView.layer.shadowOffset = CGSize(width: 1, height: 1)
        newView.layer.masksToBounds = false
        newView.center = CGPoint(x: 110, y: 510)
        
        label.textAlignment = .center
        label.text = "Restaurant"
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 25.0)
        label.center = CGPoint(x: newView.bounds.midX, y: 70.0)
        
        iconImage.set(image: #imageLiteral(resourceName: "restaurant"), focusOnFaces: false)
        iconImage.alpha = 0.07
        iconImage.center = CGPoint(x: newView.bounds.midX, y: 70.0)
        
        btn.setTitle("Call Now", for: .normal)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 25.0)
        btn.setTitleColor(.white, for: .normal)
        btn.backgroundColor = UIColor(red:0.00, green:0.50, blue:0.04, alpha:1.0)
        btn.cornerRadius = 70.0
        btn.center = CGPoint(x: newView.bounds.midX, y: 150.0)
        
        btn.addTarget(self, action: #selector(defaultHandling), for: .touchUpInside)
        view.addSubview(newView)
        newView.addSubview(label)
        newView.addSubview(iconImage)
        newView.addSubview(btn)
        
        newView.translatesAutoresizingMaskIntoConstraints = true
    }
    // end handling Restaurant View
    
    // handle General Information View
    func information() {
        let newView = UIView(frame: CGRect(x: 0, y: 0, width: 170.0, height: 180.0))
        let iconImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 110.0, height: 110.0))
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 150.0, height: 40.0))
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 150.0, height: 35.0))
        
        newView.backgroundColor = .white
        newView.layer.shadowColor = UIColor.lightGray.cgColor
        newView.layer.shadowRadius = 3.0
        newView.layer.shadowOpacity = 3.0
        newView.cornerRadius = 100.0
        newView.layer.shadowOffset = CGSize(width: 1, height: 1)
        newView.layer.masksToBounds = false
        newView.center = CGPoint(x: width-110, y: 510) 
        
        label.textAlignment = .center
        label.text = "Information"
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 25.0)
        label.center = CGPoint(x: newView.bounds.midX, y: 70.0)
        
        iconImage.set(image: #imageLiteral(resourceName: "information"), focusOnFaces: false)
        iconImage.alpha = 0.07
        iconImage.center = CGPoint(x: newView.bounds.midX, y: 70.0)
        
        btn.setTitle("Call Now", for: .normal)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 25.0)
        btn.setTitleColor(.white, for: .normal)
        btn.backgroundColor = UIColor(red:0.00, green:0.50, blue:0.04, alpha:1.0)
        btn.cornerRadius = 70.0
        btn.center = CGPoint(x: newView.bounds.midX, y: 150.0)
        
        
        btn.addTarget(self, action: #selector(defaultHandling), for: .touchUpInside)
        view.addSubview(newView)
        newView.addSubview(label)
        newView.addSubview(iconImage)
        newView.addSubview(btn)
        
        newView.translatesAutoresizingMaskIntoConstraints = true
    }
    // end handling General Information View
    
    @objc
    func defaultHandling() {
        let alert: UIAlertController = UIAlertController(title: "NOTICE", message: "The service is unavailable", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in alert.dismiss(animated: true, completion: nil)}))

        self.present(alert, animated: true, completion: nil)
    }

    @objc
    func handleReservation() {
        let safariURL = SafariServiceViewController(aRequest: URL(string: "https://vkirirom.com/en/reservation.php")!, isReaderModeEnabled: false)
        self.present(safariURL.loadUrl(), animated: true, completion: nil)
    }
    
//    #############################################################
//    #############################################################
//    #############################################################
    
    
    // need to be connected to Internal Phone Server
    
    //    @objc
    //    func handleReception() {
    //        let url: NSURL = URL(string: "TEL://119")! as NSURL
    //        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    //    }
    //
    //    @objc
    //    func handleHousekeeping() {
    //        let url: NSURL = URL(string: "TEL://0")! as NSURL
    //        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    //    }
    //
    //    @objc
    //    func handleMassage() {
    //        let url: NSURL = URL(string: "TEL://0")! as NSURL
    //        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    //    }
    //
    //    @objc
    //    func handleRestaurant() {
    //        let url: NSURL = URL(string: "TEL://115")! as NSURL
    //        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    //    }
    //
    //    @objc
    //    func handleActivity() {
    //        let url: NSURL = URL(string: "TEL://114")! as NSURL
    //        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    //    }
    //
    //    @objc
    //    func handleInformation() {
    //        let url: NSURL = URL(string: "TEL://0")! as NSURL
    //        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    //    }
    
    
}
