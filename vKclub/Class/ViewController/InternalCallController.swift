//
//  InternalCallController.swift
//  vKclub
//
//  Created by Heng Senghak on 11/13/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import MaterialComponents
//import MaterialComponents

class InternalCallController: UIViewController {
    let appBar = MDCAppBar()
    let tabBar = MDCTabBar()
    let heroHeaderView = HeroHeaderView()
    
    var inProgressTask: Cancellable?
    let textField = UITextField()
    
    
    
}




extension InternalCallController {
    
//    func configureTextField() {
//        textField.placeholder = "Enter Phone Number"
//        textField.minimumFontSize = 17.0
//        textField.borderStyle = .bezel
//        textField.textAlignment = .center
//        textField.backgroundColor = UIColor(red:0.92, green:0.91, blue:0.92, alpha:1.0)
//        textField.font = UIFont(name: "System", size: 30.0)
//        textField.frame = CGRect (
//            x: 0,
//            y: 112,
//            width: 415,
//            height: 112 )
//
//    }
    
    
    
    func configureAppBar() {
        self.addChildViewController(appBar.headerViewController)
        
        appBar.navigationBar.backgroundColor = .clear
        appBar.navigationBar.title = nil
        
        let headerView = appBar.headerViewController.headerView
        headerView.backgroundColor = .clear
        headerView.maximumHeight = HeroHeaderView.Constants.maxHeight
        headerView.minimumHeight = HeroHeaderView.Constants.minHeight
        
        heroHeaderView.frame = headerView.bounds
        headerView.insertSubview(heroHeaderView, at: 0)
        
//        headerView.trackingScrollView = self.collectionView
        
        appBar.addSubviewsToParent()
        
        // to wire up the header layout delegate
//        appBar.headerViewController.layoutDelegate = self
    }
    
    func configureTabBar() {
        tabBar.itemAppearance = .titledImages
        tabBar.items = barItem.allValues.enumerated().map { (arg) -> UITabBarItem in
            
            let (index, source) = arg
            return UITabBarItem(title: source.title, image: source.image.image, tag: index)
        }
        
        tabBar.selectedItem = tabBar.items[0]
        
        //        tabBar.delegate = self
        appBar.headerStackView.bottomBar = tabBar
    }
    
    
    
    
    // MARK: UIScrollViewDelegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let headerView = appBar.headerViewController.headerView
        if scrollView == headerView.trackingScrollView {
            headerView.trackingScrollDidScroll()
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let headerView = appBar.headerViewController.headerView
        if scrollView == headerView.trackingScrollView {
            headerView.trackingScrollDidEndDecelerating()
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let headerView = appBar.headerViewController.headerView
        if scrollView == headerView.trackingScrollView {
            headerView.trackingScrollDidEndDraggingWillDecelerate(decelerate)
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint,
                                            targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let headerView = appBar.headerViewController.headerView
        if scrollView == headerView.trackingScrollView {
            headerView.trackingScrollWillEndDragging(withVelocity: velocity,
                                                     targetContentOffset: targetContentOffset)
        }
    }
    // end UIScrollViewDelegate
    
    
    
//    func configureCollectionView() {
//        let cellNib = UINib(nibName: "ArticleCell", bundle: nil)
//        collectionView?.register(cellNib, forCellWithReuseIdentifier: ArticleCell.cellID)
//        collectionView?.backgroundColor = UIColor(white: 0.9, alpha: 1.0)
//    }
}

/*
 extension InternalCallController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.bounds.width - (ArticleCell.cellPadding * 2), height: ArticleCell.cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: ArticleCell.cellPadding, left: ArticleCell.cellPadding, bottom: ArticleCell.cellPadding, right: ArticleCell.cellPadding)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return ArticleCell.cellPadding
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
}
*/
// MARK: UICollectionViewDataSource and Delegate
/*
extension InternalCallController {
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return articles.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ArticleCell.cellID, for: indexPath) as? ArticleCell {
            cell.article = articles[indexPath.row]
            return cell
        } else {
            fatalError("Missing cell for indexPath: \(indexPath)")
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let article = articles[indexPath.row]
        
        guard let url = article.articleURL else {
            return
        }
        
        let config = SFSafariViewController.Configuration()
        config.entersReaderIfAvailable = true
        let safariVC = SFSafariViewController(url: url, configuration: config)
        self.present(safariVC, animated: true, completion: nil)
    }
    
}
*/
// MARK: Data
extension InternalCallController {
    
    func refreshContent() {
        guard inProgressTask == nil else {
            inProgressTask?.cancel()
            inProgressTask = nil
            return
        }
        
        guard let selectedItem = tabBar.selectedItem else {
            return
        }
        
        
        
/*
        if source.title == "Recent" {
            let recent = UIViewController()
            recent.title = "Recents"
            recent.view.backgroundColor = UIColor.orange
        }
        else if source.title == "Dialer" {
            let dialer = UIViewController()
            dialer.title = "Dialer"
            dialer.view.backgroundColor = UIColor.blue
        }
        else if source.title == "Contacts" {
            let contacts = UIViewController()
            contacts.title = "Contacts"
            contacts.view.backgroundColor = UIColor.cyan
        }
*/
        
        
        
        
        
        
        
//        inProgressTask = apiClient.articles(forSource: source) { [weak self] (articles, error) in
//            self?.inProgressTask = nil
//            if let articles = articles {
//                self?.articles = articles
//                self?.collectionView?.reloadData()
//            } else {
//                self?.showError()
//            }
//        }
    }
    
    func showError() {
        
    }
    
    
    
}


// MARK: MDCFlexibleHeaderViewLayoutDelegate
//extension InternalCallController: MDCFlexibleHeaderViewLayoutDelegate {

//    public func flexibleHeaderViewController(_ flexibleHeaderViewController: MDCFlexibleHeaderViewController,
//                                             flexibleHeaderViewFrameDidChange flexibleHeaderView: MDCFlexibleHeaderView) {
//        heroHeaderView.update(withScrollPhasePercentage: flexibleHeaderView.scrollPhasePercentage)
//    }
//}


// MARK: MDCTabBarDelegate
extension InternalCallController: MDCTabBarDelegate {
    
    func tabBar(_ tabBar: MDCTabBar, didSelect item: UITabBarItem) {
        refreshContent()
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class HeroHeaderView: UIView {
    struct Constants {
        static let statusBarHeight: CGFloat = UIApplication.shared.statusBarFrame.height
        static let tabBarHeight: CGFloat = 72.0
        static let minHeight: CGFloat = 44 + statusBarHeight + tabBarHeight
        static let maxHeight: CGFloat = 400.0
    }
    
    //// add effect to HeroHeaderView to respond to header scroll position changes
    
//    func update(withScrollPhasePercentage scrollPhasePercentage: CGFloat) {
//
//        let imageAlpha = min(scrollPhasePercentage.scaled(from: 0...0.8, to: 0...1), 1.0)
//        imageView.alpha = imageAlpha
//
//
//        let fontSize = scrollPhasePercentage.scaled(from: 0...1, to: 22.0...60.0)
//        let font = UIFont(name: "CourierNewPS-BoldMT", size: fontSize)
//        titleLabel.font = font
//    }
    
    
    
    // MARK: Properties
    
    let imageView: UIImageView = {
        let imageView = UIImageView(image: nil)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = false
        return imageView
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = NSLocalizedString("850000056", comment: "")
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.textAlignment = .left
        label.backgroundColor = UIColor(red:0.26, green:0.55, blue:0.39, alpha:1.0)
        label.textColor = .white
        
//        label.shadowOffset = CGSize(width: 1, height: 1)
//        label.shadowColor = .white
        return label
    }()
    
    
    
    
/*
     //Create Attachment
     let imageAttachment =  NSTextAttachment()
     imageAttachment.image = UIImage(named:"iPhoneIcon")
     
     //Set bound to reposition
     let imageOffsetY:CGFloat = -5.0;
     imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: imageAttachment.image!.size.width, height: imageAttachment.image!.size.height)
     
     //Create string with attachment
     let attachmentString = NSAttributedString(attachment: imageAttachment)
     
     //Initialize mutable string
     let completeText = NSMutableAttributedString(string: "")
     
     //Add image to mutable string
     completeText.append(attachmentString)
     
     //Add your text to mutable string
     let  textAfterIcon = NSMutableAttributedString(string: "Using attachment.bounds!")
     completeText.append(textAfterIcon)
     self.mobileLabel.textAlignment = .center;
     self.mobileLabel.attributedText = completeText;
*/
    // MARK: Init
    
    init() {
        super.init(frame: .zero)
        autoresizingMask = [.flexibleWidth, .flexibleHeight]
        clipsToBounds = true
        configureView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: View
    
    func configureView() {
        backgroundColor = UIColor(red:0.26, green:0.55, blue:0.39, alpha:1.0)
//        addSubview(imageView)
        addSubview(titleLabel)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        imageView.frame = bounds
        titleLabel.frame = CGRect(
            x: 50,
            y: Constants.statusBarHeight,
            width: frame.width,
            height: frame.height - Constants.statusBarHeight - Constants.tabBarHeight)
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





extension InternalCallController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        customNavigationBar()
        configureAppBar()
        configureTabBar()
//        configureTextField()
        
    }
    
}





////////////////////////////////////////////







enum barItem: String {
    case recent = "recent"
    case dialer = "dialer"
    case contacts = "contacts"
    
    var title: String {
        switch self {
        case .recent: return "Recent"
        case .dialer: return "Dialer"
        case .contacts: return "Contacts"
        }
    }
    var image: UIImageView{
        switch self {
        case .recent: return UIImageView(image: #imageLiteral(resourceName: "history"))     // rendering mode adjust template -> to change icon color
        case .dialer: return UIImageView(image: #imageLiteral(resourceName: "dialPad"))
        case .contacts: return UIImageView(image: #imageLiteral(resourceName: "user"))
        }
    }
    
    
    static var allValues: [barItem] {
        return [
            .recent,
            .dialer,
            .contacts
        ]
    }
    
}
protocol Cancellable {
    func cancel()
}
////////////////////////////////////////////
