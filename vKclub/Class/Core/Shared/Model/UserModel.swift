//
//  LoginViewModel.swift
//  vKclub
//
//  Created by Pisal on 9/11/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import Foundation
import Firebase

protocol Identifiable {
    var id: String? {get set}
}

class UserProfileModel : Codable, Identifiable{
    
    
    var id: String? = nil
    var email: String?
    var name: String?
    var pfpUrl: String?
    var presence: Bool?
    var role: String?
    var phoneNumber: String?
    var isVKpointUser: Bool?
    var vkclubNumber: String?
    var vkPassword: String?
    
    init(email: String, name: String, pfpUrl: String, presence: Bool, role: String, phoneNumber: String,isVKpointUser: Bool, vkclubNumber: String, vkPassword: String) {
        
        self.email = email
        self.name = name
        self.pfpUrl = pfpUrl
        self.presence = presence
        self.role = role
        self.phoneNumber = phoneNumber
        self.isVKpointUser = isVKpointUser
        self.vkclubNumber = vkclubNumber
        self.vkPassword = vkPassword
        
    }
    
    
}

class MapModel: NSObject {
    
    var title: String?
    var geoPoint: GeoPoint!
    
    init(title: String, geoPoint: GeoPoint ) {
        
        self.title = title
        self.geoPoint = geoPoint
        
    }
    
}





