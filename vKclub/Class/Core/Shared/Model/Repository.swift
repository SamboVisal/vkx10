//
//  Repository.swift
//  vKclub
//
//  Created by Pisal on 11/30/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import CoreData
import Firebase

protocol RepositoryUserProtocol {
    
    func getUsersCore()->[UserProfileCore]
    func getUserCompletionBlock(userID: String , completion: @escaping([UserProfileCore],_ success: Bool) -> Void)
    func getUserCore(userId: String)-> [UserProfileCore]
    func insertUserCore(userModel: UserProfileModel, completion: @escaping(_ success: Bool) -> Void)
    func updateCurrentUserCore(userModel: UserProfileModel)
    func deleteUserCore(userId: String)
    func updateSpecificField(userID: String, fieldName: String, valueNew: String)
}

protocol RepositoryUserCloudProtocol {
    
    func getUserFirestore<T: Encodable & Decodable>(in collectionReference: FIRCollectionReference, and documentID: String, returning objectType: T.Type, completion: @escaping([T], _ success: Bool) -> Void)
    func updateUserFirestore<T: Encodable & Decodable>(for encodableObject: T, in collectionReference: FIRCollectionReference, and documentID: String,completion: @escaping(_ success: Bool) -> Void)
    
    
    
}

class RepositoryClass: RepositoryUserProtocol, RepositoryUserCloudProtocol {
    
    
    public init () {}
    private var extensions = ""
    private var message = ""
    typealias completionBlock = (_ success: Bool) -> Void
    public let user: UserProfileModel = UserRef.User()
    static let shared = RepositoryClass()
    public let mdUserModelLocal = NSFetchRequest<NSFetchRequestResult>(entityName: "UserProfileCore")
    public let mdPhoneBookModelLocal = NSFetchRequest<NSFetchRequestResult>(entityName: "UserPhoneBookCore")
    public let mdPhoneContactModelLocal = NSFetchRequest<NSFetchRequestResult>(entityName: "UserPhoneContactCore")
    public let mdSipRecentCallLocal = NSFetchRequest<NSFetchRequestResult>(entityName: "SipCallData")
    
    //public let queryUserProfile = RepositoryClass().getUserCore(userId: UserDefaults.standard.getUserId())
    
    
    func insertUserCore(userModel: UserProfileModel , completion: @escaping(_ success: Bool) -> Void) {
        
        let newUserCore = NSEntityDescription.insertNewObject(forEntityName: "UserProfileCore", into: managedObjectContext)
        managedObjectContext.mergePolicy = NSMergePolicy.mergeByPropertyObjectTrump
        guard let userID = Auth.auth().currentUser?.uid else {
            return
        }
        newUserCore.setValue(userID, forKey: "userID")
        newUserCore.setValue(userModel.name, forKey: "name")
        newUserCore.setValue(userModel.email, forKey: "email")
        newUserCore.setValue(userModel.pfpUrl, forKey: "pfpUrl")
        newUserCore.setValue(userModel.presence, forKey: "presence")
        newUserCore.setValue(userModel.role, forKey: "role")
        newUserCore.setValue(userModel.vkclubNumber, forKey: "vkclubNumber")
        newUserCore.setValue(userModel.isVKpointUser, forKey: "isVKpointUser")
        newUserCore.setValue(userModel.vkPassword, forKey: "vkPassword")
        newUserCore.setValue(userModel.phoneNumber, forKey: "phoneNumber")
        
        do{
            try  managedObjectContext.save()
            completion(true)
        } catch {
            print("error")
            completion(false)
            
        }
        
    }
    
    
    func getUserCompletionBlock(userID: String, completion: @escaping ([UserProfileCore], Bool) -> Void) {
        
        do {
            
            let response = try managedObjectContext.fetch(mdUserModelLocal)
            
            completion(response as! [UserProfileCore], true)
        } catch {
            
            print(error)
            completion([UserProfileCore](), false)
            
        }
        print("Done")
        
    }
    
    func updateCurrentUserCore(userModel: UserProfileModel) {
        
        if let user = Auth.auth().currentUser {
            
           let getUser = self.getUserCore(userId: user.uid)
            
            for user in getUser {
            
                user.name = userModel.name
                user.email = userModel.email
                user.pfpUrl = userModel.pfpUrl
                user.presence = userModel.presence!
                user.vkclubNumber = userModel.vkclubNumber
                user.isVKpointUser = userModel.isVKpointUser!
                user.phoneNumber = userModel.phoneNumber
                user.role = userModel.role
                
                do{
                    try  managedObjectContext.save()
                    
                }catch{
                    print("error")
                }
            }
            
        }
        
        
        
        
    }
    
    func updateSpecificField(userID: String, fieldName: String, valueNew: String) {
        
        let getUser = self.getUserCore(userId: userID)
    
        for user in getUser {
            user.setValue(valueNew, forKey: fieldName)
            do{
                try  managedObjectContext.save()
                
                print("UDPATE USER SPECIFIC FIELD")
                let user = self.getUserCore(userId: userID)
                for i in user {
                    print("Username is \(i.name ?? "")")
                    print("User")
                }
                
            }catch{
                print("error")
            }
        }
    }
    

    func deleteUserCore(userId: String){
        
        mdUserModelLocal.returnsObjectsAsFaults = false
        
        do {
            let results = try managedObjectContext.fetch(mdUserModelLocal)
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                managedObjectContext.delete(managedObjectData)
                try managedObjectContext.save()
                print("DELETE USER CORE DATA FROM TABLE")
            }
        } catch let error as NSError {
            print("Detele all data in \(userId) error : \(error) \(error.userInfo)")
        }
        
    }
    
    
    
}

extension RepositoryClass {
    
    // get all users
    func getUsersCore() -> [UserProfileCore] {
        
        do {
            
            let response = try managedObjectContext.fetch(mdUserModelLocal)
            
            return response as! [UserProfileCore]
        } catch {
            
            print(error)
            return [UserProfileCore]()
            
        }
        
        
    }
    
    
    // get user by firebase ID as array
    func getUserCore(userId: String) -> [UserProfileCore]
    {
        
        let predicate = NSPredicate(format: "userID == %@", userId)
        mdUserModelLocal.predicate = predicate
        
        do {
            
            let fetch = try managedObjectContext.fetch(mdUserModelLocal)
            
            return fetch as! [UserProfileCore]
            
        } catch {
            return [UserProfileCore]()
        }
        
        
    }
    
    
}

// Repository using firebase
extension RepositoryClass {
    
    
    func getRecentCallData(completion: @escaping([SipCallData],_ success: Bool) -> Void) {
        mdSipRecentCallLocal.returnsObjectsAsFaults = false
        do {
            let result = try managedObjectContext.fetch(mdSipRecentCallLocal)
            
            completion(result as! [SipCallData] ,true)
           
        } catch {
            
            completion([],false)
            
            
        }
        
    }
    
    func getUserFirestore<T: Encodable & Decodable>(in collectionReference: FIRCollectionReference, and documentID: String, returning objectType: T.Type, completion: @escaping([T], _ success: Bool) -> Void) {
        
        db.collection(collectionReference.rawValue).document(documentID).getDocument { (snapshot, error) in
            
            print("This is json \(objectType)")
            
            guard let snapshot = snapshot else {
                
                print("Error getting snapshot")
                
                return
            }
            
            var append = [T]()
            do {
                if snapshot.exists {
                    
                    let object = try snapshot.decode(as: objectType.self)
                    append.append(object)
                    
                    completion(append, true)
                } else {
                    
                    print("snapshot is not exist ")
                    completion(append, false)
                }
                
            } catch {
                
                completion(append, false)
                
                print(error)
                
            }
            
            
            
        }
        
    }
    
    
    func updateUserFirestore<T: Encodable & Decodable>(for encodableObject: T, in collectionReference: FIRCollectionReference, and documentID: String,  completion: @escaping( _ success: Bool) -> Void) {
        
        do {
            
            let json = try encodableObject.toJson()
            db.collection(collectionReference.rawValue).document(documentID).setData(json) { (error) in
                if error == nil {
                    
                    print("Update data is completed in firestore")
                    completion(true)
                } else {
                    completion(false)
                }
            }
            
        } catch {
            print("Error updating to firebstore")
        }
    }
    
    
    func updateUserFirestoreSpecific(in collectionReference: FIRCollectionReference, and documentID: String, forField fieldValue: String, forValue valueField: String,  completion: @escaping( _ success: Bool) -> Void)  {
        db.collection(collectionReference.rawValue).document(documentID).setData([
            fieldValue : valueField
        ], options: SetOptions.merge()) { (error) in
            if error == nil {
                print("No error")
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    
    func verifyUser(with phoneNumber: String ,completion: @escaping( _ message: String, _ success: Bool) -> Void){
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
            
            guard let verificationID = verificationID else {
                guard let error = error?.localizedDescription else {
                    return
                }
                completion(error, false)
                return
            }
            
            if error == nil {
                
                print("This is verificationID \(verificationID)")
                // store verifcationID of particular phonenumber
                UserDefaults.standard.userVerificationID(value: verificationID)
                completion("successfully",true)
            } else {
                guard let error = error?.localizedDescription else {
                    return
                }
                completion(error, false)
            }
            
        }
        
    }
    
    func signInUser(with verifcationCode: String, completion: @escaping(_ success: Bool) -> Void) {
        
        let verficationID = UserDefaults.standard.returnUserVerificationID()
        print("This is verification ID \(verficationID) and verificationCode \(verifcationCode)" )
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: verficationID, verificationCode: verifcationCode)
        
        Auth.auth().signInAndRetrieveData(with: credential) { (result, error) in
            
            guard let result = result else {
                completion(false)
                return
            }
            if error == nil {
                
                print("User is signed in \(result.user)")
                
                print("This is userID \(result.user.uid)")
              
                completion(true)
                
            } else {
                
                if let error = error {
                    print("Error during verification \(error.localizedDescription)")
                }
                completion(false)
                
            }
            
        }
        
        
    }
    
}


// create one switch
extension RepositoryClass {
    
    func createOneSwitch(email:String,name: String, completionHandler: @escaping (Bool,String,String) -> Void)  {
        print("Username is \(name)")
        var userFirstName = ""
        var userLastName = ""
        print("This is phone number \(email)")
        if name.containsWhitespace {
            
            let string = name
            var token = string.components(separatedBy: .whitespaces)
            print("username\(token[0])")
            
            //            if let first = string.components(separatedBy: .whitespaces)
            
            userFirstName = token[0]
            userLastName = token[1]
            print("User firstname", token[0])
            print("User lastname", token[1])
            
            print("Username contain white space")
        } else {
            
            userFirstName = name
            userLastName = name
            
            print("Username has no white space")
        }
        
        
        guard let url = URL(string: "https://digital.keen-vc.cf:8443/api/v2/onecore/u-exts") else {
            print("Unknown url ")
            return
        }
        var request = URLRequest(url: url.absoluteURL)
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue( "Basic NWMwNWZkN2ZlNzUwN2UzMDc4MjliNWNjOmV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUp5YjJ4bElqb2lRVkJRSWl3aVgybGtJam9pTldNd05XWmtOMlpsTnpVd04yVXpNRGM0TWpsaU5XTmpJaXdpYzJOdmNHVWlPaUpXUzBOTVZVSTZVa1ZCUkNJc0ltRndjQ0k2SWxaTFEweFZRaUlzSW1Oc2FXVnVkQ0k2SWtaSlVsTlVJaXdpYTJWNVgybGtJam9pU0VWeVlrVlpRU3RVV2xjNWVWbDJOVkp5YTFCU1ltWXhVVkYxYVV4bWJWaFBMMjh5S3pCNlFYTkxURmhrUjFSWE5WSnhPRXBzU0VSQ0sxZFdjR1J4UW5GVlJWbHFjemN6Um1wT1kwcGxhRVZXVGtocVkyZzJhVzFYZG5Kck9YVkRXazB2TmxoaEswbHdRa3h1Wm1oRWRtVnBSRWRvYWtVdmJWSlVMMnQ1WmxwVVRtbGxha053Wm0xVllpOTBVWEp6VFZRd1VsbG1iMWxQUzFwd1dtTlBOa00zTlU1bWRuTjFXRFpJV0VacWNESm5ZMkl5Tkc5WlUyd3hlVXA2TmxkS1dDdEdaU3RsVEdKRmMzZG1hMFZoY1dScGVFNUJjV1prZGpoa1FtaHpLMFJhVm5oVWRGVldWa2ROYkhoVlNGQlNlVEUzWTI4eU5UTk1Ra05WZFZvM0syOUpTa1p0TTNGUlJtZG1RVkZSUTJsek1teFhVMnBrT0VGV1RYUkhhVzU1WlVaMFZsbFVUbWxUZFVWTFRFRlBVV05LYmpKS05WWTJZV3hXTDJsVFNpdE9VVFZWYkVreFpIaDNUemNyTDNoelQwZ3dja3hCUFQwaUxDSnBjM04xWlY5emRHRnRjQ0k2TVRVME16ZzVOalEwTnpnd09Td2lhV0YwSWpveE5UUXpPRGsyTkRRM0xDSnVZbVlpT2pFMU5ETTRPVFkwTlRJc0ltVjRjQ0k2TVRZd05qazJPRFEwTnl3aVlYVmtJam9pUVZCUUlpd2lhWE56SWpvaWIyNWxjM2RwZEdOb0lpd2ljM1ZpSWpvaVZrdERURlZDUUc5dVpYTjNhWFJqYUNJc0ltcDBhU0k2SWpWak1EVm1aRGRtWlRjMU1EZGxNekEzT0RJNVlqVmpZeUo5LjBqMm1wMldraDNmUm9xYldYV2lldFdKVllOZEFLZGQtMDc0YkhLeU96R2s=" ,forHTTPHeaderField: "OS-API-KEY")
        
        let parameters = [
            
            "domain":"192.168.7.136" ,
            "extPassword":
                ["type":"md5",
                 "pass": UserDefaults.standard.returnRandomVkPasswrod()]
            ,"identifier":
                [
                "tel": email
            ] ,
             "password":"Jn125*",
             "vmPassword":"12356",
             "metadata":
                ["firstName": "firstName",
                 "lastName": "lastName",
                 "dob":"12-12-1998"],
             "scope":"WRITE+DELETE"
            
            ] as [String : Any]
        
        print("This is parameter \(parameters)")
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
            
        } catch let error {
            print("error free switch ",error.localizedDescription)
        }
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if error != nil {
                self.message = (error?.localizedDescription)!
                print("message error \(self.message)")
                completionHandler(false, self.message,self.extensions)
            } else {
                
                do {
                    guard let dataReq = data else {
                        print("Data req error \(String(describing: error?.localizedDescription))")
                        completionHandler(false, (error?.localizedDescription)!, "")
                        return
                    }
                    
                    let json = try JSONSerialization.jsonObject(with: dataReq, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:Any]
                    
                    if let dataJson = json {
                        
                        print("this json data \(dataJson)")
                        
                        if dataJson["success"] as! Bool == true {
                            let data = dataJson["data"] as! [String : Any]
                            let ext = data["ext"] as! [String: Any]
                            self.extensions = ext["extension"] as! String
                            print("Extension number \(self.extensions)")
                            completionHandler(true, self.message , self.extensions)
                        } else {
                            
                            self.message = dataJson["message"] as! String
                            
                            print("message err \(self.message)")
                            
                            completionHandler(false, self.message, self.extensions)
                            
                        }
                    } else {
                        
                        print("Cannot get free switch data")
                        completionHandler(false, (error?.localizedDescription)!, self.extensions)
                    }
                    
                    
                    
                    
                } catch {
                    
                    print("eror free switch")
                }
            }
        }
        task.resume()
        
        
        
    }
    
    func addVOIPdataTofireStore(uid: String) {
        
        db.collection("users").document(uid).setData(
            
            [
                
                "extension" : extensions
                
            ]
        )
        print("Extension number is \(extensions)")
    }
    
}

