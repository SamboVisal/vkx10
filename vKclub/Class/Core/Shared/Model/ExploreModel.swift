//
//  ExploreModel.swift
//  vKclub
//
//  Created by Pisal on 11/17/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import Foundation

struct ExploreModel: Codable {
    
    var imageUrl: String?
    var link: String?
    var snippet: String?
    var title: String?
    
    init(imageUrl: String, link: String, snippet: String, title: String) {
        
        self.imageUrl = imageUrl
        self.link = link
        self.snippet = snippet
        self.title = title
    }
    
}
