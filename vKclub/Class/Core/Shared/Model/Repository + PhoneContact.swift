//
//  Repository + PhoneContact.swift
//  vKclub
//
//  Created by Chhayrith on 12/7/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import CoreData
import Firebase

extension RepositoryClass {
    func insertPhoneContactCore(contactModel: [ContactModel] , completion: @escaping(_ success: Bool) -> Void) {
        let fetchAllPhoneContacts: [UserPhoneContactCore] = self.getAllPhoneContactData()
        if fetchAllPhoneContacts == [] {
            for newContactCore in contactModel {
                addNewContactCoreHelper(newContactCore: newContactCore)
            }
        }else {
            print("contact Model \(contactModel)")
            for newContactCore in contactModel {
                print("contact data \(self.getAllPhoneContactData()) ")
                var isContain: Bool = false
                
                for oneContact in fetchAllPhoneContacts {
                    if oneContact.id == newContactCore.id {
                        isContain = true
                        break
                    }
                }
                
                if isContain == false {
                    addNewContactCoreHelper(newContactCore: newContactCore)
                }
            }
        }
        
        do{
            try  managedObjectContext.save()
            completion(true)
        } catch {
            print("error")
            completion(false)
        }
    }
    
    func addNewContactCoreHelper(newContactCore: ContactModel) {
//        let newPhoneContactCore = NSEntityDescription.insertNewObject(forEntityName: "UserPhoneBookCore", into: managedObjectContext)
//        newPhoneContactCore.setValue(newContactCore.id, forKey: "id")
//        newPhoneContactCore.setValue(newContactCore.name, forKey: "name")
//        newPhoneContactCore.setValue(newContactCore.email, forKey: "email")
//        newPhoneContactCore.setValue(newContactCore.pfpUrl, forKey: "pfpUrl")
//        newPhoneContactCore.setValue(newContactCore.presence, forKey: "presence")
//        newPhoneContactCore.setValue(newContactCore.role, forKey: "role")
//        newPhoneContactCore.setValue(newContactCore.vkclubNumber, forKey: "vkclubNumber")
    }
    
    // get phonebook data from core data as array
    func getAllPhoneContactData() -> [UserPhoneContactCore]
    {
        do {
            let fetch = try managedObjectContext.fetch(mdPhoneContactModelLocal)
            
            return fetch as! [UserPhoneContactCore]
            
        } catch {
            return [UserPhoneContactCore]()
        }
    }
    
    // get phonebook data from core data as array
//    func getOnePhoneContactData(userId: String) -> [UserPhoneContactCore]
//    {
//        do {
//            let allContacts = try managedObjectContext.fetch(mdPhoneContactModelLocal)
//            for contact in allContacts {
//                // contact
//            }
//            return fetch as! [UserPhoneContactCore]
//
//        } catch {
//            return [UserPhoneContactCore]()
//        }
//    }
}
