//
//  Repository + Phonebook.swift
//  vKclub
//
//  Created by Chhayrith on 12/7/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import CoreData

extension RepositoryClass {
    func insertPhoneBookCore(userModel: [UserProfileModel] , completion: @escaping(_ success: Bool) -> Void) {
        let fetchAllPhonebookContacts: [UserPhoneBookCore] = self.getAllPhoneBookData()
        
        if fetchAllPhonebookContacts == [] {
            for newUserCore in userModel {
                addNewUserCoreHelper(newUserCore: newUserCore)
            }
        }else {
            print("User Model \(userModel)")
            for newUserCore in userModel {
                print("user data \(self.getAllPhoneBookData()) ")
                var isContain: Bool = false
                
                for oneContact in fetchAllPhonebookContacts {
                    if oneContact.id == newUserCore.id {
                        isContain = true
                        break
                    }
                }
                
                if isContain == false {
                    addNewUserCoreHelper(newUserCore: newUserCore)
                }
            }
        }
        
        do{
            try  managedObjectContext.save()
            completion(true)
        } catch {
            print("error")
            completion(false)
        }
    }
    
    func addNewUserCoreHelper(newUserCore: UserProfileModel) {
        let newPhoneBookCore = NSEntityDescription.insertNewObject(forEntityName: "UserPhoneBookCore", into: managedObjectContext)
        newPhoneBookCore.setValue(newUserCore.id, forKey: "id")
        newPhoneBookCore.setValue(newUserCore.name, forKey: "name")
        newPhoneBookCore.setValue(newUserCore.email, forKey: "email")
        newPhoneBookCore.setValue(newUserCore.pfpUrl, forKey: "pfpUrl")
        newPhoneBookCore.setValue(newUserCore.presence, forKey: "presence")
        newPhoneBookCore.setValue(newUserCore.role, forKey: "role")
        newPhoneBookCore.setValue(newUserCore.vkclubNumber, forKey: "vkclubNumber")
    }
    
    // get one phone book data
    func getOnePhoneBookData(userId: String) -> ContactModel
    {
        var result: ContactModel?
        do {
            let allPhoneBookContacts = try managedObjectContext.fetch(mdPhoneBookModelLocal) as! [UserPhoneBookCore]
            for oneContact in allPhoneBookContacts {
                if oneContact.id == userId {
                    result = ContactModel(Name: oneContact.name == nil ? "" : oneContact.name!,
                                          Number: oneContact.vkclubNumber == nil ? "" : oneContact.vkclubNumber!,
                                          Email: oneContact.email == nil ? "" : oneContact.email!,
                                          Address: "",
                                          Note: "",
                                          ProfileImgUrl: (oneContact.pfpUrl != nil) ? oneContact.pfpUrl! : "")
                    break
                }
            }
        } catch {
            print("No data in phone book")
        }
        return result!
    }
    
    // Check If UserID is Valid
    func checkPhoneBookUserIsValid(userId: String) -> Bool
    {
        var result: Bool = false
        do {
            let allPhoneBookContacts = try managedObjectContext.fetch(mdPhoneBookModelLocal) as! [UserPhoneBookCore]
            for phonebook in allPhoneBookContacts {
                if phonebook.id == userId {
                    result = true
                    break
                }
            }
        } catch {
            print("No data in phone book")
        }
        print("==> IS USER VALID: \(result)")
        return result
    }
    
    // get phonebook data from core data as array
    func getAllPhoneBookData() -> [UserPhoneBookCore]
    {
        do {
            let fetch = try managedObjectContext.fetch(mdPhoneBookModelLocal)
            return fetch as! [UserPhoneBookCore]
            
        } catch {
            return [UserPhoneBookCore]()
        }
    }
    
    // Delete phonebook
    func deleteAllPhonebookCore(){
        
        mdPhoneBookModelLocal.returnsObjectsAsFaults = false
        
        do {
            let results = try managedObjectContext.fetch(mdPhoneBookModelLocal)
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                managedObjectContext.delete(managedObjectData)
                try managedObjectContext.save()
                print("DELETE PHONEBOOK CORE DATA FROM TABLE")
            }
        } catch let error as NSError {
            print("Detele all data in error : \(error)")
        }
    }
}
