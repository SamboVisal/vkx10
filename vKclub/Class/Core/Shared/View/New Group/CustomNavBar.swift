//
//  CustomNavBar.swift
//  vKclub
//
//  Created by Chhayrith on 12/2/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func CustomNavigationBar(title navBarTitle: String, isShadow: Bool, isPresentVC: Bool) {
        //self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        //self.navigationController?.navigationBar.shadowImage = nil
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        // Adding center title to dashboard navigation controller
        self.navigationItem.title = navBarTitle
        // title color
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        // navigation items and bar button items
        self.navigationController?.navigationBar.barTintColor = BaseColor.colorAccentDark
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        // add shadow to navigation controller
        if isShadow == true {
            self.navigationController?.navigationBar.isTranslucent = false
            self.navigationController?.navigationBar.layer.shadowColor = BaseColor.colorBlack.cgColor
            self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 2.0, height: 1.0)
            self.navigationController?.navigationBar.layer.shadowRadius = 4.0
            self.navigationController?.navigationBar.layer.shadowOpacity = 0.5
        }else if isShadow == false{
            self.navigationController?.navigationBar.isTranslucent = false
            if let navigationBar = self.navigationController?.navigationBar {
                let navDivider = CGRect(x: 0, y: navigationBar.frame.height, width: navigationBar.frame.width, height: 1)
                
                let thirdLabel = UIView(frame: navDivider)
                thirdLabel.backgroundColor = BaseColor.colorAccentDark

                navigationBar.addSubview(thirdLabel)
            }
        }
        
        
        // check if View controller is being presented
        if isPresentVC == true {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "clear"), style: .done, target: self, action: #selector(dismissViewController))
        }
        
    }
    
    @objc
    func dismissViewController() {
        self.dismiss(animated: true, completion: nil)
    }
}
