//
//  ErrorString.swift
//  vKclub
//
//  Created by Pisal on 11/30/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import Foundation

class DialogErrorMessage {
    
    lazy var errorTitle = "Error"
    lazy var warningTitle = "Warning"
    lazy var errorEmptyUsername = "Please enter your username."
    lazy var errorEmptyPassword = "Please enter your password."
    lazy var errorEmptyEmail = "Please enter your email."
    lazy var errorConfirmPasswordNotMatch = "Your confirm password does not match each other. Please kindly check again."
    lazy var errorInternetConnectionOffline = "Your internet connection appears to be offline."
    lazy var errorUsernamePasswordNotMatch = "The username and password you entered did not match our records. Please double-check and try again."
    
    lazy var errorPasswordNotMatch = "The password is invalid or the user does not have a password."
    lazy var errorEmailNotMatch = "The email you entered did not match our records. Please double-check and try again."
    lazy var errorUsernameSameData = "If you wish to update your profile please don't input the same data."
    lazy var errorLoginSignupProcess = "We could not proceed your form. Please kindly double-check and try again."
    lazy var warningLeavingScreen = "Are you sure want to leave? This will not save your information."
    lazy var confirmUserToLogin = "Please Sign In in order to use this feature."
}


class DialogTitleMessage {
    
    lazy var successTitle = "Success"
    lazy var okayTitle = "Okay"
    lazy var logoutTitle = "Logout"
    lazy var signIn = "Sign In"
    lazy var cancelTitle = "Cancel"
    lazy var confirmLogout = "Are you sure want to logout?"
    lazy var confirmEmailAdd = "Please verify your email address with a link that we have already sent you to procced login in."
    
}
