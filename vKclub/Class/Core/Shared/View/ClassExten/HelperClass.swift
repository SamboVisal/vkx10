//
//  UIComponentsHelper.swift
//  vKclub Version 2
//
//  Created by Machintos-HD on 7/2/18.
//  Copyright © 2018 Pisal. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications
import SVProgressHUD
import ProgressHUD
import MaterialComponents.MaterialDialogs
import MaterialComponents.MaterialSnackbar
import PopupDialog
import Material


class UIComponentHelper {
    
    
    static var activity: MDCActivityIndicator!
    
    static func showActivityLoading(view: UIView, option: Bool) {
        activity = MDCActivityIndicator()
        if option {
            print("View width \(view.frame.width) view height \(view.frame.height)")
            
            activity.indicatorMode = .indeterminate
            activity.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(activity)
            
            NSLayoutConstraint.activate([
                
                activity.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                activity.centerYAnchor.constraint(equalTo: view.centerYAnchor),
                ])
            
            activity.startAnimating()
            
        } else {
            
            //activity.removeFromSuperview()
            activity.stopAnimating()
        }
        
        
    }
    
    
    
    static func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    static func AvoidSpecialCharaters(specialcharaters : String) -> Bool {
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ")
        
        if specialcharaters.rangeOfCharacter(from: characterset.inverted) != nil {
            print("string contains special characters")
            return false
        }
        
        return true
    }
    static func Countwhitespece(_whitespece : String) -> Int{
        let regex = try! NSRegularExpression(pattern: "\\s")
        let numberOfWhitespaceCharacters = regex.numberOfMatches(in: _whitespece , range: NSRange(location: 0, length: _whitespece.utf16.count))
        return numberOfWhitespaceCharacters
        
    }
    static func Whitespeceatbeginning(_whitespece : String) -> Bool{
        let i : String = _whitespece
        let r = i.index(i.startIndex, offsetBy: 1)
        let url : String = (i.substring(to: r))
        if url == " " {
            
            return true
        }
        return false
    }
    static func scheduleNotification(_title: String, _body: String, _inSeconds: TimeInterval) {
        let localNotification = UNMutableNotificationContent()
        
        localNotification.title = _title
        localNotification.body = _body
        
        let localNotificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: _inSeconds, repeats: false)
        
        let request = UNNotificationRequest(identifier: "localNotification", content: localNotification, trigger: localNotificationTrigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: { error in
            if error == nil {
                print("LocalNotificationSuccess =====")
            } else {
                print("LocalNotificationError  ===== ", error?.localizedDescription as Any)
            }
        })
        
    }
    static func showProgressWith(status: String, interact: Bool) {

        ProgressHUD.show(status, interaction: interact)


    }
  

    static func ProgressDismiss() {
        ProgressHUD.dismiss()
    }
   
  
    // Find out current date YYYY/MM/DD/HH/MM/SS
    static func GetTodayString() -> (String, String, String, String, String, String) {
        let date = Date()
        let calender = Calendar.current
        let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
        
        var am_pm = ""
        let year = components.year
        let month = components.month! < 10 ? "0" + String(components.month!) : String(components.month!)
        let day = components.day! < 10 ? "0" + String(components.day!) : String(components.day!)
        var hour = components.hour! < 10 ? "0" + String(components.hour!) : String(components.hour!)
        
        if Int(hour)! > 12 {
            hour = String((Int(hour)!) - 12)
            am_pm = "PM"
        } else {
            am_pm = "AM"
        }
        
        let minute = components.minute! < 10 ? "0" + String(components.minute!) + am_pm : String(components.minute!) + am_pm
        let second = components.second! < 10 ? "0" + String(components.second!) : String(components.second!)
        
        return (String(year!), String(month), String(day), String(hour), String(minute), String(second))
    }
}

extension String {
    var containsWhitespace : Bool {
        return(self.rangeOfCharacter(from: .whitespacesAndNewlines) != nil)
    }
    
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
}

extension UIViewController {

    func showSnackbarMessage(title: String) {
        
        let message = MDCSnackbarMessage()
        message.text = title
        let action = MDCSnackbarMessageAction()
        action.title = "Dismiss"
        message.action = action
        MDCSnackbarManager.show(message)
        
        
        
    }
    
    
    func getTopViewController() -> UIViewController
    {
        var topViewController = UIApplication.shared.delegate!.window!!.rootViewController!
        while (topViewController.presentedViewController != nil) {
            topViewController = topViewController.presentedViewController!
        }
        return topViewController
    }
    
    func alertError(message: String) {
        
        let dialog = MDCAlertController(title: DialogErrorMessage().errorTitle, message: message)
        
        dialog.buttonTitleColor = UIColor(red:0.03, green:0.62, blue:0.09, alpha:1.0)
        
        let okayButton = MDCAlertAction(title: DialogTitleMessage().okayTitle) { (action) in
            
        }
        dialog.addAction(okayButton)
        
        dialogPresent(view: dialog)
        
    }
    
    func dialogPresentViewController(title: String, message: String, actionTitle: String, viewController: UIViewController) {
        
        let dialog = MDCAlertController(title: title, message: message)
        dialog.buttonTitleColor = UIColor(red:0.03, green:0.62, blue:0.09, alpha:1.0)
        let okayButton = MDCAlertAction(title: actionTitle) { (action) in
            
            let viewToPresnt = UINavigationController(rootViewController: viewController)
            
            self.present(viewToPresnt, animated: true, completion: nil)
        }
        let cancelButton = MDCAlertAction(title: DialogTitleMessage().cancelTitle, handler: nil)
        dialog.addAction(okayButton)
        dialog.addAction(cancelButton)
        
        dialogPresent(view: dialog)
    }
    
    // get dialog instance
    func dialogMessage(title: String, message: String) -> MDCAlertController {
        
        let alert = MDCAlertController(title: title, message: message)
        alert.buttonTitleColor = UIColor(red:0.03, green:0.62, blue:0.09, alpha:1.0)
        
        
        return alert
    }
    
    
    // show single message
    func alertMessageDialog(title: String, message: String, actionTitle: String) {
        
        let dialog = MDCAlertController(title: title, message: message)
        
        dialog.buttonTitleColor = UIColor(red:0.03, green:0.62, blue:0.09, alpha:1.0)
        
        let okayButton = MDCAlertAction(title: actionTitle) { (action) in
            
        }
        dialog.addAction(okayButton)
      
        dialogPresent(view: dialog)
    }
    func dialogPresent(view: UIViewController) {
        
        self.present(view, animated: true, completion: {
            
            view.view.superview?.subviews[0].isUserInteractionEnabled = false
            
        })
        
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func isLoggedIn() -> Bool {
        return UserDefaults.standard.isLoggedIn()
    }
    
    func userId() -> String{
        return UserDefaults.standard.getUserId()
    }
    
    func getAuthVerificationID() -> String{
        
        return UserDefaults.standard.returnUserVerificationID()
        
    }

    func getRandomVkPassword() -> String {
        
        return UserDefaults.standard.returnRandomVkPasswrod()
        
    }
    
    
}
extension String {
    
    func generateAlphaNumberic(length: Int) -> String{
        
        let randomNumber = String(randomWithLength: 1, allowedCharactersType: .numeric)
        let randomAlpha = String(randomWithLength: length, allowedCharactersType: .alphabetic)
        let string = randomNumber + randomAlpha
        print("This is random AlpaNumber \(string)")
        return string
        
    }
    
}

extension UserDefaults {
   
    
    func userVerificationID(value: String) {
        set(value, forKey: "AuthVerificationID")
        synchronize()
    }
    func returnUserVerificationID() -> String {
        
        return value(forKey: "AuthVerificationID") as! String
    }
    
    func setRandomVkPassword(value: String) {
        set(value, forKey: "RandomVkPassword")
        synchronize()
    }
    func returnRandomVkPasswrod() -> String {
        
        return value(forKey: "RandomVkPassword") as! String
        
    }
    
    func setIsLoggedIn(value: Bool){
        set(value, forKey: "isLoggedIn")
        synchronize()
    }
    func isLoggedIn() -> Bool {
        return bool(forKey: "isLoggedIn")
    }
    
    func setUserId(value: String) {
        set(value, forKey: "userID")
        synchronize()
    }
    
//    func setQRUserID(value: String) {
//        
//        set(value, forKey: "QRCode")
//        synchronize()
//    }
    
    
    func getUserId() -> String {
        
        return value(forKey: "userID") as! String
        
    }
    
}



extension UIApplication {
    typealias BackgroundTaskCompletion = () -> Void
    func executeBackgroundTask(f: (BackgroundTaskCompletion) -> Void) {
        let identifier = beginBackgroundTask {
            // nothing to do here
        }
        f() { [unowned self] in
            self.endBackgroundTask(identifier)
        }
    }
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}

class AppNavigationController: NavigationController {
    
    open override func prepare() {
        super.prepare()
        isMotionEnabled = true
        motionNavigationTransitionType = .fade
        guard let v = navigationBar as? NavigationBar else {
            return
        }
        v.backgroundColor = BaseColor.colorPrimary
        v.depthPreset = .depth4
        v.tintColor = .white
        //v.dividerColor = Color.grey.lighten2
    }
}
