//
//  View.swift
//  vKclub Version 2
//
//  Created by Pisal on 6/25/2561 BE.
//  Copyright © 2561 BE Pisal. All rights reserved.
//

import Foundation
import UIKit



class View {
   
    static func squareFrame( midX : CGFloat,midY:CGFloat , pX : CGFloat,pY:CGFloat , w: CGFloat ,h:CGFloat ) -> CGRect {
        
        return CGRect(x: midX * pX, y: midY * pY, width: w , height: h )
    }
 
}

class ImageCirCleView: UIImageView {
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let radius: CGFloat = self.bounds.size.width / 2.0
        
        self.layer.cornerRadius = radius
    }
}
class CardImageView: UIImageView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.curveImageToCorners()
    }
    
    func curveImageToCorners() {
        // The main image from the xib is taken from: https://unsplash.com/photos/wMzx2nBdeng
        // License details: https://unsplash.com/license
        var roundingCorners = UIRectCorner.topLeft
        if (UIDevice.current.orientation == .portrait ||
            UIDevice.current.orientation == .portraitUpsideDown) {
            roundingCorners.formUnion(.topRight)
        } else {
            roundingCorners.formUnion(.bottomLeft)
        }
        
        let bezierPath = UIBezierPath(roundedRect: self.bounds,
                                      byRoundingCorners: roundingCorners,
                                      cornerRadii: CGSize(width: 4,
                                                          height: 4))
        let shapeLayer = CAShapeLayer()
        shapeLayer.frame = self.bounds
        shapeLayer.path = bezierPath.cgPath
        self.layer.mask = shapeLayer
    }
    
}
extension UIView {
    
    func anchor (top: NSLayoutYAxisAnchor?, left: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, right: NSLayoutXAxisAnchor?, paddingTop: CGFloat, paddingLeft: CGFloat, paddingBottom: CGFloat, paddingRight: CGFloat, width: CGFloat, height: CGFloat, enableInsets: Bool) {
        var topInset = CGFloat(0)
        var bottomInset = CGFloat(0)
        
        if #available(iOS 11, *), enableInsets {
            let insets = self.safeAreaInsets
            topInset = insets.top
            bottomInset = insets.bottom
        }
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            self.topAnchor.constraint(equalTo: top, constant: paddingTop+topInset).isActive = true
        }
        if let left = left {
            self.leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }
        if let right = right {
            rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
        }
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom-bottomInset).isActive = true
        }
        
        if height != 0 {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
        if width != 0 {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        
    }
    
}

