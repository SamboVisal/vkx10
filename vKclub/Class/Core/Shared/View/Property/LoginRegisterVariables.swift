//
//  MembershipDetailVariable.swift
//  vKclub
//
//  Created by Pisal on 7/16/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialButtons
import MaterialComponents.MaterialButtons_ButtonThemer

import JVFloatLabeledTextField

class EditProfileVariables {
    
    struct Constant {
        
        static let typeName = "Username"
        static let typePass = "Password"
        static let typeNewPass = "New Password"
        static let typeConfirmPass = "Confirm Password"
        static let typeEmail = "Email"
        static let saveBtn = "Save"
        static let loginLabel = "Phone number"
        static let createAccountLabel = "Create Account"
        static let forgotPasswordLabel = "Forgot Password"
        static let createAccButton = "Create a new Account"
        static let createAccRegisterBtn = "Create an Account"
        static let forgotPasswordButton = "Forgot Password?"
        static let resetPassBtn = "Save"
        static let resetPassButton = "Reset Password"
        static let resetPssLabel = "Reset Password"
        static let roboto_bold = "Roboto-Bold"
        static let roboto_regular = "Roboto-Regular"
        static let setNameLabel = "Set Nickname"
        static let placeholderNationalNumber = "+855"
        static let verificationConfirm = "You will get sms with a confirmation code to this number "
    }
    lazy var loginLabel: UILabel = EditProfileVariables.getLabelRegister(title: Constant.loginLabel)
    lazy var nicknameLabel : UILabel = EditProfileVariables.getLabelRegister(title: Constant.setNameLabel)
    lazy var createAccLabel: UILabel = EditProfileVariables.getLabelRegister(title: Constant.createAccountLabel)
    lazy var resetPassLabel: UILabel = EditProfileVariables.getLabelRegister(title: Constant.resetPssLabel)
    lazy var forgotPasslabel: UILabel = EditProfileVariables.getLabelRegister(title: Constant.forgotPasswordLabel)
    
    lazy var usernameTextField: JVFloatLabeledTextField = EditProfileVariables.getTextField(type: Constant.typeName)
    lazy var emailTextField: JVFloatLabeledTextField = EditProfileVariables.getTextField(type: Constant.typeEmail)
    lazy var currentPassword: JVFloatLabeledTextField = EditProfileVariables.getTextField(type: Constant.typePass)
    lazy var passwordConfirmField: JVFloatLabeledTextField = EditProfileVariables.getTextField(type: Constant.typeConfirmPass)
    lazy var newPasswordField: JVFloatLabeledTextField = EditProfileVariables.getTextField(type: Constant.typeNewPass)

    
    lazy var loginButton: MDCButton = EditProfileVariables.getUIButton(title: Constant.loginLabel)
    lazy var forgotPasswordButton: MDCButton = EditProfileVariables.getUIButton(title: Constant.forgotPasswordButton)
    lazy var createAccButton: MDCButton = EditProfileVariables.getUIButton(title: Constant.createAccButton)
    lazy var registerAccBtn: MDCButton = EditProfileVariables.getUIButton(title: Constant.createAccRegisterBtn)
    lazy var resetPassBtn : MDCButton = EditProfileVariables.getUIButton(title: Constant.resetPassButton)
    lazy var editSaveBtton: MDCButton = EditProfileVariables.getUIButton(title: Constant.saveBtn)
    lazy var resetPasswordButton: MDCButton = EditProfileVariables.getUIButton(title: Constant.resetPassBtn)
    
    lazy var getImageView: UIImageView = EditProfileVariables.getImageView(imageName: "logo")
    lazy var getConfirmSMSImage: UIImageView = EditProfileVariables.getImageView(imageName: "phone-confirmation")
    
    lazy var getPlaceholderNum: JVFloatLabeledTextField = EditProfileVariables.getPhoneNumberField(placeholder: Constant.placeholderNationalNumber)
    
    lazy var getPhoneNumberInput: JVFloatLabeledTextField = EditProfileVariables.getPhoneNumberField(placeholder: "Phone Number")
    
    lazy var firstCodeField: JVFloatLabeledTextField = EditProfileVariables.getPhoneNumberField(placeholder: "")
    
    
    lazy var secondCodeField: JVFloatLabeledTextField = EditProfileVariables.getPhoneNumberField(placeholder: "")
    
    lazy var thirdCodeField: JVFloatLabeledTextField = EditProfileVariables.getPhoneNumberField(placeholder: "")
    
    lazy var fourthCodeField: JVFloatLabeledTextField = EditProfileVariables.getPhoneNumberField(placeholder: "")
    
    lazy var fifthCodeField: JVFloatLabeledTextField = EditProfileVariables.getPhoneNumberField(placeholder: "")
    
    lazy var sixthCodeField: JVFloatLabeledTextField = EditProfileVariables.getPhoneNumberField(placeholder: "")
    
    lazy var confirmSMS : UILabel = EditProfileVariables.getLabelRegister(title: Constant.verificationConfirm)
    
}

extension JVFloatLabeledTextField {
    
    func setupBottomBorder() {
        
        self.borderStyle = .none
        self.backgroundColor = .white
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 0
        
    }
    
}

extension EditProfileVariables {
    
    static func getImageView(imageName: String) -> UIImageView {
        
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: imageName)
        imageView.layer.masksToBounds = true
        return imageView
        
    }
    
    static func getLabelRegister(title: String) -> UILabel {
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        if title == Constant.verificationConfirm {
            label.numberOfLines = 2
            label.textAlignment = .center
            label.font = UIFont(name: Constant.roboto_regular, size: 14)
            label.textColor = UIColor.gray
            
        } else {
            
            label.font = UIFont(name: Constant.roboto_bold, size: 22)
            label.textColor = UIColor(red:0.00, green:0.50, blue:0.00, alpha:1.0)
        }
        label.text = title
        return label
    }
    
    static func getMaterialTextField(palceholder: String) -> MDCTextField {
        
        let textfield = MDCTextField()
        textfield.keyboardType = .numberPad
        if palceholder != Constant.placeholderNationalNumber {
         
            textfield.clearButtonMode = .whileEditing
            
        }
        
        
        return textfield
        
    }
    
    static func getPhoneNumberField(placeholder: String) -> JVFloatLabeledTextField  {
        
        let textField = JVFloatLabeledTextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = UIFont(name: Constant.roboto_regular, size: 18)
        textField.placeholder = placeholder
        textField.textAlignment = .center
        textField.keyboardType = UIKeyboardType.numberPad
        if placeholder == Constant.placeholderNationalNumber {
            textField.placeholderColor = UIColor.black
            
        } else {
            
            textField.font = UIFont(name: Constant.roboto_regular, size: 16)
        }
        textField.setupBottomBorder()
        return textField
    }
    
    static func getTextField(type: String) -> JVFloatLabeledTextField {
        
        let textField = JVFloatLabeledTextField()
        //textField.borderColor = UIColor(red:0.00, green:0.50, blue:0.00, alpha:1.0)
    
        textField.clearButtonMode = .whileEditing
        textField.borderStyle = .roundedRect
        textField.layer.borderWidth = 0.5
        textField.layer.borderColor = UIColor(red:0.00, green:0.50, blue:0.00, alpha:1.0).cgColor
        if type == Constant.typePass || type == Constant.typeConfirmPass || type == Constant.typeNewPass{
            textField.isSecureTextEntry = true
            textField.placeholder = type
            textField.keyboardType = UIKeyboardType.numbersAndPunctuation
        }
        if type == Constant.typeEmail{
            textField.placeholder = type
            textField.keyboardType = UIKeyboardType.emailAddress
        }
        if type == Constant.typeName {
            textField.placeholder = type
            textField.keyboardType = UIKeyboardType.alphabet
        }
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = UIFont(name: Constant.roboto_regular, size: 16)
        return textField
        
    }
    static func getSeparateView () -> UIView{
        let view = UIView()
        view.backgroundColor = .gray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    static func getUIButton(title: String) -> MDCButton {
        
        let containedButton = MDCButton()
//        containedButton.isOpaque = false
        let buttonScheme = MDCButtonScheme()
        if let customFont = UIFont(name: Constant.roboto_regular, size:16.0) {
            let typographyScheme = MDCTypographyScheme()
            typographyScheme.button = customFont
            buttonScheme.typographyScheme = typographyScheme
        }
        containedButton.setTitle(title, for: .normal)
        containedButton.sizeToFit()
        containedButton.translatesAutoresizingMaskIntoConstraints = false
        
        if title == Constant.loginLabel || title == Constant.createAccRegisterBtn || title == Constant.saveBtn || title == Constant.resetPassBtn {
            MDCContainedButtonThemer.applyScheme(buttonScheme, to: containedButton)
            containedButton.setTitleColor(UIColor.white, for: .normal)
            containedButton.backgroundColor = UIColor(red:0.00, green:0.50, blue:0.00, alpha:1.0)
            containedButton.setElevation(ShadowElevation.none, for: .normal)
            containedButton.setShadowColor(UIColor.white, for: .normal)
            containedButton.titleLabel?.textAlignment = .left
        } else {
            
            if title == Constant.forgotPasswordButton || title == Constant.resetPassButton{
                if let customFont = UIFont(name: Constant.roboto_bold, size:14.0) {
                    let typographyScheme = MDCTypographyScheme()
                    typographyScheme.button = customFont
                    buttonScheme.typographyScheme = typographyScheme
                }
                MDCTextButtonThemer.applyScheme(buttonScheme, to: containedButton)
//                MDCOutlinedButtonThemer.applyScheme(buttonScheme, to: containedButton)
                
            }else {
                if let customFont = UIFont(name:Constant.roboto_bold, size:16.0) {
                    let typographyScheme = MDCTypographyScheme()
                    typographyScheme.button = customFont
                    buttonScheme.typographyScheme = typographyScheme
                }
                MDCOutlinedButtonThemer.applyScheme(buttonScheme, to: containedButton)
                
            }
            containedButton.setTitleColor(UIColor(red:0.00, green:0.50, blue:0.00, alpha:1.0), for: .normal)
            containedButton.backgroundColor = .white
        }
        
        return containedButton
        
//        let button = UIButton()
//        button.setTitle(title, for: .normal)
//        button.titleLabel?.font = UIFont(name: "SFCompactText-Regular", size: 16)
//        button.translatesAutoresizingMaskIntoConstraints = false
//
//        if title == Constant.loginLabel || title == Constant.createAccRegisterBtn || title == Constant.saveBtn || title == Constant.resetPassBtn{
//            button.setTitleColor(UIColor.white, for: .normal)
//            button.backgroundColor = UIColor(red:0.00, green:0.50, blue:0.00, alpha:1.0)
//            button.layer.cornerRadius = 5
//            button.titleLabel?.textAlignment = .left
//        }
//        else {
//
//            button.setTitleColor(UIColor(red:0.00, green:0.50, blue:0.00, alpha:1.0), for: .normal)
//            button.backgroundColor = .white
//
//            if title == Constant.forgotPasswordButton || title == Constant.resetPassButton{
//
//                button.titleLabel?.font = UIFont(name: "SFCompactText-Bold", size: 14)
//            }else {
//                button.titleLabel?.font = UIFont(name: "SFCompactText-Bold", size: 16)
//            }
//
//        }
//
//
//        return button
    }
    
}

