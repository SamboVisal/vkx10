//
//  UIViewProperty.swift
//  vKclub
//
//  Created by Pisal on 7/24/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import UIKit

import MaterialComponents.MaterialCards
import MaterialComponents
import Material

class ExploreCategoryComponents {
    
    lazy var imageUserCircleView : ImageCirCleView = {
        
        let imageview = ImageCirCleView()
        imageview.contentMode = .scaleAspectFill
        imageview.translatesAutoresizingMaskIntoConstraints = false
        imageview.layer.masksToBounds = true
        imageview.layer.borderWidth = 1
        imageview.layer.borderColor = UIColor(red:0.00, green:0.50, blue:0.00, alpha:1.0).cgColor
        imageview.focusOnFaces = true
        imageview.debugFaceAware = true
        imageview.image = UIImage(named: "detailprofile-icon")
        imageview.image = imageview.image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        imageview.backgroundColor = .white
        
        return imageview
    }()
    
    let getDeviceType = UIDevice.modelName
    
    lazy var collectionViewInstance: UICollectionView = {
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: .zero,collectionViewLayout: flowLayout)
        collectionView.backgroundColor =  .white
        collectionView.alwaysBounceVertical = true
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.showsHorizontalScrollIndicator = false
        return collectionView
        
    }()
    lazy var activityIndicator: MDCActivityIndicator = {
        
        
        let activity = MDCActivityIndicator()
        activity.translatesAutoresizingMaskIntoConstraints = false
        activity.indicatorMode = .indeterminate
        activity.sizeToFit()
        
        return activity
        
    }()
    lazy var mainCardView: MDCCard = {
        
        let view = MDCCard()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isInteractable = false
        return view
        
    }()
    
    static func getNavigationMenuButton(image: UIImage) -> IconButton {
        let button = IconButton()
        button.tintColor = .white
        button.image = image
        return button
    }
    
    lazy var menuArrowDownButton: IconButton = ExploreCategoryComponents.getNavigationMenuButton(image: Icon.cm.arrowDownward!)
    lazy var menuDeleteButton: IconButton = ExploreCategoryComponents.getNavigationMenuButton(image: Icon.cm.clear!)

    
 
    
    lazy var cardView: MDCCard = {
        
        let view = MDCCard()
        
        view.backgroundColor = .white
        view.isInteractable = false

        return view
        
    }()
    

    let categoryDetailContent: UILabel = {
        
        let categoryDetailContent = UILabel()
        categoryDetailContent.numberOfLines = 6
        categoryDetailContent.text = "Content describes particular accommodation. This can be many lines of content as you want, but based on the main view."
        if getDeviceModelName.userDeviceIphone5() {
            categoryDetailContent.font = UIFont(name: roboto_regular, size: 12)
        }
        else if getDeviceModelName.userDeviceIphone678() {
            categoryDetailContent.font = UIFont(name: roboto_regular, size: 14)
        }
        else if getDeviceModelName.userDeviceIphone678Plus() {
            categoryDetailContent.font = UIFont(name: roboto_regular, size: 16)
        } else {
            
            categoryDetailContent.font = UIFont(name: roboto_regular, size: 14)
        }
        
        categoryDetailContent.translatesAutoresizingMaskIntoConstraints = false
        return categoryDetailContent
    }()
    
    lazy var exploreCategoryLabel: UILabel = {
        
        let categoryLabel = UILabel()
        
        
        categoryLabel.text = "Description"
        categoryLabel.textColor = .black
        categoryLabel.numberOfLines = 2
        if getDeviceModelName.userDeviceIphone5() {
            categoryLabel.font = UIFont(name: roboto_bold, size: 16)
        }
        else if getDeviceModelName.userDeviceIphone678() {
            categoryLabel.font = UIFont(name: roboto_bold, size: 18)
        }
        else if getDeviceModelName.userDeviceIphone678Plus(){
           
            categoryLabel.font = UIFont(name: roboto_bold, size: 20)
        } else {
            
            categoryLabel.font = UIFont(name: roboto_bold, size: 18)
        }
        categoryLabel.translatesAutoresizingMaskIntoConstraints = false
        return categoryLabel
        
    }()
    
    
    lazy var materialImageView: CardImageView = {
        
        let imageView = CardImageView()

        imageView.image = UIImage(named: "pineresort")
        imageView.contentMode = .scaleAspectFill
        imageView.layer.masksToBounds = true
        
        return imageView
        
        
    }()
    
    
}



