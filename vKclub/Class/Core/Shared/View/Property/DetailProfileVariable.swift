//
//  DetailProfileVariable.swift
//  vKclub
//
//  Created by Pisal on 7/16/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import UIKit




class DetailProfileVariables {
    
    struct membershipConstant {
        static let typeName = "Set nickname"
        static let typeEmail = "someone@gmail.com"
    }
    
    lazy var opacityView: UIView = {
        
        
        let view = UIView()
        
        view.backgroundColor = UIColor.black.withAlphaComponent(0.40)
        view.isOpaque = false
        view.layer.masksToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
        
    }()
    lazy var detailProfileView: UIView = {
        
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.masksToBounds = true
        return view
        
    }()
    

    
    lazy var imageUserDetailProfileView: UIImageView = {
        
        let imageView = UIImageView()
        
       
        imageView.contentMode = .scaleAspectFill

        imageView.image = UIImage(named: "detailprofile-icon")
        imageView.image = imageView.image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        imageView.clipsToBounds = true
    
        return imageView
        
        
    }()
    lazy var imageUserCircleView : ImageCirCleView = {
       
        let imageview = ImageCirCleView()
        imageview.contentMode = .scaleAspectFill
        imageview.translatesAutoresizingMaskIntoConstraints = false
        imageview.layer.masksToBounds = true
        imageview.layer.borderWidth = 1
        imageview.layer.borderColor = UIColor(red:0.00, green:0.50, blue:0.00, alpha:1.0).cgColor
        imageview.focusOnFaces = true
        imageview.debugFaceAware = true
        imageview.isOpaque = false
        imageview.image = UIImage(named: "detailprofile-icon")
        imageview.image = imageview.image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        imageview.backgroundColor = .white
        
        return imageview
    }()
    
    lazy var camera: UIImageView = {
        
        let imageview = UIImageView()
        imageview.contentMode = .scaleAspectFill
        imageview.translatesAutoresizingMaskIntoConstraints = false
        imageview.layer.masksToBounds = true
        imageview.image = R.image.camera()
        imageview.image = imageview.image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        return imageview
    }()
    
    lazy var nameUserDetailProfileView: UILabel = DetailProfileVariables.initUserLabel(title: membershipConstant.typeName)
    lazy var emailUserDetailProfileView: UILabel = DetailProfileVariables.initUserLabel(title: membershipConstant.typeEmail)
    
    
    
    let divideView: UIView = {
        
        
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.gray
        return view
    }()
    lazy var simepleView: UIView = DetailProfileVariables.simpleView()
    
    
}
extension DetailProfileVariables {
    
    static func initUserLabel (title: String) -> UILabel {
        let label = UILabel()
        label.text = title
        label.sizeToFit()
        label.minimumScaleFactor = 0.2
        if title == membershipConstant.typeName {
            label.font = UIFont(name: roboto_bold, size: 22)
        } else {
            label.font = UIFont(name: roboto_regular, size: 16)
            label.textColor = .gray
        }
        
        return label
    }
    
    static func getButtonWithName(titleName: String) -> UIButton {
        
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        
        button.contentHorizontalAlignment = .right
        
        button.setImage(UIImage(named: "cancel-edit"), for: .normal)
        button.setTitle(titleName, for: .normal)
        button.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        button.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        button.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        button.setTitleColor(UIColor.black, for: .normal)
        button.titleLabel?.font = UIFont(name: roboto_regular, size: 13)
        return button
        
    }
    
    static func simpleView() -> UIView {
        
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = BaseColor.colorPrimary
        return view
    }
    
    static func CGRectView () -> UIView {
        
        let view = UIView()
        view.backgroundColor = UIColor(red:0.00, green:0.50, blue:0.00, alpha:1.0)
        
        return view
    }
    
    
}
