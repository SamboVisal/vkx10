
//
//  Network.swift
//  vKclub Version 2
//
//  Created by Pisal on 6/25/2561 BE.
//  Copyright © 2561 BE Pisal. All rights reserved.
//

import Foundation

import AVFoundation

class PermissionAppUser {
    
    
    static func AskAudioPermission() {
  
        let session = AVAudioSession.sharedInstance()
        
        if (session.responds(to: #selector(AVAudioSession.requestRecordPermission(_:)))) {
            AVAudioSession.sharedInstance().requestRecordPermission({(granted: Bool)-> Void in
                if granted {
                    
                    do {
                        try session.setCategory(AVAudioSessionCategoryPlayAndRecord)
                        try session.setActive(true)
                    }
                    catch {
                        print("Couldn't set Audio session category")
                    }
                } else{
                    print("not granted")
                }
            })
        }
        
    }
    static  func CheckAudioPermission() -> Bool {
        var audioPermission : Bool
        switch AVAudioSession.sharedInstance().recordPermission() {
        case AVAudioSessionRecordPermission.granted:
            audioPermission  = true
            break
        default:
            audioPermission  = false
            break
        }
        return  audioPermission
    }
    
    
}
