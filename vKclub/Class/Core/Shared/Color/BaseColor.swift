//
//  BaseColor.swift
//  vKclub
//
//  Created by Chhayrith on 11/28/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit

struct BaseColor {
    // #4C9A3C
    static let colorAccentLightHalf = UIColor(red:0.30, green:0.60, blue:0.24, alpha:1.0)
    
    // #1c6f0b
    static let colorAccentDark = UIColor(red:0.11, green:0.44, blue:0.04, alpha:1.0)
    
    // #60E614
    static let colorAccentLight = UIColor(red:0.38, green:0.90, blue:0.08, alpha:1.0)
    
    // #fdab1c
    static let colorAccent1 = UIColor(red:0.99, green:0.67, blue:0.11, alpha:1.0)
    
    // #EDEEED
    static let colorLightAccent = UIColor(red:0.93, green:0.93, blue:0.93, alpha:1.0)
    
    // #616161
    static let colorLightDark = UIColor(red:0.38, green:0.38, blue:0.38, alpha:1.0)
    
    // #CCCCCC
    static let colorLightGray = UIColor(red:0.80, green:0.80, blue:0.80, alpha:1.0)
    
    // #c63010
    static let colorSecondaryDark = UIColor(red:0.78, green:0.19, blue:0.06, alpha:1.0)
    
    // #e0e3da
    static let colorBackground = UIColor(red:0.88, green:0.89, blue:0.85, alpha:1.0)
    
    // #f4f4f4
    static let lightGrey = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
    
    // #0B7F17
    static let colorPrimary = UIColor(red:0.04, green:0.50, blue:0.09, alpha:1.0)
    
    // #4DFC04
    static let colorPrimaryLight = UIColor(red:0.30, green:0.99, blue:0.02, alpha:1.0)
    
    // #E82222
    static let colorSecondary = UIColor(red:0.91, green:0.13, blue:0.13, alpha:1.0)
    
    // #DC9D07
    static let colorAccent = UIColor(red:0.86, green:0.62, blue:0.03, alpha:1.0)
    
    // #000000
    static let colorBlack = UIColor(red:0.00, green:0.00, blue:0.00, alpha:1.0)
    static let colorBlack20Percent = UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.2)
    
    // #ffffff
    static let colorWhite = UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0)
}
