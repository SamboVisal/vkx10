//
//  UserProfileCore+CoreDataProperties.swift
//  
//
//  Created by Pisal on 11/30/2561 BE.
//
//

import Foundation
import CoreData


extension UserProfileCore {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserProfileCore> {
        return NSFetchRequest<UserProfileCore>(entityName: "UserProfileCore")
    }

    @NSManaged public var email: String?
    @NSManaged public var name: String?
    @NSManaged public var userID: String?
    @NSManaged public var pfpUrl: String?
    @NSManaged public var presence: Bool
    @NSManaged public var role: String?
    @NSManaged public var vkclubNumber: String?

}
