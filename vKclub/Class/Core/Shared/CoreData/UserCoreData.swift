//
//  UserCoreData.swift
//  vKclub
//
//  Created by Pisal on 9/7/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import CoreData


class UserProfileCoreData {
    
    // check if user close the notification
    func CreatnotificationCoredata(_notification_num: Int , _notification_body: String,_notification_title : String ){
        let newNotification = NSEntityDescription.insertNewObject(forEntityName: "Notifications", into: managedObjectContext)
        newNotification.setValue(_notification_num, forKey: "notification_num")
        newNotification.setValue(_notification_body, forKey: "notification_body")
        newNotification.setValue(_notification_title, forKey: "notification_title")
        do{
            try  managedObjectContext.save()
        } catch {
            print("error")
        }
        
    }
    
    func StoreCallDataLog(_callerID: String , _callerName: String, _callDuration : String, _callIndicatorIcon: String) {
        let (year, month, date, hour, min, sec) = UIComponentHelper.GetTodayString()
        let timeStamp = "\(hour):\(min)"
        let callLogTime = "\(year)-\(month)-\(date)-\(hour)-\(min)-\(sec)"
        //Used to check and update duration into the this particular call log
        
        InComeCallController.callLogTime = callLogTime
        
        let CallDataLog = NSEntityDescription.insertNewObject(forEntityName: "SipCallData", into: managedObjectContext)
        CallDataLog.setValue(_callDuration, forKey: "callDuration")
        CallDataLog.setValue(_callerID, forKey: "callerID")
        CallDataLog.setValue(_callerName, forKey: "callerName")
        CallDataLog.setValue(_callIndicatorIcon, forKey: "callIndicatorIcon")
        CallDataLog.setValue(timeStamp, forKey: "timeStamp")
        CallDataLog.setValue(callLogTime, forKey: "callLogTime")
        do {
            try managedObjectContext.save()
            print("===== Saved ", CallDataLog)
            TestRecentCallController().loadData()
            //RecentCallController.LoadCallDataCell()
        } catch {
            print("Could not save CallDataLog into CoreData \(error.localizedDescription) ===")
        }
    }
    
    // Gets a person by id
    func getByIdUserProfile(_id: NSManagedObjectID) -> UserProfiles? {
        return managedObjectContext.object(with: _id) as? UserProfiles
    }
    
    func getCallDataByID (_id: NSManagedObjectID) -> SipCallData? {
        return managedObjectContext.object(with: _id) as? SipCallData
    }
    
    func updateUserProfile(_updatedPerson: UserProfiles){
        
        if let person = getByIdUserProfile(_id: _updatedPerson.objectID){
           
            person.imageData = _updatedPerson.imageData
            person.email     = _updatedPerson.email
            person.username  = _updatedPerson.username
            
            do{
                try  managedObjectContext.save()
                
            }catch{
                print("error")
            }
        }
        
    }
    
    func getUserProfile(withPredicate queryPredicate: NSPredicate) -> [UserProfiles] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UserProfiles")
        
        fetchRequest.predicate = queryPredicate
        
        do {
            let response = try managedObjectContext.fetch(fetchRequest)
            return response as! [UserProfiles]
            
        } catch let error as NSError {
            // failure
            print(error)
            return [UserProfiles]()
        }
    }
    
    func deleteAllData(entity: String) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        
        do {
            let results = try managedObjectContext.fetch(fetchRequest)
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                managedObjectContext.delete(managedObjectData)
                try managedObjectContext.save()
                
            }
        } catch let error as NSError {
            print("Detele all data in \(entity) error : \(error) \(error.userInfo)")
        }
    }
    
    func getSipCallDataFetchResult(withPredicate queryPredicate: NSPredicate) -> [SipCallData] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "SipCallData")
        fetchRequest.predicate = queryPredicate
        do {
            let response = try managedObjectContext.fetch(fetchRequest)
            return response as! [SipCallData]
            
        } catch let error as NSError {
            // failure
            print(error)
            return [SipCallData]()
        }
    }
    
    func getRecentCallData() -> [RecentCalls] {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "RecentCalls")
        do {
            
            let response = try managedObjectContext.fetch(fetchRequest)
            
            return response as! [RecentCalls]
            
        } catch {
            print(error)
            return [RecentCalls]()
        }
        
    }
    
    
    static func deleteAllData(entity: String) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        
        do {
            let results = try managedObjectContext.fetch(fetchRequest)
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                (managedObject as AnyObject).delete(managedObjectData)
                try (managedObject as AnyObject).save()
                
            }
        } catch let error as NSError {
            print("Detele all data in \(entity) error : \(error) \(error.userInfo)")
        }
    }
    
    
}


