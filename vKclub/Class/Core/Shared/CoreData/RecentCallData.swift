//
//  RecentCallData.swift
//  vKclub
//
//  Created by Pisal on 11/30/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import Foundation
import CoreData


class RecentCallDataClass {
    
    func storeCallData(callerID: String, callName: String) {
        
        let dataLog = NSEntityDescription.insertNewObject(forEntityName: "RecentCalls", into: managedObjectContext)
        
        dataLog.setValue(callerID, forKey: "callerID")
        dataLog.setValue(callName, forKey: "CallName")
        
        do {
            try managedObjectContext.save()
        } catch {
            print("Could not save CallDataLog into CoreData \(error.localizedDescription) ===")
        }
        
    }
    
    
    
}
