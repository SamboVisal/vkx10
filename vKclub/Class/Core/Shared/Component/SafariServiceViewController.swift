//
//  SafariServiceViewController.swift
//  vKclub
//
//  Created by Chhayrith on 11/15/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import SafariServices

class SafariServiceViewController: UIViewController {
    
    private var url: URL! = nil
    private var isReaderModeEnabled: Bool? = false
    let navTitle = "Movie"
    public convenience init(aRequest: URL, isReaderModeEnabled: Bool) {
        self.init()
        self.isReaderModeEnabled = isReaderModeEnabled
        self.url = aRequest
    }
    
    override func viewDidLoad() {
        self.navigationItem.title = navTitle
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.viewDidDisappear(true)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    func loadUrl() -> SFSafariViewController {
        let webURL = url
        let config = SFSafariViewController.Configuration()
        config.entersReaderIfAvailable = isReaderModeEnabled!
        
        let safariVC = SFSafariViewController(url: webURL!, configuration: config)
        safariVC.preferredBarTintColor = BaseColor.colorAccentDark
        safariVC.configuration.barCollapsingEnabled = true
        safariVC.statusBarController?.statusBarStyle = .lightContent
        return safariVC
    }

    
}

