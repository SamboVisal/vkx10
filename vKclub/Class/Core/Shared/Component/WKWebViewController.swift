//
//  StreamVideoViewController.swift
//  vKclub
//
//  Created by Pisal on 9/10/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import UIKit
import WebKit
import PopupDialog
import MaterialComponents.MDCActivityIndicator
import SnapKit


class StreamVideoViewController: UIViewController , WKUIDelegate{
    
    var storedStatusColor: UIBarStyle?
    var buttonColor: UIColor? = nil
    var titleColor: UIColor? = nil
    var closing: Bool! = false
    var urlTarget: URL!
    
    lazy var backBarButton: UIBarButtonItem = {
        var BackBarButtonItem = UIBarButtonItem(image: StreamVideoViewController.bundledImage(named: "backward"),
                                                style: UIBarButtonItemStyle.plain,
                                                target: self,
                                                action: #selector(StreamVideoViewController.goBackTapped(_:)))
        BackBarButtonItem.width = 18.0
        BackBarButtonItem.tintColor = self.buttonColor
        return BackBarButtonItem
    }()
    lazy var forwardBarButton: UIBarButtonItem = {
        var ForwardBarButton = UIBarButtonItem(image: StreamVideoViewController.bundledImage(named: "foward"),
                                               style: UIBarButtonItemStyle.plain,
                                               target: self,
                                               action: #selector(StreamVideoViewController.goForwardTapped(_:)))
        ForwardBarButton.width = 18.0
        ForwardBarButton.tintColor = self.buttonColor
        return ForwardBarButton
    }()
    lazy var refreshBarButtonItem: UIBarButtonItem = {
        var tempRefreshBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.refresh,
                                                       target: self,
                                                       action: #selector(StreamVideoViewController.reloadTapped(_:)))
        tempRefreshBarButtonItem.tintColor = self.buttonColor
        return tempRefreshBarButtonItem
    }()
    
    lazy var stopBarButtonItem: UIBarButtonItem = {
        var tempStopBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.stop,
                                                    target: self,
                                                    action: #selector(StreamVideoViewController.stopTapped(_:)))
        tempStopBarButtonItem.tintColor = self.buttonColor
        return tempStopBarButtonItem
    }()
    
    
    lazy var webView: WKWebView = {
        var tempWebView = WKWebView(frame: UIScreen.main.bounds)
        tempWebView.uiDelegate = self
        tempWebView.navigationDelegate = self
        return tempWebView;
    }()
    lazy var noInternetLabel: UILabel = {
       
        let label = UILabel()
        label.text = "Server is not responding. Please try again."
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.numberOfLines = 2
        if getDeviceModelName.userDeviceIphone5() {
          label.font = UIFont(name: "SFCompactText-Regular", size: 22)
        } else {
            label.font = UIFont(name: "SFCompactText-Regular", size: 26)
        }
        return label
    }()
    
    var request: URLRequest!
    var lastContentOffset: CGFloat = 0
    let indicator = ExploreCategoryComponents()
    var sharingEnabled = true
    var bookNowpage = true
    
    deinit {
        
        webView.stopLoading()
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        webView.uiDelegate = nil;
        webView.navigationDelegate = nil;
    }
    
    public convenience init(urlString: String, sharingEnabled: Bool = true,  bookNowpage: Bool = true) {
        print("Your url string \(urlString)")
        var urlString = urlString
        if !urlString.hasPrefix("https://") && !urlString.hasPrefix("http://") {
            urlString = "https://"+urlString
        }
        self.init(pageURL: URL(string: urlString)!, sharingEnabled: sharingEnabled, bookNowpage: bookNowpage)
    }
    
    public convenience init(pageURL: URL, sharingEnabled: Bool = true, bookNowpage: Bool = true) {
        self.init(aRequest: URLRequest(url: pageURL), sharingEnabled: sharingEnabled, bookNowpage: bookNowpage)
    }
    
    public convenience init(aRequest: URLRequest, sharingEnabled: Bool = true, bookNowpage: Bool = true) {
        self.init()
        self.sharingEnabled = sharingEnabled
        self.bookNowpage = bookNowpage
        self.request = aRequest
        
    }
    
    
    
}

// load webview
extension StreamVideoViewController : MDCActivityIndicatorDelegate{
    
    
    func loadRequest(_ request: URLRequest){
        webView.load(request)
        webView.allowsLinkPreview = false
    }
    fileprivate func animateActivityIndicator() {
        
       
       
        webView.addSubview(indicator.activityIndicator)

        indicator.activityIndicator.delegate = self
        indicator.activityIndicator.startAnimating()
       
        
        indicator.activityIndicator.snp.makeConstraints { (make) in
            make.center.equalTo(self.view)
        }
        
    }
    
    
    // setup toolbar whether it is Movie or Booking website
    fileprivate func setupToolbarw() {
        self.navigationItem.title = "Loading..."
        if !bookNowpage {
            
            // for booking page
            UIApplication.shared.statusBarStyle = .lightContent
            self.setupToolbar()
        } else {
            // for watching movie
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "cancel-edit")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), style: .done, target: self, action: #selector(backHome))
            self.navigationController?.navigationBar.barTintColor = UIColor(red:0.00, green:0.50, blue:0.00, alpha:1.0)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
            UIApplication.shared.statusBarStyle = .lightContent
        }
    }
    
    fileprivate func setupWKWebviewDelegate() {
        webView = WKWebView()
        
        webView.uiDelegate = self
        webView.navigationDelegate = self
        webView.allowsBackForwardNavigationGestures = true
        //view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - 20)
        view = webView
    }
    
    override func loadView() {
        
        
        setupWKWebviewDelegate()
        
        setupToolbarw()
        animateActivityIndicator()
        
        loadRequest(request)
        
        print("View is being loaded")
    }
    override func viewWillAppear(_ animated: Bool) {
        
        assert(self.navigationController != nil, "SVWebViewController needs to be contained in a UINavigationController. If you are presenting SVWebViewController modally, use SVModalWebViewController instead.")
        super.viewWillAppear(animated)

        if bookNowpage {
            if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone) {
                self.navigationController?.setToolbarHidden(false, animated: false)
            }
            else if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad) {
                self.navigationController?.setToolbarHidden(true, animated: true)
            }
        }
    }
    
    
    
    
    fileprivate func stopAnimating() {
        if indicator.activityIndicator.isAnimating {
            
            self.indicator.activityIndicator.stopAnimating()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        
        stopAnimating()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        UIApplication.shared.statusBarStyle = .default
        if !(self.navigationController?.toolbar.isHidden)! {
            self.navigationController?.setToolbarHidden(true, animated: true)
        }
    }
}

// handle UIToolbarItems action and setup view
extension StreamVideoViewController {
    
    
    func avoidLeaving() {
//        CustomPopupDialogView()
//        
//        let dialog = PopupDialog(title: "Warning", message: "Are you sure want to leave? This will not save your current booking if you have booked.", image: nil, buttonAlignment: .horizontal, transitionStyle: .bounceUp, preferredWidth: view.frame.width / 1.3, tapGestureDismissal: false, panGestureDismissal: false, hideStatusBar: true, completion: nil)
//        
//        let okay = DestructiveButton(title: "Okay") {
//            self.dismiss(animated: true, completion: nil)
//        }
//        let cancel = CancelButton(title: "Cancel") {
//            
//        }
//        dialog.addButtons([okay, cancel])
//        self.present(dialog, animated: true, completion: nil)
        
    }
    
    /// Navigation item for video streaming
    func setupToolbar() {
        
        
        let refreshStopBarButtonItem: UIBarButtonItem = webView.isLoading ? stopBarButtonItem : refreshBarButtonItem
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "cancel-edit")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), style: .plain, target: self, action: #selector(backHome))
        refreshStopBarButtonItem.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        refreshStopBarButtonItem.tintColor = .white
        self.navigationItem.rightBarButtonItem = refreshStopBarButtonItem
        self.navigationController?.navigationBar.barTintColor = UIColor(red:0.00, green:0.50, blue:0.00, alpha:1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
    }
    
    /// Toobar items for booking page webView
    func updateToolBarItems(){
        backBarButton.isEnabled = webView.canGoBack
        forwardBarButton.isEnabled = webView.canGoForward
        
        let refreshStopBarButtonItem: UIBarButtonItem = webView.isLoading ? stopBarButtonItem : refreshBarButtonItem
        
        let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        let flexibleSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad) {
            
            let toolbarWidth: CGFloat = 250.0
            fixedSpace.width = 35.0
            
            let items: NSArray = sharingEnabled ? [fixedSpace, refreshStopBarButtonItem, fixedSpace, backBarButton, fixedSpace, forwardBarButton, fixedSpace] : [fixedSpace, refreshStopBarButtonItem, fixedSpace, backBarButton, fixedSpace, forwardBarButton]
            
            let toolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: toolbarWidth, height: 44.0))
            if !closing {
                toolbar.items = items as? [UIBarButtonItem]
                if presentingViewController == nil {
                    toolbar.barTintColor = navigationController!.navigationBar.barTintColor
                }
                else {
                    toolbar.barStyle = navigationController!.navigationBar.barStyle
                }
                toolbar.tintColor = navigationController!.navigationBar.tintColor
            }
            navigationItem.rightBarButtonItems = items.reverseObjectEnumerator().allObjects as? [UIBarButtonItem]
            
        }
        else {
            let items: NSArray = sharingEnabled ? [fixedSpace, backBarButton, flexibleSpace, forwardBarButton, flexibleSpace, refreshStopBarButtonItem, flexibleSpace, fixedSpace] : [fixedSpace, backBarButton, flexibleSpace, forwardBarButton, flexibleSpace, refreshStopBarButtonItem, fixedSpace]
            
            if let navigationController = navigationController, !closing {
                if presentingViewController == nil {
                    navigationController.toolbar.barTintColor = navigationController.navigationBar.barTintColor
                }
                else {
                    navigationController.toolbar.barStyle = navigationController.navigationBar.barStyle
                }
                navigationController.toolbar.tintColor = navigationController.navigationBar.tintColor
                toolbarItems = items as? [UIBarButtonItem]
            }
        }
    }

    
    
}

// handle button actions
extension StreamVideoViewController {
    
    @objc
    func goBackTapped(_ sender: UIBarButtonItem) {
        
        webView.goBack()
    }
    
    @objc
    func goForwardTapped(_ sender: UIBarButtonItem) {
        webView.goForward()
    }
    
    @objc
    func reloadTapped(_ sender: UIBarButtonItem) {
        
        noInternetLabel.removeFromSuperview()
        if webView.url != nil{
            webView.reload()
        } else {
            startAnimating()
            self.navigationItem.title = "Loading..."
            webView.load(request)
        }
        
    }
    
    @objc
    func stopTapped(_ sender: UIBarButtonItem) {
        webView.stopLoading()
        if !bookNowpage {
            
            setupToolbar()
        } else {
            updateToolBarItems()
        }
        
    }
    
    func startAnimating() {
        
        indicator.activityIndicator.startAnimating()
    }
    
    @objc
    func reloadWeb() {
        
        
        if let url = webView.url {
            print("URL ", url)
            webView.reload()
        } else {
            startAnimating()
            self.navigationItem.title = "Loading..."
            if !bookNowpage {
                self.navigationItem.rightBarButtonItem?.isEnabled = false
                UIComponentHelper.showProgressWith(status: "Loading...", interact: true)
            }
            noInternetLabel.removeFromSuperview()
            webView.load(request)
            
        }
        self.noInternetLabel.removeFromSuperview()
    }
    @objc
    func backHome() {
        if !bookNowpage {
            self.dismiss(animated: true, completion: nil)
        } else {
            if webView.url != nil {
                avoidLeaving()
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        
        
    }
}

extension StreamVideoViewController {
    
    func setupNoInternet() {
        if let url = webView.url {
            print("We have url ", url)
        } else {
            if !bookNowpage {
                
                self.navigationItem.title = "Offline Mode"
            } else if bookNowpage {
                
                self.navigationItem.title = "Offline Mode"
                self.navigationController?.setToolbarHidden(false, animated: true)
            }
            self.view.addSubview(noInternetLabel)
            NSLayoutConstraint.activate([
                
                noInternetLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                noInternetLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                noInternetLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor),
                noInternetLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor)
                
                ])
        }
        
        
        
        
    }
    class func bundledImage(named: String) -> UIImage? {
        let image = UIImage(named: named)
        if image == nil {
            return UIImage(named: named, in: Bundle(for: StreamVideoViewController.classForCoder()), compatibleWith: nil)
        }
        return image
    }
    
    
    
}

extension UIScrollView {
    func scrollToTop() {
        let desiredOffset = CGPoint(x: 0, y: -contentInset.top)
        setContentOffset(desiredOffset, animated: true)
    }
}

// webview delegate
extension StreamVideoViewController : WKNavigationDelegate , UIScrollViewDelegate{
    

    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if bookNowpage {
            
            if webView.url != nil {
                if velocity.y > 0 {
                    
                    self.navigationController?.setToolbarHidden(true, animated: true)
                    
                } else {
                    self.navigationController?.setToolbarHidden(false, animated: true)
                }
            }
            
        }
        
        
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if bookNowpage {
//            if webView.url != nil {
//                if (self.lastContentOffset > scrollView.contentOffset.y) {
//
//                    if scrollView.scrollsToTop {
//                        self.navigationController?.setToolbarHidden(false, animated: true)
//                    }
//
//                    self.navigationController?.setToolbarHidden(false, animated: true)
//
//
//                    print("move up")
//                }
//                else if (self.lastContentOffset < scrollView.contentOffset.y) {
//
//
//
//                    self.navigationController?.setToolbarHidden(true, animated: true)
//
//
//                    print("move down")
//                }
//                self.lastContentOffset = scrollView.contentOffset.y
//            }
//
//        }
        
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        if bookNowpage {
           updateToolBarItems()
        } else {
            setupToolbar()
        }
        
        
    }
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        self.stopAnimating()
        print("Error in opening WEBVIEW \(error.localizedDescription)")
        if (error.localizedDescription == "The Internet connection appears to be offline."){
//            self.PresentDialogOneActionController(title: "Error", message: "You are not connect to our network. Please try again", actionTitle: "Got it")
            
            if bookNowpage {
                updateToolBarItems()
            } else {
                setupToolbar()
            }
            self.setupNoInternet()
            return
        }
//        self.PresentDialogOneActionController(title: "Error", message: "You are not connect to our network. Please try again", actionTitle: "Got it")
        if bookNowpage {
            updateToolBarItems()
        }else {
            setupToolbar()
        }
        self.setupNoInternet()
        return
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        self.stopAnimating()
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        webView.evaluateJavaScript("document.title", completionHandler: {(response, error) in
            
            if let title = response {
                self.navigationItem.title = title as? String
            }
            if self.bookNowpage {
                self.webView.scrollView.delegate = self
                self.updateToolBarItems()
            } else {
                self.setupToolbar()
               //self.navigationItem.rightBarButtonItem?.isEnabled = true
            }
        })
        
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.stopAnimating()
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        if bookNowpage {
            updateToolBarItems()
        } else {
            self.setupToolbar()
//            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
//        self.PresentDialogOneActionController(title: "Error", message: "You are not connect to our network. Please try again", actionTitle: "Got it")
    }
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        print("Navigation handler \(navigationAction.request)")
        
        decisionHandler(.allow)
    }
    
}
