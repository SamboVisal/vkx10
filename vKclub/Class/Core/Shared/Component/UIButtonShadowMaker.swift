//
//  UIButtonShadowMaker.swift
//  vKclub
//
//  Created by Chhayrith on 12/12/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit

class UIButtonShadowMaker {
    class func makeShadowButton(button: UIButton) -> Void{
        button.layer.shadowColor = BaseColor.colorBlack.cgColor
        button.layer.shadowOffset = CGSize(width: 2.0, height: 1.0)
        button.layer.shadowRadius = 4.0
        button.layer.shadowOpacity = 0.3
    }
}
