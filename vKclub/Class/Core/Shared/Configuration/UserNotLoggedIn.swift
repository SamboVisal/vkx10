//
//  CheckUserLoggedIn.swift
//  vKclub
//
//  Created by Chhayrith on 12/4/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import MaterialComponents.MDCAlertController
import Firebase

class UserNotLoggedIn: UIViewController {
    var that: UIViewController?
    private let detailProfileVariables = DetailProfileVariables()
    private let buttonComponents = ExploreVariables()
    

    
    func presentDialog(title: String, message: String, isCheckUserLoggedIn: Bool) {
        let alert = MDCAlertController(title: title, message: message)
        alert.buttonTitleColor = UIColor(red:0.03, green:0.62, blue:0.09, alpha:1.0)
        //MDCAlertControllerThemer.applyScheme(alertScheme, to: alert)
        let okayAction = MDCAlertAction(title: "Okay") { (action) in
            if title == "Logout" {
                UserDefaults.standard.setIsLoggedIn(value: false)
                UserDefaults.standard.setUserId(value: "")
                
                
                //IMAGELOAD = false
                try! Auth.auth().signOut()
                if isCheckUserLoggedIn {
                    self.handleLoginAndLogout()
                }
                //self.userIsNotLoggedIn()
            } else {
                let loginController = UINavigationController(rootViewController: LoginViewController())
                self.present(loginController, animated: true, completion: nil)
            }
            
        }
        let cancelAction = MDCAlertAction(title: "Cancel", handler: nil)
        alert.addAction(okayAction)
        alert.addAction(cancelAction)
        
        that?.present(alert, animated: true, completion: {
            alert.view.superview?.subviews[0].isUserInteractionEnabled = false
        })
    }
    
    func userIsNotLoggedIn() {
        
        detailProfileVariables.imageUserCircleView.image = UIImage(named: "detailprofile-icon")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        detailProfileVariables.nameUserDetailProfileView.numberOfLines = 2
        detailProfileVariables.nameUserDetailProfileView.text = "Please Sign In to make call and additional features."
        detailProfileVariables.nameUserDetailProfileView.font = UIFont.systemFont(ofSize: 18)
        detailProfileVariables.emailUserDetailProfileView.isHidden = true
        buttonComponents.logoutButton.isHidden = false
        buttonComponents.logoutButton.setTitle("Sign In", for: .normal)
        buttonComponents.detailProfile.isHidden = true
        buttonComponents.logoutButton.addTarget(self, action: #selector(handleLoginAndLogout), for: .touchUpInside)
    }
    
    @objc
    func handleLoginAndLogout() {
        
        let loginController = UINavigationController(rootViewController: LoginViewController())
        that?.present(loginController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
