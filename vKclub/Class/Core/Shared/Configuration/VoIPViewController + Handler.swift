//
//  VoIPViewController + Handler.swift
//  vKclub
//
//  Created by Chhayrith on 12/4/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit

extension VoIPViewController {
    @objc func handleQRCodeContact() {
        let qrVC = UINavigationController(rootViewController: QRViewController())
        self.present(qrVC, animated: true, completion: nil)
    }
}
