//
//  MovieViewController.swift
//  vKclub
//
//  Created by Chhayrith on 11/28/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import WebKit

class MovieViewController: UIViewController, WKUIDelegate {
    var webView: WKWebView!
    let web = StreamVideoViewController(urlString: "http://iptv.vkirirom.org", sharingEnabled: false, bookNowpage: false)
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let webURL = URL(string: "http://iptv.vkirirom.org")
        let webRequest = URLRequest(url: webURL!)
        webView?.load(webRequest)
        

        self.navigationItem.title = "Movie"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Close", style: .done, target: self, action: #selector(backToRoot))
    }
    
    @objc func backToRoot(){
        dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
   
    
    
    

}
