//
//  AboutUsController.swift
//  vKclub
//
//  Created by Pisal on 11/15/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import UIKit
import Firebase
import MaterialComponents.MaterialCollections
import SDWebImage

import Material
import XLPagerTabStrip


class AccommodationCollectionViewController: MDCCollectionViewController, IndicatorInfoProvider{
    
    let cellId = "cellId"
    var accommodation = [ExploreModel]()
    let activity = ExploreCategoryComponents()

    fileprivate func setupActivityIndicator() {
        view.addSubview(activity.activityIndicator)
        activity.activityIndicator.snp.makeConstraints { (make) in
            make.center.equalTo(self.view)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupActivityIndicator()
        activity.activityIndicator.startAnimating()
        
        
        FIRFireStoreService.shared.readExploreInformation(in: .information, and: .explore, type: .accommodation ,returning: ExploreModel.self) { (data, done) in
            
            if done {
                
                DispatchQueue.main.async(execute: {
                    self.accommodation = data
                    self.collectionView?.reloadData()
                })
            } else {
                
                DispatchQueue.main.async(execute: {
                    self.collectionView?.reloadData()
                })
                
            }
            
            
        }
        
        
        // #1
        collectionView?.backgroundColor = .white
        collectionView?.register(AccommodationCell.self, forCellWithReuseIdentifier: cellId)
        
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if accommodation.count > 0 {
            return accommodation.count
        }
        return 0
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! AccommodationCell
        
        // it's going to populate each data one by one
        let data = accommodation[indexPath.item]
        // #2
        cell.collectionView = self
        
        // it's going to pass all data
        cell.configure(data)
        
        
        
        //  it's going to re-rasterize the layer for each frame of the animation
        cell.layer.shouldRasterize = true
        return cell
    }

    // Return size of the entire collectionView
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 382)
    }

    // Return UIEdgeInsets
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
    }

    // Return space between each cell of the collectionView
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }

    // This will handle when user select on specific cell of collectionView
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let link = accommodation[indexPath.item].link {
            let cellUrl = URL(string: link)
            let safari = SafariServiceViewController(aRequest: cellUrl!, isReaderModeEnabled: false)
            self.present(safari.loadUrl(), animated: true, completion: nil)
        }
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Accommodation")
    }
}

class AccommodationCell: CollectionViewCell {
    
    
    let feature = ExploreCategoryComponents()
    var collectionView = AccommodationCollectionViewController()
    
    func configure(_ viewModel: ExploreModel) {
        feature.materialImageView.backgroundColor = BaseColor.colorPrimary
        
        // Download image from URL and save to cache memory
        if viewModel.imageUrl == nil {
            feature.materialImageView.image = UIImage(named: "placeholder")
        }else{
            feature.materialImageView.sd_setImage(with: URL(string: viewModel.imageUrl!), placeholderImage: nil, options: [.retryFailed, .continueInBackground]) { (image, error, cache, url) in
                
                if error == nil {
                    self.collectionView.activity.activityIndicator.stopAnimating()
                    print("Image success loaded")
                } else {
                    self.collectionView.activity.activityIndicator.stopAnimating()
                    print("Error in loading image \(String(describing: error?.localizedDescription))")
                }
            }
        }
        
        // Set data
        feature.exploreCategoryLabel.text = viewModel.title
        feature.categoryDetailContent.text = viewModel.snippet
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        feature.cardView.snp.makeConstraints { (make) in
            
            make.leading.equalTo(self).offset(8)
            make.trailing.equalTo(self).offset(-8)
            make.height.top.equalTo(self)
            
        }
        feature.materialImageView.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalTo(feature.cardView)
            make.height.equalTo(194)
            
        }
        feature.exploreCategoryLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(feature.cardView).offset(16)
            make.top.equalTo(feature.materialImageView.snp.bottom).offset(16)
            make.trailing.equalTo(feature.cardView).offset(-24)
        }
        feature.categoryDetailContent.snp.makeConstraints { (make) in
            make.top.equalTo(feature.exploreCategoryLabel.snp.bottom).offset(16)
            make.leading.trailing.equalTo(feature.exploreCategoryLabel)
        }
        
    }
    
    func setupView() {
        
        addSubview(feature.cardView)
        addSubview(feature.materialImageView)
        addSubview(feature.exploreCategoryLabel)
        addSubview(feature.categoryDetailContent)
    }
    
}


