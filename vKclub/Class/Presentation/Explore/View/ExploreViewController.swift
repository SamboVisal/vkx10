//
//  AboutUsViewController.swift
//  vKclub
//
//  Created by Chhayrith on 11/7/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import XLPagerTabStrip


class ExploreViewController: ButtonBarPagerTabStripViewController {

    override func viewDidLoad() {
        self.pagerMenuBarCustomization()
        super.viewDidLoad()
        self.CustomNavigationBar(title: "Explore", isShadow: true, isPresentVC: false)

        // set explore view controller's background color
        view.backgroundColor = BaseColor.colorWhite
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //self.CustomNavigationBar(title: "Explore", isShadow: false, isPresentVC: false)
//        customNavigation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //self.CustomNavigationBar(title: "Explore", isShadow: false, isPresentVC: false)
        
    }
    
    // Mark- Menu Bar configuration
    func pagerMenuBarCustomization() {
        settings.style.buttonBarBackgroundColor = .white
        settings.style.buttonBarItemBackgroundColor = BaseColor.colorAccentDark
        
        settings.style.buttonBarItemFont = UIFont(name: "Roboto-Bold", size: 16.0)!
        
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarHeight = 50
        settings.style.selectedBarBackgroundColor = .white
        
        
        // Changing item text color on swipe
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .white
            newCell?.label.textColor = .white
        }
    }
    
    // Mark- Menu Bar's tab configuration
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        let child_1 = AccommodationCollectionViewController()
        let child_2 = ActivityCollectionViewController()
        let child_3 = PropertyCollectionViewController()
        
        return [child_1, child_2, child_3]
    }
}

