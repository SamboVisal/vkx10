//
//  InternalCall.swift
//  vKclub
//
//  Created by Pisal on 11/28/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import Foundation
import UIKit

class ButtonBorder: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //        layer.borderWidth = 1
        //        layer.borderColor = UIColor.white.cgColor
        
        let topBorder = UIView(frame: CGRect(x: 0,y:  0, width: frame.size.width * 3, height: 1))
        let leftBorder = UIView(frame: CGRect(x: 0,y:  0, width: 1, height: frame.size.height * 3))
        topBorder.backgroundColor = UIColor.white
        leftBorder.backgroundColor = UIColor.white
        addSubview(topBorder)
        addSubview(leftBorder)
        
    }
    
    
    
}
class InternalPhoneCall : UIViewController{
    
    @IBOutlet weak var numberTextField: UITextField!
    
    var dialPhoneNumber: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getTopViewController().dismiss(animated: false, completion: nil)
        view.backgroundColor = .clear
        
    }
    
    
    @IBAction func NumberButtonClicked(_ sender: Any) {
        
        dialPhoneNumber = dialPhoneNumber + ((sender as AnyObject).titleLabel??.text)!
        
        numberTextField.text = dialPhoneNumber
    }
    
    
    @IBAction func DeleteButtonClicked(_ sender: Any) {
        
        print("Delete button clicked")
        
    }
    
    
    @IBAction func CallButtonClicked(_ sender: Any) {
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
}
