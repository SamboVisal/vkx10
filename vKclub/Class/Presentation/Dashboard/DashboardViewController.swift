//
//  DashboardViewController.swift
//  vKclub
//
//  Created by Machintos-HD on 11/1/18.
//  Copyright © 2018 vKirirom.com. All rights reserved.
//

import UIKit
import MaterialComponents
import WebKit
import SafariServices
import Firebase
import Localize_Swift
import PhoneNumberKit
import HandySwift
// class variables

class DashboardViewController: UIViewController , UITabBarControllerDelegate {
    
    
    @IBOutlet weak var mapBtn: MDCButton!
    @IBOutlet weak var serviceBtn: MDCButton!
    @IBOutlet weak var documentBtn: MDCButton!
    @IBOutlet weak var videoBtn: MDCButton!
    @IBOutlet weak var aboutUsBtn: MDCButton!
    @IBOutlet weak var membershipBtn: MDCButton!
    @IBOutlet weak var featureContainer: UIView!
    let repo = RepositoryClass()
    let language = Localize.availableLanguages()
    private let setting = UserDefaults.standard.integer(forKey: "setting")
    
    
  
}





// APP Life cycle
extension DashboardViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("This is random alphabet \(String().generateAlphaNumberic(length: 6))")
        let phoneNumberKit = PhoneNumberKit()
        do {
            
            let phone = try phoneNumberKit.parse("+855010243263")
            print("Country code \(phone.countryCode)")
            print("Country national number \(phone.nationalNumber)")
            print("Country phone type \(phone)")
            print("Country \(phone.leadingZero)")
            let phoneNumberCustom = try phoneNumberKit.parse("+855010243263", withRegion: "FR", ignoreType: true)
            print("PhonenumberKit \(phoneNumberCustom.nationalNumber)")
            print("PhonenumberKit \(phoneNumberCustom.numberString)")
            
        } catch {
            
            print("PhonenumberKit throws exception: ",error)
        }
        
        print("User device code ")
        if let user = Auth.auth().currentUser {
            print("This is user \(user)")
        }
        
        let user = RepositoryClass.shared.getUsersCore()
        print("Amount of all users \(user.count)")
        
        print("Available languages \(language)")
        
        if setting == 0 || setting == 1 {
            UIApplication.shared.registerForRemoteNotifications()
        } else {
            UIApplication.shared.unregisterForRemoteNotifications()
        }
        
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        self.tabBarController?.delegate = self
    
        // set background color of the page
        view.backgroundColor = BaseColor.colorWhite

        
        featureContainer.depthPreset = .depth5

        let btnScheme = MDCButtonScheme()
        let typographyScheme = MDCTypographyScheme()
        let btnArray: Array<MDCButton> = [mapBtn, serviceBtn, documentBtn, videoBtn, aboutUsBtn, membershipBtn]
        MDThemeApplier(btnName: btnArray, typographyScheme: typographyScheme, btnScheme: btnScheme)
        documentBtn.addTarget(self, action: #selector(handleDocumentWebView), for: .touchUpInside)
        videoBtn.addTarget(self, action: #selector(handleMovieWebView), for: .touchUpInside)
        aboutUsBtn.addTarget(self, action: #selector(handleExplore), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        customNavigationBar()
        guard let userId = Auth.auth().currentUser?.uid else {
            return
        }
        
        let userCore = self.repo.getUserCore(userId: userId)
        print("Amount of user is \(userCore.count)")
        for user in userCore {
            
            print("Here is user vkNumber \(String(describing: user.vkclubNumber)) and photoUrl \(user.pfpUrl)")
            
        }
        let allUser = self.repo.getUsersCore()
        print("All user is \(allUser.count)")
        for i in allUser {
            
            print("All user \(i)")
            
        }
        if let user = Auth.auth().currentUser {
            print("User is available \(user)")
            if !LinphoneManager.CheckLinphoneConnectionStatus() {
                if !connectionProgress {
                    LinphoneConfigure.linphoneRegister()

                }

            }
        }
        
    }
    
    @objc
    func handleDocumentWebView() {
        let safariURL = SafariServiceViewController(aRequest: URL(string: "http://vdrive.vkirirom.org/")!, isReaderModeEnabled: true)
        //let safariURL = SafariServiceViewController(aRequest: URL(string: "http://libgen.io/")!, isReaderModeEnabled: true)
        self.present(safariURL.loadUrl(), animated: true, completion: nil)
    }
    @objc
    func handleMovieWebView() {
        let web = StreamVideoViewController(urlString: "http://iptv.vkirirom.org", sharingEnabled: false, bookNowpage: false)
        let navigation = UINavigationController(rootViewController: web)
        self.present(navigation, animated: true)
        
//        let movieController = UINavigationController(rootViewController: MovieViewController())
//        self.navigationController?.present(movieController, animated: true, completion: nil)
    }
    
    @objc
    func handleExplore() {
        let view = ExploreViewController()
        navigationController?.pushViewController(view, animated: true)
    }

    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        print("User should select on tab \(tabBarController.viewControllers)")
        if viewController == tabBarController.viewControllers![2] {
            
            if Auth.auth().currentUser == nil {
                print("User is not logged in yet")
                self.dialogPresentViewController(title: DialogErrorMessage().warningTitle, message: DialogErrorMessage().confirmUserToLogin, actionTitle: DialogTitleMessage().signIn, viewController: LoginPhoneNumberViewController())
                return false
            } else {
                return true
            }
            
        } else {
            return true
        }
        
    }
    func presentDialog() {
        
        let presentDialog = MDCAlertController(title: "Warning", message: "You have not logged in yet, Please login to continue.")
        presentDialog.buttonTitleColor = UIColor(red:0.03, green:0.62, blue:0.09, alpha:1.0)
        presentDialog.buttonInkColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0.5)
        let okayAction = MDCAlertAction(title: "Okay") { (action) in
            let login = UINavigationController(rootViewController: LoginViewController())
            self.present(login, animated: true, completion: nil)
            print("User click okay")
        }
        
        let cancelAction = MDCAlertAction(title: "Cancel", handler: nil)
        presentDialog.addAction(okayAction)
        presentDialog.addAction(cancelAction)
        //self.view.window?.rootViewController?.present(presentDialog, animated: true, completion: nil)
        //self.present(presentDialog, animated: false, completion: nil)
        self.tabBarController?.present(presentDialog, animated: true, completion: nil)
    }
}

// Custom Navigation Bar
extension DashboardViewController {
    /// Custom the upper navigation bar to invisible.
    func customNavigationBar() {
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
    }
    
    
    
    /**
     Objective C function to hundle notification badge in dashboard notification controller
     */
    @objc func handleNotification(){
        
    }
}

// Custom design on Dashboard using Material Design
extension DashboardViewController{
    /// function to apply scheme to Material Design Button. It will take *MDCButton name as parameter.
    func MDThemeApplier(btnName: Array<MDCButton> = [], typographyScheme: MDCTypographyScheme, btnScheme: MDCButtonScheme){
//        let customFont = UIFont(name: "Roboto-Regular", size: 20)!
        for btn in btnName {
            MDCTextButtonThemer.applyScheme(btnScheme, to: btn)
            btn.inkColor = UIColor(red:0.04, green:0.50, blue:0.09, alpha:0.2)
            
            typographyScheme.button = UIFont.systemFont(ofSize: 20)
            MDCButtonTypographyThemer.applyTypographyScheme(typographyScheme, to: btn)
            btn.setTitleColor(UIColor .gray, for: .normal)
            //btn.setTitleFont(UIFont(name: "Roboto-Regular", size: 20), for: UIControlState.normal)
        }
    }
}
extension DashboardViewController {
    
}

