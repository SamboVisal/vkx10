//
//  TabBarViewController.swift
//  vKclub
//
//  Created by Pisal on 11/8/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import UIKit
import Material

class AppBottomNavigationController: BottomNavigationController {
    
    open override func prepare() {
        super.prepare()
        prepareTabBar()
    }
    
    private func prepareTabBar() {
        tabBar.depthPreset = .depth5
        tabBar.barTintColor = .white
        tabBar.unselectedItemTintColor = .red
        tabBar.dividerColor = Color.grey.lighten2
    }
}

class TabBarMaterialComponent : UITabBarController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.layer.shadowColor = UIColor.red.cgColor
        self.tabBarController?.tabBar.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.tabBarController?.tabBar.layer.shadowRadius = 5
        self.tabBarController?.tabBar.layer.shadowOpacity = 1
        self.tabBarController?.tabBar.layer.masksToBounds = false
    }
    
    
}


