//
//  MapViewController.swift
//  vKclub
//
//  Created by Chhayrith on 11/5/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import GoogleMaps
import Jelly
import Firebase
import MaterialComponents


class MapViewController: UIViewController ,GMSMapViewDelegate, CLLocationManagerDelegate{
    
    var mapView: GMSMapView?
    var locationManager = CLLocationManager()
    var isLocationAuthorized: Bool = false
    
    var currentDestination: kiriromLocation?
    let backwardImage = UIImage(named: "backBtn")
    var animator: JellyAnimator?
    
    let markerLocation = [
        
        kiriromLocation(title: "Kirirom Institute of Technology", location: CLLocationCoordinate2DMake(11.3152,104.0676),dec: "The first boarding school in Cambodia, specialized in software engineering and offers internship while studying.", zoom: 14),
        
        kiriromLocation(title: "Activity Center", location: CLLocationCoordinate2DMake(11.3165,104.0648),dec: "Information regarding various types of fun activities to refresh your mind. Open 8:00am - 5pm", zoom: 14),
        
        kiriromLocation(title: "Pine View Kitchen", location: CLLocationCoordinate2DMake(11.3167,104.0653),dec: "An open space restaurant where you can enjoy modern Khmer cuisine", zoom: 14),
        
        kiriromLocation(title: "Reception", location: CLLocationCoordinate2DMake(11.3174,104.0649),dec: "VKirirom Pine Resort reception welcomes customers from 8 AM to 8 PM. Our helpful staffs can speak English, Japanese, and Khmer. Amenities for customers are provided here as well.", zoom: 14),
        
        kiriromLocation(title: "Conference Tent", location: CLLocationCoordinate2DMake(11.3137,104.0667),dec: "A large tent which is suitable for big events,conferences and many activities even during rainy days.", zoom: 14),
        
        kiriromLocation(title: "Moringa", location: CLLocationCoordinate2DMake(11.3154,104.0638),dec: "Khmer style infused restaurant which serves authentic Khmer meals and sells breads and drinks.", zoom: 14),
        
        
        kiriromLocation(title: "Villa Jasmine", location: CLLocationCoordinate2DMake(11.3181,104.0633),dec: "Quite elegant cottage which promises you a lot of pleasant experiences on the cool Kirirom Mountain top for couples and small families.", zoom: 14),
        
        kiriromLocation(title: "Villa Suite", location: CLLocationCoordinate2DMake(11.3180,104.0655),dec: "This two bedroom luxurious modern villa is suitable for big families or groups.", zoom: 14),
        
        kiriromLocation(title: "Pipe Room", location: CLLocationCoordinate2DMake(11.3126,104.0628),dec: "The most uniquely designed room derived from an earthen pipe which serves best amongst all.", zoom: 14),
        
        kiriromLocation(title: "Luxury Tent", location: CLLocationCoordinate2DMake(11.3145,104.0646),dec: "High quality modern tent houses offers the comfort of a hotel room in an outer space that features a king size bed and then nice shower room.", zoom: 14),
        
        kiriromLocation(title: "Khmer Cottage", location: CLLocationCoordinate2DMake(11.3149,104.0644),dec: "Khmer farmer style open houses built with natural materials to experience the Khmer tradition.", zoom: 14),
        
        kiriromLocation(title: "Playground Field", location: CLLocationCoordinate2DMake( 11.3162, 104.0654),dec: "Enjoy many kinds of activities including football, tennis, volleyball, bubble sumo etc. Feel free to ask our activity staff for details.", zoom: 14),
        
        kiriromLocation(title: "Camping Area", location: CLLocationCoordinate2DMake( 11.3134,  104.0648),dec: "Enjoy camping with camp fire in a large open space with high level of security.", zoom: 14),
        
        kiriromLocation(title: "Kirirom Lake", location: CLLocationCoordinate2DMake( 11.3344,104.0516  ),dec: "A beautiful lake,  which provides enough water supply for all the villagers", zoom: 14),
        
        kiriromLocation(title: "Village", location: CLLocationCoordinate2DMake( 11.3348, 104.0550  ),dec: "A Village where people enjoy living on Kirirom Mountain with nice view.", zoom: 14),
        
        kiriromLocation(title: "Ministry of Environment", location: CLLocationCoordinate2DMake( 11.3330,104.0531   ),dec: "Ministry of Environment that supports the whole Kirirom environment", zoom: 14),
        
        kiriromLocation(title: "Bungalow", location: CLLocationCoordinate2DMake( 11.3165, 104.0658  ),dec: "Come and stay in our specially designed Bungalows to experience the invigorating fresh air and peaceful life and the pine forest of Kirirom.", zoom: 14),
        
        kiriromLocation(title: "Swimming Pool", location: CLLocationCoordinate2DMake( 11.3168, 104.0658 ),dec: "Enjoy our outdoor swimming pool with family and friends.", zoom: 14),
        
        kiriromLocation(title: "Tennis Court", location: CLLocationCoordinate2DMake( 11.3121, 104.0653 ),dec: "Enjoy tennis on top of the mountain surrounded by pine trees.", zoom: 14),
        
        kiriromLocation(title: "Parking Area", location: CLLocationCoordinate2DMake( 11.3169, 104.0647),dec: "Large secure Parking space for customers.", zoom: 14),
        
        kiriromLocation(title: "Container Cafe", location: CLLocationCoordinate2DMake( 11.3139, 104.0654),dec: "Big secure parking space for customers vehicle.", zoom: 14),
        
        kiriromLocation(title: "Farm", location: CLLocationCoordinate2DMake( 11.3134,104.0636),dec: "An organic farm that grows a variety of foods such as Strawberry, Pineapple, Passions etc.", zoom: 14),
        
        kiriromLocation(title: "Crazy Hill", location: CLLocationCoordinate2DMake( 11.3136,104.0751),dec: "An outdoor stage for outdoor events and parties.", zoom: 14),
        
        kiriromLocation(title: "Dragon Roundabout", location: CLLocationCoordinate2DMake(  11.3409,104.0597),dec: "A landmark statue of snake god with four heads which is situated in the middle of the intersection.", zoom: 14),
        
        kiriromLocation(title: "Old Kirirom Pagoda", location: CLLocationCoordinate2DMake(  11.3201,104.0362),dec: "It is a Buddhist temple with the longest history in Kirirom. It reminds of the old good days in Cambodia. Please follow the appropriate pattern of worship there.", zoom: 14),
        
        kiriromLocation(title: "New Kirirom Pagoda", location: CLLocationCoordinate2DMake( 11.3304,104.0769),dec: "A mural painting of buddha's life inside of a building which is located.", zoom: 14),
        
        kiriromLocation(title: "Otrosek Waterfall", location: CLLocationCoordinate2DMake( 11.3111,104.0784),dec: "Otrosek Waterfall is a lovely place that is allowed by the locals and is highly recommended during rainy season.", zoom: 14),
        
        kiriromLocation(title: "Srash Srang Lake", location: CLLocationCoordinate2DMake( 11.3291,104.0379),dec: "A picture perfect landscape where you feel the magnificence of Nature. ", zoom: 14),
        
        kiriromLocation(title: "King's Residence", location: CLLocationCoordinate2DMake( 11.3309,104.0606),dec: "The King's residence is an Old- fashioned cottage built of bricks which stands quietly in Pine Grove.", zoom: 14),
        
        kiriromLocation(title: "Visitor Center", location: CLLocationCoordinate2DMake( 11.3351, 104.0407),dec: "A visitor center, which introduces the history of Kirirom. It is a wonderful photogenic spot.", zoom: 14),
        
        kiriromLocation(title: "Football Court", location: CLLocationCoordinate2DMake( 11.3133, 104.0657),dec: "Customers can enjoy football with their friends in the resort.", zoom: 14),
        
        kiriromLocation(title: "Volleyball Court", location: CLLocationCoordinate2DMake(11.3129, 104.0657),dec: "Customers can also enjoy volleyball with their friends in the resort.", zoom: 14),
        
        kiriromLocation(title: "Kirirom Elementary School", location: CLLocationCoordinate2DMake(11.3345,  104.0553),dec: "Established by the vKirirom stuff which is the only elementary school in Kirirom.", zoom: 14),
        
        kiriromLocation(title: "Bell tent", location: CLLocationCoordinate2DMake( 11.3126,104.0646 ),dec: "Glamping, glamourous camping provides you a good sleep on a fluffy bed.", zoom: 14),
        
        kiriromLocation(title: "Climbing Theater", location: CLLocationCoordinate2DMake( 11.3158,104.0649 ),dec: "A multipurpose building which is used for wall climbing during the day and as a movie screen night time at night.", zoom: 14),
        
        kiriromLocation(title: "First Waterfall", location: CLLocationCoordinate2DMake( 11.3438,104.0348 ),dec: "The most beautiful waterfall in Kirirom mountain which attracts lots of tourists who comes to visit.", zoom: 14),
        
        ]
    
    // let kiriromMapData = RepositoryClass.shared.readAllMapMarkerData()
    let kiriromMapData = mapDataCache.object(forKey: "mapData" as AnyObject) as! [MapDataModelHelper]
}

extension MapViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.CustomNavigationBar(title: "Map", isShadow: true, isPresentVC: false)
        view.backgroundColor = .white
        // AIzaSyBZhtZuXOn_4Sk4AJELB7jjZl20jUMpS_8
        self.CheckIfEnableLocation()
        
        self.Map()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }

    func CheckIfEnableLocation () {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus()
            {
            case .notDetermined, .restricted, .denied:
                print("No access")
                openSetting()
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                self.isLocationAuthorized = true
            }
        } else {
            print("Location services are not enabled")
        }
    }


    func openSetting() {
        let alert = UIAlertController(title: "Location Disabled", message: "Location access is restricted. In order to use tracking, please enable Location Service in the Settigs app.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Go to Settings now", style: UIAlertActionStyle.default, handler: { (alert: UIAlertAction!) in
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: { (done) in
                if done {
                    print("Done")
                    self.isLocationAuthorized = true
                }
            })
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            self.navigationController?.popToRootViewController(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }

    func Map () {
        GMSServices.provideAPIKey("AIzaSyBZhtZuXOn_4Sk4AJELB7jjZl20jUMpS_8")
        // Zoom into user's current location.
        let camera: GMSCameraPosition
        if self.isLocationAuthorized {
            camera = GMSCameraPosition.camera(withTarget: (locationManager.location?.coordinate)!, zoom: 17)
        }else {
            camera = GMSCameraPosition.camera(withLatitude: 11.3167, longitude: 104.0651, zoom : 15)
        }
        mapView = GMSMapView.map(withFrame: .zero, camera: camera)
        
        // mapView?.setMinZoom(10, maxZoom: 17)
        mapView?.setMinZoom(15, maxZoom: 20)

        mapView?.settings.compassButton = true;
        mapView?.settings.myLocationButton = true;
        mapView?.isMyLocationEnabled = true
        mapView?.delegate = self
        view = mapView
        

        //Map overlay
        let southWest = CLLocationCoordinate2D(latitude: 11.3432, longitude:104.0323 )
        let northEast = CLLocationCoordinate2D(latitude: 11.3040, longitude: 104.0846)
        let overlayBounds = GMSCoordinateBounds(coordinate: southWest, coordinate: northEast)
        let icon  = UIImage(named: "vkirirom_map")!
        let overlay = GMSGroundOverlay(bounds: overlayBounds, icon: icon)
        overlay.bearing = 0
        overlay.map = mapView

        // Marker for each location

        if currentDestination == nil {
            currentDestination = markerLocation.first
        }
        
        if !kiriromMapData.isEmpty {
            for data in kiriromMapData {
                let location = CLLocationCoordinate2DMake(data.latitude, data.longitude)
                let marker = GMSMarker(position: location)
                marker.title = data.title
                marker.icon = UIImage(named: data.title)
                marker.map = mapView
            }
        }else{
            for currentDestination in markerLocation{
                let marker = GMSMarker(position: currentDestination.location)
                marker.title = currentDestination.title
                marker.icon = UIImage(named: currentDestination.title)
                marker.map = mapView
            }
        }
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        mapView?.clear()

        mapView?.removeFromSuperview()
        mapView = nil
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)

    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let mapDetailVC = MapDetailBottomSheet()
        var markerList = [MapDataModelHelper]()
        markerList = kiriromMapData.filter { (oneLocation) -> Bool in
            return oneLocation.title == marker.title!
        }
        guard let value = markerList.first else {
            return false
        }
        mapDetailVC.mapDetailModel = MapDetailModel(title: value.title, snippet: value.snippet, images: value.images)
        
        let presentation = JellySlideInPresentation(
            dismissCurve: .linear,
            presentationCurve: .linear,
            cornerRadius: 15,
            backgroundStyle: .dimmed(alpha: 0.1),
            jellyness: .jellier,
            duration: .normal,
            directionShow: .bottom,
            directionDismiss: .bottom,
            widthForViewController: .fullscreen,
            heightForViewController: .custom(value: 260),
            horizontalAlignment: .center,
            verticalAlignment: .bottom,
            marginGuards: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0),
            corners: [.topLeft,.topRight])
        self.animator = JellyAnimator(presentation: presentation)
        self.animator?.prepare(viewController: mapDetailVC)
        
        self.present(mapDetailVC, animated: true, completion: nil)
        return true
    }
}

class kiriromLocation: NSObject {

    let title: String
    let location: CLLocationCoordinate2D
    let dec  : String

    let zoom: Float

    init(title: String, location: CLLocationCoordinate2D, dec: String,zoom: Float) {
        self.title = title
        self.location = location
        self.dec = dec
        self.zoom = zoom
    }

}

