//
//  Repository + Map.swift
//  vKclub
//
//  Created by Chhayrith on 12/16/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import CoreData

extension RepositoryClass {
    func writeAllMapMarkersCoreData(mapDataModel: MapDataModelHelper,
                                          completion: @escaping(_ success: Bool) -> Void)
    {
        // Mark: 1
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        // Mark: 2
        guard let entityMarker = NSEntityDescription.entity(forEntityName: "MapMarkers", in: managedContext) else {
            return
        }
        let mapMarker = NSManagedObject(entity: entityMarker, insertInto: managedContext)
        
        // Mark: 4 Check if already exist
        let filteredMapData = readAllMapMarkerData().filter { (data) -> Bool in
            return mapDataModel.id == data.id
        }
        print("==> Filtered Map Data \(filteredMapData)")
        if filteredMapData.isEmpty {
            // Mark: 5 Assign value
            mapMarker.setValue(mapDataModel.id, forKey: "id")
            mapMarker.setValue(mapDataModel.icon, forKey: "iconUrl")
            mapMarker.setValue(mapDataModel.latitude, forKey: "latitude")
            mapMarker.setValue(mapDataModel.longitude, forKey: "longitude")
            mapMarker.setValue(mapDataModel.snippet, forKey: "snippet")
            mapMarker.setValue(mapDataModel.title, forKey: "title")
            mapMarker.setValue(mapDataModel.images, forKey: "images")
            mapMarker.setValue(mapDataModel.zoom, forKey: "zoom")
            
            // Write map images to NSCache
            // mapImagesCache.setObject(mapDataModel.images as AnyObject, forKey: mapDataModel.id as AnyObject)
        }
        // Mark: 6
        do{
            try managedContext.save()
            completion(true)
        }catch let error as NSError{
            print("==> Map data couldn't added to core data. Error code: \(error.code)")
            completion(false)
        }
    }
    
    func readAllMapMarkerData() -> [MapDataModelHelper] {
        // Mark: 1
        let fetchMarkersRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MapMarkers")
        var mapModel = [MapDataModelHelper]()
        // Mark: 2
        do {
            let markers = try managedObjectContext.fetch(fetchMarkersRequest)
            print("==> markers as! MapMarkers: \(markers as! [MapMarkers])")
            let allMarkers = markers as! [MapMarkers]
            for marker in allMarkers {
                mapModel.append(MapDataModelHelper(title: marker.title == nil ? "" : marker.title!,
                                                   snippet: marker.snippet == nil ? "" : marker.snippet!,
                                                   latitude: marker.latitude,
                                                   longitude: marker.longitude,
                                                   icon: marker.iconUrl == nil ? "" : marker.iconUrl!,
                                                   images: marker.images == nil ? [] : marker.images!))
            }
            return mapModel
        }catch let error as NSError {
            print("==> Can't read map data from core data. With error code: \(error.code)")
            return []
        }
    }
    
    func deleteAllMapDataFromCore(){
        // Mark: 1
        let fetchMarkersRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MapMarkers")
        
        fetchMarkersRequest.returnsObjectsAsFaults = false
        
        do {
            let results = try managedObjectContext.fetch(mdPhoneBookModelLocal)
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                managedObjectContext.delete(managedObjectData)
                try managedObjectContext.save()
                print("==>DELETE Map data from CORE DATA TABLE")
            }
        } catch let error as NSError {
            print("Detele all data in error : \(error)")
        }
    }
}
