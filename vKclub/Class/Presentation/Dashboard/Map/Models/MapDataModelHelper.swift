//
//  MapDataModelHelper.swift
//  vKclub
//
//  Created by Chhayrith on 12/16/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

// Mark: - Model
class MapDataModelHelper:Codable, Identifiable {
    var id: String? = nil
    public let title: String
    public let snippet: String
    public let latitude: Double
    public let longitude: Double
    public let icon: String
    public var images = [String]()
    public let zoom: Int
    
    public init(title: String = "", snippet: String = "", latitude: Double, longitude: Double, icon: String = "", images: [String] = []){
        self.title = title
        self.snippet = snippet
        self.icon = icon
        self.images = images
        self.latitude = latitude
        self.longitude = longitude
        self.zoom = 14
    }
}
