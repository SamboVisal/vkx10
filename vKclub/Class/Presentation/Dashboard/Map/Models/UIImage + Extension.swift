//
//  UIImage + Extension.swift
//  vKclub
//
//  Created by Chhayrith on 12/17/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit

extension UIImage {
    
    func loadImageUsingWithUrlString( urlString: String = "", defaultImage: UIImage, cacheName: NSCache<AnyObject, AnyObject>) -> UIImage {
        var returnImage: UIImage = defaultImage
        print("==> URL String \(urlString)")
        
        //check cache for image first
        if let cachedImage = cacheName.object(forKey: urlString as AnyObject) as? UIImage {
            print("==> image load from Cache \(contactProfileImgCache)")
            returnImage = cachedImage
            return cachedImage
        }
        
        //otherwise fire off a new download
        let url = URL(string: urlString)
        
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            
            //download hit an error so lets return out
            if error != nil {
                print(error ?? "")
                return
            }
            
            DispatchQueue.main.async(execute: {
                if let downloadedImage = UIImage(data: data!) {
                    cacheName.setObject(downloadedImage, forKey: urlString as AnyObject)
                    print("==> image load from Server")
                    returnImage = downloadedImage
                }
            })
            
        }).resume()
        
        return returnImage
    }
}
