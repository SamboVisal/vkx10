//
//  MapDataModel.swift
//  vKclub
//
//  Created by Chhayrith on 12/16/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import Firebase


// Mark: - Model
class MapDataModel: GeoPoint, Identifiable {
    var id: String? = nil
    public let title: String
    public let snippet: String
    public let latLng: GeoPoint
    public let icon: String
    public var images = [String]()
    
    public init(title: String = "", snippet: String = "", latLng: GeoPoint, icon: String = "", images: [String] = []){
        self.title = title
        self.snippet = snippet
        self.latLng = latLng
        self.icon = icon
        self.images = images
        super.init(latitude: latLng.longitude, longitude: latLng.longitude)
    }
}
