//
//  MapDetailViewModel.swift
//  vKclub
//
//  Created by Chhayrith on 12/14/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import Firebase
import ImageSlideshow

// Make: - View Model
class MapDetailViewModel {
    private let mapDetailModel:MapDetailModel
    
    public init(mapDetailModel: MapDetailModel){
        self.mapDetailModel = mapDetailModel
    }
    
    public func detailPictures() -> [InputSource]{
        // return self.mapDetailModel.detailPictures
        var inputSource: [InputSource] = []
        if !self.mapDetailModel.images.isEmpty {
            print("==> map image is empty: \(self.mapDetailModel.images)")
            for imageUrl in self.mapDetailModel.images {
                let image: UIImage = R.image.placeholder()!
                inputSource.append(ImageSource(image: image.loadImageUsingWithUrlString(urlString: imageUrl,
                                                                                        defaultImage: R.image.placeholder()!,
                                                                                        cacheName: mapImageCache)))
            }
        }
        print("==> map image is empty: \(inputSource)")
        return inputSource
    }
    
    public var locationTitle: String {
        return self.mapDetailModel.title
    }
    
    public var locationDetail: String {
        return self.mapDetailModel.snippet
    }
    
    public func configure(view: MapDetailBottomSheet){
        view.mapTitle.text = locationTitle
        view.mapDescription.text = locationDetail
        view.slideshow.setImageInputs(detailPictures())
    }
}
