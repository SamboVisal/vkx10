//
//  MapDetailModel.swift
//  vKclub
//
//  Created by Chhayrith on 12/14/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation

// Mark: - Model
class MapDetailModel {
    public let title: String
    public let snippet: String
    public var images = [String]()
    
    public init(title: String = "", snippet: String = "", images: [String] = []){
        self.title = title
        self.snippet = snippet
        self.images = images
    }
}
