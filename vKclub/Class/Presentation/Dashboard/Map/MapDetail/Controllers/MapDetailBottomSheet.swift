//
//  MapDetailBottomSheet.swift
//  vKclub
//
//  Created by Chhayrith on 12/14/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import ImageSlideshow

class MapDetailBottomSheet: UIViewController {
//    public var mapDetail: kiriromLocation? = nil
    public var mapDetailModel: MapDetailModel? = nil
    public let slideshow: ImageSlideshow = ImageSlideshow()
    // Photo List
    let photoContainerView: UIView = {
        let vw = UIView()
        vw.layer.shadowColor = UIColor.lightGray.cgColor
        vw.layer.shadowRadius = 3.0
        vw.layer.shadowOpacity = 3.0
        vw.layer.shadowOffset = CGSize(width: 1, height: 1)
        vw.layer.masksToBounds = false
        vw.translatesAutoresizingMaskIntoConstraints = false
        return vw
    }()
    
    let descriptionContainerView: UIView = {
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        return vw
    }()
    
    let mapTitle: UILabel = {
       let label = UILabel()
        label.font = R.font.robotoBold(size: 20)
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let mapDescription: UILabel = {
        let label = UILabel()
        label.numberOfLines = 5
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let viewModel = MapDetailViewModel(mapDetailModel: mapDetailModel!)
        
        // Mark: - Setup slide show
        slideshow.slideshowInterval = 5.0
        slideshow.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
        slideshow.contentScaleMode = UIViewContentMode.scaleAspectFill
        slideshow.pageIndicator = nil
        slideshow.circular = false
        slideshow.activityIndicator = DefaultActivityIndicator()
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
        slideshow.addGestureRecognizer(recognizer)
        
        setupView()
        viewModel.configure(view: self)
    }
    
    @objc func didTap() {
        let fullScreenController = slideshow.presentFullScreenController(from: self)
        // set the activity indicator for full screen controller (skipping the line will show no activity indicator)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
    
    func setupView() {
        view.backgroundColor = BaseColor.colorWhite
        view.addSubview(photoContainerView)
        view.addSubview(descriptionContainerView)
        descriptionContainerView.addSubview(mapTitle)
        descriptionContainerView.addSubview(mapDescription)
        photoContainerView.addSubview(slideshow)
        
        photoContainerView.snp.makeConstraints { (make) in
            make.leading.equalTo(self.view)
            make.top.equalTo(self.view)//.inset(8)
            make.width.equalTo(self.view)
            make.height.equalTo(140)
        }
        
        descriptionContainerView.snp.makeConstraints { (make) in
            make.leading.equalTo(self.view)
            make.top.equalTo(self.photoContainerView.snp.bottom)
            make.width.equalTo(self.view)
            make.height.equalTo(125)
        }
        
        mapTitle.snp.makeConstraints { (make) in
            make.leading.equalTo(descriptionContainerView).offset(16)
            make.top.equalTo(descriptionContainerView.snp.top).inset(8)
            make.width.equalTo(self.view)
            // make.height.equalTo(20)
        }
        
        mapDescription.snp.makeConstraints { (make) in
            make.leading.equalTo(descriptionContainerView).offset(8)
            make.top.equalTo(self.mapTitle.snp.bottom)
            make.width.equalTo(self.view).inset(8)
            // make.height.equalTo(140)
        }
        
        slideshow.snp.makeConstraints { (make) in
            make.leading.equalTo(photoContainerView)
            make.centerY.equalTo(photoContainerView)
            make.width.equalTo(photoContainerView)
            make.height.equalTo(140)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func handleDismiss() {
        self.dismiss(animated: true, completion:nil)
    }

}
