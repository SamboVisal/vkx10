//
//  MenuViewController.swift
//  vKclub
//
//  Created by Pisal on 11/6/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import UIKit
import Firebase
import MaterialComponents
import Material
import SDWebImage
import PopupDialog
import CoreData
import MessageUI

class MenuViewController: UIViewController {
    
    private let cardViewInstance = ExploreCategoryComponents()
    private let detailProfileVariables = DetailProfileVariables()
    private let buttonComponents = ExploreVariables()
    private let tableView = UITableView()
    private let repoRef = RepositoryClass()
    private var featureData: DetailProfileInitData?
    private let cellId = "cellId"
    private let detailHeader = "headerId"
    
    var userCoreData = [UserProfileCore]()

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// App life cycle
extension MenuViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        detailProfileVariables.imageUserCircleView.addSubview(cardViewInstance.activityIndicator)
        view.addSubview(cardViewInstance.mainCardView)
        
        customNavigationItem()
        self.loadDataFromCore()
        self.setupDetailProfile()
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        setNeedsStatusBarAppearanceUpdate()
        registerLinphoneService()
        extractUserDetailFromCoreData()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        confirmLoginAgainIfResetPassword()
        
    }
}

// Setup Menu View programmatically
extension MenuViewController {
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // Setup user profile
        cardViewInstance.activityIndicator.snp.makeConstraints { (constraint) in
            constraint.center.equalTo(detailProfileVariables.imageUserCircleView)
            
        }
        
        cardViewInstance.mainCardView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view).offset(16)
            make.leading.equalTo(view).offset(16)
            make.trailing.equalTo(view).offset(-16)
            make.height.equalTo(self.view).dividedBy(4.5)
            make.centerX.equalTo(view)
        }
        
        detailProfileVariables.imageUserCircleView.snp.makeConstraints { (make) in
            make.centerY.equalTo(cardViewInstance.mainCardView)
            make.leading.equalTo(cardViewInstance.mainCardView).offset(16)
            make.height.equalTo(cardViewInstance.mainCardView).dividedBy(2.4)
            make.width.equalTo(cardViewInstance.mainCardView.snp.height).dividedBy(2.4)
        }
        
        detailProfileVariables.nameUserDetailProfileView.snp.makeConstraints { (make) in
            make.top.equalTo(detailProfileVariables.imageUserCircleView)
            make.leading.equalTo(detailProfileVariables.imageUserCircleView.snp.trailing).offset(16)
            make.trailing.equalTo(cardViewInstance.mainCardView.snp.trailing).offset(-16)
        }
        detailProfileVariables.emailUserDetailProfileView.snp.makeConstraints { (make) in
            make.top.equalTo(detailProfileVariables.nameUserDetailProfileView.snp.bottom).offset(8)
            make.leading.trailing.equalTo(detailProfileVariables.nameUserDetailProfileView)
        }
        buttonComponents.logoutButton.setBackgroundColor(BaseColor.colorPrimary, for: .normal)
        buttonComponents.logoutButton.setTitleColor(BaseColor.colorWhite, for: .normal)
        buttonComponents.logoutButton.snp.makeConstraints { (make) in
            make.top.equalTo(detailProfileVariables.nameUserDetailProfileView.snp.bottom).offset(8)
            make.leading.equalTo(detailProfileVariables.nameUserDetailProfileView).offset(16)
            make.width.equalTo(cardViewInstance.mainCardView).dividedBy(3.7)
            make.height.equalTo(48)
        }
        buttonComponents.detailProfile.snp.makeConstraints { (make) in
            
            make.centerY.equalTo(cardViewInstance.mainCardView)
            make.top.equalTo(detailProfileVariables.imageUserCircleView).offset(-8)
            make.bottom.equalTo(detailProfileVariables.imageUserCircleView).offset(8)
            make.leading.trailing.equalTo(cardViewInstance.mainCardView)
        }
        
        buttonComponents.detailProfile.addTarget(self, action: #selector(handleEditProfile), for: .touchUpInside)
        // end of setup profile
        
        // Setup setting feature
        cardViewInstance.cardView.snp.makeConstraints { (make) in
            make.top.equalTo(cardViewInstance.mainCardView.snp.bottom).offset(16)
            make.width.equalTo(self.view).offset(-32)
            make.centerX.equalTo(self.view)
            make.height.equalTo(self.view).dividedBy(3.2)
        }
        
        tableView.snp.makeConstraints { (make) in
            make.top.left.equalTo(cardViewInstance.cardView).offset(2)
            make.bottom.right.equalTo(cardViewInstance.cardView).offset(-2)
            
        }
        
        
        
        
        buttonComponents.sosButton.snp.makeConstraints { (make) in
            make.leading.equalTo(self.view).offset(16)
            make.trailing.equalTo(self.view).offset(-16)
            make.top.equalTo(cardViewInstance.cardView.snp.bottom).offset(16)
            make.height.equalTo(48)
        }
        print("View didlayout subview")
    }
}

// Function helper
extension MenuViewController : MFMessageComposeViewControllerDelegate{
    
    //send SMS
    func SMS() {
        
        let currentLocaltion_lat = String(12)
        let currentLocation_long = String(312)
        print(currentLocaltion_lat)
        
        if (MFMessageComposeViewController.canSendText()) {
            let phone = "+18557414949"
            let message = "Please help! I'm currently facing an emergency problem. Here is my Location: http://maps.google.com/?q="+currentLocaltion_lat+","+currentLocation_long+""
            
            let controller = MFMessageComposeViewController()
            controller.body = message
            controller.recipients = [phone]
            controller.messageComposeDelegate = self as MFMessageComposeViewControllerDelegate
            self.present(controller, animated: true, completion: nil)
        } else {
            self.alertError(message: "Sorry! your device is not support with this feature.")
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result.rawValue) {
        case MessageComposeResult.cancelled.rawValue:
            controller.dismiss(animated: false, completion: nil)
        case MessageComposeResult.failed.rawValue:
            controller.dismiss(animated: false, completion: nil)
        case MessageComposeResult.sent.rawValue:
            controller.dismiss(animated: false, completion: nil)
        default:
            break;
        }
    }
    
    @objc
    func handleEditProfile() {
        
        print("User click edit profile")
        let editController = AppNavigationController(rootViewController: EditProfileViewController())
        self.present(editController, animated: true, completion: nil)
        
    }
    
    @objc
    func handleLoginAndLogout() {
        
        let loginController = UINavigationController(rootViewController: LoginPhoneNumberViewController())
        self.present(loginController, animated: true, completion: nil)
        
    }
    
    fileprivate func registerLinphoneService() {
        
        if let user = Auth.auth().currentUser {
            
            print("User is available \(user)")
            if !LinphoneManager.CheckLinphoneConnectionStatus() {
                if !connectionProgress {
                    LinphoneConfigure.linphoneRegister()
                }
                
            }
        }
    }
    func setupDetailProfile() {
        
        cardViewInstance.mainCardView.addSubview(detailProfileVariables.imageUserCircleView)
        cardViewInstance.mainCardView.addSubview(detailProfileVariables.nameUserDetailProfileView)
        cardViewInstance.mainCardView.addSubview(detailProfileVariables.emailUserDetailProfileView)
        cardViewInstance.mainCardView.addSubview(buttonComponents.logoutButton)
        cardViewInstance.mainCardView.addSubview(buttonComponents.detailProfile)
        
        detailProfileVariables.nameUserDetailProfileView.translatesAutoresizingMaskIntoConstraints = false
        detailProfileVariables.emailUserDetailProfileView.translatesAutoresizingMaskIntoConstraints = false
        
        
        view.addSubview(cardViewInstance.cardView)
        cardViewInstance.cardView.translatesAutoresizingMaskIntoConstraints = false
        
        
        cardViewInstance.cardView.addSubview(tableView)
        view.addSubview(buttonComponents.sosButton)
        buttonComponents.sosButton.addTarget(self, action: #selector(confirmSosClicked), for: .touchUpInside)
        tableViewDelegateDatasource()
    }
    
    @objc
    func confirmSosClicked() {
        let alert = dialogMessage(title: "Emergency SOS", message: "We will generate a SMS along with your current location to our supports. We suggest you not to move far away from your current position, as we're trying our best to get there as soon as possible. \n (Standard SMS rates may apply)")
        let okayButton = MDCAlertAction(title: DialogTitleMessage().okayTitle) { (action) in
            self.SMS()
        }
        let cancelButton = MDCAlertAction(title: DialogTitleMessage().cancelTitle, handler: nil)
        alert.addAction(okayButton)
        alert.addAction(cancelButton)
        dialogPresent(view: alert)
    }
    
    fileprivate func loadDataFromCore() {
        // Load data of user from core data
        if let userID = Auth.auth().currentUser?.uid {
            
            userCoreData = repoRef.getUserCore(userId: userID)
        }
        featureData = DetailProfileInitData.getData()
    }
    
    fileprivate func customNavigationItem() {
        navigationItem.titleLabel.text = "Menu"
        navigationItem.titleLabel.textColor = .white
    }
    
    /// Load all relevent data from user in core data
    fileprivate func extractUserDetailFromCoreData() {
        
        if let userId = Auth.auth().currentUser?.uid {
            
            let userCoreDatas = repoRef.getUserCore(userId: userId)
            
            print("User is available \(userId)")
            
            buttonComponents.logoutButton.isHidden = true
            for i in userCoreDatas {
                
                detailProfileVariables.emailUserDetailProfileView.isHidden = false
                detailProfileVariables.nameUserDetailProfileView.text = i.name
                detailProfileVariables.emailUserDetailProfileView.text = i.phoneNumber
                buttonComponents.detailProfile.isHidden = false
                print("==== This is user photo URL \(i.pfpUrl ?? "Profile image not found")")
                
                if !IMAGELOAD {
                    
                    self.cardViewInstance.activityIndicator.startAnimating()
                    let placeholderImage = UIImage(named: "detailprofile-icon")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                    detailProfileVariables.imageUserCircleView.sd_setImage(with: URL(string: i.pfpUrl!), placeholderImage: placeholderImage, options: [.progressiveDownload, .continueInBackground, .refreshCached]) { (image, error, cache, url) in
                        
                        if error == nil {
                            
                            self.imageLoadSuccess()
                            print("image loaded success")
                            
                        } else {
                            self.imageLoadWithError(error: (error?.localizedDescription)!)
                            print("Unable to load image")
                            
                        }
                        
                    }
                    
                    
                } else {
                    
                    self.detailProfileVariables.imageUserCircleView.sd_setImage(with: URL(string: i.pfpUrl!), placeholderImage: nil, options: [ .fromCacheOnly]) { (image, error, cache, url) in
                        
                        if error == nil {
                            self.imageLoadSuccess()
                            print("Image loaded success")
                        } else {
                            
                            IMAGELOAD = false
                            self.cardViewInstance.activityIndicator.stopAnimating()
                        }
                        
                    }
                }
                
            }
            
            
        } else {
            
            print("User is not logged ")
            self.userIsNotLoggedIn()
            
        }
    }
    
    func imageLoadSuccess() {
        
        self.cardViewInstance.activityIndicator.stopAnimating()
        print("success loaded image from server")
        
        IMAGELOAD = true
    }
    func imageLoadWithError(error: String) {
        
        IMAGELOAD = false
        self.cardViewInstance.activityIndicator.stopAnimating()
    }
    
    fileprivate func userIsNotLoggedIn() {
        
        detailProfileVariables.imageUserCircleView.image = UIImage(named: "detailprofile-icon")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        detailProfileVariables.nameUserDetailProfileView.numberOfLines = 2
        detailProfileVariables.nameUserDetailProfileView.text = "Please Sign In to make call and additional features."
        detailProfileVariables.nameUserDetailProfileView.font = UIFont.systemFont(ofSize: 15)
        detailProfileVariables.emailUserDetailProfileView.isHidden = true
        buttonComponents.logoutButton.isHidden = false
        buttonComponents.logoutButton.setTitle("Sign In", for: .normal)
        buttonComponents.detailProfile.isHidden = true
        buttonComponents.logoutButton.addTarget(self, action: #selector(handleLoginAndLogout), for: .touchUpInside)
        
    }
    
    
    
    fileprivate func tableViewDelegateDatasource() {
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(FeaturesContentCell.self, forCellReuseIdentifier: cellId)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.isScrollEnabled = false
        //tableView.backgroundColor = UIColor(white: 0, alpha: 0.1)
        tableView.showsVerticalScrollIndicator = false
        tableView.tableFooterView = UIView(frame: .zero)
    }
    
    func confirmLoginAgainIfResetPassword() {
        
        if USER_RESET_PASS {
            
            self.showSnackbarMessage(title: "You have successfully updated password.")
            USER_RESET_PASS = false
            //            let dialog = dialogMessage(title: "Login again", message: "You have successfully updated password.")
            //
            //            CustomPopupDialogView()
            //            let alertUserLogout = PopupDialog(title: "Warning", message: "You have successfully updated password. Do you want to login again now or later?", image: nil, buttonAlignment: .horizontal, transitionStyle: PopupDialogTransitionStyle.bounceUp, preferredWidth: view.frame.width / 1.3, tapGestureDismissal: false, hideStatusBar: false, completion: {
            //                USER_RESET_PASS = false
            //
            //            })
            //            let loginBtn = DefaultButton(title: "Login", action: {
            //                let loginViewController = LoginViewController()
            //                let navigationController = View.setupNavigationViewController(viewController: loginViewController)
            //                self.present(navigationController, animated: true, completion: nil)
            //            })
            //            let laterBtn = CancelButton(title: "Later", action: {
            //
            //            })
            //            alertUserLogout.addButtons([loginBtn, laterBtn])
            //            self.present(alertUserLogout, animated: true, completion: {
            //            })
            
        } else {
            print("USER NOT RESET PASSWORD")
        }
    }
    
    
}

/// Tableview Life cycle
extension MenuViewController: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = featureData?.data?.count {
            return count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! FeaturesContentCell
        cell.initData = featureData?.data![indexPath.item]
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return cardViewInstance.cardView.frame.height / CGFloat((featureData?.data?.count)!)
        
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
        case 0:
            print("User selected setting")

            
            let settingViewController = SettingViewTable()
            self.navigationController?.pushViewController(settingViewController, animated: true)
            
        case 1:
            print("User selected contact us")
            let viewController = ContactUsContentController()
            
            let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: viewController)
            
            self.present(bottomSheet, animated: true, completion: nil)
        case 2:
            print("User selected feedback")
            
            if Auth.auth().currentUser != nil {
                let feedbackController = UINavigationController(rootViewController: FeedbackViewController())
                self.navigationController?.present(feedbackController, animated: true, completion: nil)
            } else {
                
                self.presentDialog(title: "Warning", message: "Please Login in order to use this feature.")
            }
         

        default:
            
            if Auth.auth().currentUser != nil {
//
//                let notification = UINavigationController(rootViewController: NotificationViewController())
//
//                self.navigationController?.present(notification, animated: true, completion: nil)
                self.navigationController?.pushViewController(NotificationViewController(), animated: true)
                print("User is logged in")
            } else {
                
                self.presentDialog(title: "Warning", message: "Please Login in order to use this feature.")
            }
            
            
            
            print("User selected notification")
            
        }
        
    }
    
    
    
}

extension MenuViewController {
    
    
    func presentDialog(title: String, message: String) {
        
        dialogPresentViewController(title: DialogErrorMessage().warningTitle, message: DialogErrorMessage().confirmUserToLogin, actionTitle: DialogTitleMessage().signIn, viewController: LoginPhoneNumberViewController())
        
    }
    
}

class DetailProfileContent: NSObject {
    
    var contentName: String?
    var imageName: String?
    
}
class DetailProfileInitData: NSObject {
    
    var data: [DetailProfileContent]?
    
    
    static func fetchData() -> [DetailProfileInitData] {
        
        let data: DetailProfileInitData = getData()
        
        return [data]
        
    }
    
    static func getData() -> DetailProfileInitData{
        
        let featureData = DetailProfileInitData()
        
        var featureDataAppend = [DetailProfileContent]()
        let setting = DetailProfileInitData.initData(contentName: "Setting", imageName: "setting")
        featureDataAppend.append(setting)
        
        
        
        let contactUs = DetailProfileInitData.initData(contentName: "Contact Us", imageName: "contact-us")
        featureDataAppend.append(contactUs)
        
        let feedback = DetailProfileInitData.initData(contentName: "Feedback", imageName: "feedback")
        featureDataAppend.append(feedback)
        
        let notification = DetailProfileInitData.initData(contentName: "Notification", imageName: "notification")
        featureDataAppend.append(notification)
        
        
        
        featureData.data = featureDataAppend
        
        return featureData
    }
    
    static func initData(contentName: String, imageName: String) -> DetailProfileContent{
        
        let data = DetailProfileContent()
        data.contentName = contentName
        data.imageName = imageName
        return data
    }
    
}

class FeaturesContentCell: UITableViewCell {
    
    
    var initData: DetailProfileContent? {
        
        didSet{
            
            if let imageName = initData?.imageName {
                
                featureImage.image = UIImage(named: imageName)
                content.text = initData?.contentName
                featureImage.image = featureImage.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                featureImage.tintColor = UIColor.gray
                
            }
        }
        
    }
    
    let featureImage: UIImageView = FeaturesContentCell.getUIImage(forName: "detailprofile-icon")
    let moreinfoImage: UIImageView = FeaturesContentCell.getUIImage(forName: "moreinfo")
    let content: UILabel = FeaturesContentCell.getLabel(forName: "unknown")
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.setupViews()
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        featureImage.snp.makeConstraints { (constraint) in
            constraint.leading.equalTo(self.snp.leading).offset(14)
            constraint.centerY.equalTo(self)
            constraint.width.equalTo(self.snp.height).dividedBy(1.5)
            constraint.height.equalTo(self.snp.height).dividedBy(1.5)
        }
        
        
        content.snp.makeConstraints { (constraint) in
            constraint.centerY.equalTo(self.snp.centerY)
            constraint.leading.equalTo(featureImage.snp.trailing).offset(16)
        }
        
        moreinfoImage.snp.makeConstraints { (constraint) in
            
            constraint.centerY.equalTo(self.snp.centerY)
            constraint.trailing.equalTo(self.snp.trailing).offset(-14)
            constraint.width.height.equalTo(20)
            
        }
        
    }
    
    func setupViews() {
        addSubview(featureImage)
        addSubview(content)
        addSubview(moreinfoImage)
    }
    
    
}
extension FeaturesContentCell {
    
    static func getUIImage(forName: String) -> UIImageView {
        
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.translatesAutoresizingMaskIntoConstraints = false
        image.image = UIImage(named: forName)
        return image
    }
    static func getLabel(forName: String) -> UILabel {
        
        let label = UILabel()
        label.text = forName
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: roboto_regular, size: 16)
        return label
    }
    
}

