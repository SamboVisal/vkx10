//
//  DashboardViewController.swift
//  vKclub
//
//  Created by Machintos-HD on 11/1/18.
//  Copyright © 2018 vKirirom.com. All rights reserved.
//

import UIKit
import MaterialComponents


// class variables
class DashboardViewController: UIViewController {

    
    @IBOutlet weak var mapBtn: MDCButton!
    @IBOutlet weak var serviceBtn: MDCButton!
    @IBOutlet weak var documentBtn: MDCButton!
    @IBOutlet weak var videoBtn: MDCButton!
    @IBOutlet weak var aboutUsBtn: MDCButton!
    @IBOutlet weak var membershipBtn: MDCButton!
    @IBOutlet weak var featureContainer: UIView!
    
}

// APP Life cycle
extension DashboardViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // let colorScheme = MDCButtonScheme()
        let colorScheme = MDCTypographyScheme()
        let btnArray: Array<MDCButton> = [mapBtn, serviceBtn, documentBtn, videoBtn, aboutUsBtn, membershipBtn]
        MDThemeApplier(btnName: btnArray, scheme: colorScheme)
        
        
        
        
        // documentBtn.addTarget(self, action: #selector(handleWebView), for: .touchUpInside)
        
        documentBtn.addTarget(self, action: #selector(handleDocumentWebView), for: .touchUpInside)
        videoBtn.addTarget(self, action: #selector(handleMovieWebView), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        customNavigationBar()
    }
    
    @objc
    func handleDocumentWebView() {
        let safariURL = SafariServiceViewController(aRequest: URL(string: "https://www.google.com")!, isReaderModeEnabled: true)
        self.present(safariURL.loadUrl(), animated: true, completion: nil)
    }
    @objc
    func handleMovieWebView() {
        let safariURL = SafariServiceViewController(aRequest: URL(string: "http://10.10.70.78:8096")!, isReaderModeEnabled: false)
        self.present(safariURL.loadUrl(), animated: true, completion: nil)
    }
   
}

// Custom Navigation Bar
extension DashboardViewController {
    /// Custom the upper navigation bar to invisible.
    func customNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
    }
    
    /**
        Objective C function to hundle notification badge in dashboard notification controller
     */
    @objc func handleNotification(){
       
    }
    
    
    /// function to apply scheme to Material Design Button. It will take *MDCButton name as parameter.
    func MDThemeApplier(btnName: Array<MDCButton> = [], scheme: MDCTypographyScheme){
        for btn in btnName {
            //MDCTextButtonThemer.applyScheme(scheme, to: btn)
            // btn.inkColor = UIColor(red:1.00, green:0.76, blue:0.00, alpha:1.0)
            
            MDCButtonTypographyThemer.applyTypographyScheme(scheme, to: btn)
            btn.setTitleColor(UIColor .gray, for: .normal)
            btn.setTitleFont(UIFont(name: "Roboto-Regular", size: 20), for: UIControlState.normal)
        }
    }
    
    
}

