//
//  LoginViewController.swift
//  vKclub
//
//  Created by Pisal on 7/31/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import UIKit
import FirebaseAuth
import MaterialComponents.MaterialDialogs
import ProgressHUD
import FirebaseFirestore
import FirebaseDatabase

class LoginViewController: UIViewController, UITextFieldDelegate, UIGestureRecognizerDelegate , UINavigationControllerDelegate{
//    let personService = UserProfileCoreData()
    
    let profileVariables = ProfileOverlayNavigationBar()
    let detailProfileVariables = DetailProfileVariables()
    let cardViewInstance = ExploreCategoryComponents()
    let loginRegisterComponent = EditProfileVariables()
    var emailUserInput: String!
    var extensions = ""
    var message = ""
    let repoRef = RepositoryClass()
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        setupViewAppear()
    
//        if AccountCreated {
//            self.PresentDialogOneActionController(title: "Successfully", message: "You have successfully registered with our server. Now you can login.", actionTitle: "Okay")
//        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
        hideKeyboardWhenTappedAround()
        setupViews()
        loginRegisterComponent.emailTextField.delegate = self
        loginRegisterComponent.currentPassword.delegate = self
      
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
//        AccountCreated = false
    }
    
}

//App Cycle


// Setup all views
extension LoginViewController {
    
    func setupViews() {
        view.addSubview(profileVariables.profileView)
        view.addSubview(detailProfileVariables.detailProfileView)
        view.addSubview(loginRegisterComponent.createAccButton)
        
        self.constraintProfileView()
        self.constraintViewBelowProfileView()
        
        self.profileViewSubview()
        self.constraintImageCoverView()
        
        self.setupLoginMainView ()
        
        self.constraintCreateAcc ()
        self.setupForgotPasswordButton ()
        self.setupButtonAction ()
    }
    
}

// Handle Keyboard
extension LoginViewController {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
            
        case self.loginRegisterComponent.emailTextField:
            loginRegisterComponent.currentPassword.becomeFirstResponder()
            
        case self.loginRegisterComponent.currentPassword:
            
            loginTouchUpInside()
            self.view.endEditing(true)
        default:
            loginTouchUpInside()
        }
        
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        switch textField {
        case self.loginRegisterComponent.currentPassword:
            loginRegisterComponent.currentPassword.returnKeyType = UIReturnKeyType.go
        default:
            print("=====")
        }
        
    }
    
}

// Handle Buttons Action
extension LoginViewController {
    

    @objc
    func createAccTouchUpInside () {
        
        print("Moving to next view")
        
        let createAccController = CreateAccountController()
       
        if (loginRegisterComponent.currentPassword.text?.isEmpty == false) ||  (loginRegisterComponent.emailTextField.text?.isEmpty == false) {
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
            self.presentDialogForLeavingView(title: DialogErrorMessage().warningTitle, message: DialogErrorMessage().warningLeavingScreen, nextViewController: createAccController, leaveToSipController: false)
            
        } else {
            //self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
            self.navigationController?.pushViewController(createAccController, animated: true)
        }
        
        
        
    }
    
    @objc
    func loginTouchUpInside () {
        
        
        ProgressHUD.show("Loading...", interaction: false)
        InternetConnection.second = 0
        InternetConnection.countTimer.invalidate()
        if loginRegisterComponent.emailTextField.text == "" && loginRegisterComponent.currentPassword.text == "" {
            ProgressHUD.dismiss()
            self.alertError(message: DialogErrorMessage().errorEmptyEmail)
            return
            
        }
        if ( loginRegisterComponent.emailTextField.text?.isEmpty)! {
            ProgressHUD.dismiss()
            self.alertError(message: DialogErrorMessage().errorEmptyEmail)
        
            return
        }
        if (loginRegisterComponent.currentPassword.text?.isEmpty)! {
            ProgressHUD.dismiss()
            self.alertError(message: DialogErrorMessage().errorEmptyPassword)
            return
        } else {
            //handle firebase sign in
            //InternetConnection.CountTimer()
            Auth.auth().signIn(withEmail: loginRegisterComponent.emailTextField.text!, password: loginRegisterComponent.currentPassword.text!, completion: { (user, error) in
//                if InternetConnection.second == 15 {
//                    InternetConnection.countTimer.invalidate()
//                    InternetConnection.second = 0
//                    UIComponentHelper.ProgressDismiss()
//                    return
//                }
                
                if error == nil {
//                    InternetConnection.countTimer.invalidate()
//                    InternetConnection.second = 0
                 
                    
                    
                    if (user?.isEmailVerified)! {
                       
                        if(user?.photoURL == nil){
                            let img = UIImage(named: "detailprofile-icon")
                            let newImage = UIComponentHelper.resizeImage(image: img!, targetSize: CGSize(width: 400, height: 400))
                            _ = UIImagePNGRepresentation(newImage)
                            
                            //self.create(username: (user?.displayName)!,email : (user?.email)!, imagData: imageProfiles! as NSData  )
                            

                        } else {
                            
                            self.getDataFromUrl(url: (user?.photoURL!)!){
                                (data, response, error)  in
                                guard let data = data, error == nil
                                    else {
                                        return
                                }
                                _ = data as NSData?
                                
                                //self.create(username: (user?.displayName)!,email : (user?.email)!, imagData: image!  as NSData )
                            }
                            
                            
                        }
                        guard let uid = Auth.auth().currentUser?.uid else {
                            try! Auth.auth().signOut()
                            return
                        }
                        //self.validation(uid: (Auth.auth().currentUser?.uid)!)
                        self.userLoginProcess(uid: uid)
                        
                        
                    }
                    else {
                        ProgressHUD.dismiss()
                        self.alertError(message: DialogTitleMessage().confirmEmailAdd)
                        
        
                    }
                
                    
                }else {
                    ProgressHUD.dismiss()
                    let check: String = (error?.localizedDescription)!
                    print(check,"||")
                    switch check {
                    case "There is no user record corresponding to this identifier. The user may have been deleted.":
                        self.alertError(message:  DialogErrorMessage().errorUsernamePasswordNotMatch)
                        break
                    case "The password is invalid or the user does not have a password.":
                        Auth.auth().fetchProviders(forEmail: self.loginRegisterComponent.emailTextField.text!, completion: { (accData, error) in
                            if error == nil{
                                if accData == nil {
                                    self.alertError(message:  "The email you entered did not match our records. Please double-check and try again.")
                                    
                                    return
                                }else {
                                    self.alertError(message: "The password is invalid or the user does not have a password.")
                                    
                                }
                                
                            } else {
                                self.alertError(message: (error?.localizedDescription)!)
                              
                                return
                                
                            }
                        })
                        
                        break
                    default:
                        self.alertError(message: (error?.localizedDescription)!)
                      
                        break
                        
                        
                    }
                    
                }
            })
            
            
        }
        
    }
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask (with: url) { (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    
    func userLoginProcess(uid: String) {
        
        guard let user = Auth.auth().currentUser else {
            return
        }
        repoRef.getUserFirestore(in: FIRCollectionReference.users, and: user.uid, returning: UserProfileModel.self) { (data, done) in
            if done {
                
                print("User data is \(data)")
                for data in data {
                    
                    if let numberExt = data.vkclubNumber {
                        
                        guard let userID = Auth.auth().currentUser?.uid else {
                            return
                        }
                        
                        let userCoreData = self.repoRef.getUserCore(userId: userID)
                        
                        print("Amount of user core data \(userCoreData.count)")
                        if userCoreData.count == 0 {
                            self.repoRef.insertUserCore(userModel: data, completion: { (done) in
                                if done {
                                    print("Done inserting data core data")
                                    self.userLoginSuccess(id: user.uid)
                                }
                            })
                        }
                        
                        print("User already has numberExt in server \(numberExt)")
                        
                        
                        
                    } else {
                        
                        print("User has no extension number yet")
                        
                        if let user = Auth.auth().currentUser {
                            
                            let url = user.photoURL
                            let stringURL = url?.absoluteString
                            
                            DispatchQueue.main.async(execute: {
                                
                                self.repoRef.createOneSwitch(email: user.email!, name: user.displayName!, completionHandler: { (done, message, extnumber) in
                                    if done {
                                        
                                        // Insert data to core data
                                        self.repoRef.insertUserCore(userModel: UserProfileModel(email: user.email!, name: user.displayName!, pfpUrl: stringURL!, presence: false, role: "Student", phoneNumber: "null",isVKpointUser: false, vkclubNumber: extnumber, vkPassword: ""), completion: { (done) in
                                            if done {
                                                
                                                print("Data is completed inserting in core data ")
                                                
                                            }
                                        })
                                        
                                        
                                        self.repoRef.updateUserFirestore(for: UserProfileModel(email: user.email!, name: user.displayName!, pfpUrl: stringURL!, presence: false, role: "Student", phoneNumber: "null",isVKpointUser: false, vkclubNumber: extnumber, vkPassword: ""), in: .users, and: user.uid, completion: { (done) in
                                            
                                            if done {
                                                print("data is updated to firestore")
                                            }
                                            
                                        })
                                        
                                        self.userLoginSuccess(id: user.uid)
                                        
                                    } else {
                                        
                                        print("Cannot register")
                                        if message.contains("duplicate key error collection:") {
                                            
                                            print("This is EXT number \(extnumber)")
                                            
                                            if Auth.auth().currentUser != nil {
                                                try! Auth.auth().signOut()
                                            }
                                            
                                        } else {
                                            if Auth.auth().currentUser != nil {
                                                try! Auth.auth().signOut()
                                            }
                                        }
                                        
                                        
                                        
                                    }
                                })
                            })
                            
                            
                            
                            
                        }
                        
                        
                    }
                    
                }
                
                
                
            } else {
                
                print("Cannot register")
                
                
                if Auth.auth().currentUser != nil {
                    try! Auth.auth().signOut()
                }
                
            }
        }
        
        
    }

    
    
    func userLoginSuccess(id: String) {
        
        print("User id ", id)
        UserDefaults.standard.setUserId(value: id)
        ProgressHUD.dismiss()
        UserDefaults.standard.setIsLoggedIn(value: true)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func createOneSwitch(email:String,name: String, completionHandler: @escaping (Bool,String) -> Void)  {
        print("Username is \(name)")
        var userFirstName = ""
        var userLastName = ""
        
        if name.containsWhitespace {
            
            let string = name
            var token = string.components(separatedBy: .whitespaces)
            print("username\(token[0])")
            
//            if let first = string.components(separatedBy: .whitespaces)
            
            userFirstName = token[0]
            userLastName = token[1]
            print("User firstname", token[0])
            print("User lastname", token[1])
            
            print("Username contain white space")
        } else {
            
            userFirstName = name
            userLastName = name
            
            print("Username has no white space")
        }
        var request = URLRequest(url: URL(string:"https://os.keen-vc.cf/api/v2/onecore/u-exts" )!)
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Basic NWI4ZTkwOTRiZDJmMDcxODU2MzFjMWY0OmV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUp5YjJ4bElqb2lRVkJRSWl3aVgybGtJam9pTldJNFpUa3dPVFJpWkRKbU1EY3hPRFUyTXpGak1XWTBJaXdpYzJOdmNHVWlPaUpXUzBOTVZVSTZVa1ZCUkNJc0ltRndjQ0k2SWxaTFEweFZRaUlzSW1Oc2FXVnVkQ0k2SWtaSlVsTlVJaXdpYTJWNVgybGtJam9pVFdGamFua3dkRFkxT1c5T1pYZEdjbUZJU2s1NWVHTTNNV1JRV2xwalNsa3hNbE5wYm01UFYwcHJkRUo0TkhsdFEzcExSMnRQU0drNU5uSXdZakZEYnpKYWMzQlNUVU5KYWtsc01IUklPSGR2VGpkeGNHRjJkazVqZUhkVGNrNUJRa1YwTDFGWVkxZHhPRlV4UlZJMU1GUnBZMWREYjJ0NmFuUmtWMnhuWjFsQ2JXOUtaRzFuY21WeFFsTmxNWE0zV21sRFVVNUdiVWR4WTNJeE1pc3hVVFpPTlVaM1NtZ3hURFJuVlZoS2NXUXhhR0ZxUzJOalpHTlBjaTk1TWxNNWJUWXZiWEZJUkdNM1Vsa3lWbm9yWkVWdFlXZERUelJYVEZKMFlVOUxabXBpZFRWNVZtNHdRMlpCUVZCMFZHMDRja3hEZUhrNFJsWldVell2TVc4MWNtNHdiSFJ0T0dSR1ZVMU9TMDFSZUdOWFdUVTVWa05KYVV0S2FGcEJUVlJDU2tKNU5FNWxMMU1yY0hoUFpHRjViVXBWZVdrMFkyOUdaSGRFVTNFNFF6VXdTSGRpYjJoS01ucEtVMnR3WjBOeVlWUXdiR3BCUFQwaUxDSnBjM04xWlY5emRHRnRjQ0k2TVRVek5qQTJPVGM0TURreE1Td2lhV0YwSWpveE5UTTJNRFk1Tnpnd0xDSnVZbVlpT2pFMU16WXdOamszT0RVc0ltVjRjQ0k2TVRVNU9URTBNVGM0TUN3aVlYVmtJam9pUVZCUUlpd2lhWE56SWpvaWIyNWxjM2RwZEdOb0lpd2ljM1ZpSWpvaVZrdERURlZDUUc5dVpYTjNhWFJqYUNJc0ltcDBhU0k2SWpWaU9HVTVNRGswWW1ReVpqQTNNVGcxTmpNeFl6Rm1OQ0o5Ljl5VmhUaVVraWE0TTc3RzNhLUpsaVBxdm5GRFEtRWk2YVFCT192MVpHYVk=" ,forHTTPHeaderField: "OS-API-KEY")
        
        let parameters = ["domain":"192.168.7.136" , "extPassword":["type":"raw","pass":"VoIP125*KIT"] ,"email": email,"password":"VoIP125*KIT","vmPassword":"12356","metadata":["firstName":userFirstName,"lastName": userLastName,"dob":"12-12-1998"],"scope":"WRITE+DELETE"] as [String : Any]
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if error != nil {
                self.message = (error?.localizedDescription)!
                print("message error \(self.message)")
                completionHandler(false, self.message)
            } else {
                
                do {
                    
                    var json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:Any]
                    
                
                    if json!["success"] as! Bool == true {
                        let data = json!["data"] as! [String : Any]
                        let ext = data["ext"] as! [String: Any]
                        self.extensions = ext["extension"] as! String
                        print("Extension number \(self.extensions)")
                        completionHandler(true, self.message)
                    } else {
                        
                        self.message = json!["message"] as! String
                        
                        print("message err \(self.message)")
                        
                        completionHandler(false, self.message)
                        
                    }
                    
                    
                } catch {
                    
                    print("eror")
                }
                
            }
        }
        
        
        
        task.resume()
        
        
        
    }
    
    func addVOIPdataTofireStore(uid: String , email: String) {
        db.collection("users").document(uid).setData(
            
            [
                "createdOneSwitch" : true,
                "email" : email,
                "extension" : extensions,
                "password" : "VoIP125*KIT"
            ]
        )
        print("Extension number is \(extensions)")
    }
    
    @objc
    func backHome() {
        
        if (loginRegisterComponent.currentPassword.text?.isEmpty == false) ||  (loginRegisterComponent.emailTextField.text?.isEmpty == false) {
            
            self.presentDialogForLeavingView(title: "Warning", message: "Are you sure want to leave? This will not save your information.", nextViewController: self, leaveToSipController: true)
            
        } else {
            dismiss(animated: true, completion: nil)
        }
        
    }
    
    
}


class UserRef {
    
    static func User() -> UserProfileModel{
        
        let model = UserProfileModel(email: "", name: "", pfpUrl: "", presence: false, role: "customer",phoneNumber: "", isVKpointUser: false, vkclubNumber: "", vkPassword: "")
        return model
    }
    
}

// Helper function and setup btn action

extension LoginViewController {
    
    func presentDialogForLeavingView (title: String, message: String, nextViewController: UIViewController, leaveToSipController: Bool) {
        
        let dialog = dialogMessage(title: title, message: message)
        let okay = MDCAlertAction(title: "Okay") { (action) in
            
            if self.loginRegisterComponent.emailTextField.text != nil {
                self.loginRegisterComponent.emailTextField.text = ""
            }
            if self.loginRegisterComponent.currentPassword.text != nil {
                self.loginRegisterComponent.currentPassword.text = ""
            }
            
            if leaveToSipController {
                self.dismiss(animated: true, completion: nil)
            }
            else {
                //self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
                let viewController = nextViewController
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
        let cancel = MDCAlertAction(title: "Cancel", handler: nil)
        dialog.addAction(okay)
        dialog.addAction(cancel)
        dialogPresent(view: dialog)
        
        
    }
    
    func setupNavigation() {
        view.backgroundColor = .white
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "cancel-edit")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backHome))
    }
    func setupViewAppear() {
        UIApplication.shared.statusBarStyle = .lightContent
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    func setupButtonAction () {
        
        loginRegisterComponent.createAccButton.addTarget(self, action: #selector(createAccTouchUpInside), for: .touchUpInside)
        
        loginRegisterComponent.loginButton.addTarget(self, action: #selector(loginTouchUpInside), for: .touchUpInside)
        
       
        loginRegisterComponent.forgotPasswordButton.addTarget(self, action: #selector(forgotPassTouchUpInside), for: .touchUpInside)
    }
   
  
    @objc
    func forgotPassTouchUpInside() {
       
        let forgotPassViewController = ForgotPasswordViewController()
        self.navigationController?.pushViewController(forgotPassViewController, animated: true)
        
    }
}
