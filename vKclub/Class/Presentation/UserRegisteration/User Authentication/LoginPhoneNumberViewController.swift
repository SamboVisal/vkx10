//
//  LoginPhoneNumberViewController.swift
//  vKclub
//
//  Created by Pisal on 12/8/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import UIKit
import Firebase
import PhoneNumberKit
import ProgressHUD

class LoginPhoneNumberViewController: UIViewController , UITextFieldDelegate{
    
    let loginRegisterComponent = EditProfileVariables()
    typealias CompletionHandler = (_ success:Bool) -> Void
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        hideKeyboardWhenTappedAround()
        print("This is device model \(getDeviceModelName.userDeviceModel)")
    
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "clear")?.withRenderingMode(UIImageRenderingMode.alwaysTemplate), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backHome))
        self.navigationItem.leftBarButtonItem?.tintColor = BaseColor.colorPrimary
        
        setupViewLoginView()
        
        loginRegisterComponent.getPlaceholderNum.delegate = self
        loginRegisterComponent.getPhoneNumberInput.delegate = self
        
        setupActionButtons()
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
        case self.loginRegisterComponent.getPlaceholderNum:
            loginRegisterComponent.getPhoneNumberInput.becomeFirstResponder()
        case self.loginRegisterComponent.getPhoneNumberInput:
            self.verifyPhoneNumber()
            
            self.view.endEditing(true)
        default:
            self.verifyPhoneNumber()
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField {
        case loginRegisterComponent.getPlaceholderNum:
            
            let text = loginRegisterComponent.getPlaceholderNum.text ?? ""
            guard let stringRange = Range(range, in: text) else { return false }
            
            let updatedText = text.replacingCharacters(in: stringRange, with: string)
            
            return updatedText.count <= 4
        default:
            let text = loginRegisterComponent.getPhoneNumberInput.text ?? ""
            guard let stringRange = Range(range, in: text) else { return false }
            
            let updatedText = text.replacingCharacters(in: stringRange, with: string)
            
            return updatedText.count <= 12
        }
     
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.shared.statusBarStyle = .default
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.tintColor = BaseColor.colorPrimary
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }

    func setupViewLoginView() {
        
        view.addSubview(loginRegisterComponent.getImageView)
        loginRegisterComponent.getImageView.image = loginRegisterComponent.getImageView.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        loginRegisterComponent.getImageView.tintColor = BaseColor.colorPrimary
        view.addSubview(loginRegisterComponent.loginLabel)
        view.addSubview(loginRegisterComponent.getPlaceholderNum)
        view.addSubview(loginRegisterComponent.getPhoneNumberInput)
        view.addSubview(loginRegisterComponent.loginButton)
        loginRegisterComponent.loginButton.setTitle("Continue", for: .normal)
        
      
    
        
    }
    
    func setupActionButtons() {
        
        loginRegisterComponent.loginButton.addTarget(self, action: #selector(verifyPhoneNumber), for: .touchUpInside)
        loginRegisterComponent.forgotPasswordButton.addTarget(self, action: #selector(iAmStuff), for: .touchUpInside)
        
    }
    
    @objc
    func verifyPhoneNumber() {
        
        ProgressHUD.show("Loading...", interaction: false)
        
        var placeholderText = ""
        var phonenumber = ""
        
        print("This is placeholder \(String(describing: loginRegisterComponent.getPlaceholderNum.placeholder))")
        if (loginRegisterComponent.getPhoneNumberInput.text?.isEmpty)! {
            
            ProgressHUD.dismiss()
            self.alertError(message: "Phone number field could not empty.")
            
            return
        }
        if (loginRegisterComponent.getPlaceholderNum.text?.isEmpty)! {
            placeholderText = loginRegisterComponent.getPlaceholderNum.placeholder!
        } else {
            placeholderText = loginRegisterComponent.getPlaceholderNum.text!
        }
        print("This is also placeholder \(loginRegisterComponent.getPlaceholderNum.placeholder ?? "No placeholder value") this is placeholder with text \(loginRegisterComponent.getPlaceholderNum.text ?? "No text in placeholder")")
        
        phonenumber =  placeholderText + loginRegisterComponent.getPhoneNumberInput.text!
        
        print("This user phonenumber overall \(phonenumber)")
        
        self.verifyUserPhoneNumber(number: phonenumber) { (phoneNumber, done) in
            
            if done {
                
                RepositoryClass.shared.verifyUser(with: phoneNumber, completion: { ( message ,done) in
                    if done {
                        
                        ProgressHUD.dismiss()
                        print("This is a valid phone number ")
                        let signInController = SignInPhoneNumberViewController()
                        signInController.phoneNumber = phoneNumber
                        self.navigationController?.pushViewController(signInController, animated: true)
                    } else {
                        
                        ProgressHUD.dismiss()
                        self.alertError(message: message)
                    }
                })
                
                
            } else {
                
                ProgressHUD.dismiss()
                self.alertError(message: phoneNumber)
            }
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        
    }
    
    func verifyUserPhoneNumber(number: String, completion: @escaping(_ validPhoneNumber: String, _ success: Bool) -> Void)  {
        
        let phoneNumberKit = PhoneNumberKit()
        do {
            
            let phone = try phoneNumberKit.parse(number)
            print("Country code \(phone.countryCode)")
            print("Country national number \(phone.nationalNumber)")
            print("Country phone type \(phone)")
            print("Country \(phone.leadingZero)")
            
            completion(phone.numberString, true)
            
        } catch {
            completion(error.localizedDescription, false)
            
            
            print("PhonenumberKit throws exception: ",error)
            
          
        }
        
    }
    
    func phoneKit() -> Bool{
        
        
        return true
        
    }
    
    @objc
    func iAmStuff() {
        
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        
        loginRegisterComponent.getImageView.snp.makeConstraints { (make) in
            
            make.centerX.equalTo(view)
            make.top.equalTo(view).offset(96)
            make.width.equalTo(view).dividedBy(2)
            make.height.equalTo(loginRegisterComponent.getImageView.snp.width)
            
        }
        
        loginRegisterComponent.loginLabel.snp.makeConstraints { (make) in
            make.top.equalTo(loginRegisterComponent.getImageView.snp.bottom).offset(16)
            make.centerX.equalTo(view)
        }
        
        loginRegisterComponent.getPlaceholderNum.snp.makeConstraints { (make) in
            make.top.equalTo(loginRegisterComponent.loginLabel.snp.bottom).offset(16)
            if getDeviceModelName.userDeviceIphoneX() {
                make.leading.equalTo(view).offset(70)
            }
            if getDeviceModelName.userDeviceIphone5() {
                make.leading.equalTo(view).offset(50)
            }
            else {
                make.leading.equalTo(view).offset(96)
            }
            
            make.height.equalTo(50)
            make.width.equalTo(70)
        }
        
        loginRegisterComponent.getPhoneNumberInput.snp.makeConstraints { (make) in
            
            make.top.equalTo(loginRegisterComponent.getPlaceholderNum)
            make.leading.equalTo(loginRegisterComponent.getPlaceholderNum.snp.trailing).offset(8)
            if getDeviceModelName.userDeviceIphoneX() {
                make.trailing.equalTo(view).offset(-70)
            }
            if getDeviceModelName.userDeviceIphone5() {
                make.trailing.equalTo(view).offset(-50)
            }
            else {
                make.trailing.equalTo(view).offset(-96)
            }
            make.height.equalTo(loginRegisterComponent.getPlaceholderNum)
        }
        
        loginRegisterComponent.loginButton.snp.makeConstraints { (make) in
            
            make.top.equalTo(loginRegisterComponent.getPhoneNumberInput.snp.bottom).offset(16)
            make.centerX.equalTo(view)
            make.height.equalTo(48)
            
            
        }
        
  
        
        
        
        
        
    }
    
    @objc
    func backHome() {
        self.dismiss(animated: true, completion: nil)
    }
    

}
