//
//  NicknameViewController.swift
//  vKclub
//
//  Created by Pisal on 12/14/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseStorage
import ProgressHUD
import Photos


class SetNicknameViewController: UIViewController , UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    var imagePicker : UIImagePickerController = UIImagePickerController()
    let cardViewInstance = ExploreCategoryComponents()
    let textField = EditProfileVariables()
    var phoneNumber = ""
    let storageRef = Storage.storage().reference()
    var photoUrl: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        imagePicker.delegate = self
        view.addSubview(cardViewInstance.mainCardView)
        cardViewInstance.mainCardView.addSubview(cardViewInstance.imageUserCircleView)
        cardViewInstance.mainCardView.addSubview(textField.nicknameLabel)
        cardViewInstance.mainCardView.addSubview(textField.usernameTextField)
        cardViewInstance.mainCardView.addSubview(textField.loginButton)
        textField.loginButton.setTitle("Continue", for: .normal)
        cardViewInstance.imageUserCircleView.isUserInteractionEnabled = true
        
        cardViewInstance.imageUserCircleView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.imageDidClicked)) )
        textField.loginButton.addTarget(self, action: #selector(loginButtonClicked), for: .touchUpInside)
        
    }
    
    
    
    @objc
    func loginButtonClicked() {
        ProgressHUD.show("Loading...", interaction: false)
        guard let nicknameField = textField.usernameTextField.text else {
            ProgressHUD.dismiss()
            self.alertError(message: "Nickname could not empty.")
            return
        }
        if nicknameField.isEmpty {
            print("Nick name empty")
            ProgressHUD.dismiss()
            self.alertError(message: "Nickname could not empty.")
            return
        } else {
            print("Nick name not empty")
        }
        let randomVkPassword = String().generateAlphaNumberic(length: 6)
        UserDefaults.standard.setRandomVkPassword(value: randomVkPassword)
        if self.photoUrl != nil {
            print("User has photo url")
        } else {
            self.photoUrl = ""
            print("User not input photo urll")
        }
        
        let model = RepositoryClass.shared.user
        model.vkPassword = UserDefaults.standard.returnRandomVkPasswrod()


        FIRFireStoreService.shared.createUserFirestore(for: model, in: .users, completion: { (done) in

            if done {
                guard let id = Auth.auth().currentUser?.uid else {
                    return
                }
                print("Done created first user in firestore")
                print("This is phone number \(self.phoneNumber)")
                RepositoryClass.shared.createOneSwitch(email: self.phoneNumber, name: nicknameField, completionHandler: { (done, message, extnumber) in
                    if done {
                        
                        print("===== This is extension \(extnumber)")
                        model.vkclubNumber = extnumber
                        model.name = nicknameField
                        model.pfpUrl = self.photoUrl
                        // Insert data to core data
                        RepositoryClass.shared.insertUserCore(userModel: model, completion: { (done) in
                            if done {

                                print("Data is completed inserting in core data ")

                            } else {
                                ProgressHUD.dismiss()
                                self.alertError(message: "Account could not created right now.")
                                try! Auth.auth().signOut()
                            }
                        })


                        RepositoryClass.shared.updateUserFirestore(for: model, in: .users, and: id, completion: { (done) in

                            if done {
                                self.userLoggedInSuccess(id: id)
                                print("data is updated to firestore")


                            } else {
                                ProgressHUD.dismiss()
                                self.alertError(message: "Account could not created right now.")
                                try! Auth.auth().signOut()
                            }

                        })


                    } else {
                        
                        print("Error")
                        if message.contains("duplicate key error collection"){
                            print("User is duplicate key")
                            try! Auth.auth().signOut()
                             ProgressHUD.dismiss()
                            self.dismiss(animated: true, completion: nil)
                        }
                        
                    }
                })

            } else {

                ProgressHUD.dismiss()
                self.alertError(message: "Account could not created right now.")
                try! Auth.auth().signOut()
            }

        })
        
    }
    
    func userLoggedInSuccess(id: String) {
        print("User id ", id)
        UserDefaults.standard.setUserId(value: id)
        ProgressHUD.dismiss()
        UserDefaults.standard.setIsLoggedIn(value: true)
        self.dismiss(animated: true, completion: nil)
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        cardViewInstance.mainCardView.snp.makeConstraints { (make) in
            
            make.center.equalTo(view)
            make.width.equalTo(view).dividedBy(1.4)
            make.height.equalTo(view).dividedBy(3)
            
        }
        cardViewInstance.imageUserCircleView.snp.makeConstraints { (make) in
            
            make.width.height.equalTo(100)
            make.centerX.equalTo(cardViewInstance.mainCardView)
            make.top.equalTo(cardViewInstance.mainCardView).offset(-50)
            
            
        }
        textField.nicknameLabel.snp.makeConstraints { (make) in
            
            make.top.equalTo(cardViewInstance.imageUserCircleView.snp.bottom).offset(8)
            make.centerX.equalTo(cardViewInstance.mainCardView)
            
        }
        textField.usernameTextField.snp.makeConstraints { (make) in
            
            make.centerY.equalTo(cardViewInstance.mainCardView)
            make.left.equalTo(cardViewInstance.mainCardView).offset(16)
            make.right.equalTo(cardViewInstance.mainCardView).offset(-16)
            make.height.equalTo(48)
            
            
        }
        textField.loginButton.snp.makeConstraints { (make) in
            make.bottom.equalTo(cardViewInstance.mainCardView).offset(-8)
            make.left.right.height.equalTo(textField.usernameTextField)
            
        }
        
    }
    
}

extension SetNicknameViewController {
    
    @objc
    func imageDidClicked() {
        print("User clicked image")
        if InternetConnection.isConnectedToNetwork() {
            print("have internet")
        } else {
            self.alertError(message: "Can not upload to server. Please Check your internet connection ")
            return
        }
        UIComponentHelper.showProgressWith(status: "Loading...", interact: false)
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) == true
        {
            
            UIApplication.shared.statusBarStyle = .default
            self.imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: {
                
                UIComponentHelper.ProgressDismiss()
            })
        } else {
            UIComponentHelper.ProgressDismiss()
            print("NO")
            
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("User press cancel button")
        
        dismiss(animated: true, completion: {
            UIComponentHelper.ProgressDismiss()
        })
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        UIComponentHelper.showProgressWith(status: "Loading...", interact: false)
        
        var selectedImageFromPicker : UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            selectedImageFromPicker = editedImage
        }else {
            
            print("Cannot get image")
            
        }
        
        if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage{
            selectedImageFromPicker = originalImage
        }
        if selectedImageFromPicker == nil {
            self.alertError(message: "The process could not be completed. Please try again")
            return
        }
        
        if let selectedImage = selectedImageFromPicker {
            
            
            let currentUser = Auth.auth().currentUser
            let newImage = UIComponentHelper.resizeImage(image: selectedImage, targetSize: CGSize(width: 400, height: 400))
            guard  let imageProfiles = UIImagePNGRepresentation(newImage) else {
                self.alertError(message: "The process could not be completed. Please try again")
                return
            }
            let imageData : NSData = NSData(data: imageProfiles)
            let imageSize :Int = imageData.length
            
            if let user = currentUser {
                let riversRef = storageRef.child("userprofile-photo").child(user.uid)
                riversRef.putData(imageProfiles, metadata: nil, completion: { (metadata, error) in
                    
                    if Double(imageSize) > 500000 {
                        self.alertError(message: "You can not upload image more then 5 MB")
                        return
                    }
                    if let err = error {
                        UIComponentHelper.ProgressDismiss()
                        self.alertError(message: err.localizedDescription)
                        return
                    }else {
                        
                        print("NO Error Occur")
                    }
                    guard let metadata = metadata else {
                        UIComponentHelper.ProgressDismiss()
                        self.alertError(message: "Please check your internet connection and try again.")
                        return
                    }
                    // Metadata contains file metadata such as size, content-type, and download URL.
                    let downloadURL = metadata.downloadURL()!.absoluteString
                    let url = NSURL(string: downloadURL) as URL?
                    
                    let changeProfileimage = user.createProfileChangeRequest()
                    changeProfileimage.photoURL = url
                    changeProfileimage.commitChanges(completion: { (error) in
                        if error != nil {
                            self.alertError( message: (error?.localizedDescription)!)
                        }
                    })
                    
                    guard let urlString = url?.absoluteString else {
                        return
                    }
                    self.photoUrl = urlString
                    self.cardViewInstance.imageUserCircleView.sd_setImage(with: url, placeholderImage: UIImage(named: "detailprofile-icon")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), options: [ .retryFailed, .continueInBackground], completed: { (image, error, ImageCache, Url) in
                        if error == nil {
                            UserCommitChnage = true
                            UIComponentHelper.ProgressDismiss()
                            RepositoryClass.shared.updateSpecificField(userID: user.uid, fieldName: "pfpUrl", valueNew: urlString)
                            RepositoryClass.shared.updateUserFirestoreSpecific(in: .users, and: user.uid, forField: "pfpUrl", forValue: urlString, completion: { (done) in
                                if done {
                                    
                                    self.alertMessageDialog(title: "Success", message: "You have updated your profile.", actionTitle: "Okay")
                                    
                                }
                            })
                            
                            
                        }else {
                            UIComponentHelper.ProgressDismiss()
                            
                            self.alertError(message: "Please try to upload again.")
                            
                            
                        }
                    })
                    
                })
            }
            
            
        }
        else {
            print("error something")
        }
        dismiss(animated: true, completion: {
            
            if selectedImageFromPicker == nil {
                print("NO IMAGE ")
                self.alertError(message: "The process could not be completed. Please try again")
                
                return
            }
            
        })
        
        
    }
    
}
