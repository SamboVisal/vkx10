//
//  ForgotPasswordViewController.swift
//  vKclub
//
//  Created by Pisal on 7/31/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD
import ProgressHUD
import PopupDialog

class ForgotPasswordViewController: UIViewController , UITextFieldDelegate{
    
    
    private let profileVariables = ProfileOverlayNavigationBar()
    private let detailProfileVariables = DetailProfileVariables()
    private let forgotPassComponent = EditProfileVariables()
    private let cardViewInstance = ExploreCategoryComponents()
    private let currentUser = Auth.auth().currentUser
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        hideKeyboardWhenTappedAround()
        view.addSubview(profileVariables.profileView)
        view.addSubview(detailProfileVariables.detailProfileView)
        
        self.constraintProfileView()
        self.constraintDetailProfileView()
        
        
        self.profileViewSubview()
        self.constraintImageUserProfileView()
        
        self.setupForgotPasswordView()
        
        forgotPassComponent.emailTextField.delegate = self
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
        case self.forgotPassComponent.emailTextField:
            handleSubmitBtn()
            self.view.endEditing(true)
        default:
            handleSubmitBtn()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        switch textField {
        case self.forgotPassComponent.emailTextField:
            
            forgotPassComponent.emailTextField.returnKeyType = UIReturnKeyType.go
        default:
            print("nothing....")
        }
        
    }
    
    @objc
    func handleSubmitBtn() {
    
        print("submitting...")
        
            ProgressHUD.show("Loading...", interaction: false)
            print("User availability....")
            if (forgotPassComponent.emailTextField.text?.isEmpty)! {
                ProgressHUD.dismiss()
                self.alertError(message: "Please enter your email.")
                return
            } else {
                Auth.auth().fetchProviders(forEmail: forgotPassComponent.emailTextField.text!, completion: { (emailData, error) in
                    if error == nil {
                        
                        if emailData == nil {
                            ProgressHUD.dismiss()
                            self.alertError(message: "Email you entered doesn't match with our records. Please re-check and try again.")
                            return
                        } else {
                            
                            InternetConnection.CountTimer()
                            
                            Auth.auth().sendPasswordReset(withEmail: self.forgotPassComponent.emailTextField.text!, completion: { (error) in
                                if InternetConnection.second == 10 {
                                    InternetConnection.countTimer.invalidate()
                                    InternetConnection.second = 0
                                    UIComponentHelper.ProgressDismiss()
                                    return
                                }
                                InternetConnection.countTimer.invalidate()
                                InternetConnection.second = 0
                                if let error = error {
                                    self.alertError(message: error.localizedDescription)
                                
                                } else {
                                    ProgressHUD.dismiss()
                                    self.alertMessageDialog(title: "Success", message: "Please check your email to recovery your password.", actionTitle: "Okay")
                                    self.navigationController?.popViewController(animated: true)
                                    
                                }
                            })
                            
                        }
                        
                    } else {
                        
                        UIComponentHelper.ProgressDismiss()
                        self.alertError(message: (error?.localizedDescription)!)
                        
                    }
                })
            }
            
       
        
    }
    
    
}
extension ForgotPasswordViewController {
    
    
    func constraintProfileView() {
        
        profileVariables.profileView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        profileVariables.profileView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        profileVariables.profileView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        profileVariables.profileView.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        profileVariables.profileView.heightAnchor.constraint(equalToConstant: view.frame.height / 2.5).isActive = true
        
        
        
        
    }
    func constraintDetailProfileView() {
        
        detailProfileVariables.detailProfileView.topAnchor.constraint(equalTo: profileVariables.profileView.bottomAnchor).isActive = true
        detailProfileVariables.detailProfileView.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        detailProfileVariables.detailProfileView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        detailProfileVariables.detailProfileView.backgroundColor = .white
        
        
    }
    
    func profileViewSubview() {
        
        profileVariables.profileView.addSubview(profileVariables.imageUser)
        profileVariables.imageUser.addSubview(profileVariables.opacityDetailEachExplore)
        
        profileVariables.opacityDetailEachExplore.topAnchor.constraint(equalTo: profileVariables.imageUser.topAnchor).isActive = true
        profileVariables.opacityDetailEachExplore.leftAnchor.constraint(equalTo: profileVariables.imageUser.leftAnchor).isActive = true
        profileVariables.opacityDetailEachExplore.rightAnchor.constraint(equalTo: profileVariables.imageUser.rightAnchor).isActive = true
        profileVariables.opacityDetailEachExplore.heightAnchor.constraint(equalTo: profileVariables.profileView.heightAnchor).isActive = true
        
    }
    
    func constraintImageUserProfileView() {
        
        profileVariables.imageUser.topAnchor.constraint(equalTo: profileVariables.profileView.topAnchor).isActive = true
        profileVariables.imageUser.leftAnchor.constraint(equalTo: profileVariables.profileView.leftAnchor).isActive = true
        profileVariables.imageUser.rightAnchor.constraint(equalTo: profileVariables.profileView.rightAnchor).isActive = true
        profileVariables.imageUser.heightAnchor.constraint(equalTo: profileVariables.profileView.heightAnchor).isActive = true
        
    }
    
    func setupForgotPasswordView () {
        view.addSubview(cardViewInstance.mainCardView)
        cardViewInstance.mainCardView.addSubview(forgotPassComponent.forgotPasslabel)
        
        cardViewInstance.mainCardView.snp.makeConstraints { (make) in
            
            make.centerY.equalTo(view).offset(-50)
            make.centerX.equalTo(view)
            make.leading.equalTo(view).offset(16)
            make.trailing.equalTo(view).offset(-16)
            make.height.equalTo(view).dividedBy(4)
            
        }
        
        forgotPassComponent.forgotPasslabel.snp.makeConstraints { (make) in
            
            make.top.equalTo(cardViewInstance.mainCardView).offset(8)
            make.centerX.equalTo(cardViewInstance.mainCardView)
            
        }
        
        
        self.setupEmailField()
        
    }
    
    
    func setupEmailField() {
        
        cardViewInstance.mainCardView.addSubview(forgotPassComponent.emailTextField)
        cardViewInstance.mainCardView.addSubview(forgotPassComponent.editSaveBtton)
        forgotPassComponent.editSaveBtton.setTitle("Submit", for: .normal)
        
        // Constraint email text field
        
        forgotPassComponent.emailTextField.snp.makeConstraints { (make) in
            
            make.top.equalTo(forgotPassComponent.forgotPasslabel.snp.bottom).offset(8)
            make.leading.equalTo(cardViewInstance.mainCardView).offset(16)
            make.trailing.equalTo(cardViewInstance.mainCardView).offset(-16)
            make.height.equalTo(cardViewInstance.mainCardView).dividedBy(3.8)
            
            
        }
        
        
       
        // Constraint action button
        forgotPassComponent.editSaveBtton.snp.makeConstraints { (make) in
            
            make.top.equalTo(forgotPassComponent.emailTextField.snp.bottom).offset(8)
            make.trailing.leading.equalTo(forgotPassComponent.emailTextField)
            make.height.equalTo(56)
            
        }
 
        
        forgotPassComponent.editSaveBtton.addTarget(self, action: #selector(handleSubmitBtn), for: .touchUpInside)
    }
}
