//
//  SignInPhoneNumberViewController.swift
//  vKclub
//
//  Created by Pisal on 12/8/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField
import ProgressHUD
import FirebaseAuth

class SignInPhoneNumberViewController: UIViewController {

    
    let signInComponent = EditProfileVariables()
    let containview = UIView()
    var phoneNumber: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        print("This is phone number \(phoneNumber)")
        
        setupViews()
        
        textFieldsDelegate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.navigationBar.tintColor = BaseColor.colorPrimary
    }
    
    @objc func verifySMSCode() {
        if (signInComponent.firstCodeField.text?.isEmpty)! || (signInComponent.secondCodeField.text?.isEmpty)! || (signInComponent.thirdCodeField.text?.isEmpty)! || (signInComponent.fourthCodeField.text?.isEmpty)! || (signInComponent.fifthCodeField.text?.isEmpty)! || (signInComponent.sixthCodeField.text?.isEmpty)! {
            
            self.alertError(message: "Please don't leave with an empty field.")
            return
        }
        print("Doing verify code of SMS")
        ProgressHUD.show("Loading...", interaction: false)
        //ProgressHUD.show("Loading...", interaction: false)
        let code = self.signInComponent.firstCodeField.text! + self.signInComponent.secondCodeField.text! + self.signInComponent.thirdCodeField.text! + self.signInComponent.fourthCodeField.text! + self.signInComponent.fifthCodeField.text! + self.signInComponent.sixthCodeField.text!
        
        print("this is code \(code)")
        
        RepositoryClass.shared.signInUser(with: code) { (done) in
            
            if done {
                
                guard let id = Auth.auth().currentUser?.uid else {
                    return
                    
                }
                
                RepositoryClass.shared.getUserFirestore(in: .users, and: id, returning: UserProfileModel.self, completion: { (data, done) in

                    if done == false {
                        ProgressHUD.dismiss()
                        print("====== No data in firestore ======")
                        print("====== This is new users =====")
                        
                        let nicknameController = SetNicknameViewController()
                        nicknameController.phoneNumber = self.phoneNumber!
                        self.navigationController?.pushViewController(nicknameController, animated: true)
                        
                    } else {
                        
                        
                        print("===== There is data =====")
                        print("==== This is old user ====")
                        for data in data {
                            print("==== This is user data firestore \(data)")
                            // If user has extension number
                            if let number = data.vkclubNumber {
                                
                                guard let userID = Auth.auth().currentUser?.uid else {
                                    return
                                }
                                
                                let userCoreData = RepositoryClass.shared.getUserCore(userId: userID)
                                
                                print("Amount of user core data \(userCoreData.count)")
                                if userCoreData.count == 0 {
                                    
                                    RepositoryClass.shared.insertUserCore(userModel: data, completion: { (done) in
                                        if done {
                                            print("Done inserting data core data")
                                            self.userLoggedInSuccess(id: userID)
                                        }
                                    })
                                }
                                
                                print("User already has numberExt in server \(number)")
                                
                            } else {
                                
//                                print("This is phone number \(self.phoneNumber!)")
//
//                                // Register account for new user
//                                RepositoryClass.shared.createOneSwitch(email: self.phoneNumber!, name: "Pisal", completionHandler: { (done, message, extNumber) in
//                                    if done {
//                                        guard let id = Auth.auth().currentUser?.uid else {
//                                            return
//                                        }
//                                        print("Create one switch \(extNumber)")
//                                        RepositoryClass.shared.updateSpecificField(userID: id, fieldName: "vkclubNumber", valueNew: extNumber)
//                                        RepositoryClass.shared.updateUserFirestoreSpecific(in: .users, and: id, forField: "vkclubNumber", forValue: extNumber, completion: { (done) in
//                                            if done {
//
//                                                self.userLoggedInSuccess(id: id)
//                                                print("Done instering data firestore")
//
//                                            } else {
//                                                ProgressHUD.dismiss()
//                                                try! Auth.auth().signOut()
//                                                print("Failed instert")
//                                            }
//                                        })
//
//                                    } else {
//
//                                        try! Auth.auth().signOut()
//                                        ProgressHUD.dismiss()
//                                        print("Error created one switch \(message)")
//                                    }
//                                })
//
                            }
                            
                        }
                    }


                })
                
                
                
                
//                RepositoryClass.shared.insertUserCore(userModel: UserProfileModel(email: "", name: "pisal", pfpUrl: "", presence: true, role: "staff", phoneNumber: self.phoneNumber!, vkclubNumber: ""), completion: { (done) in
//                    if done {
//                       print("Done created first user in core daat")
//                    }
//                })
                print("Not null anymore")
            } else {
                self.alertError(message: "Verification failed please check the code that we send and try again.")
                ProgressHUD.dismiss()
                try! Auth.auth().signOut()
                print("Verification failed")
            }
            
        }
        
    }
    
    func userLoggedInSuccess(id: String) {
        print("User id ", id)
        UserDefaults.standard.setUserId(value: id)
        ProgressHUD.dismiss()
        UserDefaults.standard.setIsLoggedIn(value: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
}

// App cycle

// Textfield delegate
extension SignInPhoneNumberViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
       
        textField.text = ""
        
    }
    
    fileprivate func textFieldsDelegate() {
        signInComponent.firstCodeField.delegate = self
        signInComponent.secondCodeField.delegate = self
        signInComponent.thirdCodeField.delegate = self
        signInComponent.fourthCodeField.delegate = self
        signInComponent.fifthCodeField.delegate = self
        signInComponent.sixthCodeField.delegate = self
        
        signInComponent.firstCodeField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        signInComponent.secondCodeField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        signInComponent.thirdCodeField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        signInComponent.fourthCodeField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        signInComponent.fifthCodeField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        signInComponent.sixthCodeField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
    }
    @objc
    func textFieldDidChange(textField: JVFloatLabeledTextField) {
        
        let text = textField.text
        
        
        
        if (text?.utf16.count)! >= 1 {
            
            switch textField {
                
            case signInComponent.firstCodeField:
                
                signInComponent.secondCodeField.becomeFirstResponder()
                
            case signInComponent.secondCodeField:
                signInComponent.thirdCodeField.becomeFirstResponder()
                
            case signInComponent.thirdCodeField:
                signInComponent.fourthCodeField.becomeFirstResponder()
                
            case signInComponent.fourthCodeField:
                signInComponent.fifthCodeField.becomeFirstResponder()
                
            case signInComponent.fifthCodeField:
                signInComponent.sixthCodeField.becomeFirstResponder()
                
            case signInComponent.sixthCodeField:
                verifySMSCode()
                
                self.view.endEditing(true)
            default:
                verifySMSCode()
            }
            
        }
        
        
    }
}


// Setup views constraint
extension SignInPhoneNumberViewController {
    
    func setupViews() {
        
        
        view.addSubview(signInComponent.getConfirmSMSImage)
        signInComponent.getConfirmSMSImage.image = signInComponent.getConfirmSMSImage.image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        view.addSubview(signInComponent.loginLabel)
        signInComponent.loginLabel.text = "Verification"
        
        view.addSubview(signInComponent.confirmSMS)
        signInComponent.confirmSMS.text! = signInComponent.confirmSMS.text! + phoneNumber!
        containview.translatesAutoresizingMaskIntoConstraints = false
        containview.backgroundColor = .white
        view.addSubview(containview)
        
        containview.addSubview(signInComponent.firstCodeField)
        containview.addSubview(signInComponent.secondCodeField)
        containview.addSubview(signInComponent.thirdCodeField)
        containview.addSubview(signInComponent.fourthCodeField)
        containview.addSubview(signInComponent.fifthCodeField)
        containview.addSubview(signInComponent.sixthCodeField)
        
        view.addSubview(signInComponent.loginButton)
        signInComponent.loginButton.setTitle("Continue", for: .normal)
        signInComponent.loginButton.addTarget(self, action: #selector(verifySMSCode), for: .touchUpInside)
        
        view.addSubview(signInComponent.forgotPasswordButton)
        signInComponent.forgotPasswordButton.setTitle("Resend Code", for: .normal)
        signInComponent.forgotPasswordButton.addTarget(self, action: #selector(resendCode), for: .touchUpInside)
    }
    
    @objc
    func resendCode() {
        
        guard let phoneNumber = phoneNumber else {
            return
        }
        RepositoryClass.shared.verifyUser(with: phoneNumber, completion: { ( message ,done) in
            if done {
                
              
                print("This is a valid phone number ")
                self.alertMessageDialog(title: DialogTitleMessage().successTitle, message: "Code has been sent to this number \(phoneNumber)", actionTitle: "Okay")
            } else {
                
                
                self.alertError(message: message)
            }
        })
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        signInComponent.getConfirmSMSImage.snp.makeConstraints { (make) in
            
            make.centerX.equalTo(view)
            make.top.equalTo(view).offset(96)
            make.width.equalTo(view).dividedBy(2)
            make.height.equalTo(signInComponent.getConfirmSMSImage.snp.width)
            
        }
        
        signInComponent.loginLabel.snp.makeConstraints { (make) in
            make.top.equalTo(signInComponent.getConfirmSMSImage.snp.bottom).offset(16)
            make.centerX.equalTo(view)
        }
        
        signInComponent.confirmSMS.snp.makeConstraints { (make) in
            
            make.top.equalTo(signInComponent.loginLabel.snp.bottom).offset(16)
            make.width.equalTo(view).offset(-32)
            make.centerX.equalTo(view)
            
        }
        
        containview.snp.makeConstraints { (make) in
            
            make.top.equalTo(signInComponent.confirmSMS.snp.bottom).offset(16)
            make.height.equalTo(24)
            make.centerX.equalTo(view)
            make.width.equalTo(view).dividedBy(1.7)
        }
        
        signInComponent.firstCodeField.snp.makeConstraints { (make) in
            
            make.top.left.equalTo(containview)
            make.width.equalTo(containview).dividedBy(6.85)
            make.height.equalTo(containview)
            
        }
        
        signInComponent.secondCodeField.snp.makeConstraints { (make) in
            
            make.top.equalTo(containview)
            make.left.equalTo(signInComponent.firstCodeField.snp.right).offset(8)
            make.width.height.equalTo(signInComponent.firstCodeField)
        }
        
        signInComponent.thirdCodeField.snp.makeConstraints { (make) in
            
            make.top.equalTo(containview)
            make.left.equalTo(signInComponent.secondCodeField.snp.right).offset(8)
            make.width.height.equalTo(signInComponent.firstCodeField)
            
        }
        
        signInComponent.fourthCodeField.snp.makeConstraints { (make) in
            
            make.top.equalTo(containview)
            make.left.equalTo(signInComponent.thirdCodeField.snp.right).offset(8)
            make.width.height.equalTo(signInComponent.firstCodeField)
            
        }
        
        signInComponent.fifthCodeField.snp.makeConstraints { (make) in
            
            make.top.equalTo(containview)
            make.left.equalTo(signInComponent.fourthCodeField.snp.right).offset(8)
            make.width.height.equalTo(signInComponent.firstCodeField)
            
        }
        
        signInComponent.sixthCodeField.snp.makeConstraints { (make) in
            
            make.top.equalTo(containview)
            make.left.equalTo(signInComponent.fifthCodeField.snp.right).offset(8)
            make.width.height.equalTo(signInComponent.firstCodeField)
            
        }
        
        signInComponent.loginButton.snp.makeConstraints { (make) in
            
            make.top.equalTo(containview.snp.bottom).offset(16)
            make.centerX.equalTo(view)
            make.height.equalTo(48)
            
        }
        
        signInComponent.forgotPasswordButton.snp.makeConstraints { (make) in
            
            make.bottom.equalTo(view).offset(-16)
            make.centerX.equalTo(view)
            make.height.equalTo(48)
            
        }
        
        //
        
        
    }
}

