//
//  newvkPointViewController.swift
//  vKclub
//
//  Created by Heng Senghak on 11/30/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import Firebase
import MaterialComponents.MDCAlertController
import ProgressHUD

class newvkPointViewController: UIViewController {
    enum Role {
        case student
        case staff
        case customer
    }
    // var model: vkPointModel? = vkPointModel(imageUrl: "", point: 0)
    
    @IBOutlet weak var QRContainer: UIView! {
        didSet {
            QRContainer.backgroundColor = BaseColor.colorPrimary
            QRContainer.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    
    @IBOutlet weak var point: UILabel!
    
    lazy var QRCode: UIImageView = {
        let img = UIImageView()
        img.image = R.image.logo()?.withRenderingMode(.alwaysTemplate)
        img.contentMode = .scaleAspectFill
        img.backgroundColor = BaseColor.colorWhite
        img.layer.borderWidth = 10
        img.layer.borderColor = BaseColor.colorWhite.cgColor
        img.layer.cornerRadius = 5
        img.layer.masksToBounds = true
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    let refreshControl = UIRefreshControl()
    
}

extension newvkPointViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ProgressHUD.show()
        self.CustomNavigationBar(title: "vKpoint", isShadow: true, isPresentVC: false)
        pointLabelConfig()
        setupQRContainer()
        QRContainer.addSubview(QRCode)
        setupQRCode()
        
        
        refreshControl.addTarget(self, action: #selector(doSomething), for: .valueChanged)
        
        // Setup vkPoint Validation
        self.userNotLoginAlert()
        self.userEmailNotExistAlert()
        self.executionUserDataWithRole()
        self.point.text = "0 Points"
    }
    
    @objc func doSomething(refreshControl: UIRefreshControl) {
        if let userID = Auth.auth().currentUser?.uid {
            guard let currentUser = RepositoryClass.shared.getUserCore(userId: userID).first else {
                return
            }
            // Mark: Validate if user has email address
            self.APIExecutionProcess(currentUser: currentUser)
        }
        refreshControl.endRefreshing()
    }
    
    func userNotLoginAlert() {
        if Auth.auth().currentUser?.uid == nil {
            self.dialogPresentViewController(title: "Warning", message: "Please login in order to use vkPoint.", actionTitle: "Okay", viewController: LoginPhoneNumberViewController())
        }
    }
    
    func userEmailNotExistAlert(){
        if let userID = Auth.auth().currentUser?.uid {
            guard let currentUser = RepositoryClass.shared.getUserCore(userId: userID).first else {
                return
            }
            // Mark: Validate if user has email address
            if currentUser.email == nil || currentUser.email == "" {
                self.dialogPresentViewController(title: "", message: "Please register KIT email in order to use vkPoint.", actionTitle: "Okay", viewController: EditProfileViewController())
            }
        }
    }
    
    
    
    func userRoleByEmail(currentUser: UserProfileCore, completion: @escaping(_ success: Bool, _ role: Role)-> Void) {
        guard let email = currentUser.email else {
            return
        }
        // check if user is KITian or not.
        if email.hasSuffix("@kit.edu.kh") {
            let emailPrefix = String(email.split(separator: "@").first ?? "")
            do {
                let regex = try NSRegularExpression(pattern: "[\\w]+[\\d]{2}", options: [])
                let regexResult = regex.matches(in: emailPrefix, options: [], range: NSRange(emailPrefix.startIndex ..< emailPrefix.endIndex, in: emailPrefix))
                
                // register for student
                if regexResult != []{
                    // Mark: update user's role to "student"
                    RepositoryClass.shared.updateUserFirestoreSpecific(in: FIRCollectionReference.users, and: (Auth.auth().currentUser?.uid)!, forField: "role", forValue: "student") { (done) in
                        if done {
                            completion(true, .student)
                        }
                    }
                    // role = Role.student
                }else{
                    // Mark: update user's role to "staff"
                    RepositoryClass.shared.updateUserFirestoreSpecific(in: FIRCollectionReference.users, and: (Auth.auth().currentUser?.uid)!, forField: "role", forValue: "staff") { (done) in
                        if done {
                            completion(true, .staff)
                        }
                    }
                    // role = Role.staff
                }
            }catch{
                print("Error create regular expression to varify email")
            }
        }else{
            // role = Role.customer
            completion(true, .customer)
        }
    }
    
    func registerUserToVkPoint(currentUser: UserProfileCore, completion: @escaping(_ sucess: Bool, _ response: String) -> Void) {
        if let userEmail = currentUser.email, let userID = Auth.auth().currentUser?.uid {
            // Mark: Setup http request.
            var request = URLRequest(url: URL(string: "http://192.168.7.246:8000/student/create")!)
            request.httpMethod = "POST"
            request.addValue("vkPoint" ,forHTTPHeaderField: "client-name")
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpBody = "email=\(String(userEmail.lowercased()))&uid=\(String(userID))".data(using: .utf8)
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                if error != nil {
                    print("==> Request Error in local server.")
                } else {
                    do {
                        guard let data = data, error == nil else{
                            print ("error : \(error)")
                            return
                        }
                        // Mark: check for http errors
                        if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                            print("statusCode should be 200, but is \(httpStatus.statusCode)")
                            print("response = \(response)")
                        }
                        
                        let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any]
                        print("==> Recieved json data: \(json!)")
                        if json!["error"] as? String != nil {
                            completion(false, json!["error"] as! String)
                        }else if json!["success"] as! String == "Done" {
                            
                            let currentUser = UserProfileModel(email: currentUser.email!, name: currentUser.name!, pfpUrl: currentUser.pfpUrl!, presence: currentUser.presence, role: currentUser.role!, phoneNumber: currentUser.phoneNumber!, isVKpointUser: currentUser.isVKpointUser, vkclubNumber: currentUser.vkclubNumber!, vkPassword: currentUser.vkPassword!)
                            currentUser.isVKpointUser = true
                            RepositoryClass.shared.updateCurrentUserCore(userModel: currentUser)
                            RepositoryClass.shared.updateUserFirestore(for: currentUser, in: FIRCollectionReference.users, and: userID) { (done) in
                                if done {
                                    print("==> changed isVKpointUser to true")
                                    // Call reload vkpoint data function
                                    completion(true, "Congratulations, you have successfully registered to vkPoint.")
                                }
                            }
                        }else{
                            completion(false, "There is an error during registration to vkPoint. Please try again later.")
                        }
                    } catch {
                        print("error")
                    }
                }
            }
            task.resume()
        }
    }
    
    func isVKpointUser() -> Bool {
        let currentUser = RepositoryClass.shared.getUserCore(userId: (Auth.auth().currentUser?.uid)!).first
        return currentUser!.isVKpointUser
    }
    
    func APIExecutionProcess(currentUser: UserProfileCore){
        if isVKpointUser() {
            self.getVKPointData(currentUser: currentUser) { (point, response, done) in
                if done {
                    DispatchQueue.main.async(execute: {
                        self.point.text = "\(point) Points"
                        ChhayrithReusableFunction.shared.loadImageToCacheWithUrlString(urlString: response, cacheName: vkPointQRImageCache, completion: { (done, image) in
                            if done {
                                self.QRCode.image = image
                            }
                        })
                    })
                    ProgressHUD.dismiss()
                }else{
                    let alert = self.dialogMessage(title: "vkPoint Load Failed", message: "Unable to load vkPoint data right now. Please try again.")
                    let okayBtn = MDCAlertAction(title: DialogTitleMessage().okayTitle, handler: { (alert) in
                        self.navigationController?.popToRootViewController(animated: true)
                    })
                    alert.addAction(okayBtn)
                    ProgressHUD.dismiss()
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }else{
            self.registerUserToVkPoint(currentUser: currentUser) { (done, response) in
                if done {
                    let alert = self.dialogMessage(title: "Sign-up Successful", message: response)
                    let okayBtn = MDCAlertAction(title: DialogTitleMessage().okayTitle, handler: {(alert) in
                        self.APIExecutionProcess(currentUser: currentUser)
                    })
                    alert.addAction(okayBtn)
                    ProgressHUD.dismiss()
                    self.present(alert, animated: true, completion: nil)
                }else{
                    let alert = self.dialogMessage(title: "Sign-up Failed", message: response)
                    let okayBtn = MDCAlertAction(title: DialogTitleMessage().okayTitle, handler: { (alert) in
                        self.navigationController?.popToRootViewController(animated: true)
                    })
                    alert.addAction(okayBtn)
                    ProgressHUD.dismiss()
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func getVKPointData(currentUser: UserProfileCore, completion: @escaping(_ point: Int, _ imageUrl: String, _ success: Bool)-> Void){
        var request = URLRequest(url: URL(string: "http://192.168.7.246:8000/student/get")!)
        request.httpMethod = "POST"
        request.addValue("vkPoint" ,forHTTPHeaderField: "client-name")
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        if let userEmail = currentUser.email {
            request.httpBody = "email=\(userEmail.lowercased())".data(using: .utf8)
        }
        
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if error != nil {
                print("==> Request Error in local server.")
            } else {
                do {
                    guard let data = data, error == nil else {
                        print("==> error \(error)")
                        return
                    }
                    
                    let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:Any]
                    if json!["error"] as? String != nil {
                        completion(0, json!["error"] as! String, false)
                    }else{
                        let point = json!["point"] as! Int
                        var imageUrl = json!["imageUrl"] as! String
                        imageUrl = imageUrl.replacingOccurrences(of: "10.10.11.4", with: "192.168.7.246")
                        completion(point, imageUrl, true)
                    }
                } catch {
                    completion(0, "Unable to reach vkPoint server.", false)
                }
            }
        }
        task.resume()
    }
    
    func executionUserDataWithRole(){
        guard let currentUser = RepositoryClass.shared.getUserCore(userId: (Auth.auth().currentUser?.uid)!).first else {return}
        self.userRoleByEmail(currentUser: currentUser, completion: { (done, role) in
            if done {
                switch role {
                case .student:
                    self.APIExecutionProcess(currentUser: currentUser)
                    break
                case .staff:
                    self.APIExecutionProcess(currentUser: currentUser)
                    break
                case .customer:
                    ProgressHUD.dismiss()
                    self.dialogPresentViewController(title: "", message: "Please register KIT email in order to use vkPoint.", actionTitle: "Okay", viewController: EditProfileViewController())
                    break
                }
            }
        })
    }
    
    func setupQRContainer (){
        QRContainer.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        QRContainer.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        QRContainer.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        QRContainer.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 1/2).isActive = true
    }
    
    func setupQRCode(){
        QRCode.centerXAnchor.constraint(equalTo: QRContainer.centerXAnchor).isActive = true
        QRCode.centerYAnchor.constraint(equalTo: QRContainer.centerYAnchor).isActive = true
        QRCode.widthAnchor.constraint(equalToConstant: 170).isActive = true
        QRCode.heightAnchor.constraint(equalToConstant: 170).isActive = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    func pointLabelConfig() {
        point.layer.cornerRadius = 10.0
        point.layer.borderColor = UIColor.lightGray.cgColor
        
        point.layer.borderWidth = 1.0
        
    }
    
}

extension UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = 5
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = 5
        }
    }
}
