//
//  File.swift
//  vKclub
//
//  Created by Chhayrith on 12/17/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit

class vkPointModel {
    public let point: Int
    public let imageUrl: String
    public init (imageUrl: String = "", point: Int = 0){
        self.imageUrl = imageUrl
        self.point = point
    }
    
}
