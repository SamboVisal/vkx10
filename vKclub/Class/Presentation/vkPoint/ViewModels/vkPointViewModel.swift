//
//  vkPointViewModel.swift
//  vKclub
//
//  Created by Chhayrith on 12/17/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import Firebase
import MaterialComponents.MDCAlertController

class vkPointViewModel {
    private let vkPointModel: vkPointModel
    public init(vkPointModel: vkPointModel){
        self.vkPointModel = vkPointModel
    }
    
    
    public var vkpoint: String {
        return "\(self.vkPointModel.point) Points"
    }
    
    public var QRImage: UIImage {
       return UIImage().loadImageUsingWithUrlString( urlString: self.vkPointModel.imageUrl, defaultImage: R.image.logo()!, cacheName: vkPointQRImageCache)
    }
    
    public func configure(_ view: newvkPointViewController){
        view.point.text = self.vkpoint
        view.QRCode.image = QRImage
    }
}
