//
//  PhoneBookViewController.swift
//  vKclub
//
//  Created by Chhayrith on 12/3/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import Firebase
import MaterialComponents.MDCAlertController



class PhoneBookViewController: UIViewController, UISearchResultsUpdating, UITableViewDelegate, UITableViewDataSource {

    var resultSearchController = UISearchController(searchResultsController: nil)

    var userNotLoggedIn: UserNotLoggedIn?

    let CellId = "cellId"

    var contacts = [UserPhoneBookCore]()
    var filteredContact = [UserPhoneBookCore]()
    var searchActive: Bool = false


    lazy var tableView: UITableView = {
       let phonebook = UITableView()
        phonebook.translatesAutoresizingMaskIntoConstraints = false
        return phonebook
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.CustomNavigationBar(title: "Phone Book", isShadow: false, isPresentVC: false)

        view.addSubview(tableView)
        setupTableView()
        userNotLoggedIn = UserNotLoggedIn()
        userNotLoggedIn?.that = self
        hideKeyboardWhenTappedAround()
        
        setupSearchBar()
        
        tableView.tableHeaderView = resultSearchController.searchBar
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tableView.register(phonebookCell.self, forCellReuseIdentifier: CellId)
        tableView.separatorStyle = .none
    }

    func setupSearchBar(){
        resultSearchController.searchResultsUpdater = self
        resultSearchController.searchBar.placeholder = " Search"

        resultSearchController.searchBar.barStyle = UIBarStyle.default
        resultSearchController.searchBar.tintColor = BaseColor.colorLightDark
        resultSearchController.searchBar.sizeToFit()

        resultSearchController.searchBar.isTranslucent = true
        resultSearchController.hidesNavigationBarDuringPresentation = false
        resultSearchController.dimsBackgroundDuringPresentation = false
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Query data
        DispatchQueue.main.async {
            self.contacts = RepositoryClass.shared.getAllPhoneBookData().sorted(by: { (this: UserPhoneBookCore, that: UserPhoneBookCore) -> Bool in
                return this.name! < that.name!
            })
            self.tableView.reloadData()
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
    }
    
    func setupTableView() {
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }

    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return resultSearchController.searchBar.text?.isEmpty ?? true
    }

    func isFiltering() -> Bool {
        if searchBarIsEmpty() {
            filteredContact = []
        }
        return resultSearchController.isActive && !searchBarIsEmpty()
    }

    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }

    func filterContentForSearchText(_ searchText: String = "", scope: String = "All") {
        filteredContact = contacts.filter({ (contact) -> Bool in
            return (contact.name?.uppercased().contains(searchText.uppercased()))!
        })
        tableView.reloadData()
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return self.filteredContact.count
        }

        return contacts.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellId, for: indexPath) as! phonebookCell
        let contact: UserPhoneBookCore
        if isFiltering() {
            contact  = filteredContact[indexPath.row]
        }else {
            contact = contacts[indexPath.row]
        }

        cell.contactName.text = contact.name // == nil ? "No Name" : contacts[indexPath.row].name
        cell.vkNumber.text = contact.vkclubNumber // == nil ? "No Number" : contacts[indexPath.row].vkclubNumber
        
        print("==> Profile url \(contact.pfpUrl)")
        print("==> Profile url is nil: \(contact.pfpUrl != nil)")
        if contact.pfpUrl != nil {
            // cell.profileImage.image = getImageFromCache(pfpUrl: contact.pfpUrl)// contacts[indexPath.row].pfpUrl)
            cell.profileImage.loadImageUsingPhonebookImageCacheWithUrlString(contact.pfpUrl!)
        }else {
            cell.profileImage.image = R.image.detailprofileIcon()
        }
        cell.profileImage.image?.withRenderingMode(.alwaysTemplate)
        cell.profileImage.tintColor = BaseColor.colorPrimary


        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if Auth.auth().currentUser != nil {
            let alert: UIAlertController = UIAlertController(title: "", message: "Are you sure you want to call \(contacts[indexPath.row].name!)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
            alert.addAction(UIAlertAction(title: "Call", style: .default, handler: { action in alert.dismiss(animated: true, completion: nil)}))
            self.present(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Attention", message: "Please Login in order to call this contact.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
            alert.addAction(UIAlertAction(title: "Login", style: .default, handler: { (alert) in
                let loginController = UINavigationController(rootViewController: LoginViewController())
                self.present(loginController, animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }

        tableView.deselectRow(at: indexPath, animated: true)
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

}

extension PhoneBookViewController {
    func getImageFromCache(pfpUrl: String?) -> UIImage {
        if phonebookImageCache.object(forKey: pfpUrl! as AnyObject) != nil {
            return phonebookImageCache.object(forKey: pfpUrl! as AnyObject) as! UIImage
        }else{
            return UIImage(named: "detailprofile-icon")!
        }
    }
}

class phonebookCell: UITableViewCell {
    lazy var profileImage: UIImageView = {
        let image = UIImageView()
        image.layer.cornerRadius = 25
        image.layer.masksToBounds = true
        image.translatesAutoresizingMaskIntoConstraints = false
        // image.image = R.image.detailprofileIcon()
        image.contentMode = .scaleAspectFill
        return image
    }()
    lazy var contactName: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.fontSize = 15
        label.textColor = BaseColor.colorBlack
        return label
    }()
    lazy var vkNumber: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.fontSize = 13
        label.textColor = BaseColor.colorLightGray
        return label
    }()

    fileprivate func setupViews() {
        addSubview(profileImage)
        addSubview(contactName)
        addSubview(vkNumber)
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        setupViews()

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {

        profileImage.snp.makeConstraints { (make) in
            make.width.height.equalTo(50)
            make.leading.equalTo(self).offset(16)
            make.centerY.equalTo(self)
        }

        contactName.snp.makeConstraints { (make) in
            make.leading.equalTo(profileImage.snp.trailing).offset(24)
            make.top.equalTo(profileImage).offset(4)
        }

        vkNumber.snp.makeConstraints { (make) in
            make.top.equalTo(contactName.snp.bottom).offset(4)
            make.leading.equalTo(contactName)
        }
    }
}


