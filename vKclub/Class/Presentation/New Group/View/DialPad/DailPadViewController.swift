//
//  DailPadChildViewController.swift
//  vKclub
//
//  Created by Chhayrith on 11/18/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import AVFoundation
import MaterialComponents
import FirebaseAuth

class DailPadViewController: UIViewController, IndicatorInfoProvider, UITextFieldDelegate{
    
    var dailPhoneNumber: String = ""
    var lastCallNumber: String = ""
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(image: UIImage(named: "dialPad"))
    }
    // Keypad Outlet
    @IBOutlet weak var num1: MDCButton!
    @IBOutlet weak var num2: MDCButton!
    @IBOutlet weak var num3: MDCButton!
    @IBOutlet weak var num4: MDCButton!
    @IBOutlet weak var num5: MDCButton!
    @IBOutlet weak var num6: MDCButton!
    @IBOutlet weak var num7: MDCButton!
    @IBOutlet weak var num8: MDCButton!
    @IBOutlet weak var num9: MDCButton!
    @IBOutlet weak var call: MDCButton!
    @IBOutlet weak var num0: MDCButton!
    @IBOutlet weak var delete: MDCButton!
    let userCoreDataInstance = UserProfileCoreData()
    @IBOutlet weak var numberTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        PermissionAppUser.AskAudioPermission()
        //numberTextField.isUserInteractionEnabled = false
        numberTextField.delegate = self
        numberTextField.inputView = UIView()
        customizeBtnStyle()
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(deletedAll))
        delete.addGestureRecognizer(longPress)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("Text begin editing")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        return updatedText.count <= 9
    }
    @objc
    func deletedAll()  {
        
        print("user doing long press")
        if !(numberTextField.text?.isEmpty)! && !dailPhoneNumber.isEmpty {
            numberTextField.text = ""
            dailPhoneNumber = ""
        }
        
    }
    
    func customizeBtnStyle() {
        let btnScheme = MDCButtonScheme()
        let typographyScheme = MDCTypographyScheme()
        let btnArray: Array<MDCButton> = [num1, num2, num3, num4, num5, num6, num7, num8, num9, num0, delete, call]
        
        colorKeyPad(btnName: btnArray, typographyScheme: typographyScheme, btnScheme: btnScheme)
    }
    
    func colorKeyPad(btnName: Array<MDCButton> = [], typographyScheme: MDCTypographyScheme, btnScheme: MDCButtonScheme){
        for btn in btnName {
            btnScheme.cornerRadius = 0
            
            MDCTextButtonThemer.applyScheme(btnScheme, to: btn)
            
            btn.inkColor = UIColor(red:1.00, green:1.00, blue:1.00, alpha:0.2)
            btn.backgroundColor = BaseColor.colorPrimary
            typographyScheme.button = UIFont.systemFont(ofSize: 25)
            MDCButtonTypographyThemer.applyTypographyScheme(typographyScheme, to: btn)
            btn.setTitleColor(BaseColor.colorWhite, for: .normal)
        }
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        print("View will appear of dail pad")
        
    }
    
    @IBAction func NumberButtonClicked(_ sender: Any) {
        
        dailPhoneNumber = dailPhoneNumber + ((sender as AnyObject).titleLabel??.text)!
        print("Number clicked \(dailPhoneNumber)")
        print("Number of input \(dailPhoneNumber.count)")
        numberTextField.text = dailPhoneNumber
        
    }
    
    @IBAction func DeleteNumberButtonClicked(_ sender: Any) {
        
        if dailPhoneNumber.count > 0 {
            
            let lastIndex = dailPhoneNumber.index(before: dailPhoneNumber.endIndex)
            dailPhoneNumber = String(dailPhoneNumber[..<lastIndex])
            numberTextField.text = dailPhoneNumber
        }
        
    }
    
    
    @IBAction func CallButtonClicked(_ sender: Any) {
        
        if PermissionAppUser.CheckAudioPermission() {
            
            if LinphoneManager.CheckLinphoneConnectionStatus() {
                
                guard let userID = Auth.auth().currentUser?.uid else {
                    return
                }
                let vkNumber = RepositoryClass.shared.getUserCore(userId: userID)
                
                if dailPhoneNumber.count != 0 {
                    InComeCallController.phoneNumber = dailPhoneNumber
                    lastCallNumber = dailPhoneNumber
                    
                    if dailPhoneNumber.count > 10 {
                        self.alertError(message: "This not a valid phone number.")
                        return
                    }
                    for i in vkNumber {
                        
                        if let number = i.vkclubNumber {
                            if dailPhoneNumber == number {
                                self.alertError(message: "You are about to call to your own ID. Please choose another ID and try again.")
                                return
                            }
                        }
                        
                    }
                    
                    InComeCallController.dialPhone = true
                    InComeCallController.dialPhoneNumber = dailPhoneNumber
                    InComeCallController.callToFlag = true
                    LinphoneManager.makeCall(phoneNumber : dailPhoneNumber)
                   
//                    self.present(InComeCallController(), animated: false, completion: {
//
//                        
//
//                    })
                    
                } else if lastCallNumber.count != 0 {
                    
                    print("This is Last called number \(lastCallNumber)")
                    numberTextField.text = lastCallNumber
                    dailPhoneNumber = lastCallNumber
                }
                
            } else {
                
                self.alertError(message: "Our network is not available at a moment. Please try again later.")
                
                print("Not connect to linphone")
                
            }
            
        } else {
            
            print("Not granted permission")
            PermissionAppUser.AskAudioPermission()
        }
        
    }
    
    
    
    
    
}
