//
//  EditContactViewModel.swift
//  vKclub
//
//  Created by Chhayrith on 12/12/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit

class EditContactViewModel {
    private var contactModel: ContactModel
    
    public init(contactModel: ContactModel){
        self.contactModel = contactModel
    }
    
    public func profileImage() -> UIImage{
        if self.contactModel.ProfileImgUrl != "" {
            return contactProfileImgCache.object(forKey: contactModel.ProfileImgUrl as AnyObject) == nil ? R.image.detailprofileIcon()! : contactProfileImgCache.object(forKey: contactModel.ProfileImgUrl as AnyObject) as! UIImage
        }else {
            return R.image.detailprofileIcon()!
        }
    }
    
    public var name: String {
        return contactModel.Name!
    }
    
    public var vkNumber: String {
        return contactModel.Number!
    }
    
    public var email: String {
        return contactModel.Email!
    }
    
    public var address: String {
        return contactModel.Address!
    }
    
    public var note: String {
        return contactModel.Note!
    }
    
    // Configure Function
    public func configure(_ view: EditContactController){
        view.profileImage.image = profileImage()
        view.nameTextField.text = name
        view.numberTextField.text = vkNumber
        view.emailTextField.text = email
        view.addressTextField.text = address
        view.noteTextField.text = note
    }
}
