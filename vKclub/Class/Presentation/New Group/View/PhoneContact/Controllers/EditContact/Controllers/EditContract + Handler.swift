//
//  AddContactHandler.swift
//  vKclub
//
//  Created by Chhayrith on 12/10/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import Firebase
import MaterialComponents.MDCAlertController
import ProgressHUD

extension EditContactController {
    @objc func handleSelectProfileImageView() {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        
        present(picker, animated: true, completion: nil)
    }
    
    @objc func handleEditContact() {
        // hideKeyboardWhenTappedAround()
        
        // Validate user input
        if self.nameTextField.text! == "" || self.nameTextField.text == nil {
            let alert = dialogMessage(title: "Warning", message: "Please make sure you have already input contact name.")
            let okayBtn = MDCAlertAction(title: DialogTitleMessage().okayTitle, handler: nil)
            alert.addAction(okayBtn)
            self.present(alert, animated: true, completion: nil)
        }else if self.numberTextField.text! == "" || self.numberTextField.text == nil {
            let alert = dialogMessage(title: "Warning", message: "Please make sure you have already input contact number.")
            let okayBtn = MDCAlertAction(title: DialogTitleMessage().okayTitle, handler: nil)
            alert.addAction(okayBtn)
            self.present(alert, animated: true, completion: nil)
        }else if Int(self.numberTextField.text!) == nil {
            let alert = dialogMessage(title: "Warning", message: "Please input only number in vkclub number field.")
            let okayBtn = MDCAlertAction(title: DialogTitleMessage().okayTitle, handler: nil)
            alert.addAction(okayBtn)
            self.present(alert, animated: true, completion: nil)
            
        }else {
            ProgressHUD.show("Saving Contact", interaction: false)
            var storageRef = Storage.storage().reference().child("users")
            if let userId = Auth.auth().currentUser?.uid,
                let contactId:String = contactModel?.id  {
                
                storageRef = storageRef.child(userId).child("contacts").child(contactId).child("contactProfilImage.jpg")
                if let contactProfileImage = self.profileImage.image, let uploadData = UIImageJPEGRepresentation(contactProfileImage, 0.3){
                    
                    storageRef.putData(uploadData, metadata: nil, completion: { (_, err) in
                        if let error = err {
                            print(error)
                            return
                        }
                        
                        storageRef.downloadURL(completion: { (url, err) in
                            if let err = err {
                                print(err)
                                return
                            }
                            
                            guard let url = url else { return }
                            let oneContact = ContactModel(
                                Name: self.nameTextField.text!,
                                Number: self.numberTextField.text!,
                                Email: self.emailTextField.text! ,
                                Address: self.addressTextField.text ?? "",
                                Note: self.noteTextField.text ?? "",
                                ProfileImgUrl: url.absoluteString)
                            
                            FIRFireStoreService.shared.editOneContact(for: oneContact, get: contactId, in: FIRCollectionReference.users, and: FIRCollectionReference.contacts) { (done) in
                                if done {
                                    print("==> contact edited to DB !!!!")
                                    ProgressHUD.dismiss()
                                    let alert = self.dialogMessage(title: "Contact Edited!", message: "")
                                    let okayBtn = MDCAlertAction(title: DialogTitleMessage().okayTitle) { (alert) in
                                        self.dismiss(animated: true, completion: nil)
                                    }
                                    alert.addAction(okayBtn)
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    print("==> ERR editing contact")
                                }
                            }
                            // self.registerUserIntoDatabaseWithUID(uid, values: values as [String : AnyObject])
                        })
                    })
                }
            }
        }
    }
    
    @objc func cancelEditContact() {
        // hideKeyboardWhenTappedAround()
        let alert = dialogMessage(title: "Are you sure to cancel editing this contact?", message: "")
        let cancelBtn = MDCAlertAction(title: DialogTitleMessage().cancelTitle, handler: nil)
        let okayBtn = MDCAlertAction(title: DialogTitleMessage().okayTitle) { (alert) in
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(okayBtn)
        alert.addAction(cancelBtn)
        self.present(alert, animated: true, completion: nil)
    }
    
}
