//
//  AddContactController.swift
//  vKclub
//
//  Created by Chhayrith on 12/10/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit

class AddContactController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    public var userID: String = ""
    
    let containerView: UIView = {
        let vw = UIView()
        vw.backgroundColor = BaseColor.colorPrimary
        vw.translatesAutoresizingMaskIntoConstraints = false
        return vw
    }()
    
    public let profileImage: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleToFill
        img.image = R.image.detailprofileIcon()?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        img.tintColor = BaseColor.colorWhite
        img.backgroundColor = BaseColor.colorAccentDark
        img.layer.cornerRadius = 62.5
        img.layer.borderColor = BaseColor.colorPrimary.cgColor
        img.layer.borderWidth = 3
        img.layer.masksToBounds = true
        img.translatesAutoresizingMaskIntoConstraints = false
        
        return img
    }()
    
    public let textFieldContainerView: UIView = {
        let vw = UIView()
        vw.backgroundColor = BaseColor.colorWhite
        vw.translatesAutoresizingMaskIntoConstraints = false
        return vw
    }()
    
    public let nameTextFieldRow: UIView = {
        let vw = UIView()
        vw.backgroundColor = BaseColor.colorWhite
        vw.translatesAutoresizingMaskIntoConstraints = false
        return vw
    }()
    
    public let nameIcon: UIImageView = {
       let icon = UIImageView()
        icon.image = R.image.userBlack()
        icon.contentMode = .scaleAspectFill
        icon.translatesAutoresizingMaskIntoConstraints = false
        return icon
    }()
    
    public let nameTextField: UITextField = {
       let textField = UITextField()
        textField.text = ""
        textField.placeholder = "Name (Required)"
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    public let nameDivider: UIView = {
       let line = UIView()
        line.backgroundColor = BaseColor.colorLightGray
        line.translatesAutoresizingMaskIntoConstraints = false
        return line
    }()
    
    public let numberTextFieldRow: UIView = {
        let vw = UIView()
        vw.backgroundColor = BaseColor.colorWhite
        vw.translatesAutoresizingMaskIntoConstraints = false
        return vw
    }()
    
    public let numberIcon: UIImageView = {
        let icon = UIImageView()
        icon.image = R.image.phonebook()
        icon.contentMode = .scaleAspectFill
        icon.translatesAutoresizingMaskIntoConstraints = false
        return icon
    }()
    
    public let numberTextField: UITextField = {
        let textField = UITextField()
        textField.text = ""
        textField.placeholder = "vkClub Number (Required)"
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    public let numberDivider: UIView = {
        let line = UIView()
        line.backgroundColor = BaseColor.colorLightGray
        line.translatesAutoresizingMaskIntoConstraints = false
        return line
    }()
    
    public let emailTextFieldRow: UIView = {
        let vw = UIView()
        vw.backgroundColor = BaseColor.colorWhite
        vw.translatesAutoresizingMaskIntoConstraints = false
        return vw
    }()
    
    public let emailIcon: UIImageView = {
        let icon = UIImageView()
        icon.image = R.image.mailBlank()
        icon.contentMode = .scaleAspectFill
        icon.translatesAutoresizingMaskIntoConstraints = false
        return icon
    }()
    
    public let emailTextField: UITextField = {
        let textField = UITextField()
        textField.text = ""
        textField.placeholder = "Email"
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    public let emailDivider: UIView = {
        let line = UIView()
        line.backgroundColor = BaseColor.colorLightGray
        line.translatesAutoresizingMaskIntoConstraints = false
        return line
    }()
    
    public let addressTextFieldRow: UIView = {
        let vw = UIView()
        vw.backgroundColor = BaseColor.colorWhite
        vw.translatesAutoresizingMaskIntoConstraints = false
        return vw
    }()
    
    public let addressIcon: UIImageView = {
        let icon = UIImageView()
        icon.image = R.image.markerBlack()
        icon.contentMode = .scaleAspectFill
        icon.translatesAutoresizingMaskIntoConstraints = false
        return icon
    }()
    
    public let addressTextField: UITextField = {
        let textField = UITextField()
        textField.text = ""
        textField.placeholder = "Address"
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    public let addressDivider: UIView = {
        let line = UIView()
        line.backgroundColor = BaseColor.colorLightGray
        line.translatesAutoresizingMaskIntoConstraints = false
        return line
    }()
    
    public let noteTextFieldRow: UIView = {
        let vw = UIView()
        vw.backgroundColor = BaseColor.colorWhite
        vw.translatesAutoresizingMaskIntoConstraints = false
        return vw
    }()
    
    public let noteIcon: UIImageView = {
        let icon = UIImageView()
        icon.image = R.image.note()
        icon.contentMode = .scaleAspectFill
        icon.translatesAutoresizingMaskIntoConstraints = false
        return icon
    }()
    
    public let noteTextField: UITextField = {
        let textField = UITextField()
        textField.text = ""
        textField.placeholder = "Short note"
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    public let noteDivider: UIView = {
        let line = UIView()
        line.backgroundColor = BaseColor.colorLightGray
        line.translatesAutoresizingMaskIntoConstraints = false
        return line
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        self.CustomNavigationBar(title: "Add Contact", isShadow: true, isPresentVC: false)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: R.image.doneIcon(), style: .done, target: self, action: #selector(handleCreateContact))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: R.image.clear(), style: .done, target: self, action: #selector(cancelAddContact))
        
        if userID != "" {
            let viewModel = AddContactViewModel(userID: userID)
            viewModel.configure(self)
        }
        
        setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupView(){
        view.backgroundColor = BaseColor.colorWhite
        view.addSubview(containerView)
        setupContainerView()
        view.addSubview(profileImage)
        setupProfileImage()
        view.addSubview(textFieldContainerView)
        setupTextFieldContainerView()
    }
    
    func setupContainerView() {
        containerView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        containerView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        containerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        containerView.heightAnchor.constraint(equalToConstant: 150).isActive = true
    }
    
    func setupProfileImage () {
        profileImage.isUserInteractionEnabled = true
        profileImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleSelectProfileImageView)))
        profileImage.widthAnchor.constraint(equalToConstant: 125).isActive = true
        profileImage.heightAnchor.constraint(equalToConstant: 125).isActive = true
        profileImage.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        profileImage.centerYAnchor.constraint(equalTo: containerView.centerYAnchor, constant: 75).isActive = true
    }
    
    func setupTextFieldContainerView(){
        textFieldContainerView.addSubview(nameTextFieldRow)
        textFieldContainerView.addSubview(numberTextFieldRow)
        textFieldContainerView.addSubview(emailTextFieldRow)
        textFieldContainerView.addSubview(addressTextFieldRow)
        textFieldContainerView.addSubview(noteTextFieldRow)
        inputConstraint()
        
        textFieldContainerView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        textFieldContainerView.topAnchor.constraint(equalTo: profileImage.bottomAnchor, constant: 20).isActive = true
        textFieldContainerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        textFieldContainerView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    func inputConstraint(){
        nameTextFieldRow.addSubview(nameIcon)
        nameTextFieldRow.addSubview(nameTextField)
        nameTextFieldRow.addSubview(nameDivider)
        nameTextFieldRow.snp.makeConstraints { (make) in
            make.top.equalTo(textFieldContainerView).offset(4)
            make.trailing.equalTo(textFieldContainerView)
            make.leading.equalTo(textFieldContainerView)
            make.height.equalTo(50)
        }
        nameIcon.snp.makeConstraints { (make) in
            make.leading.equalTo(nameTextFieldRow).offset(16)
            make.centerY.equalTo(nameTextFieldRow).inset(8)
            make.height.equalTo(24)
            make.width.equalTo(24)
        }
        nameTextField.snp.makeConstraints { (make) in
            make.leading.equalTo(nameIcon).offset(32)
            make.trailing.equalTo(nameTextFieldRow).inset(16)
            make.bottom.equalTo(nameIcon)
        }
        nameDivider.snp.makeConstraints { (make) in
            make.leading.equalTo(nameIcon)
            make.top.equalTo(nameTextFieldRow.snp.bottom)
            make.trailing.equalTo(nameTextFieldRow)
            make.height.equalTo(1)
        }
        
        numberTextFieldRow.addSubview(numberIcon)
        numberTextFieldRow.addSubview(numberTextField)
        numberTextFieldRow.addSubview(numberDivider)
        numberTextFieldRow.snp.makeConstraints { (make) in
            make.top.equalTo(nameTextFieldRow.snp.bottom).offset(4)
            make.trailing.equalTo(textFieldContainerView)
            make.leading.equalTo(textFieldContainerView)
            make.height.equalTo(50)
        }
        numberIcon.snp.makeConstraints { (make) in
            make.leading.equalTo(numberTextFieldRow).offset(16)
            make.centerY.equalTo(numberTextFieldRow).inset(8)
            make.height.equalTo(24)
            make.width.equalTo(24)
        }
        numberTextField.snp.makeConstraints { (make) in
            make.leading.equalTo(numberIcon).offset(32)
            make.trailing.equalTo(numberTextFieldRow).inset(16)
            make.bottom.equalTo(numberIcon)
        }
        numberDivider.snp.makeConstraints { (make) in
            make.leading.equalTo(numberIcon)
            make.top.equalTo(numberTextFieldRow.snp.bottom)
            make.trailing.equalTo(numberTextFieldRow)
            make.height.equalTo(1)
        }
        
        emailTextFieldRow.addSubview(emailIcon)
        emailTextFieldRow.addSubview(emailTextField)
        emailTextFieldRow.addSubview(emailDivider)
        emailTextFieldRow.snp.makeConstraints { (make) in
            make.top.equalTo(numberTextFieldRow.snp.bottom).offset(4)
            make.trailing.equalTo(textFieldContainerView)
            make.leading.equalTo(textFieldContainerView)
            make.height.equalTo(50)
        }
        emailIcon.snp.makeConstraints { (make) in
            make.leading.equalTo(emailTextFieldRow).offset(16)
            make.centerY.equalTo(emailTextFieldRow).inset(8)
            make.height.equalTo(24)
            make.width.equalTo(24)
        }
        emailTextField.snp.makeConstraints { (make) in
            make.leading.equalTo(emailIcon).offset(32)
            make.trailing.equalTo(emailTextFieldRow).inset(16)
            make.bottom.equalTo(emailIcon)
        }
        emailDivider.snp.makeConstraints { (make) in
            make.leading.equalTo(emailIcon)
            make.top.equalTo(emailTextFieldRow.snp.bottom)
            make.trailing.equalTo(emailTextFieldRow)
            make.height.equalTo(1)
        }
        
        addressTextFieldRow.addSubview(addressIcon)
        addressTextFieldRow.addSubview(addressTextField)
        addressTextFieldRow.addSubview(addressDivider)
        addressTextFieldRow.snp.makeConstraints { (make) in
            make.top.equalTo(emailTextFieldRow.snp.bottom).offset(4)
            make.trailing.equalTo(textFieldContainerView)
            make.leading.equalTo(textFieldContainerView)
            make.height.equalTo(50)
        }
        addressIcon.snp.makeConstraints { (make) in
            make.leading.equalTo(addressTextFieldRow).offset(16)
            make.centerY.equalTo(addressTextFieldRow).inset(8)
            make.height.equalTo(24)
            make.width.equalTo(24)
        }
        addressTextField.snp.makeConstraints { (make) in
            make.leading.equalTo(addressIcon).offset(32)
            make.trailing.equalTo(addressTextFieldRow).inset(16)
            make.bottom.equalTo(addressIcon)
        }
        addressDivider.snp.makeConstraints { (make) in
            make.leading.equalTo(addressIcon)
            make.top.equalTo(addressTextFieldRow.snp.bottom)
            make.trailing.equalTo(addressTextFieldRow)
            make.height.equalTo(1)
        }
        
        noteTextFieldRow.addSubview(noteIcon)
        noteTextFieldRow.addSubview(noteTextField)
        noteTextFieldRow.addSubview(noteDivider)
        noteTextFieldRow.snp.makeConstraints { (make) in
            make.top.equalTo(addressTextFieldRow.snp.bottom).offset(4)
            make.trailing.equalTo(textFieldContainerView)
            make.leading.equalTo(textFieldContainerView)
            make.height.equalTo(50)
        }
        noteIcon.snp.makeConstraints { (make) in
            make.leading.equalTo(noteTextFieldRow).offset(16)
            make.centerY.equalTo(noteTextFieldRow).inset(8)
            make.height.equalTo(24)
            make.width.equalTo(24)
        }
        noteTextField.snp.makeConstraints { (make) in
            make.leading.equalTo(noteIcon).offset(32)
            make.trailing.equalTo(noteTextFieldRow).inset(16)
            make.bottom.equalTo(noteIcon)
        }
        noteDivider.snp.makeConstraints { (make) in
            make.leading.equalTo(noteIcon)
            make.top.equalTo(noteTextFieldRow.snp.bottom)
            make.trailing.equalTo(noteTextFieldRow)
            make.height.equalTo(1)
        }
    }

}
