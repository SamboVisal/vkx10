//
//  AddContactViewModel.swift
//  vKclub
//
//  Created by Chhayrith on 12/10/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit

class AddContactViewModel {
    private var userID: String
    private var userDetail: ContactModel
    
    public init(userID: String){
        self.userID = userID
        self.userDetail = RepositoryClass.shared.getOnePhoneBookData(userId: userID)
    }
    
    public func profileImage() -> UIImage{
        var profileImage: UIImage
        if self.userDetail.ProfileImgUrl != "" {
            profileImage = phonebookImageCache.object(forKey: self.userDetail.ProfileImgUrl as AnyObject) as! UIImage
            // self.image = (profileImage as? UIImage)!
            return profileImage
        }else {
            return R.image.detailprofileIcon()!
        }
    }
    
    public var name: String {
        return userDetail.Name!
    }
    
    public var vkNumber: String {
        return userDetail.Number!
    }
    
    public var email: String {
        return userDetail.Email!
    }
    
    public var address: String {
        return userDetail.Address!
    }
    
    public var note: String {
        return userDetail.Note!
    }
    
    // Configure Function
    public func configure(_ view: AddContactController){
        view.profileImage.image = profileImage()
        view.nameTextField.text = name
        view.numberTextField.text = vkNumber
        view.emailTextField.text = email
        view.addressTextField.text = address
        view.noteTextField.text = note
    }
}
