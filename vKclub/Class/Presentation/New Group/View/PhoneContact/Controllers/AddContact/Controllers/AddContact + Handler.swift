//
//  AddContactHandler.swift
//  vKclub
//
//  Created by Chhayrith on 12/10/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import Firebase
import MaterialComponents.MDCAlertController
import ProgressHUD

extension AddContactController {
    @objc func handleSelectProfileImageView() {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        
        present(picker, animated: false, completion: nil)
    }
    
    @objc func handleCreateContact() {
        // hideKeyboardWhenTappedAround()
        
        // Validate user input
        if self.nameTextField.text! == "" || self.nameTextField.text == nil {
            let alert = dialogMessage(title: "Warning", message: "Please make sure you have already input contact name.")
            let okayBtn = MDCAlertAction(title: DialogTitleMessage().okayTitle, handler: nil)
            alert.addAction(okayBtn)
            self.present(alert, animated: true, completion: nil)
        }else if self.numberTextField.text! == "" || self.numberTextField.text == nil {
            let alert = dialogMessage(title: "Warning", message: "Please make sure you have already input contact number.")
            let okayBtn = MDCAlertAction(title: DialogTitleMessage().okayTitle, handler: nil)
            alert.addAction(okayBtn)
            self.present(alert, animated: true, completion: nil)
        }else if Int(self.numberTextField.text!) == nil {
            let alert = dialogMessage(title: "Warning", message: "Please input only number in vkclub number field.")
            let okayBtn = MDCAlertAction(title: DialogTitleMessage().okayTitle, handler: nil)
            alert.addAction(okayBtn)
            self.present(alert, animated: true, completion: nil)
        }else {
            ProgressHUD.show("Adding Contact", interaction: false)
            var storageRef = Storage.storage().reference().child("users")
            if let userId = Auth.auth().currentUser?.uid,
                let contactId:String? = db.collection("users").document(userId).collection("contacts").document().documentID  {
                
                storageRef = storageRef.child(userId).child("contacts").child(contactId!).child("contactProfilImage.jpg")
                if let contactProfileImage = self.profileImage.image, let uploadData = UIImageJPEGRepresentation(contactProfileImage, 0.4){
                    
                    storageRef.putData(uploadData, metadata: nil, completion: { (_, err) in
                        if let error = err {
                            print(error)
                            return
                        }
                        
                        storageRef.downloadURL(completion: { (url, err) in
                            if let err = err {
                                print(err)
                                return
                            }
                            
                            guard let url = url else { return }
                            let oneContact = ContactModel(
                                Name: self.nameTextField.text!,
                                Number: self.numberTextField.text!,
                                Email: self.emailTextField.text! ,
                                Address: self.addressTextField.text ?? "",
                                Note: self.noteTextField.text ?? "",
                                ProfileImgUrl: url.absoluteString)
                            
                            FIRFireStoreService.shared.addOneContact(for: oneContact, in: FIRCollectionReference.users, and: FIRCollectionReference.contacts) { (done) in
                                if done {
                                    print("contact added to DB !!!!")
                                    ProgressHUD.dismiss()
                                    let alert = self.dialogMessage(title: "Contact Added!", message: "")
                                    let okayBtn = MDCAlertAction(title: DialogTitleMessage().okayTitle) { (alert) in
                                        self.dismiss(animated: true, completion: nil)
                                    }
                                    alert.addAction(okayBtn)
                                    self.present(alert, animated: true, completion: nil)
                                }else{
                                    print("ERR adding contact")
                                }
                            }
                            // self.registerUserIntoDatabaseWithUID(uid, values: values as [String : AnyObject])
                        })
                    })
                }
            }
        }
    }
    
    @objc func cancelAddContact() {
        // hideKeyboardWhenTappedAround()
        let alert = dialogMessage(title: "Are you sure to cancel adding this contact?", message: "")
        let cancelBtn = MDCAlertAction(title: DialogTitleMessage().cancelTitle, handler: nil)
        let okayBtn = MDCAlertAction(title: DialogTitleMessage().okayTitle) { (alert) in
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(okayBtn)
        alert.addAction(cancelBtn)
        self.present(alert, animated: true, completion: nil)
    }
}
