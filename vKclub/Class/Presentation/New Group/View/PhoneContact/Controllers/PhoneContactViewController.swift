//
//  PhoneContactChilldViewController.swift
//  vKclub
//
//  Created by Chhayrith on 11/18/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Firebase

//class PhoneContactViewController: UIViewController, IndicatorInfoProvider, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating {
class PhoneContactViewController: UIViewController, IndicatorInfoProvider, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(image: UIImage(named: "user"))
    }
  
    let searchController = UISearchController(searchResultsController: nil)
    
   
    @IBOutlet weak var tableView: UITableView!
    var contacts = [ContactModel]()
    var filteredContact = [ContactModel]()
    
    let userId = Auth.auth().currentUser?.uid
    
    let cellId = "cellId"
    
    
    let addContact: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(R.image.addUser(), for: .normal)
        btn.backgroundColor = BaseColor.colorAccentDark
        btn.tintColor = BaseColor.colorWhite
        btn.addTarget(self, action: #selector(handleAddContact), for: .touchUpInside)
        btn.layer.cornerRadius = 30
        btn.layer.masksToBounds = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        setupSearchBar()
        view.addSubview(addContact)
        addContact.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        addContact.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -60).isActive = true
        addContact.widthAnchor.constraint(equalToConstant: 60).isActive = true
        addContact.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tableView.register(phoneConactCell.self, forCellReuseIdentifier: cellId)
        tableView.separatorStyle = .none
    }
    
    func setupSearchBar(){
        searchController.searchResultsUpdater = self
        searchController.searchBar.placeholder = " Search"
        
        searchController.searchBar.barStyle = UIBarStyle.default
        searchController.searchBar.tintColor = BaseColor.colorWhite
        searchController.searchBar.sizeToFit()
        
        searchController.searchBar.isTranslucent = true
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        tableView.tableHeaderView = searchController.searchBar
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        queryDataFromFireBase()
    }
    
    // Hasn't called yet
    private func queryDataFromFireBase() {
        FIRFireStoreService.shared.readContactListInfo(in: FIRCollectionReference.users, and: FIRCollectionReference.contacts, returning: ContactModel.self) { (contacts, done) in
            
            if done {
                self.contacts = contacts
                DispatchQueue.main.async(execute: {
                    contactCache.setObject(contacts as AnyObject, forKey: contactCacheKey(userId: self.userId!) as AnyObject)
                    
                    self.tableView?.reloadData()
                })
            }else{
                DispatchQueue.main.async {
                    self.tableView?.reloadData()
                }
            }
        }
    }
    
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func isFiltering() -> Bool {
        if searchBarIsEmpty() {
            filteredContact = []
        }
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
    
    func filterContentForSearchText(_ searchText: String = "", scope: String = "All") {
        filteredContact = contacts.filter({ (contact) -> Bool in
            return (contact.Name?.uppercased().contains(searchText.uppercased()))!
            // return (contact.?.uppercased().contains(searchText.uppercased()))!
        })
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return filteredContact.count
        }
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! phoneConactCell
        
        let contact: ContactModel?
        if isFiltering() {
            contact = filteredContact[indexPath.row]
        }else{
            contact = contacts[indexPath.row]
        }
        
        cell.profileImage.loadImageUsingContactProfileImgCacheWithUrlString((contact?.ProfileImgUrl!)!)
        cell.contactName.text = contact?.Name
        cell.vkNumber.text = contact?.Number
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userContactVC = self.storyboard?.instantiateViewController(withIdentifier: "UserContactViewController") as! UserContactViewController
        userContactVC.contact = contacts[indexPath.row]
        let next = UINavigationController(rootViewController: userContactVC)
        tableView.deselectRow(at: indexPath, animated: true)
        present(next, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

class phoneConactCell: UITableViewCell {
    
    lazy var profileImage: UIImageView = {
        let img = UIImageView()
        img.image = R.image.detailprofileIcon()?.withRenderingMode(.alwaysTemplate)
        img.tintColor = BaseColor.colorPrimary
        img.layer.cornerRadius = 25
        img.layer.masksToBounds = true
        return img
    }()
    
    lazy var contactName: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.fontSize = 15
        label.textColor = BaseColor.colorBlack
        return label
    }()
    lazy var vkNumber: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.fontSize = 13
        label.textColor = BaseColor.colorLightGray
        return label
    }()
    
    fileprivate func setupViews() {
        addSubview(profileImage)
        addSubview(contactName)
        addSubview(vkNumber)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        
        profileImage.snp.makeConstraints { (make) in
            make.width.height.equalTo(50)
            make.leading.equalTo(self).offset(16)
            make.centerY.equalTo(self)
        }
        
        contactName.snp.makeConstraints { (make) in
            make.leading.equalTo(profileImage.snp.trailing).offset(24)
            make.top.equalTo(profileImage).offset(4)
        }
        
        vkNumber.snp.makeConstraints { (make) in
            make.top.equalTo(contactName.snp.bottom).offset(4)
            make.leading.equalTo(contactName)
        }
    }
    
    
}

