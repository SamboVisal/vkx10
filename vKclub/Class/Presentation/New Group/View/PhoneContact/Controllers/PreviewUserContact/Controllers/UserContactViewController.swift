//
//  UserContactViewController.swift
//  vKclub
//
//  Created by Heng Senghak on 11/30/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import Firebase


//class customCell: UITableViewCell {}


struct iconAndEmail {
    var icon: String?
    var label: String?
    init(icon: String, label: String){
        self.icon = icon
        self.label = label
    }
}

class UserContactViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var contact: ContactModel?
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    
    let profileImageBackground: UIView = {
        let vw = UIView()
        vw.backgroundColor = BaseColor.colorPrimary
        
        vw.translatesAutoresizingMaskIntoConstraints = false
        return vw
    }()
    
    let contactProfile: UIImageView! = {
        let contactProfile = UIImageView()
        contactProfile.contentMode = .scaleAspectFit
        contactProfile.backgroundColor = BaseColor.colorPrimary
        contactProfile.translatesAutoresizingMaskIntoConstraints = false
        return contactProfile
    }()
    
    let contactName: UILabel = {
       let name = UILabel()
        name.font = UIFont(name: "Roboto-Bold", size: 20)
        name.textColor = .gray
        name.translatesAutoresizingMaskIntoConstraints = false
        return name
    }()
    
    let editContact: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(R.image.editIcon(), for: .normal)
        btn.backgroundColor = BaseColor.colorAccentDark
        btn.tintColor = BaseColor.colorWhite
        btn.addTarget(self, action: #selector(handleEditContact), for: .touchUpInside)
        btn.layer.cornerRadius = 30
        btn.layer.masksToBounds = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    
    var contactInfo = [iconAndEmail]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.CustomNavigationBar(title: (contact?.Name)!, isShadow: true, isPresentVC: true)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: R.image.delete(), style: .plain, target: self, action: #selector(handleDeleteContact))
        
        appendToContactInfo()

        profileImageBackground.addSubview(contactProfile)
        view.addSubview(profileImageBackground)
        view.addSubview(contactName)
        view.addSubview(editContact)
        setupProfileImageBackground()
        setupContactProfileImage ()
        setupContactName()
        setupTableView()
        setupEditContact()
        
        tableView?.tableFooterView = UIView()
        tableView?.delegate = self
        tableView?.dataSource = self
    }
    
    func appendToContactInfo() {
        self.contactInfo.append(iconAndEmail(icon: "phoneBlack", label: (self.contact?.Number)!))
        self.contactInfo.append(iconAndEmail(icon: "mailBlank", label: (self.contact?.Email)!))
        self.contactInfo.append(iconAndEmail(icon: "markerBlack", label: (self.contact?.Address)!))
        self.contactInfo.append(iconAndEmail(icon: "note", label: (self.self.contact?.Note)!))
    }
    
    func setupProfileImageBackground(){
        profileImageBackground.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        profileImageBackground.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        profileImageBackground.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        profileImageBackground.heightAnchor.constraint(equalToConstant: 300).isActive = true
    }
    
    func setupContactProfileImage () {
        contactProfile.loadImageUsingContactProfileImgCacheWithUrlString((contact?.ProfileImgUrl)!)
        contactProfile.centerXAnchor.constraint(equalTo: profileImageBackground.centerXAnchor).isActive = true
        contactProfile.centerYAnchor.constraint(equalTo: profileImageBackground.centerYAnchor).isActive = true
        contactProfile.widthAnchor.constraint(equalTo: profileImageBackground.widthAnchor).isActive = true
        contactProfile.heightAnchor.constraint(equalTo: profileImageBackground.heightAnchor).isActive = true
    }
    
    func setupContactName() {
        contactName.text = contact?.Name
        contactName.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        contactName.topAnchor.constraint(equalTo: profileImageBackground.bottomAnchor, constant: 16).isActive = true
        contactName.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -32).isActive = true
        contactName.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
    
    func setupTableView() {
        tableView.isScrollEnabled = false
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: contactName.bottomAnchor, constant:8).isActive = true
        tableView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        tableView.heightAnchor.constraint(equalToConstant: 347).isActive = true
    }
    
    func setupEditContact(){
        editContact.snp.makeConstraints { (make) in
            make.trailing.equalTo(self.view).inset(24)
            make.bottom.equalTo(self.view).inset(24)
            make.width.height.equalTo(60)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath)
        
        let viewContainter: UIView = {
           let vw = UIView(frame: CGRect(x: 0, y: 0, width: cell.frame.width, height: cell.frame.height))
            vw.translatesAutoresizingMaskIntoConstraints = false
            return vw
        }()
        
        let iconImage: UIImageView = {
            let img = UIImageView()
            img.image = UIImage(named: contactInfo[indexPath.row].icon!)
            img.layer.masksToBounds = true
            img.contentMode = .scaleAspectFill
            img.translatesAutoresizingMaskIntoConstraints = false
            return img
        }()
        
        let labelText: UILabel = {
            let name = UILabel()
            name.text = contactInfo[indexPath.row].label!
            name.translatesAutoresizingMaskIntoConstraints = false
            return name
        }()
        
        viewContainter.addSubview(labelText)
        viewContainter.addSubview(iconImage)
        
        iconImage.leftAnchor.constraint(equalTo: viewContainter.leftAnchor, constant: 25).isActive = true
        iconImage.topAnchor.constraint(equalTo: viewContainter.topAnchor, constant: 10).isActive = true
        iconImage.widthAnchor.constraint(equalToConstant: 20).isActive = true
        iconImage.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        labelText.leftAnchor.constraint(equalTo: iconImage.rightAnchor, constant: 35).isActive = true
        labelText.topAnchor.constraint(equalTo: viewContainter.topAnchor, constant: 10).isActive = true
        labelText.widthAnchor.constraint(equalTo: viewContainter.widthAnchor, constant: -75).isActive = true
        labelText.heightAnchor.constraint(equalTo: viewContainter.heightAnchor).isActive = true
        
        cell.addSubview(viewContainter)
        
        if indexPath.row == 0 {
            cell.isUserInteractionEnabled = true
        }else{
            cell.isUserInteractionEnabled = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alert: UIAlertController = UIAlertController(title: "Internal Call Service", message: "Call to \(contact?.Name!)?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in alert.dismiss(animated: true, completion: nil)}))
        self.present(alert, animated: true, completion: nil)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
