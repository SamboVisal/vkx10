//
//  PreviewContact + Handler.swift
//  vKclub
//
//  Created by Chhayrith on 12/11/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import Firebase
import MaterialComponents.MDCAlertController

extension UserContactViewController {
    @objc func handleEditContact() {
        let editContactVC = EditContactController()
        editContactVC.contactModel = contact
        let next = UINavigationController(rootViewController: editContactVC)
        present(next, animated: true, completion: nil)
    }
    
    @objc func handleDeleteContact() {
        let alert = dialogMessage(title: "Are you sure to delete this contact?", message: "")
        let cancelBtn = MDCAlertAction(title: DialogTitleMessage().cancelTitle, handler: nil)
        let okayBtn = MDCAlertAction(title: DialogTitleMessage().okayTitle) { (delete) in
            self.deleteThisContact()
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(okayBtn)
        alert.addAction(cancelBtn)
        self.present(alert, animated: true, completion: nil)
    }
    
    func deleteThisContact() {
        FIRFireStoreService.shared.deleteOneContact(get: (contact?.id)!, in: FIRCollectionReference.users, and: FIRCollectionReference.contacts) { (done) in
            if done {
                print("==> Done delete one contact")
            }
        }
    }
}

