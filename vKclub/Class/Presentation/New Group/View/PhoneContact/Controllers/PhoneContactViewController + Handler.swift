//
//  PhoneContactViewController + Handler.swift
//  vKclub
//
//  Created by Chhayrith on 12/1/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import Firebase

extension PhoneContactViewController {
    @objc func handleAddContact() {
        // let addContactVC = self.storyboard?.instantiateViewController(withIdentifier: "AddContactViewController") as! AddContactViewController
        let next = UINavigationController(rootViewController: AddContactController())
        present(next, animated: true, completion: nil)
        
    }
}
