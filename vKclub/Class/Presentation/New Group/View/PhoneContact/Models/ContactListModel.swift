//
//  ContactListModel.swift
//  vKclub
//
//  Created by Chhayrith on 11/30/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import Foundation
struct ContactModel: Codable, Identifiable {
    var id: String? = nil
    public let  Name: String?
    public let Number: String?
    public let  Email: String?
    public let  Address: String?
    public let  Note: String?
    public let  ProfileImgUrl: String?
    
    public init(Name: String, Number: String, Email: String = "", Address: String = "", Note: String = "",ProfileImgUrl: String = ""){
        self.Name = Name
        self.Number = Number
        self.Email = Email
        self.Address = Address
        self.Note = Note
        self.ProfileImgUrl = ProfileImgUrl
    }
}

class ContactListModel {
    
}
