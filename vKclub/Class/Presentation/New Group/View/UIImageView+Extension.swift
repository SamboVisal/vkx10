//
//  UIImageView+Extension.swift
//  vKclub
//
//  Created by Chhayrith on 12/1/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit

extension UIImageView {
    func loadImageUsingContactProfileImgCacheWithUrlString(_ urlString: String = "") {
        
        self.image = R.image.detailprofileIcon()?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        self.tintColor = BaseColor.colorPrimary // nil
        
        //check cache for image first
        if let cachedImage = contactProfileImgCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = cachedImage
            print("image load from Cache \(contactProfileImgCache)")
            return
        }
        
        //otherwise fire off a new download
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            
            //download hit an error so lets return out
            if error != nil {
                print(error ?? "")
                return
            }
            
            DispatchQueue.main.async(execute: {
                
                if let downloadedImage = UIImage(data: data!) {
                    contactProfileImgCache.setObject(downloadedImage, forKey: urlString as AnyObject)
                    print("image load from Server")
                    self.image = downloadedImage
                }
            })
            
        }).resume()
    }
    
    func loadImageUsingPhonebookImageCacheWithUrlString(_ urlString: String = "") {
        
        self.image = R.image.detailprofileIcon()?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        self.tintColor = BaseColor.colorPrimary // nil
        
        //check cache for image first
        if let cachedImage = phonebookImageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = cachedImage
            print("image load from Cache \(contactProfileImgCache)")
            return
        }
        
        //otherwise fire off a new download
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            
            //download hit an error so lets return out
            if error != nil {
                print(error ?? "")
                return
            }
            
            DispatchQueue.main.async(execute: {
                
                if let downloadedImage = UIImage(data: data!) {
                    phonebookImageCache.setObject(downloadedImage, forKey: urlString as AnyObject)
                    print("image load from Server")
                    self.image = downloadedImage
                }
            })
            
        }).resume()
    }
    
    func loadImageUsingWithUrlString(urlString: String = "", defaultImage: UIImage, cacheName: NSCache<AnyObject, AnyObject>) {
        
        self.image = defaultImage.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        
        //check cache for image first
        if let cachedImage = cacheName.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = cachedImage
            print("image load from Cache \(contactProfileImgCache)")
            return
        }
        
        //otherwise fire off a new download
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            
            //download hit an error so lets return out
            if error != nil {
                print(error ?? "")
                return
            }
            
            DispatchQueue.main.async(execute: {
                
                if let downloadedImage = UIImage(data: data!) {
                    cacheName.setObject(downloadedImage, forKey: urlString as AnyObject)
                    print("image load from Server")
                    self.image = downloadedImage
                }
            })
            
        }).resume()
    }
}


