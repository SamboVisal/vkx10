import UIKit
import Firebase
import AVFoundation
import SnapKit

extension ScanQRViewController {
    func setupQRScanner() {
        captureDevice = AVCaptureDevice.default(for: .video)
        // Check if captureDevice returns a value and unwrap it
        if let captureDevice = captureDevice {
            
            do {
                let input = try AVCaptureDeviceInput(device: captureDevice)
                captureSession.addInput(input)
                captureSession.addOutput(captureMetadataOutput)
                captureMetadataOutput.setMetadataObjectsDelegate(self, queue: .main)
                captureMetadataOutput.metadataObjectTypes = [.qr] //AVMetadataObject.ObjectType
                
                videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                videoPreviewLayer?.videoGravity = .resizeAspectFill
                videoPreviewLayer?.borderColor = BaseColor.colorWhite.cgColor
                videoPreviewLayer?.borderWidth = 3
                videoPreviewLayer?.frame = CGRect(
                    x: view.frame.minX + 32,
                    y: view.frame.minY + 100,
                    width: view.frame.width - 64,
                    height: view.frame.width - 64
                )
                view.layer.addSublayer(videoPreviewLayer!)
                
            } catch {
                print("Error Device Input")
            }
            
        }
    }
    
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects.count == 0 {
            print("No Input Detected")
            return
        }else{
            // Stop capture session
            videoPreviewLayer?.isHidden = true
            self.videoPreviewLayer?.isHidden = true
            self.captureSession.stopRunning()
        }
        
        let metadataObject = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        
        if let QRCodeValue = metadataObject.stringValue {
            print("Idscanned: \(QRCodeValue) ")
            isQRValid = RepositoryClass.shared.checkPhoneBookUserIsValid(userId: QRCodeValue)
            if isQRValid! {
                let addContactVC = AddContactController()
                addContactVC.userID = QRCodeValue
                let addContact = UINavigationController(rootViewController: addContactVC)
                self.present(addContact, animated: false, completion: nil)
                
            }else{
                print("==> USER QRCODE IS NOT VALID")
                let alert: UIAlertController = UIAlertController(title: "", message: "Invalid QR Code \n Please verify that the code is still valid and try again.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    // Start capturing QRCode again
                    self.videoPreviewLayer?.isHidden = false
                    self.videoPreviewLayer?.isHidden = false
                    self.captureSession.startRunning()
                    alert.dismiss(animated: true, completion: nil)}
                ))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}
