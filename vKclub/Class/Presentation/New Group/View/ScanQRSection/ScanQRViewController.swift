//
//  ScanQRViewController.swift
//  vKclub
//
//  Created by Chhayrith on 12/5/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Firebase
import EFQRCode
import AVFoundation
import SnapKit


class ScanQRViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate, IndicatorInfoProvider {
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "QR Code Reader")
    }
    // QRCode Scanner
    var captureDevice:AVCaptureDevice?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    let captureSession = AVCaptureSession()
    let captureMetadataOutput = AVCaptureMetadataOutput()
    
    
    var userId: String?
    var isQRValid: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //navigationItem.title = "Scanner"
        view.backgroundColor = BaseColor.colorBlack
        
        setupQRScanner()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool){
        captureSession.startRunning()
        videoPreviewLayer?.isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
}
