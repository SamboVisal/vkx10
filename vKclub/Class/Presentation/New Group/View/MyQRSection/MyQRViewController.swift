//
//  MyQRViewController.swift
//  vKclub
//
//  Created by Chhayrith on 12/5/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Firebase
import EFQRCode
import SnapKit

class MyQRViewController: UIViewController, IndicatorInfoProvider {
    var QRsize: EFIntSize = EFIntSize(width: 1024, height: 1024)

    let QRImage: UIImageView = {
        let img = UIImageView()
        
        return img
    }()
    var magnification: EFIntSize? = EFIntSize(width: 9, height: 9)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = BaseColor.colorBlack
        if let userId = Auth.auth().currentUser?.uid {
            let QR = EFQRCodeGenerator(content: userId, size: EFIntSize(width: 300, height: 300))
            QR.setIcon(icon: UIImage(named: "Company")?.toCGImage(), size: EFIntSize(width: 100, height: 100))
            QRImage.image = UIImage(cgImage: QR.generate()!)
        }
        
        
        view.addSubview(QRImage)
        QRImage.snp.makeConstraints { (make) in
            make.center.equalTo(self.view)
        }
        
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "My QR Code")
    }
    
    
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}
