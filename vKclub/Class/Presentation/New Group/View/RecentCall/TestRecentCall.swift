//
//  TestRecentCall.swift
//  vKclub
//
//  Created by Pisal on 11/29/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import CoreData



struct recentCallVar {
    
    var callerID: String?
    var nameUser: String?
    var imageName: String?
    var timeStamp: String?
}

class TestRecentCallController: UITableViewController , IndicatorInfoProvider{
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(image: UIImage(named: "history"))
        
    }
    lazy var noRecentCall: UILabel = {
       
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "No Recents"
        label.font = UIFont.systemFont(ofSize: 22)
        return label
    }()
    let (todayYear, todayMonth, todayDate, todayHour, todayMinute, todaySec) = UIComponentHelper.GetTodayString()
    
    let callDataRequest: NSFetchRequest<SipCallData> = SipCallData.fetchRequest()
    var callLogData = [SipCallData]()
    let cellId = "Cellid"
    let incomingCallInstance = InComeCallController()
    let recentContent = [
        
        recentCallVar(callerID: "800000011", nameUser: "Cheat", imageName: "out-goingcall", timeStamp: "1:00pm"),
        recentCallVar(callerID: "800000012", nameUser: "Wan", imageName: "misscall", timeStamp: "2:20pm"),
        recentCallVar(callerID: "800000013", nameUser: "Chhayrith", imageName: "misscall", timeStamp: "3:20pm"),
        recentCallVar(callerID: "800000014", nameUser: "Senghak", imageName: "out-goingcall", timeStamp: "4:20pm"),
        recentCallVar(callerID: "800000011", nameUser: "Cheat", imageName: "out-goingcall", timeStamp: "1:00pm"),
        recentCallVar(callerID: "800000012", nameUser: "Wan", imageName: "misscall", timeStamp: "2:20pm"),
        recentCallVar(callerID: "800000013", nameUser: "Chhayrith", imageName: "out-goingcall", timeStamp: "3:20pm"),
        recentCallVar(callerID: "800000014", nameUser: "Senghak", imageName: "misscall", timeStamp: "4:20pm")
    
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        tableView.addSubview(noRecentCall)
        noRecentCall.snp.makeConstraints { (make) in
            make.center.equalTo(tableView)
        }
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .singleLine
        tableView.separatorInset.right = tableView.separatorInset.left
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
        tableView.register(TestRecentCell.self, forCellReuseIdentifier: cellId)
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.loadData()
    }
    func loadData() {
        
        RepositoryClass.shared.getRecentCallData { (data,done) in
            
            if done {
                self.callLogData = data
                self.tableView.reloadData()
            }
            
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if callLogData.count == 0 {
            
            noRecentCall.isHidden = false
            
        } else {
            noRecentCall.isHidden = true
        }
        
        
        return callLogData.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! TestRecentCell
        
        // start from the last row
        let lastRow: Int = self.tableView.numberOfRows(inSection: 0) - (indexPath.row + 1)
        let reverseIndexPath = IndexPath(row: lastRow, section: 0)
        
        let calldataItem = callLogData[reverseIndexPath.row]
        print("====== This is callLog data \(calldataItem) =======")
        
        cell.callerID.textColor = UIColor.black
        cell.callerID.text = calldataItem.callerName != "" ? calldataItem.callerName : calldataItem.callerID
        
        if let imageName = calldataItem.callIndicatorIcon {
            cell.imageCall.image = UIImage(named: imageName)
            cell.imageCall.image = cell.imageCall.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            if imageName != "misscall" {
                cell.imageCall.tintColor = BaseColor.colorPrimary
            } else {
                
                cell.callerID.textColor = .red
                
                cell.imageCall.tintColor = .red
            }
            
        }
        
        
//        cell.timeStamp.text = calldataItem.callDuration == "" ? calldataItem.callerID : "\(calldataItem.callerID!) (\(calldataItem.callDuration!) mn)"
        
        
        let _timeLogArray = calldataItem.callLogTime?.components(separatedBy: "-")
        //if the call log data date is today
        
        if todayYear == _timeLogArray?[0] && todayMonth == _timeLogArray?[1] && todayDate == _timeLogArray?[2] {
            cell.timeStamp.text = (_timeLogArray?[3])! + ":" + (_timeLogArray?[4])!
        } else {
            //if the call log data date is not today
            cell.timeStamp.text = (_timeLogArray?[1])! + "/" + (_timeLogArray?[2])! + "/" + (_timeLogArray?[0])!
        }
        
        
        
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        //start index path from last index instead
        let lastRow: Int = self.tableView.numberOfRows(inSection: 0) - (indexPath.row + 1)
        let reverseIndexPath = IndexPath(row: lastRow, section: 0)
        
        //Handle delete cell
        if editingStyle == .delete {
            handleDeleteCallData(_cellIndex: indexPath, _reverseIndex: reverseIndexPath)
        }
        
    }
    
    func handleDeleteCallData(_cellIndex: IndexPath, _reverseIndex: IndexPath) {
        
        let calldata = callLogData[_reverseIndex.row]
        callLogData.remove(at: _reverseIndex.row)
        do {
            managedObjectContext.delete(calldata)
            try managedObjectContext.save()
        } catch {
            print("CAN\'T SAVE IN CELL DELETION \(error.localizedDescription)")
        }
        self.tableView.deleteRows(at: [_cellIndex], with: .automatic)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let lastRow: Int = self.tableView.numberOfRows(inSection: 0) - (indexPath.row + 1)
        let reverseIndexPath = IndexPath(row: lastRow, section: 0)
        
        let recentCallData = callLogData[reverseIndexPath.row]
        if let vkNumber = recentCallData.callerID {
    
            if LinphoneManager.CheckLinphoneConnectionStatus() {

                InComeCallController.phoneNumber = vkNumber
                InComeCallController.dialPhone = true
                InComeCallController.dialPhoneNumber = vkNumber
                InComeCallController.callToFlag = true
                LinphoneManager.makeCall(phoneNumber : vkNumber)
                print("===== vknumber \(vkNumber)")
                
            }
            
        }
        
        
    }
    
}

class TestRecentCell: UITableViewCell {
    
    lazy var imageCall: UIImageView = {
       
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFill
        
        
        return image
    }()
    lazy var callerID: UILabel = {
       
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "No Call Data"
        label.fontSize = 13
        label.textColor = .gray
        return label
    }()
    lazy var nameUser: UILabel = {
       
        let name = UILabel()
        name.translatesAutoresizingMaskIntoConstraints = false
        name.text = "Unknown"
        name.fontSize = 16
        return name
    }()
    lazy var timeStamp: UILabel = {
       
        let time = UILabel()
        time.translatesAutoresizingMaskIntoConstraints = false
        time.fontSize = 13
        time.textColor = .gray
        return time
        
    }()
    
    lazy var cellDivider: UIView = {
       let vw = UIView()
        vw.backgroundColor = BaseColor.colorLightGray
        vw.translatesAutoresizingMaskIntoConstraints = false
        return vw
    }()
    
    fileprivate func setupViews() {
        addSubview(imageCall)
        addSubview(nameUser)
        addSubview(callerID)
        addSubview(timeStamp)
        //addSubview(cellDivider)
        
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        
        imageCall.snp.makeConstraints { (make) in
            make.width.height.equalTo(30)
           
            make.leading.equalTo(self).offset(16)
            make.centerY.equalTo(self)
           
        }
        
        nameUser.snp.makeConstraints { (make) in
            
            make.leading.equalTo(imageCall.snp.trailing).offset(32)
            make.top.equalTo(self).offset(8)
            
        }
        callerID.snp.makeConstraints { (make) in
            
            make.top.equalTo(nameUser.snp.bottom).offset(8)
            make.leading.equalTo(nameUser)
        }
        
        timeStamp.snp.makeConstraints { (make) in
            
            make.trailing.equalTo(self).offset(-16)
            make.top.equalTo(callerID)
            
        }
        
//        cellDivider.snp.makeConstraints { (make) in
//            make.bottom.equalTo(self.snp.bottom).offset(-1)
//            make.height.equalTo(0.5)
//            make.width.equalTo(self)
//        }
        
    }
    
    
}

