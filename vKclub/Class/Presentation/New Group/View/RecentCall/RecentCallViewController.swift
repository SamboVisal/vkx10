//
//  RecentCallChildViewController.swift
//  vKclub
//
//  Created by Chhayrith on 11/18/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import CoreData
import DZNEmptyDataSet

class RecentCallCell: UITableViewCell {
    
    @IBOutlet weak var callIndicatorIcon: UIImageView!
    
    @IBOutlet weak var callLogTimeLabel: UILabel!
    
    
    @IBOutlet weak var callerID: UILabel!
    
    @IBOutlet weak var timeStampLabel: UILabel!
}

class RecentCallViewController: UIViewController, IndicatorInfoProvider , UITableViewDelegate, UITableViewDataSource{
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(image: UIImage(named: "history"))

    }
    
    @IBOutlet weak var tableViews: UITableView!
    static let callDataRequest: NSFetchRequest<SipCallData> = SipCallData.fetchRequest()
    static var callLogData = [SipCallData]()
    static var tableViewRef = UITableView()
    let (todayYear, todayMonth, todayDate, todayHour, todayMinute, todaySec) = UIComponentHelper.GetTodayString()
    var incomingCallInstance = InComeCallController()
    var currentCallLogBtnHeight = 0;
    var callLogBtnColor = "#218154"
    var switchCallLogBtnColor = false {
        didSet {
            if switchCallLogBtnColor == true {
                callLogBtnColor = "#539C64"
            } else {
                callLogBtnColor = "#218154"
            }
        }
        
    }
    var callLogDataCount = 0
    
    lazy var noRecentCall: UILabel = {
       
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "No Recent"

        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        view.addSubview(noRecentCall)
        noRecentCall.snp.makeConstraints { (make) in
            make.center.equalTo(view)
        }
        
        print("This is recent call")
        self.tableViews.delegate = self
        RecentCallViewController.tableViewRef.delegate = self
        
        RecentCallViewController.LoadCallDataCell()
        
    }
    
    static func LoadCallDataCell() {
        do {
            callLogData = try managedObjectContext.fetch(callDataRequest)
            tableViewRef.reloadData()
            
            print(callLogData.count, "==COUNTINLOAD=")
        } catch {
            print("Could not get call data from CoreData \(error.localizedDescription) ===")
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //Get table view reference
        RecentCallViewController.tableViewRef = tableView
        
        if RecentCallViewController.callLogData.count == 0 {
            //Display no recents
            noRecentCall.isHidden = false
        } else {
            //Hide it back
            noRecentCall.isHidden = true
        }
        
        //noRecentCall.isHidden = true
        return 2
        //return RecentCallViewController.callLogData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let recentCallCell = tableView.dequeueReusableCell(withIdentifier: "RecentCallTableView", for: indexPath) as! RecentCallCell
        
 
        
        //start index path from last index instead
        let lastRow: Int = self.tableViews.numberOfRows(inSection: 0) - (indexPath.row + 1)
        let reverseIndexPath = IndexPath(row: lastRow, section: 0)
        
        //No call data yet
        if RecentCallViewController.callLogData.count == 0 {
            recentCallCell.callerID.textColor = UIColor(hexString: "#AAAAAA", alpha: 1)
            recentCallCell.callerID.text = "No call data"
            recentCallCell.callIndicatorIcon.image = UIImage(named: "detailprofile-icon")
            recentCallCell.callIndicatorIcon.tintColor = UIColor(hexString: "#AAAAAA", alpha: 1)
            recentCallCell.timeStampLabel.text = ""
            recentCallCell.callLogTimeLabel.text = ""
        } else {
            //Loading call log data
            let callLogDataItem = RecentCallViewController.callLogData[reverseIndexPath.row]
            if callLogDataItem.callerID != nil {
                recentCallCell.callerID.textColor = UIColor.black
                recentCallCell.callerID.text = callLogDataItem.callerName != "" ? callLogDataItem.callerName : callLogDataItem.callerID
                recentCallCell.callIndicatorIcon.image = UIImage(named: callLogDataItem.callIndicatorIcon!)
                recentCallCell.callIndicatorIcon.tintColor = UIColor(hexString: "#008040", alpha: 1)
                
                recentCallCell.timeStampLabel.text = callLogDataItem.callDuration == "" ? callLogDataItem.callerID : "\(callLogDataItem.callerID!) (\(callLogDataItem.callDuration!) mn)"
                
                
                let _timeLogArray = callLogDataItem.callLogTime?.components(separatedBy: "-")
                //if the call log data date is today
                if todayYear == _timeLogArray?[0] && todayMonth == _timeLogArray?[1] && todayDate == _timeLogArray?[2] {
                    recentCallCell.callLogTimeLabel.text = (_timeLogArray?[3])! + ":" + (_timeLogArray?[4])!
                } else {
                    //if the call log data date is not today
                    recentCallCell.callLogTimeLabel.text = (_timeLogArray?[1])! + "/" + (_timeLogArray?[2])! + "/" + (_timeLogArray?[0])!
                }
                
            }
        }
        
        //Switch button color
        //        self.switchCallLogBtnColor = !self.switchCallLogBtnColor
        //        recentCallCell.backgroundColor = UIColor(hexString: callLogBtnColor, alpha: 1)
        
        return recentCallCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //start index path from last index instead
        let lastRow: Int = self.tableViews.numberOfRows(inSection: 0) - (indexPath.row + 1)
        let reverseIndexPath = IndexPath(row: lastRow, section: 0)
        
        //cellIndex for removing table view cell, reverseIndex for removing coredata value since we reversed data in view
        PresentActionSheet(_cellIndex: indexPath, _reverseIndex: reverseIndexPath)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        //start index path from last index instead
        let lastRow: Int = self.tableViews.numberOfRows(inSection: 0) - (indexPath.row + 1)
        let reverseIndexPath = IndexPath(row: lastRow, section: 0)
        
        //Handle delete cell
        if editingStyle == .delete {
            HandleDeleteCallTableCell(_cellIndex: indexPath, _reverseIndex: reverseIndexPath)
        }
        
    }
    
    func HandleDeleteCallTableCell(_cellIndex: IndexPath, _reverseIndex: IndexPath) {
        let callLogDataItem = RecentCallViewController.callLogData[_reverseIndex.row]
        RecentCallViewController.callLogData.remove(at: _reverseIndex.row)
        do {
            managedObjectContext.delete(callLogDataItem)
            try managedObjectContext.save()
        } catch {
            print("CAN\'T SAVE IN CELL DELETION \(error.localizedDescription)")
        }
        self.tableViews.deleteRows(at: [_cellIndex], with: .automatic)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    func PresentActionSheet(_cellIndex: IndexPath, _reverseIndex: IndexPath) {
        let callLogDataItem = RecentCallViewController.callLogData[_reverseIndex.row]
        
        let actionSheet = UIAlertController(title: "Actions", message: nil, preferredStyle: .actionSheet)
        let dialBtnHandler = UIAlertAction(title: "Dial", style: .default, handler: {(action) -> Void in
            InComeCallController.phoneNumber = callLogDataItem.callerID!
            InComeCallController.callToFlag = true
            LinphoneManager.makeCall(phoneNumber: callLogDataItem.callerID!)
        })
        let cancelBtnHandler = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        actionSheet.addAction(dialBtnHandler)
        actionSheet.addAction(cancelBtnHandler)
        actionSheet.addAction(UIAlertAction(title: "Clear all", style: .default, handler: {test in
            SipCallDataClass.deleteAllData(entity: "SipCallData")
            RecentCallViewController.LoadCallDataCell()
            print("Done delete ---")
        }))
        if let popoverController =  actionSheet.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        
        self.present(actionSheet, animated: true)
        
    }
}



