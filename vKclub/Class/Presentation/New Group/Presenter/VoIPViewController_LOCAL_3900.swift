//
//  VoIPViewController.swift
//  vKclub
//
//  Created by Chhayrith on 11/18/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class VoIPViewController: ButtonBarPagerTabStripViewController{
    override func viewDidLoad() {
        self.pagerMenuBarCustomization()
        super.viewDidLoad()
        self.customNavigation()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func customNavigation() {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.title = "Internal Phone Call"
        self.navigationController?.navigationBar.barTintColor = UIColor(red:0.08, green:0.56, blue:0.38, alpha:1.0)
        // self.navigationController?.navigationBar.heightAnchor.constraint(equalToConstant: 50).isActive = true
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
    }	
    
    // Mark- Menu Bar configuration
    func pagerMenuBarCustomization() {
        settings.style.buttonBarBackgroundColor = UIColor(red:0.08, green:0.56, blue:0.38, alpha:1.0)
        settings.style.buttonBarItemBackgroundColor = UIColor(red:0.08, green:0.56, blue:0.38, alpha:1.0)
        
        settings.style.buttonBarItemFont = UIFont(name: "Helvetica", size: 10.0)!
        settings.style.buttonBarItemTitleColor = .gray
        
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        
        settings.style.selectedBarHeight = 3.0
        settings.style.selectedBarBackgroundColor = UIColor.white
        
        
        
        
        // Changing item text color on swipe
        
        
        changeCurrentIndexProgressive = {  (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            //oldCell?.label.textColor = .gray
            oldCell?.imageView.tintColor = .white
            //newCell?.label.textColor = UIColor(red:0.08, green:0.56, blue:0.38, alpha:1.0)
            newCell?.imageView.tintColor = UIColor(red:0.08, green:0.56, blue:0.38, alpha:1.0)
            newCell?.imageView.sizeThatFits(CGSize(width: 12, height: 12))
        }
    }
    
    
    
    // Mark- Menu Bar's tab configuration
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        let child_1 = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RecentCallViewController") as! RecentCallViewController
        let child_2 = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DailPadViewController") as! DailPadViewController
        let child_3 = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PhoneContactViewController") as! PhoneContactViewController
        
        return [child_1, child_2, child_3]
    }
    
    
    
}
