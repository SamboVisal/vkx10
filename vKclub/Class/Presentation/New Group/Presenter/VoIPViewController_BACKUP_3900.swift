//
//  VoIPViewController.swift
//  vKclub
//
//  Created by Chhayrith on 11/18/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class VoIPViewController: ButtonBarPagerTabStripViewController{
    var userNumber: String = "11223300"
    
    
    override func viewDidLoad() {
        self.pagerMenuBarCustomization()
        super.viewDidLoad()
        self.customNavigation()
        
        if let navigationBar = self.navigationController?.navigationBar {
            let greenDot = CGRect(x: 16, y: navigationBar.frame.height/2 - 8, width: 16, height: 16)
            let number = CGRect(x: 45, y: 0, width: navigationBar.frame.width/2, height: navigationBar.frame.height)
            let navDivider = CGRect(x: 0, y: navigationBar.frame.height, width: navigationBar.frame.width, height: 1)
            
            let firstLabel = UIView(frame: greenDot)
            firstLabel.backgroundColor = BaseColor.colorAccentLight
            firstLabel.layer.cornerRadius = 8
            firstLabel.layer.masksToBounds = true
            firstLabel.layer.shadowColor = BaseColor.colorLightDark.cgColor
            firstLabel.layer.shadowOpacity = 1
            firstLabel.layer.shadowOffset = CGSize.zero
            firstLabel.layer.shadowRadius = 8
            
            let secondLabel = UILabel(frame: number)
            secondLabel.text = userNumber
            secondLabel.textColor = BaseColor.colorWhite
            secondLabel.font = UIFont(name: "Roboto-Bold", size: 20)
            
            let thirdLabel = UIView(frame: navDivider)
            thirdLabel.backgroundColor = BaseColor.colorAccentDark
            
            navigationBar.addSubview(firstLabel)
            navigationBar.addSubview(secondLabel)
            navigationBar.addSubview(thirdLabel)
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    private func customNavigation() {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = BaseColor.colorAccentDark
        
        //self.navigationItem.leftViews = [userPhoneNumber]
    }	
    
    // Mark- Menu Bar configuration
    func pagerMenuBarCustomization() {
        settings.style.buttonBarBackgroundColor = BaseColor.colorPrimary
        settings.style.buttonBarItemBackgroundColor = BaseColor.colorPrimary
        
        settings.style.buttonBarItemFont = UIFont(name: "Helvetica", size: 10.0)!
        settings.style.buttonBarItemTitleColor = .gray
        
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        
        settings.style.selectedBarHeight = 3
        settings.style.buttonBarHeight = 70
        settings.style.selectedBarBackgroundColor = UIColor.white
        
        
        
        
        // Changing item text color on swipe
        
<<<<<<< HEAD
        
        changeCurrentIndexProgressive = {  (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
=======
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
>>>>>>> a1d48a2406f14ce3da7f5a004a2ffa02f53f81da
            guard changeCurrentIndex == true else { return }
            //oldCell?.label.textColor = .gray
            oldCell?.imageView.tintColor = .white
            oldCell?.imageView.contentMode = .scaleAspectFill
            //newCell?.label.textColor = BaseColor.colorPrimary
            newCell?.imageView.tintColor = BaseColor.colorPrimary
            newCell?.imageView.contentMode = .scaleAspectFit
        }
    }
    
    
<<<<<<< HEAD
    
=======
>>>>>>> a1d48a2406f14ce3da7f5a004a2ffa02f53f81da
    // Mark- Menu Bar's tab configuration
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        let child_1 = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RecentCallViewController") as! RecentCallViewController
        let child_2 = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DailPadViewController") as! DailPadViewController
        let child_3 = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PhoneContactViewController") as! PhoneContactViewController
        
        return [child_1, child_2, child_3]
    }
    
    
    
}
