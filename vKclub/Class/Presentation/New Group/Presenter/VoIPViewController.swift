//
//  VoIPViewController.swift
//  vKclub
//
//  Created by Chhayrith on 11/18/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class VoIPViewController: ButtonBarPagerTabStripViewController{
    
    
    override func viewDidLoad() {
        self.pagerMenuBarCustomization()
        super.viewDidLoad()
        
        
        
//        self.customNavigation()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.CustomNavigationBar(title: "", isShadow: true, isPresentVC: false)
        
        if let navigationBar = self.navigationController?.navigationBar {
            let greenDot = CGRect(x: 16, y: navigationBar.frame.height/2 - 8, width: 16, height: 16)
            let number = CGRect(x: 45, y: 0, width: navigationBar.frame.width/2, height: navigationBar.frame.height)
            let navDivider = CGRect(x: 0, y: navigationBar.frame.height, width: navigationBar.frame.width, height: 1)
            let QRBtnRect = CGRect(x: navigationBar.frame.width - 60, y: 0, width: 50, height: 50)
            
            let firstLabel = UIView(frame: greenDot)
            firstLabel.backgroundColor = BaseColor.colorAccentLight
            firstLabel.layer.cornerRadius = 8
            firstLabel.layer.masksToBounds = true
            firstLabel.layer.shadowColor = BaseColor.colorLightDark.cgColor
            firstLabel.layer.shadowOpacity = 1
            firstLabel.layer.shadowOffset = CGSize.zero
            firstLabel.layer.shadowRadius = 8
            
            let secondLabel = UILabel(frame: number)
            secondLabel.textColor = BaseColor.colorWhite
            secondLabel.font = UIFont(name: "Roboto-Bold", size: 20)
            secondLabel.text = userExtensionNumber
            
            
            let QRBtn = UIButton(frame: QRBtnRect)
            QRBtn.setImage(UIImage(named: "qrIcon"), for: .normal)
            QRBtn.addTarget(self, action: #selector(handleQRCodeContact), for: .touchUpInside)
            QRBtn.contentMode = .scaleAspectFill
            
            
            
            let thirdLabel = UIView(frame: navDivider)
            thirdLabel.backgroundColor = BaseColor.colorAccentDark
            
            navigationBar.addSubview(firstLabel)
            navigationBar.addSubview(secondLabel)
            navigationBar.addSubview(QRBtn)
            navigationBar.addSubview(thirdLabel)
        }
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
   
    
    
    // Mark- Menu Bar configuration
    func pagerMenuBarCustomization() {
        settings.style.buttonBarBackgroundColor = BaseColor.colorPrimary
        settings.style.buttonBarItemBackgroundColor = BaseColor.colorPrimary
        
        settings.style.buttonBarItemFont = UIFont(name: "Helvetica", size: 10.0)!
        settings.style.buttonBarItemTitleColor = .gray
        
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        
        settings.style.selectedBarHeight = 3
        settings.style.buttonBarHeight = 70
        settings.style.selectedBarBackgroundColor = UIColor.white
        
        
        
        
        // Changing item text color on swipe
        
        
        changeCurrentIndexProgressive = {  (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            //oldCell?.label.textColor = .gray
            oldCell?.imageView.tintColor = .white
            oldCell?.imageView.contentMode = .scaleAspectFill
            //newCell?.label.textColor = BaseColor.colorPrimary
            newCell?.imageView.tintColor = BaseColor.colorPrimary
            newCell?.imageView.contentMode = .scaleAspectFit
        }
    }
    
    
    // Mark- Menu Bar's tab configuration
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        let child1 = TestRecentCallController()
//        let child_1 = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RecentCallViewController") as! TestRecentCallController
        let child_2 = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DailPadViewController") as! DailPadViewController
        let child_3 = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PhoneContactViewController") as! PhoneContactViewController
        
        return [child1, child_2, child_3]
    }
    
    
    
}
