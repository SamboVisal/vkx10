//
//  QRViewController.swift
//  vKclub
//
//  Created by Chhayrith on 12/5/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class QRViewController: ButtonBarPagerTabStripViewController {

    override func viewDidLoad() {
        pagerMenuBarCustomization()
        super.viewDidLoad()

        self.CustomNavigationBar(title: "QR Code", isShadow: true, isPresentVC: true)
        view.backgroundColor = BaseColor.colorWhite
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pagerMenuBarCustomization() {
        settings.style.buttonBarBackgroundColor = .white
        settings.style.buttonBarItemBackgroundColor = BaseColor.colorAccentDark
        
        settings.style.buttonBarItemFont = UIFont(name: "Roboto-Bold", size: 16.0)!
        
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarHeight = 50
        settings.style.selectedBarBackgroundColor = .white
        
        
        // Changing item text color on swipe
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .white
            newCell?.label.textColor = .white
        }
    }
    
    // Mark- Menu Bar's tab configuration
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        let child_1 = MyQRViewController()
        let child_2 = ScanQRViewController()
        
        return [child_1, child_2]
    }
    

}
