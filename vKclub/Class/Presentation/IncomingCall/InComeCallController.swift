//
//  ConnectViewController.swift
//  vKclub Version 2
//
//  Created by Pisal on 6/25/2561 BE.
//  Copyright © 2561 BE Pisal. All rights reserved.
//

import UIKit
import AVFoundation

import CoreData


// APP private variables with var
class InComeCallController: UIViewController {
    
    private let incomeCallProperty = IncomeCallProperty()
    private var outGoingCallPlayer = AVAudioPlayer()
    private var callLogData = [SipCallData]()
    private var callDataRequest: NSFetchRequest<SipCallData> = SipCallData.fetchRequest()
   
    static let userCoreDataInstance = UserProfileCoreData()
    let sipCoreDataInstance = SipCallCoreData()
    static var dialPhoneNumber: String = ""
    static var callDuration = ""
    
    
}


// APP frame variables
extension InComeCallController {
    
    private var displayPhoneFrame: CGRect {
        return View.squareFrame(midX: self.view.bounds.maxX
            , midY: self.view.bounds.maxY, pX: 0.25, pY: 0.35, w: self.view.frame.width*0.5, h: self.view.frame.height*0.05)
    }
    
    private var displayCallStatusFrame: CGRect {
        return View.squareFrame(midX: self.view.bounds.maxX
            , midY: self.view.bounds.maxY, pX: 0.35, pY: 0.4 , w: self.view.frame.width*0.3, h: self.view.frame.height*0.03)
    }
    
    private var  endCallBtnFrame: CGRect {
        return View.squareFrame(midX: self.view.bounds.maxX
            , midY: self.view.bounds.maxY, pX: 0.36, pY: 0.55, w: self.view.frame.width*0.3, h: self.view.frame.height*0.06)
    }
    
    private var speakerBtnFrame : CGRect {
        return View.squareFrame(midX: self.view.bounds.maxX
            , midY: self.view.bounds.maxY, pX: 0.2, pY: 0.45, w: 90, h: 90)
        
    }
    
    private var muteBtnFrame : CGRect {
        return View.squareFrame(midX: self.view.bounds.maxX
            , midY: self.view.bounds.maxY, pX: 0.6, pY: 0.45, w: 90, h: 90)
        
    }
}

// APP let variables
extension InComeCallController {
    
    
    
}

// APP static variables
extension InComeCallController {
    
    static var waitForStreamRunningInterval: Timer?
    static var setUpCallInProgressInterval: Timer?
    static var phoneNumber : String?
    static var callKitManager: CallKitCallInit?
    static var checkProgressingCall : Bool = false
    static var callSteaming : Bool = false
    static var callLogTime = ""
    
    
    static var dialPhone : Bool = false
    
    
    static var callToFlag = false {
        
        didSet {
            
            if callToFlag == true {
                
                print("-------CALLTO_BEING_FREE_NOT_WITH_SOMEONE_ELSE--------")
                
                UIApplication.topViewController()?.present(InComeCallController(), animated: true, completion: {
                    
                    let caller = LinphoneManager.getCallerNb()
                    let contact = LinphoneManager.getContactName()
                    print("====== This is caller \(caller) and contact \(contact)")
                    print("====== This is callAction flag \(InComeCallController.callToFlag)")
                    
                    self.userCoreDataInstance.StoreCallDataLog(_callerID:  LinphoneManager.getCallerNb(), _callerName: InComeCallController.callToFlag == true ? "" : LinphoneManager.getContactName(), _callDuration: callDuration, _callIndicatorIcon: InComeCallController.incomingCallFlags == false ? "out-goingcall" : "incomecall-icon")
                    
                    print("This is caller ID \(LinphoneManager.getCallerNb())")
                    print("---- Save ", InComeCallController.dialPhoneNumber)
                    
                })
                
            } else {
                
                print("Call flag false.....")
            }
        }
    }
    
    
    static var incomingCallFlags = false {
        didSet {
            if incomingCallFlags == true {
                
                
                if #available(iOS 10, *) {
                    
                    AppDelegate.shared.displayIncomingCall(uuid: UUID(), handle: LinphoneManager.getCallerNb()) { _ in
                        
                        UIApplication.shared.endBackgroundTask(backgroundTaskIdentifier)
                        
                    }
                }
                
            } else {
                

            }
        }
    }
   
    static var acceptCallFlag = false {
        didSet {
            
            if acceptCallFlag == true {
                LinphoneManager.receiveCall()
            }
            
        }
    }
    
    static var endCall = false {
        didSet {
            if endCall == true {
                
                if InComeCallController.callSteaming {
                    LinphoneManager.endCall()
                    
                } else {
                    
                    if InComeCallController.incomingCallFlags == true {
                        self.userCoreDataInstance.StoreCallDataLog(_callerID: incomingCallUser, _callerName: InComeCallController.callToFlag == true ? "" : LinphoneManager.getContactName(), _callDuration: callDuration, _callIndicatorIcon: "misscall")
                        
                        print("===== Saved MissCall Number ======")
                    } else {
                        
                        LinphoneManager.endCall()
                    }
                    
                    
                }
                print("This is last call uid \(lastCallUUID)")
                callKitManager = CallKitCallInit(uuid: lastCallUUID, handle: "")
                callKitManager?.end(uuid: lastCallUUID)
                InComeCallController.setUpCallInProgressInterval?.invalidate()
                InComeCallController.setUpCallInProgressInterval = nil
                
                
                UIApplication.topViewController()?.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    
    
}

// APP life cycle
extension InComeCallController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor  = UIColor(red:0.23, green:0.14, blue:0.14, alpha:1.0)
        backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(expirationHandler: nil)
        callDataRequest = SipCallData.fetchRequest()
        InComeCallController.callKitManager = CallKitCallInit(uuid: UUID(), handle: "")
        loadAudio()
        PrepareCallToActionUI()
        CallValidation()
        addButtonAction()
       
        
    }
    
    override func viewDidLayoutSubviews() {
        outGoingCallFrame()
    }
    
    
    
}

// APP UI add subview
extension InComeCallController {
    
    private func PrepareCallToActionUI() {
        
        outGoingCallPlayer.prepareToPlay()
        outGoingCallPlayer.play()
        incomeCallProperty.endCallBtn.backgroundColor = .red
        incomeCallProperty.displayPhone.text = InComeCallController.phoneNumber
        incomeCallProperty.displayStatus.text = "Calling"
        incomeCallProperty.speakerBtn.isHidden = true
        incomeCallProperty.muteBtn.isHidden = true
        self.view.addSubview(incomeCallProperty.displayPhone)
        self.view.addSubview(incomeCallProperty.displayStatus)
        self.view.addSubview(incomeCallProperty.endCallBtn)
        self.view.addSubview(incomeCallProperty.speakerBtn)
        self.view.addSubview(incomeCallProperty.muteBtn)
        
    }
    
    private func PrepareInCallProgressUI() {
        
        inProgressingCallFrame()
        InComeCallController.checkProgressingCall = true
        incomeCallProperty.displayPhone.text = InComeCallController.phoneNumber
        incomeCallProperty.displayStatus.text = "00:00"
        incomeCallProperty.speakerBtn.isHidden = false
        incomeCallProperty.muteBtn.isHidden   = false
        if InComeCallController.setUpCallInProgressInterval == nil {
            InComeCallController.setUpCallInProgressInterval = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(InComeCallController.SetUpCallInProgress), userInfo: nil, repeats: true)
        }
        
        
    }
    
}

// APP UI Frame
extension InComeCallController {
    
    private func outGoingCallFrame() {
        
        incomeCallProperty.displayPhone.frame = displayPhoneFrame
        incomeCallProperty.displayStatus.frame = displayCallStatusFrame
        incomeCallProperty.endCallBtn.frame = endCallBtnFrame
        incomeCallProperty.speakerBtn.frame = speakerBtnFrame
        incomeCallProperty.muteBtn.frame = muteBtnFrame
        incomeCallProperty.muteBtn.backgroundColor =  UIColor(white: 1, alpha: 0.2)
        incomeCallProperty.muteBtn.layer.cornerRadius = 0.5 * incomeCallProperty.muteBtn.bounds.width
        incomeCallProperty.muteBtn.clipsToBounds = true
        incomeCallProperty.speakerBtn.backgroundColor = UIColor(white: 1, alpha: 0.2)
        incomeCallProperty.speakerBtn.layer.cornerRadius = 0.5 * incomeCallProperty.speakerBtn.bounds.size.width
        incomeCallProperty.speakerBtn.clipsToBounds = true
        incomeCallProperty.endCallBtn.layer.cornerRadius = 0.1 *   incomeCallProperty.endCallBtn.bounds.size.width
        incomeCallProperty.endCallBtn.clipsToBounds = true
        
    }
    
    private func inProgressingCallFrame() {
        
        incomeCallProperty.endCallBtn.frame = View.squareFrame(midX: self.view.bounds.maxX
            , midY: self.view.bounds.maxY, pX: 0.37, pY: 0.7, w: self.view.frame.width*0.3, h: self.view.frame.height*0.06)
        incomeCallProperty.displayPhone.frame = View.squareFrame(midX: self.view.bounds.maxX
            , midY: self.view.bounds.maxY, pX: 0.27, pY: 0.3, w: self.view.frame.width*0.5, h: self.view.frame.height*0.05)
        incomeCallProperty.displayStatus.frame = View.squareFrame(midX: self.view.bounds.maxX
            , midY: self.view.bounds.maxY, pX: 0.37, pY: 0.36 , w: self.view.frame.width*0.3, h: self.view.frame.height*0.03)
        
        
        
    }
    
    
    
    
    
    
    
    
    
}

// APP Phone Call logic
extension InComeCallController {
    
    @objc private func WaitForStreamRunning() {
        
        if InComeCallController.callSteaming  {
            InComeCallController.InvalidateWaitForStreamRunningInterval()
            outGoingCallPlayer.stop()
            PrepareInCallProgressUI()
        }
        
        
    }
    
    private func CallValidation() {
        
        if InComeCallController.waitForStreamRunningInterval == nil {
            InComeCallController.waitForStreamRunningInterval = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(InComeCallController.WaitForStreamRunning), userInfo: nil, repeats: true)
        }
        
    }
    
    @objc private func SetUpCallInProgress() {
        
        let (hour, minute, second) = LinphoneManager.getCurrentCallDuration()
        //convert it to have 2 digits number
        let secondStr = second < 10 ? "0" + String(second) : String(second)
        let minuteStr = minute < 10 ? "0" + String(minute) : String(minute)
        let hourStr = hour < 10 ? "0" + String(hour) : String(hour)
        
        if InComeCallController.callSteaming {
            //SetCallDurationToCoreData()
            InComeCallController.callDuration = hour == 0 ? minuteStr + ":" + secondStr : hourStr + ":" + minuteStr + ":" + secondStr
            
            SetCallDurationToCoreData()
            incomeCallProperty.displayStatus.text = InComeCallController.callDuration
        }
    }
    
    func EnableLoudSpeakerAction() {
        do {
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
        } catch let error as NSError {
            print("audioSession error: \(error.localizedDescription)")
        }
        
        incomeCallProperty.speakerBtn.tag = 1
        incomeCallProperty.speakerBtn.backgroundColor = UIColor(white: 1, alpha: 0.8)
    }
    
    
    func DisableLoudSpeakerAction() {
        do {
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.none)
        } catch let error as NSError {
            print("audioSession error: \(error.localizedDescription)")
        }
        incomeCallProperty.speakerBtn.tag = 0
        
        incomeCallProperty.speakerBtn.backgroundColor = UIColor(white: 1, alpha: 0.2)
    }
    
    
    func MuteCallAction() {
        LinphoneManager.muteMic()
        incomeCallProperty.muteBtn.tag = 1
        incomeCallProperty.muteBtn.backgroundColor = UIColor(white: 1, alpha: 0.8)
        
    }
    
    func UnmuteCallAction() {
        LinphoneManager.unmuteMic()
        incomeCallProperty.muteBtn.tag = 0
        incomeCallProperty.muteBtn.backgroundColor = UIColor(white: 1, alpha: 0.2)
        
    }
    
    
    
}

// APP Timer

extension InComeCallController {
    
    private static func InvalidateWaitForStreamRunningInterval() {
        if InComeCallController.waitForStreamRunningInterval != nil {
            InComeCallController.waitForStreamRunningInterval?.invalidate()
            InComeCallController.waitForStreamRunningInterval = nil
        }
    }
}

// APP load resources

extension InComeCallController {
    
    private func loadAudio() {
        
        do {
            let callToSoundBundle = Bundle.main.path(forResource: "OutGoingCallSound", ofType: "wav")
            let alertSound = URL(fileURLWithPath: callToSoundBundle!)
            try outGoingCallPlayer = AVAudioPlayer(contentsOf: alertSound)
        } catch {
            print("AVAudioPlayer Interrupted ===")
        }
    }
    
}

// APP adtarget

extension InComeCallController {
    private func addButtonAction() {
        self.incomeCallProperty.endCallBtn.addTarget(self, action: #selector(endCallBtn(_:)), for: .touchUpInside)
        
        self.incomeCallProperty.speakerBtn.addTarget(self, action: #selector(speakBtn(_:)), for: .touchUpInside)
        self.incomeCallProperty.muteBtn.addTarget(self, action: #selector(muteBtn(_:)), for: .touchUpInside)
        
        
    }
}

// APP action button

extension InComeCallController {
    
    
    @objc private func endCallBtn(_ sender : UIButton) {
        InComeCallController.endCall = true
    }
    
    @objc private func speakBtn(_ sender : UIButton) {
        
        if  sender.tag == 0 {
            EnableLoudSpeakerAction()
        } else {
            DisableLoudSpeakerAction()
        }
        
    }
    
    @objc private func muteBtn(_ sender : UIButton) {
        
        if sender.tag == 0 {
            MuteCallAction()
        } else {
            UnmuteCallAction()
        }
        
    }
    
}
// APP CoreData
extension InComeCallController {
    
    private func SetCallDurationToCoreData() {
        
        for _callData in callLogData {
            print("call data \(_callData)")
            if _callData.callLogTime != nil {
                if _callData.callLogTime! == InComeCallController.callLogTime {
                    let callDataItemToUpdate = sipCoreDataInstance.getCallDataByID(_id: _callData.objectID)
                    callDataItemToUpdate?.callDuration = InComeCallController.callDuration
                    do {
                        try managedObjectContext.save()
                        RecentCallViewController.LoadCallDataCell()
                    } catch {
                        print("SET CALLDURATION TO COREDATA ERROR \(error.localizedDescription) ===")
                    }
                }
            }
        }
    }
}

