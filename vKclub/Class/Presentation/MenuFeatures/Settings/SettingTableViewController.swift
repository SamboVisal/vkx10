//
//  SettingTableViewController.swift
//  vKclub
//
//  Created by Pisal on 12/2/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import UIKit
import Material
import MaterialComponents.MDCAlertController
import Firebase
import UserNotifications

class SettingViewTable: UITableViewController {
    
    let cellId = "cellid"
    let notificationCell = "cellNotification"
    let vibrationCell = "cellVibration"
    var settingData: [SettingCategory] = SettingCategory.sectionCategorySetting()
    
    var myArray: [String] = [ ]
    var featureName: [String] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.titleLabel.text = "Setting"
        navigationItem.titleLabel.textColor = .white
        self.navigationItem.backButton.tintColor = .white
        
        for i in settingData {
            
            for j in i.data! {
                featureName.append(j.titleName!)
            }
            
        }
        
        setupTableViewAndCell()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
    }
    
    fileprivate func setupTableViewAndCell() {
        view.backgroundColor = .white
        tableView.backgroundColor = .white
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        tableView.register(SwitchNotificationCell.self, forCellReuseIdentifier: notificationCell)
        tableView.register(SwitchVibrationCell.self, forCellReuseIdentifier: vibrationCell)
    }
    
    
    
    
}


// Tableview data source
extension SettingViewTable {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return settingData.count
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let letter = settingData[section]
        
        return (letter.data?.count)!
        
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let letter = settingData[indexPath.section]
        let item = letter.data![indexPath.row].titleName
        
        if indexPath.section == 0 {
            
            let firstSection = settingData[indexPath.section]
            let firstData = firstSection.data![indexPath.row]
            
            
            if indexPath.item == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: notificationCell, for: indexPath) as! SwitchNotificationCell
                cell.settingView = self
                cell.textLabel?.text = firstData.titleName
                return cell
            } else {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: vibrationCell, for: indexPath) as! SwitchVibrationCell
                cell.textLabel?.text = firstData.titleName
                return cell
                
            }
            
        } else if indexPath.section == 1 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
            
            cell.textLabel?.text = item
            
            return cell
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
            
            
            if Auth.auth().currentUser == nil {
                cell.textLabel?.text = "Sign In"
            } else {
                
                cell.textLabel?.text = "Logout"
                
            }
            
            print("This is the last cell \(item ?? "asdfghjk")")
            
            
            return cell
            
        }
        
    }
    
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        for i in settingData {
            
            myArray.append(i.sectionName!)
            
        }
        
        return myArray[section]
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let letter = settingData[indexPath.section]
        let item = letter.data![indexPath.row].titleName
        
        print("Title name \(item ?? "")")
        if indexPath.section == 2 {
            
            let cell = tableView.cellForRow(at: indexPath)
            
            if Auth.auth().currentUser != nil {
                
                let dialog = dialogMessage(title: DialogTitleMessage().logoutTitle, message: DialogTitleMessage().confirmLogout)
                
                let okaybtn = MDCAlertAction(title: DialogTitleMessage().okayTitle) { (action) in
                    UserDefaults.standard.setIsLoggedIn(value: false)
                    UserDefaults.standard.setUserId(value: "")
                    
                    RepositoryClass.shared.deleteUserCore(userId: self.userId())
                    SipCallDataClass.deleteAllData(entity: "SipCallData")
                    if LinphoneManager.CheckLinphoneConnectionStatus() {
                        connectionStatus = false
                        connectionProgress = false
                        LinphoneManager.shutdown()
                    }
                    IMAGELOAD = false
                    try! Auth.auth().signOut()
                    cell?.textLabel?.text = "Sign In"
                }
                let cancel = MDCAlertAction(title: DialogTitleMessage().cancelTitle, handler: nil)
                dialog.addAction(okaybtn)
                dialog.addAction(cancel)
                dialogPresent(view: dialog)
            } else {
                
                let loginController = UINavigationController(rootViewController: LoginPhoneNumberViewController())
                self.present(loginController, animated: true, completion: nil)
                
                
            }
            
            
        }
        
    }
    
}





