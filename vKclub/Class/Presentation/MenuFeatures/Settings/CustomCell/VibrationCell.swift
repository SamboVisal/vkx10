//
//  VibrationCell.swift
//  vKclub
//
//  Created by Pisal on 12/3/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import Foundation
import UIKit
import Material

class SwitchVibrationCell: UITableViewCell , SwitchDelegate{
    
    
    func switchDidChangeState(control: Switch, state: SwitchState) {
        
        
        
    }
    
    lazy var switchTab: Switch = {
        
        let sw = Switch()
        sw.translatesAutoresizingMaskIntoConstraints = false
        sw.switchStyle = .light
        sw.switchSize = SwitchSize.medium
        sw.tintColor = BaseColor.colorPrimary
        return sw
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(switchTab)
        switchTab.delegate = self
        switchTab.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        switchTab.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
}
