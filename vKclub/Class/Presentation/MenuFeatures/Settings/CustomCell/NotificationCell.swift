//
//  NotificationCell.swift
//  vKclub
//
//  Created by Pisal on 12/3/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import Foundation
import UIKit
import Material
import UserNotifications
import MaterialComponents.MDCAlertController

class SwitchNotificationCell : UITableViewCell , SwitchDelegate{
    
    var settingView = SettingViewTable()
    
    func switchDidChangeState(control: Switch, state: SwitchState) {
        print("Switch changed state to: ", .on == state ? "on" : "off")
    }
    
    lazy var switchTab: Switch = {
        
        let sw = Switch()
        sw.translatesAutoresizingMaskIntoConstraints = false
        sw.switchStyle = .light
        sw.switchSize = SwitchSize.medium
        sw.tintColor = BaseColor.colorPrimary
        return sw
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        
        
        addSubview(switchTab)
        switchTab.delegate = self
        switchTab.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        switchTab.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8).isActive = true
        switchTab.addTarget(self, action: #selector(ToggleChange(_:)), for: .valueChanged)
        
        check()
    }
    
    func check () {
        let center = UNUserNotificationCenter.current()
        center.getNotificationSettings { (setting) in
            
            DispatchQueue.main.async(execute: {
                if setting.authorizationStatus == .authorized {
                    self.switchTab.setSwitchState(state: .on, animated: true, completion: nil)
                    //                    self.notificationSetting.setOn(true, animated: true)
                    if UIApplication.shared.isRegisteredForRemoteNotifications {
                        self.switchTab.setSwitchState(state: .on, animated: true, completion: nil)
                        //                        self.notificationSetting.setOn(true, animated: true)
                        UserDefaults.standard.set(1, forKey: "setting")
                    } else {
                        self.switchTab.setSwitchState(state: .off, animated: true, completion: nil)
                        //                        self.notificationSetting.setOn(false, animated: true)
                        
                        UserDefaults.standard.set(2, forKey: "setting")
                    }
                } else {
                    self.switchTab.setSwitchState(state: .off, animated: true, completion: nil)
                    //                    self.notificationSetting.setOn(false, animated: true)
                }
            })
            
        }
    }
    
    @objc
    func ToggleChange (_ sender: UISwitch){
        
        let center = UNUserNotificationCenter.current()
        center.getNotificationSettings { (settting) in
            
            if settting.authorizationStatus == .authorized {
                DispatchQueue.main.async {
                    if sender.isOn {
                        self.switchTab.setSwitchState(state: .on, animated: true, completion: nil)
                        UIApplication.shared.registerForRemoteNotifications()
                        self.check()
                    } else {
                        self.switchTab.setSwitchState(state: .off, animated: true, completion: nil)
                        UIApplication.shared.unregisterForRemoteNotifications()
                        self.check()
                    }
                }
                
                
            } else {
                
                DispatchQueue.main.async {
                    
                    let dialog = MDCAlertController(title: "Notification is Disabled for vKClub App", message: "Please enable notification by go to Settings in app.")
                    
                    let okayButton = MDCAlertAction(title: "Settings", handler: { (action) in
                        UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: { (done) in
                            
                        })
                    })
                    let cancel = MDCAlertAction(title: "Cancel", handler: { (action) in
                        self.switchTab.setSwitchState(state: .off, animated: true, completion: nil)
                    })
                    dialog.addAction(okayButton)
                    dialog.addAction(cancel)
                    
                    self.settingView.present(dialog, animated: true, completion: nil)
                    
                }
                
                
            }
            
        }
        
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}
