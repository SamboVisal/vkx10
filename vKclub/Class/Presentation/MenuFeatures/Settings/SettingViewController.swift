//
//  SettingViewController.swift
//  vKclub
//
//  Created by Pisal on 11/8/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import UIKit
import UserNotifications
import MaterialComponents.MaterialCollections
import MaterialComponents.MaterialDialogs
import MaterialComponents.MaterialDialogs_ColorThemer
import MaterialComponents.MaterialDialogs_DialogThemer
import PopupDialog
import Material




class SettingViewController: MDCCollectionViewController {
    
    private let cellId = "cellId"
    private let cellHelp = "cellHelp"
    private var fontSize: CGFloat! = nil
    private let menuIcon = ExploreCategoryComponents()
    
    //var settingData: [SettingCategory] = SettingCategory.sectionCategorySetting()
    
    fileprivate func configureFontSize() {
        if getDeviceModelName.userDeviceIphone5() {
            fontSize = 18
        } else if getDeviceModelName.userDeviceIphone678() {
            fontSize = 20
        } else if getDeviceModelName.userDeviceIphone678Plus() {
            fontSize = 22
        } else {
            fontSize = 21
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.titleLabel.text = "Setting"
        self.navigationItem.titleLabel.textColor = .white
    
//        for i in settingData {
//            
//            print("Section name \(i.sectionName) and amount its data \(i.data?.count)")
//            
//        }
        
        configureFontSize()
        guard let font = fontSize else {
            return
        }
        print("Font size is \(font)")
        registerTableView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
    }
    fileprivate func registerTableView() {

        
        collectionView?.register(SettingHelpCell.self, forCellWithReuseIdentifier: cellHelp)
        
        collectionView?.register(SettingCell.self, forCellWithReuseIdentifier: cellId)
        collectionView?.reloadData()

    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 2
        
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SettingCell
            //cell.isUserInteractionEnabled = false
            
            cell.textLabel?.text = "Notification"
            cell.textLabel?.font = UIFont.systemFont(ofSize: fontSize)
            cell.settingView = self
            return cell
            
            
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellHelp, for: indexPath) as! SettingHelpCell
            cell.textLabel?.text = "Help"
            cell.textLabel?.font = UIFont.systemFont(ofSize: fontSize)
            
            return cell
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width - 32, height: view.frame.height / 10)
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16, left: 0, bottom: -16, right: 0)
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == 1 {
            print("HELPING")
            self.alertMessageDialog(title: "Notification Setting", message: "Turn on to receive any new contents otherwise, will not.", actionTitle: "Okay")
            //self.PresentDialogOneActionController(title: "Notification Setting", message: "Turn on to receive any new contents otherwise, will not.", actionTitle: "Okay")
        }
    }
    
    
}

class SettingCell: MDCCollectionViewTextCell , SwitchDelegate{
    
    func switchDidChangeState(control: Switch, state: SwitchState) {
        print("Switch changed state to: ", .on == state ? "on" : "off")
    }
    
    lazy var switchTab: Switch = {
        
        let sw = Switch()
        sw.translatesAutoresizingMaskIntoConstraints = false
        sw.switchStyle = .light
        sw.switchSize = SwitchSize.small
        
        return sw
    }()
    
    
    var settingView = SettingViewController()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(switchTab)
        switchTab.delegate = self
        switchTab.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        switchTab.widthAnchor.constraint(equalToConstant: 50).isActive = true
        switchTab.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20).isActive = true
        switchTab.addTarget(self, action: #selector(ToggleChange(_:)), for: .valueChanged)
        
        
        check()
    }
    
    
    
    func check () {
        let center = UNUserNotificationCenter.current()
        center.getNotificationSettings { (setting) in
            
            DispatchQueue.main.async(execute: {
                if setting.authorizationStatus == .authorized {
                    self.switchTab.setSwitchState(state: .on, animated: true, completion: nil)
                    //                    self.notificationSetting.setOn(true, animated: true)
                    if UIApplication.shared.isRegisteredForRemoteNotifications {
                        self.switchTab.setSwitchState(state: .on, animated: true, completion: nil)
                        //                        self.notificationSetting.setOn(true, animated: true)
                        UserDefaults.standard.set(1, forKey: "setting")
                    } else {
                        self.switchTab.setSwitchState(state: .off, animated: true, completion: nil)
                        //                        self.notificationSetting.setOn(false, animated: true)
                        
                        UserDefaults.standard.set(2, forKey: "setting")
                    }
                } else {
                    self.switchTab.setSwitchState(state: .off, animated: true, completion: nil)
                    //                    self.notificationSetting.setOn(false, animated: true)
                }
            })
            
        }
    }
    
    @objc
    func ToggleChange (_ sender: UISwitch){
        
        let center = UNUserNotificationCenter.current()
        center.getNotificationSettings { (settting) in
            
            if settting.authorizationStatus == .authorized {
                DispatchQueue.main.async {
                    if sender.isOn {
                        self.switchTab.setSwitchState(state: .on, animated: true, completion: nil)
                        UIApplication.shared.registerForRemoteNotifications()
                        self.check()
                    } else {
                        self.switchTab.setSwitchState(state: .off, animated: true, completion: nil)
                        UIApplication.shared.unregisterForRemoteNotifications()
                        self.check()
                    }
                }
                
                
            } else {
                
                DispatchQueue.main.async {
                    
                    let dialog = MDCAlertController(title: "Notification is Disabled for vKClub App", message: "Please enable notification by go to Settings in app.")
                    let okayButton = MDCAlertAction(title: "Settings", handler: { (action) in
                        UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: { (done) in
                            
                        })
                    })
                    let cancel = MDCAlertAction(title: "Cancel", handler: { (action) in
                        self.switchTab.setSwitchState(state: .off, animated: true, completion: nil)
                    })
                    dialog.addAction(okayButton)
                    dialog.addAction(cancel)
                    
                    self.settingView.present(dialog, animated: true, completion: nil)
              
                }
                
                
            }
            
        }
        
        
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class SettingHelpCell: MDCCollectionViewTextCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
