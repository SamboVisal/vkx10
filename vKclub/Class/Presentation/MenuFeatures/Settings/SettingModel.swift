//
//  SettingModel.swift
//  vKclub
//
//  Created by Pisal on 12/3/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import Foundation

class SettingCategoryData : NSObject {
    
    var titleName: String?
    
}

class SettingCategory : NSObject {
    
    private struct CategoryName{
        
        static let prefrerence = "Prefrerence"
        static let support = "Support"
        static let account = "Account"
    }
    
    var sectionName: String?
    var data: [SettingCategoryData]?
    
    
    static func sectionCategorySetting() -> [SettingCategory] {
        
        let prefrerence = SettingCategory.prefrerenceData()
        
        let support = SettingCategory.support()
        
        let account = SettingCategory.account()
        
        return [prefrerence , support, account]
        
    }
    
    static func prefrerenceData() -> SettingCategory {
        
        let prefrerence = SettingCategory()
        prefrerence.sectionName = CategoryName.prefrerence
        
        var prefrerenceAppend = [SettingCategoryData]()
        
        let notification = SettingCategory.initData(title: "Notification")
        prefrerenceAppend.append(notification)
        
        let vibration = SettingCategory.initData(title: "Vibration")
        prefrerenceAppend.append(vibration)
        
        
        // Init each section should contain how much rows
        prefrerence.data = prefrerenceAppend
        
        return prefrerence
    }
    
    static func support() -> SettingCategory{
        
        let support = SettingCategory()
        support.sectionName = CategoryName.support
        
        var supportAppend = [SettingCategoryData]()
        
        let help = SettingCategory.initData(title: "Help")
        supportAppend.append(help)
        
        let rateReview = SettingCategory.initData(title: "Rate and Review")
        supportAppend.append(rateReview)
        
        support.data = supportAppend
        
        return support
    }
    
    static func account() -> SettingCategory {
        
        let account = SettingCategory()
        account.sectionName = CategoryName.account
        
        var accountAppend = [SettingCategoryData]()
        let accountData = SettingCategory.initData(title: "Logout")
        accountAppend.append(accountData)
        
        account.data = accountAppend
        
        return account
        
    }
    
    static func initData(title: String) -> SettingCategoryData{
        
        let data = SettingCategoryData()
        
        data.titleName = title
        
        return data
    }
    
}
