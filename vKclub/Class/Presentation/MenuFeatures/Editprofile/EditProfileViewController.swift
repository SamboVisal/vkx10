//
//  EditProfileViewController.swift
//  vKclub
//
//  Created by Pisal on 11/13/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import UIKit
import MaterialComponents
import FirebaseAuth
import Photos
import SVProgressHUD
import ProgressHUD
import SDWebImage
import Firebase
import SnapKit


class EditProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    var imagePicker : UIImagePickerController = UIImagePickerController()
    private let currentUser = Auth.auth().currentUser
    let userProfileInstance = DetailProfileVariables()
    let editProfileInstance = EditProfileVariables()
    let cardViewInstance = ExploreCategoryComponents()
    let storageRef = Storage.storage().reference()
    let property = ExploreCategoryComponents()
    var userProfileCore = [UserProfileCore]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        self.readUser()
        self.setupView()
        self.loadIndicatorImageView()
        self.setupImageProfile()
        if IMAGELOAD == false {
            self.RedownloadImage()
        }
        
        imagePicker.delegate = self
        editProfileInstance.usernameTextField.delegate = self
        
    }
    
    func setupView() {
        
        view.backgroundColor = .white
        self.setupNaviagtionController()
        
        self.setupBackgroundProfileView()
        
        self.setupSavebtnAction()
    }
    
    fileprivate func loadIndicatorImageView() {
        
        self.userProfileInstance.imageUserCircleView.addSubview(cardViewInstance.activityIndicator)
        self.cardViewInstance.activityIndicator.snp.makeConstraints { (make) in
            make.center.equalTo(self.userProfileInstance.imageUserCircleView)
        }
    }
    func readUser() {
        
        if let userID = Auth.auth().currentUser?.uid {
            RepositoryClass.shared.getUserCompletionBlock(userID: userID) { (data, done) in
                
                if done {
                    self.userProfileCore = data
                    for i in self.userProfileCore {
                        print("User information \(i.name ?? "null")")
                    }
                }
                
            }
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setNeedsStatusBarAppearanceUpdate()
    }
    
}


// UITextfield delegate
extension EditProfileViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
        case self.editProfileInstance.usernameTextField:
            saveButtonClicked()
            self.view.endEditing(true)
        default:
            saveButtonClicked()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        switch textField {
        case self.editProfileInstance.usernameTextField:
            self.editProfileInstance.usernameTextField.returnKeyType = UIReturnKeyType.go
        default:
            print("nothing...")
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        return updatedText.count <= 20
    }
    
    
    
}


// handle user image picker
extension EditProfileViewController {
    
    func performAnimation (img: UIImageView) {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
            
            img.transform = CGAffineTransform.init(scaleX: 1.4, y: 1.4)
            
            //reducing the size
            UIView.animate(withDuration: 0.5, delay: 0.2, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
                img.transform = CGAffineTransform.init(scaleX: 1, y: 1)
            }) { (flag) in
            }
        }) { (flag) in
            
        }
    }
    
    func enableImageGesture () {
        
        self.userProfileInstance.imageUserCircleView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.imageDidClick)) )
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecoginzer(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecoginzer(_:)))
        let topSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecoginzer(_:)))
        let downSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecoginzer(_:)))
        
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        topSwipe.direction = .up
        downSwipe.direction = .down
        
        self.userProfileInstance.imageUserCircleView.addGestureRecognizer(leftSwipe)
        self.userProfileInstance.imageUserCircleView.addGestureRecognizer(rightSwipe)
        self.userProfileInstance.imageUserCircleView.addGestureRecognizer(topSwipe)
        self.userProfileInstance.imageUserCircleView.addGestureRecognizer(downSwipe)
        
        self.userProfileInstance.imageUserCircleView.isUserInteractionEnabled = true
        
    }
    
    @objc
    func imageDidClick() {
        
        self.performAnimation(img: self.userProfileInstance.camera)
        
        if InternetConnection.isConnectedToNetwork() {
            print("have internet")
        } else {
            self.alertError(message: "Can not upload to server. Please Check your internet connection ")
            return
        }
        UIComponentHelper.showProgressWith(status: "Loading...", interact: false)
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) == true
        {
            
            UIApplication.shared.statusBarStyle = .default
            self.imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: {
                
                UIComponentHelper.ProgressDismiss()
            })
        } else {
            UIComponentHelper.ProgressDismiss()
            print("NO")
            
        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("User press cancel button")
        
        dismiss(animated: true, completion: {
            UIComponentHelper.ProgressDismiss()
        })
    }
    
    /// When image gallery is displayed
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        UIComponentHelper.showProgressWith(status: "Loading...", interact: false)
        
        var selectedImageFromPicker : UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            selectedImageFromPicker = editedImage
        }else {
            
            print("Cannot get image")
            
        }
        
        if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage{
            selectedImageFromPicker = originalImage
        }
        if selectedImageFromPicker == nil {
            self.alertError(message: "The process could not be completed. Please try again")
            return
        }
        
        if let selectedImage = selectedImageFromPicker {
            
            
            let currentUser = Auth.auth().currentUser
            let newImage = UIComponentHelper.resizeImage(image: selectedImage, targetSize: CGSize(width: 400, height: 400))
            guard  let imageProfiles = UIImagePNGRepresentation(newImage) else {
                self.alertError(message: "The process could not be completed. Please try again")
                return
            }
            let imageData : NSData = NSData(data: imageProfiles)
            let imageSize :Int = imageData.length
            
            // Save image to storage
            if let user = currentUser {
                let riversRef = storageRef.child("userprofile-photo").child(user.uid)
                riversRef.putData(imageProfiles, metadata: nil, completion: { (metadata, error) in
                    
                    if Double(imageSize) > 500000 {
                        self.alertError(message: "You can not upload image more then 5 MB")
                        return
                    }
                    if let err = error {
                        UIComponentHelper.ProgressDismiss()
                        self.alertError(message: err.localizedDescription)
                        return
                    }else {
                        
                        print("NO Error Occur")
                    }
                    guard let metadata = metadata else {
                        UIComponentHelper.ProgressDismiss()
                        self.alertError(message: "Please check your internet connection and try again.")
                        return
                    }
                    // Metadata contains file metadata such as size, content-type, and download URL.
                    let downloadURL = metadata.downloadURL()!.absoluteString
                    let url = NSURL(string: downloadURL) as URL?
                    
                    let changeProfileimage = user.createProfileChangeRequest()
                    changeProfileimage.photoURL = url
                    changeProfileimage.commitChanges(completion: { (error) in
                        if error != nil {
                            self.alertError( message: (error?.localizedDescription)!)
                        }
                    })
                    
                    guard let urlString = url?.absoluteString else {
                        return
                    }
                    
                    self.userProfileInstance.imageUserCircleView.sd_setImage(with: url, placeholderImage: UIImage(named: "detailprofile-icon")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), options: [ .retryFailed, .continueInBackground], completed: { (image, error, ImageCache, Url) in
                        if error == nil {
                            UserCommitChnage = true
                            UIComponentHelper.ProgressDismiss()
                            RepositoryClass.shared.updateSpecificField(userID: user.uid, fieldName: "pfpUrl", valueNew: urlString)
                            RepositoryClass.shared.updateUserFirestoreSpecific(in: .users, and: user.uid, forField: "pfpUrl", forValue: urlString, completion: { (done) in
                                if done {
                                    
                                    self.alertMessageDialog(title: DialogTitleMessage().successTitle, message: "Your profile has been updated.", actionTitle: DialogTitleMessage().okayTitle)
                                    
                                }
                            })
                            
                            
                        }else {
                            UIComponentHelper.ProgressDismiss()
                            
                            self.alertError(message: "Please try to upload again.")
                            
                            
                        }
                    })
                    
                })
            }
            
            
        }
        else {
            print("error something")
        }
        dismiss(animated: true, completion: {
            
            if selectedImageFromPicker == nil {
                print("NO IMAGE ")
                self.alertError(message: "The process could not be completed. Please try again")
                
                return
            }
            
        })
        
        
        
    }
    
}


// handle user edit profile
extension EditProfileViewController {
    
    /// User click save button for edit name
    @objc
    func saveButtonClicked() {
        
        guard let username = editProfileInstance.usernameTextField.text else {
            return
        }
        guard let email = editProfileInstance.emailTextField.text else {
            return
        }
        let specialCharacter = UIComponentHelper.AvoidSpecialCharaters(specialcharaters: username)
        let whitespaces: Int = UIComponentHelper.Countwhitespece(_whitespece: username)
        var lengthPassword = username.count
        if username.isEmpty {
            self.alertError(message: "Please enter your username.")
            
            return
        }else {
            lengthPassword = username.count
        }
        
        if InternetConnection.isConnectedToNetwork() {
            print("Have internet")
        } else {
            self.alertError(message:  "Cannot update your Profile right now. Please Check your internet connection.")
            
            
            return
        }
        if lengthPassword < 5 {
            self.alertError(message:  "Please enter your name more than 5 characters.")
            
            return
        }else if specialCharacter == false {
            
            self.alertError(message:  "Your name should not contain special characters.")
            
            
            return
        }else if whitespaces >= 3 {
            
            self.alertError(message:  "Your name should not contain more than 3 white spaces.")
            return
        } else {
            
            UIComponentHelper.showProgressWith(status: "Saving...", interact: false)
            let changeRequest = self.currentUser?.createProfileChangeRequest()
            if self.editProfileInstance.usernameTextField.text == self.currentUser?.displayName {
                UIComponentHelper.ProgressDismiss()
                self.alertMessageDialog(title: DialogErrorMessage().warningTitle, message: DialogErrorMessage().errorUsernameSameData, actionTitle: DialogTitleMessage().okayTitle)
                return
            }
            
            UIComponentHelper.ProgressDismiss()
            /// Commit user change his display name in firestore
            changeRequest?.displayName = self.editProfileInstance.usernameTextField.text
            changeRequest?.commitChanges(completion: { (error) in
                if error != nil {
                    UIComponentHelper.ProgressDismiss()
                    self.alertError(message: (error?.localizedDescription)!)
                }
            })
            
            self.alertMessageConfirmDone ()
        }
        
        
    }
    
}


// Setup image profile and handle error when loading image
extension EditProfileViewController {
    
    
    /// Let user click on profile image and do some changes
    func enableImageInteract() {
        cardViewInstance.activityIndicator.stopAnimating()
        self.userProfileInstance.camera.isHidden = false
        IMAGELOAD = true
        self.userProfileInstance.imageUserCircleView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.imageDidClick)) )
        
        
    }
    
    /// User interaction with camera icon
    @objc
    func swipeRecoginzer(_ sender: UISwipeGestureRecognizer?) {
        
        print("Moving")
        if sender?.direction == .left {
            print("Moving left")
        }
        if sender?.direction == .right {
            print("Moving right")
        }
        if sender?.direction == .up {
            print("Moving up")
        }
        if sender?.direction == .down {
            print("Moving down")
        }
        self.performAnimation(img: self.userProfileInstance.camera)
        
    }
    @objc
    func imagePan() {
        
        print("Image Pan Gesture")
        self.performAnimation(img: self.userProfileInstance.camera)
    }
    func imageLoadWithError(error: String) {
        
        //self.userProfileInstance.imageUserCircleView.image = UIImage(named: "detailprofile-icon")
        cardViewInstance.activityIndicator.stopAnimating()
        IMAGELOAD = false
        self.showSnackbarMessage(title: error)
        //self.alertMessageDialog(title: "Error", message: error, actionTitle: "Okay")
        
    }
    
    func setupNaviagtionController() {
        
        self.navigationItem.leftViews = [property.menuDeleteButton]
        property.menuDeleteButton.addTarget(self, action: #selector(backToDetailProfile), for: .touchUpInside)
        
        self.navigationItem.titleLabel.text = "Edit Profile"
        self.navigationItem.titleLabel.textColor = .white
    }
    
    func setupImageProfile() {
        
        if Auth.auth().currentUser != nil {
            
            for i in userProfileCore {
                
                self.editProfileInstance.usernameTextField.placeholder = i.name
                if i.email == "" {
                    self.editProfileInstance.emailTextField.placeholder = "KIT Email Address"
                }
                guard let urlString = i.pfpUrl else {
                    return
                }
                
                userProfileInstance.imageUserCircleView.sd_setImage(with: URL(string: urlString), placeholderImage: UIImage(named: "detailprofile-icon")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), options: [ .fromCacheOnly ]) { (image, error, cache, url) in
                    
                    if error == nil {
                        self.enableImageInteract()
                        
                        self.enableImageGesture ()
                    }else {
                        
                        if let error = error {
                            
                            if urlString == "" {
                                
                            } else {
                                
                                self.imageLoadWithError(error: error.localizedDescription)
                            }
                            
                            
                            
                        }
                        
                    }
                }
                return
            }
            
            
        }
        
        
        
    }
    
    func RedownloadImage() {
        
        
        InternetConnection.second = 0
        InternetConnection.countTimer.invalidate()
        
        if Auth.auth().currentUser != nil {
            
            for i in userProfileCore {
                
                guard let urlString = i.pfpUrl else {
                    return
                }
                let imageURL = URL(string: urlString)
                let placeholder = UIImage(named: "detailprofile-icon")
                placeholder?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                
                self.userProfileInstance.camera.isHidden = true
                print("===== User Image URL \(imageURL) ======")
                cardViewInstance.activityIndicator.startAnimating()
                userProfileInstance.imageUserCircleView.sd_setImage(with: imageURL, placeholderImage: placeholder?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), options: [.progressiveDownload, .continueInBackground], completed: { (image, error, imageCache, url) in
                    
                    InternetConnection.countTimer.invalidate()
                    InternetConnection.second = 0
                    if error == nil {
                        
                        self.enableImageInteract()
                        self.enableImageGesture ()
                    }
                    else {
                        if urlString == "" {
                            print("===== User does not have URL image ", urlString)
                            
                            self.enableImageInteract()
                            
                            self.enableImageGesture ()
                            IMAGELOAD = false
                            return
                        }
                        if let error = error {
                            //
                            self.imageLoadWithError(error: error.localizedDescription)
                        }
                    }
                    
                })
                
            }
            
        } else {
            print("User need to login first")
        }
        
        
    }
    
    // Alert a dialog for confirmation that profile has updated
    func alertMessageConfirmDone () {
        
        guard let userID = Auth.auth().currentUser?.uid else {
            return
        }
        guard let user = RepositoryClass.shared.getUserCore(userId: userID).first else {return}
        let currentUser = UserProfileModel(email: user.email!, name: user.name!, pfpUrl: user.pfpUrl!, presence: user.presence, role: user.role!, phoneNumber: user.phoneNumber!, isVKpointUser: user.isVKpointUser, vkclubNumber: user.vkclubNumber!, vkPassword: user.vkPassword!)
        currentUser.name = self.editProfileInstance.usernameTextField.text!
        currentUser.email = self.editProfileInstance.emailTextField.text!
        RepositoryClass.shared.updateCurrentUserCore(userModel: currentUser)
        RepositoryClass.shared.updateUserFirestore(for: currentUser, in: FIRCollectionReference.users, and: userID) { (done) in
            if done {
                self.avoidLeaving(title: DialogTitleMessage().successTitle, message: "Your profile had been updated")
            }
        }
    }
    
}

// handle button actions
extension EditProfileViewController {
    
    func setupSavebtnAction () {
        editProfileInstance.editSaveBtton.addTarget(self, action: #selector(saveButtonClicked), for: .touchUpInside)
    }
    
    @objc
    func backToDetailProfile() {
        InternetConnection.countTimer.invalidate()
        InternetConnection.second = 0
        UIComponentHelper.ProgressDismiss()
        dismiss(animated: true, completion: nil)
        
    }
    
    func avoidLeaving(title: String, message: String) {
        
        
        let dialog = MDCAlertController(title: title, message: message)
        let okayButton = MDCAlertAction(title: "Okay") { (action) in
            
            self.editProfileInstance.usernameTextField.text! = ""
            
            self.editProfileInstance.usernameTextField.placeholder = self.currentUser?.displayName
            //self.editProfileInstance.emailTextField.placeholder = self.currentUser?.email
            print("Current user name ", self.currentUser?.displayName ?? "")
        }
        dialog.addAction(okayButton)
        
        self.present(dialog, animated: true, completion: nil)
        
    }
    
    @objc
    func backHome() {
        dismiss(animated: true, completion: nil)
    }
    
}




