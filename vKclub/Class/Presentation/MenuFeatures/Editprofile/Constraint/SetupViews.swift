//
//  SetupViews.swift
//  vKclub
//
//  Created by Pisal on 12/2/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//


import UIKit

// constraint profile view
extension EditProfileViewController {
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//
        let heightNavigation = self.navigationController?.navigationBar.frame.height
        
        guard let height = heightNavigation else {
            return
        }
        print("Height of navigation bar is \(height)")
        userProfileInstance.simepleView.snp.makeConstraints { (make) in
            
            make.top.equalTo(view)
            make.leading.trailing.equalTo(view)
            make.height.equalTo(view).dividedBy(3.3)
        }
        userProfileInstance.imageUserCircleView.snp.makeConstraints { (make) in
            
            make.center.equalTo(userProfileInstance.simepleView)
            make.height.equalTo(userProfileInstance.simepleView).dividedBy(2)
            make.width.equalTo(userProfileInstance.imageUserCircleView.snp.height)
            
        }
       
        userProfileInstance.camera.snp.makeConstraints { (make) in
            make.center.equalTo(userProfileInstance.imageUserCircleView)
        }
        cardViewInstance.mainCardView.snp.makeConstraints { (make) in
            
            make.top.equalTo(userProfileInstance.simepleView.snp.bottom).offset(16)
            make.leading.equalTo(self.view).offset(16)
            make.trailing.equalTo(self.view).offset(-16)
            make.height.equalTo(self.view).dividedBy(3)
        }
        
        editProfileInstance.usernameTextField.snp.makeConstraints { (make) in
            
            make.top.equalTo(cardViewInstance.mainCardView).offset(16)
            make.leading.equalTo(cardViewInstance.mainCardView).offset(16)
            make.trailing.equalTo(cardViewInstance.mainCardView).offset(-16)
            make.height.equalTo(cardViewInstance.mainCardView).dividedBy(4)
            
        }
        
        editProfileInstance.emailTextField.snp.makeConstraints { (make) in
            
            make.top.equalTo(editProfileInstance.usernameTextField.snp.bottom).offset(16)
            make.leading.trailing.height.equalTo(editProfileInstance.usernameTextField)
            
        }
        
        editProfileInstance.editSaveBtton.snp.makeConstraints { (make) in
            
            make.top.equalTo(editProfileInstance.emailTextField.snp.bottom).offset(16)
            make.leading.trailing.equalTo(editProfileInstance.usernameTextField)
            make.height.equalTo(cardViewInstance.mainCardView).dividedBy(4)
            
        }

    }
    
    func setupBackgroundProfileView() {
        
        self.view.addSubview(userProfileInstance.simepleView)

        userProfileInstance.simepleView.addSubview(userProfileInstance.imageUserCircleView)
        userProfileInstance.imageUserCircleView.layer.borderWidth = 2
        userProfileInstance.imageUserCircleView.layer.borderColor = UIColor.white.cgColor
        userProfileInstance.imageUserCircleView.backgroundColor = UIColor.white.withAlphaComponent(0.4)
        
        userProfileInstance.imageUserCircleView.addSubview(userProfileInstance.camera)
        view.addSubview(cardViewInstance.mainCardView)
        cardViewInstance.mainCardView.addSubview(editProfileInstance.usernameTextField)
        cardViewInstance.mainCardView.addSubview(editProfileInstance.emailTextField)
        cardViewInstance.mainCardView.addSubview(editProfileInstance.editSaveBtton)
        
    }
 
    
}




