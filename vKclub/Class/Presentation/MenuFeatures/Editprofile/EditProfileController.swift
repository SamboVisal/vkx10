//
//  EditProfileController.swift
//  vKclub
//
//  Created by Pisal on 12/4/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import MaterialComponents.MDCAlertController

class EditProfileController: UIViewController , UITextFieldDelegate{
    
    var imagePicker : UIImagePickerController = UIImagePickerController()
    let userProfileInstance = DetailProfileVariables()
    let cardViewInstance = ExploreCategoryComponents()
    let editProfileInstance = EditProfileVariables()
    let storageRef = Storage.storage().reference()
    var userProfileCore = [UserProfileCore]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "clear")?.withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(handleDismiss))
        self.navigationItem.leftBarButtonItem?.tintColor = BaseColor.colorWhite
        self.navigationItem.title = "Edit Profile"
        
        hideKeyboardWhenTappedAround()
        self.readUser()
        self.setupImageProfile()
        if IMAGELOAD == false {
            self.RedownloadImage()
        }
        
        editProfileInstance.usernameTextField.delegate = self
        imagePicker.delegate = self
        
        view.addSubview(userProfileInstance.simepleView)
        userProfileInstance.simepleView.addSubview(userProfileInstance.imageUserCircleView)
        userProfileInstance.imageUserCircleView.addSubview(userProfileInstance.opacityView)
        
        view.addSubview(cardViewInstance.mainCardView)
        cardViewInstance.mainCardView.addSubview(editProfileInstance.usernameTextField)
        cardViewInstance.mainCardView.addSubview(editProfileInstance.editSaveBtton)
        
        self.userProfileInstance.imageUserCircleView.addSubview(cardViewInstance.activityIndicator)
        self.cardViewInstance.activityIndicator.snp.makeConstraints { (make) in
            make.center.equalTo(self.userProfileInstance.imageUserCircleView)
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
        let height = self.navigationController?.navigationBar.frame.height
        guard let h = height else {
            return
        }
        print("This is navigation height \(h)")
        userProfileInstance.simepleView.snp.makeConstraints { (make) in
            
            make.top.equalTo(view).offset(h)
            make.leading.trailing.equalTo(view)
            make.height.equalTo(view).dividedBy(3.3)
        }
        userProfileInstance.imageUserCircleView.snp.makeConstraints { (make) in
            
            make.center.equalTo(userProfileInstance.simepleView)
            make.height.equalTo(userProfileInstance.simepleView).dividedBy(2.2)
            make.width.equalTo(userProfileInstance.imageUserCircleView.snp.height)
            
        }
        userProfileInstance.opacityView.snp.makeConstraints { (make) in
            
           make.height.width.equalTo(100)
        
            
        }
        cardViewInstance.mainCardView.snp.makeConstraints { (make) in
            
            make.top.equalTo(userProfileInstance.simepleView.snp.bottom).offset(16)
            make.leading.equalTo(self.view).offset(16)
            make.trailing.equalTo(self.view).offset(-16)
            make.height.equalTo(self.view).dividedBy(4)
        }
        
        editProfileInstance.usernameTextField.snp.makeConstraints { (make) in
            
            make.top.equalTo(cardViewInstance.mainCardView).offset(16)
            make.leading.equalTo(cardViewInstance.mainCardView).offset(16)
            make.trailing.equalTo(cardViewInstance.mainCardView).offset(-16)
            make.height.equalTo(cardViewInstance.mainCardView).dividedBy(3)
            
        }
        editProfileInstance.editSaveBtton.snp.makeConstraints { (make) in
            
            make.top.equalTo(editProfileInstance.usernameTextField.snp.bottom).offset(16)
            make.leading.trailing.equalTo(editProfileInstance.usernameTextField)
            make.height.equalTo(cardViewInstance.mainCardView).dividedBy(3)
            
        }
    }
    func setupSavebtnAction () {
        
        editProfileInstance.editSaveBtton.addTarget(self, action: #selector(saveButtonClicked), for: .touchUpInside)
        
        
        
    }
    func readUser() {
        
        if let userID = Auth.auth().currentUser?.uid {
            RepositoryClass.shared.getUserCompletionBlock(userID: userID) { (data, done) in
                
                if done {
                    self.userProfileCore = data
                    for i in self.userProfileCore {
                        print("User information \(i.name ?? "null")")
                    }
                }
                
            }
        }
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
        case self.editProfileInstance.usernameTextField:
            saveButtonClicked()
            self.view.endEditing(true)
        default:
            saveButtonClicked()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        switch textField {
        case self.editProfileInstance.usernameTextField:
            self.editProfileInstance.usernameTextField.returnKeyType = UIReturnKeyType.go
        default:
            print("nothing...")
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        return updatedText.count <= 20
    }
    
    func setupImageProfile() {
        
        if Auth.auth().currentUser != nil {
            
            for i in userProfileCore {
                
                self.editProfileInstance.usernameTextField.placeholder = i.name
                self.editProfileInstance.emailTextField.placeholder = i.email
                guard let urlString = i.pfpUrl else {
                    return
                }
                userProfileInstance.imageUserCircleView.sd_setImage(with: URL(string: urlString), placeholderImage: UIImage(named: "detailprofile-icon")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), options: [ .fromCacheOnly ]) { (image, error, cache, url) in
                    
                    if error == nil {
                        self.enableImageInteract()
                        
                        self.enableImageGesture ()
                    }else {
                        
                        if let error = error {
                            
                            self.imageLoadWithError(error: error.localizedDescription)
                            
                        }
                        
                    }
                }
            }
            
            
        }
        
        
        
    }
    
    func RedownloadImage() {
        
        
        InternetConnection.second = 0
        InternetConnection.countTimer.invalidate()
        
        if Auth.auth().currentUser != nil {
            
            for i in userProfileCore {
                
                guard let urlString = i.pfpUrl else {
                    return
                }
                let imageURL = URL(string: urlString)
                let placeholder = UIImage(named: "detailprofile-icon")
                placeholder?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                
                self.userProfileInstance.camera.isHidden = true
                
                cardViewInstance.activityIndicator.startAnimating()
                userProfileInstance.imageUserCircleView.sd_setImage(with: imageURL, placeholderImage: placeholder?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), options: [.progressiveDownload, .continueInBackground], completed: { (image, error, imageCache, url) in
                    
                    InternetConnection.countTimer.invalidate()
                    InternetConnection.second = 0
                    if error == nil {
                        
                        self.enableImageInteract()
                    }
                    else {
                        if let error = error {
                            //
                            self.imageLoadWithError(error: error.localizedDescription)
                        }
                    }
                    
                })
                
            }
            
        } else {
            print("User need to login first")
        }
        
        
    }
    
    
    func alertMessageConfirmDone () {
        
        guard let userID = Auth.auth().currentUser?.uid else {
            return
            
        }
        RepositoryClass.shared.updateSpecificField(userID: userID, fieldName: "name", valueNew: self.editProfileInstance.usernameTextField.text!)
        RepositoryClass.shared.updateUserFirestoreSpecific(in: .users, and: userID, forField: "name", forValue: self.editProfileInstance.usernameTextField.text!) { (done) in
            
            if done {
                self.avoidLeaving(title: "Success", message: "Your profile had been updated")
            }
        }
        
        
        
    }
    
    func avoidLeaving(title: String, message: String ) {
        
        guard let currentUser = Auth.auth().currentUser else {
            return
        }
        
        let dialog = MDCAlertController(title: title, message: message)
        let okayButton = MDCAlertAction(title: "Okay") { (action) in
            
            self.editProfileInstance.usernameTextField.text! = ""
            
            self.editProfileInstance.usernameTextField.placeholder = currentUser.displayName
            self.editProfileInstance.emailTextField.placeholder = currentUser.email
            print("Current user name ", currentUser.displayName)
            
        }
        
        dialog.addAction(okayButton)
        
        self.present(dialog, animated: true, completion: nil)
        
    }
    
    @objc
    func handleDismiss() {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
}

extension EditProfileController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func performAnimation (img: UIImageView) {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
            
            img.transform = CGAffineTransform.init(scaleX: 1.4, y: 1.4)
            
            //reducing the size
            UIView.animate(withDuration: 0.5, delay: 0.2, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
                img.transform = CGAffineTransform.init(scaleX: 1, y: 1)
            }) { (flag) in
            }
        }) { (flag) in
            
        }
    }
    
    func enableImageGesture () {
        
        self.userProfileInstance.imageUserCircleView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.imageDidClick)) )
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecoginzer(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecoginzer(_:)))
        let topSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecoginzer(_:)))
        let downSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecoginzer(_:)))
        
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        topSwipe.direction = .up
        downSwipe.direction = .down
        
        self.userProfileInstance.imageUserCircleView.addGestureRecognizer(leftSwipe)
        self.userProfileInstance.imageUserCircleView.addGestureRecognizer(rightSwipe)
        self.userProfileInstance.imageUserCircleView.addGestureRecognizer(topSwipe)
        self.userProfileInstance.imageUserCircleView.addGestureRecognizer(downSwipe)
        
        self.userProfileInstance.imageUserCircleView.isUserInteractionEnabled = true
        
    }
    
    @objc
    func imageDidClick() {
        
        self.performAnimation(img: self.userProfileInstance.camera)
        
        if InternetConnection.isConnectedToNetwork() {
            print("have internet")
        } else {
            self.alertError(message: "Can not upload to server. Please Check your internet connection ")
            return
        }
        UIComponentHelper.showProgressWith(status: "Loading...", interact: false)
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) == true
        {
            
            UIApplication.shared.statusBarStyle = .default
            self.imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: {
                
                UIComponentHelper.ProgressDismiss()
            })
        } else {
            UIComponentHelper.ProgressDismiss()
            print("NO")
            
        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("User press cancel button")
        
        dismiss(animated: true, completion: {
            UIComponentHelper.ProgressDismiss()
        })
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        UIComponentHelper.showProgressWith(status: "Loading...", interact: false)
        
        var selectedImageFromPicker : UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            selectedImageFromPicker = editedImage
        }else {
            
            print("Cannot get image")
            
        }
        
        if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage{
            selectedImageFromPicker = originalImage
        }
        if selectedImageFromPicker == nil {
            self.alertError(message: "The process could not be completed. Please try again")
            return
        }
        
        if let selectedImage = selectedImageFromPicker {
            
            
            let currentUser = Auth.auth().currentUser
            let newImage = UIComponentHelper.resizeImage(image: selectedImage, targetSize: CGSize(width: 400, height: 400))
            guard  let imageProfiles = UIImagePNGRepresentation(newImage) else {
                self.alertError(message: "The process could not be completed. Please try again")
                return
            }
            let imageData : NSData = NSData(data: imageProfiles)
            let imageSize :Int = imageData.length
            
            if let user = currentUser {
                let riversRef = storageRef.child("userprofile-photo").child(user.uid)
                riversRef.putData(imageProfiles, metadata: nil, completion: { (metadata, error) in
                    
                    if Double(imageSize) > 500000 {
                        self.alertError(message: "You can not upload image more then 5 MB")
                        return
                    }
                    if let err = error {
                        UIComponentHelper.ProgressDismiss()
                        self.alertError(message: err.localizedDescription)
                        return
                    }else {
                        
                        print("NO Error Occur")
                    }
                    guard let metadata = metadata else {
                        UIComponentHelper.ProgressDismiss()
                        self.alertError(message: "Please check your internet connection and try again.")
                        return
                    }
                    // Metadata contains file metadata such as size, content-type, and download URL.
                    let downloadURL = metadata.downloadURL()!.absoluteString
                    let url = NSURL(string: downloadURL) as URL?
                    
                    let changeProfileimage = user.createProfileChangeRequest()
                    changeProfileimage.photoURL = url
                    changeProfileimage.commitChanges(completion: { (error) in
                        if error != nil {
                            self.alertError( message: (error?.localizedDescription)!)
                        }
                    })
                    
                    guard let urlString = url?.absoluteString else {
                        return
                    }
                    
                    self.userProfileInstance.imageUserCircleView.sd_setImage(with: url, placeholderImage: UIImage(named: "detailprofile-icon")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), options: [ .retryFailed, .continueInBackground], completed: { (image, error, ImageCache, Url) in
                        if error == nil {
                            UserCommitChnage = true
                            UIComponentHelper.ProgressDismiss()
                            RepositoryClass.shared.updateSpecificField(userID: user.uid, fieldName: "pfpUrl", valueNew: urlString)
                            RepositoryClass.shared.updateUserFirestoreSpecific(in: .users, and: user.uid, forField: "pfpUrl", forValue: urlString, completion: { (done) in
                                if done {
                                    
                                    self.alertMessageDialog(title: "Success", message: "Your profile has been updated.", actionTitle: "Okay")
                                    
                                }
                            })
                            
                            
                        }else {
                            UIComponentHelper.ProgressDismiss()
                            
                            self.alertError(message: "Please try to upload again.")
                            
                            
                        }
                    })
                    
                })
            }
            
            
        }
        else {
            print("error something")
        }
        dismiss(animated: true, completion: {
            
            if selectedImageFromPicker == nil {
                print("NO IMAGE ")
                self.alertError(message: "The process could not be completed. Please try again")
                
                return
            }
            
        })
        
        
        
    }
    
    
}

extension EditProfileController {
    
    @objc
    func saveButtonClicked() {
        
        guard let currentUser = Auth.auth().currentUser else {
            return
        }
        
        var lengthUsername : Int = (editProfileInstance.usernameTextField.text?.count)!
        let specialCharacters = UIComponentHelper.AvoidSpecialCharaters(specialcharaters: editProfileInstance.usernameTextField.text!)
        let countWhiteSpace : Int = UIComponentHelper.Countwhitespece(_whitespece: editProfileInstance.usernameTextField.text!)
        if (editProfileInstance.usernameTextField.text?.isEmpty)! {
            lengthUsername = (currentUser.displayName?.count)!
        }
        if (editProfileInstance.usernameTextField.text?.isEmpty)! {
            self.alertError(message: "Please enter your username.")
            
            return
        }
        if InternetConnection.isConnectedToNetwork() {
            print("Have internet")
        } else {
            self.alertError(message:  "Cannot update your Profile right now. Please Check your internet connection.")
            
            
            return
        }
        if lengthUsername < 5 {
            
            self.alertError(message:  "Please enter your name more than 5 characters.")
            
            return
            
        } else if lengthUsername > 20 {
            
            self.alertError(message:  "Please enter your name less than 20 characters.")
            
            return
            
        } else if specialCharacters == false {
            
            self.alertError(message:  "Your name should not contain special characters or special characters.")
            
            
            return
        } else if countWhiteSpace >= 3 {
            
            self.alertError(message:  "Your name should not contain more than 3 white spaces.")
            return
        } else {
            UIComponentHelper.showProgressWith(status: "Saving...", interact: false)
            let changeRequest = currentUser.createProfileChangeRequest()
            if self.editProfileInstance.usernameTextField.text == currentUser.displayName {
                UIComponentHelper.ProgressDismiss()
                self.alertMessageDialog(title: DialogErrorMessage().warningTitle, message: DialogErrorMessage().errorUsernameSameData, actionTitle: DialogTitleMessage().okayTitle)
                return
            }
            
            UIComponentHelper.ProgressDismiss()
            changeRequest.displayName = self.editProfileInstance.usernameTextField.text
            changeRequest.commitChanges(completion: { (error) in
                if error != nil {
                    UIComponentHelper.ProgressDismiss()
                    self.alertError(message: (error?.localizedDescription)!)
                }
            })
            
            self.alertMessageConfirmDone ()
            
        }
    }
    
}

extension EditProfileController {
    
    
    func enableImageInteract() {
        cardViewInstance.activityIndicator.stopAnimating()
        self.userProfileInstance.camera.isHidden = false
        IMAGELOAD = true
        self.userProfileInstance.imageUserCircleView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.imageDidClick)) )
        
        
    }
    
    @objc
    func swipeRecoginzer(_ sender: UISwipeGestureRecognizer?) {
        
        print("Moving")
        if sender?.direction == .left {
            print("Moving left")
        }
        if sender?.direction == .right {
            print("Moving right")
        }
        if sender?.direction == .up {
            print("Moving up")
        }
        if sender?.direction == .down {
            print("Moving down")
        }
        self.performAnimation(img: self.userProfileInstance.camera)
        
    }
    @objc
    func imagePan() {
        
        print("Image Pan Gesture")
        self.performAnimation(img: self.userProfileInstance.camera)
    }
    func imageLoadWithError(error: String) {
        self.userProfileInstance.imageUserCircleView.image = UIImage(named: "detailprofile-icon")
        
        IMAGELOAD = false
        self.alertMessageDialog(title: "Error", message: error, actionTitle: "Okay")
        
    }
    
    
}
