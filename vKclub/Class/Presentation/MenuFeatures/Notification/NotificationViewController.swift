//
//  NotificationViewController.swift
//  vKclub
//
//  Created by Pisal on 11/14/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import UIKit
import Material
import MaterialComponents
import DZNEmptyDataSet
import CoreData

class NotificationViewController: UITableViewController ,DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    
    private lazy var coverView: UIView = {
        
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        
        return view
        
    }()
    private let cellId = "cellId"
    private var notificationData = [NotificationModel]()
    private var notifications = [Notifications]()
    let personService = UserProfileCoreData()
    let property = ExploreCategoryComponents()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
    
        notification_num = 0
        if isEnableLoadView {
            loadingView()
        }
        if isLoggedIn() {
            //retrieveUserNotifications()
            loadData()
        }
        
        setupNavigation()
        setupTableviewDatasource()
    
        
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        UIComponentHelper.showActivityLoading(view: coverView, option: false)
        
    }

    func loadData() {
        
        let notificationRequest:NSFetchRequest<Notifications> = Notifications.fetchRequest()
        
        do {
            notifications = try managedObjectContext.fetch(notificationRequest)
            
            
            
            for i in notifications{
                i.notification_num = 0
            }
     
            isEnableLoadView = false
            self.coverView.isHidden = true
            
            UIComponentHelper.showActivityLoading(view: self.coverView, option: false)
            
        }catch {
            print("Could not load data from database \(error.localizedDescription)")
   
            self.coverView.isHidden = true
            UIComponentHelper.showActivityLoading(view: self.coverView, option: false)
            self.notificationData.removeAll()
        }
        
    }
    
    @objc
    func back() {
        dismiss(animated: true, completion: nil)
    }

    
}
// App Cycle

// handle empty table
extension NotificationViewController {
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str =  "You don't have any notification yet."
        let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)]
   
        
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str =  "To receive notification make sure you turn it on in setting."
        let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]

        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        
        if falseGetdata == false {
            
            return UIImage(named: "empty-notification")
        } else {
            return nil
        }
        
    }
    
}
// Tableview datasource
extension NotificationViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if notifications.count == 0 {
            print("Now there is no notification")
            
            return 0
            
        } else {
            
            loadData()
            
        }
        
        return notifications.count
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! NotificationTableCell
        let data = notifications[indexPath.item]
        cell.notificationProperty.titleLabel.text = data.notification_title
        cell.notificationProperty.bodyLabel.text = data.notification_body
        cell.notificationProperty.notificationImage.image = cell.notificationProperty.notificationImage.image?.withRenderingMode( .alwaysTemplate)
        cell.notificationProperty.notificationImage.tintColor = BaseColor.colorPrimary
        
        return cell
        
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let data = notifications[indexPath.item]
        
        self.alertMessageDialog(title: data.notification_title!, message: data.notification_body!, actionTitle: DialogTitleMessage().okayTitle)

    }
    
    func handleDeleteNotification(_cellIndex: IndexPath, _reverseIndex: IndexPath) {
        let callLogDataItem = notifications[_reverseIndex.row]
        notifications.remove(at: _reverseIndex.row)
        do {
            managedObjectContext.delete(callLogDataItem)
           
            try managedObjectContext.save()
        } catch {
            print("CAN\'T SAVE IN CELL DELETION \(error.localizedDescription)")
        }
        self.tableView.deleteRows(at: [_cellIndex], with: .automatic)
        self.loadData()
        self.tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        let lastRow: Int = self.tableView.numberOfRows(inSection: 0) - (indexPath.row + 1)
        let reverseIndexPath = IndexPath(row: lastRow, section: 0)
        if editingStyle == .delete {
            
            //FIRFireStoreService.shared.delete(get: data.id!, in: .users)
            handleDeleteNotification(_cellIndex: indexPath, _reverseIndex: reverseIndexPath)
            
        }
        
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return view.frame.height / 7.6
    }
    
    
    
}

// Helper functions
extension NotificationViewController {
    
    /// Setup appearance of the navigationbar
    fileprivate func setupNavigation() {

        self.navigationItem.titleLabel.text = "Notification"
        self.navigationItem.titleLabel.textColor = .white
        self.navigationItem.rightViews = [property.menuDeleteButton]
        self.navigationItem.backButton.tintColor = .white
        property.menuDeleteButton.addTarget(self, action: #selector(clearAll), for: .touchUpInside)
        
        
    }
    
    /// Setup both delegate and datasource of tableView and also register cell
    fileprivate func setupTableviewDatasource() {
        view.backgroundColor = .white
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.tableFooterView = UIView()
        
        tableView.register(NotificationTableCell.self, forCellReuseIdentifier: cellId)
        
    }
    
    
    /// Loading indicator while data is loading from server.
    fileprivate func loadingView() {
        
        self.view.addSubview(coverView)
        
        NSLayoutConstraint.activate([
            coverView.topAnchor.constraint(equalTo: self.view.topAnchor),
            coverView.widthAnchor.constraint(equalTo: view.widthAnchor),
            coverView.heightAnchor.constraint(equalTo: view.heightAnchor)
            ])
        UIComponentHelper.showActivityLoading(view: coverView, option: true)
        //UIComponentHelper.showProgressWith(status: "Loading...", interact: true)
    }
    

    /// Present dialog to let user decides to delete all notifications data
    func presentDialogDeleteAllDocuments () {
        
        let presentDialog = dialogMessage(title: "Delete All", message: "Are you sure want to delete all notifications. This cannot be undone.")
        let okay = MDCAlertAction(title: "Okay") { (action) in
            self.personService.deleteAllData(entity: "Notifications")
          
            self.loadData()
            self.tableView.reloadData()
        }
        let cancel = MDCAlertAction(title: "Cancel", handler: nil)
        presentDialog.addAction(okay)
        presentDialog.addAction(cancel)
        
        self.present(presentDialog, animated: true, completion: nil)
        
        
    }
    
    @objc
    func clearAll() {
        
        
        if notifications.isEmpty {
            self.alertMessageDialog(title: "Warning", message: "There is no data available to be deleted.", actionTitle: "Okay")
        } else {
            self.presentDialogDeleteAllDocuments()
        }
        
        
    }
}
class NotificationTableCell: UITableViewCell {
    
    let notificationProperty = NotificationProperty()
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        
        addSubview(notificationProperty.notificationImage)
        addSubview(notificationProperty.titleLabel)
        addSubview(notificationProperty.bodyLabel)
        
        
        NSLayoutConstraint.activate([
            notificationProperty.notificationImage.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            notificationProperty.notificationImage.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            notificationProperty.notificationImage.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 1/5),
            notificationProperty.notificationImage.widthAnchor.constraint(equalTo: self.heightAnchor, multiplier: 1/5)
            ])
        NSLayoutConstraint.activate([
            
            notificationProperty.titleLabel.leadingAnchor.constraint(equalTo: notificationProperty.notificationImage.trailingAnchor, constant: 10),
            notificationProperty.titleLabel.topAnchor.constraint(equalTo: notificationProperty.notificationImage.topAnchor, constant: -12),
            notificationProperty.titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor)
            
            ])
        
        NSLayoutConstraint.activate([
            
            notificationProperty.bodyLabel.topAnchor.constraint(equalTo: notificationProperty.titleLabel.bottomAnchor, constant: 0),
            notificationProperty.bodyLabel.leadingAnchor.constraint(equalTo: notificationProperty.titleLabel.leadingAnchor),
            notificationProperty.bodyLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20)
            ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
}
class NotificationProperty {
    
    struct Constant {
        static let title = "title"
        static let body = "body"
    }
    
    lazy var notificationImage: UIImageView = {
        
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFill
        image.image = UIImage(named: "notification")
        image.image = image.image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        return image
    }()
    lazy var titleLabel: UILabel = NotificationProperty.getlabel(title: Constant.title)
    lazy var bodyLabel: UILabel = NotificationProperty.getlabel(title: Constant.body)
    
    
  
    
    static func getlabel(title: String) -> UILabel{
        let label = UILabel()
        label.text = title
        
        label.translatesAutoresizingMaskIntoConstraints = false
        if title == Constant.title {
            if getDeviceModelName.userDeviceIphone5() {
                label.font = UIFont(name: roboto_bold, size: 14)
                
            }
            if getDeviceModelName.userDeviceIphone678Plus() {
                label.font = UIFont(name: roboto_bold, size: 18)
            } else {
                label.font = UIFont(name: roboto_bold, size: 22)
            }
        }
        if title == Constant.body {
            label.numberOfLines = 2
            if getDeviceModelName.userDeviceIphone5() {
                label.font = UIFont(name: roboto_regular, size: 11)
                
            }
            if getDeviceModelName.userDeviceIphone678Plus() {
                label.font = UIFont(name: roboto_regular, size: 14)
                
            } else {
                
                label.font = UIFont(name: roboto_regular, size: 16)
                
            }
        }
        
        return label
    }
    
}
class NotificationCell: MDCCollectionViewTextCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}


