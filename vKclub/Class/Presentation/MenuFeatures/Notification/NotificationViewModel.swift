//
//  NotificationViewModel.swift
//  vKclub Version 2
//
//  Created by Pisal on 6/25/2561 BE.
//  Copyright © 2561 BE Pisal. All rights reserved.
//

import Foundation
import Firebase

struct NotificationModel: Codable, Identifiable {
    
    var id: String? = nil
    var title: String?
    var body: String?
    
    
    init(title: String, body: String) {
        self.title = title
        self.body = body
        
    }
    
}

class LoadNotificationDataStatus {
    
    var loadingDataFromServer : Bool
    var getDataSuccess : Bool
    
    init(loadingDataFromServer: Bool, getDataSuccess: Bool) {
        self.loadingDataFromServer = loadingDataFromServer
        self.getDataSuccess = getDataSuccess
    }
    
}

