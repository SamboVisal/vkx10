//
//  FeedbackViewController.swift
//  vKclub
//
//  Created by Pisal on 11/8/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import UIKit

import JVFloatLabeledTextField
import FirebaseAuth

class FeedbackViewController: UIViewController{
    //
    //    var colorScheme = MDCSemanticColorScheme()
    //    var typographyScheme = MDCTypographyScheme()
    
    //var feedbackModel = [FeedbackModel]()
    let profileVariables = ProfileOverlayNavigationBar()
    let detailProfileVariables = DetailProfileVariables()
    let cardViewInstance = ExploreCategoryComponents()
    let loginRegisterComponent = EditProfileVariables()
    
    let message: JVFloatLabeledTextView = {
        
        let message = JVFloatLabeledTextView()
        message.placeholder = "Message"
        message.translatesAutoresizingMaskIntoConstraints = false
        message.floatingLabelShouldLockToTop = true
        message.font = UIFont(name: roboto_regular, size: 16)
        return message
    }()
    
    //    let message: MDCMultilineTextField = {
    //        let message = MDCMultilineTextField()
    //        message.translatesAutoresizingMaskIntoConstraints = false
    //        message.minimumLines = 2
    //
    //        message.font = UIFont(name: "SFCompactText-Regular", size: 16)
    //        return message
    //    }()
    //
    //    var allTextFieldControllers = [MDCTextInputControllerFilled]()
    //    func style(textInputController:MDCTextInputControllerFilled) {
    //        MDCFilledTextFieldColorThemer.applySemanticColorScheme(colorScheme, to: textInputController)
    //        MDCTextFieldTypographyThemer.applyTypographyScheme(typographyScheme, to: textInputController)
    //        if let textInput = textInputController.textInput {
    //            MDCTextFieldTypographyThemer.applyTypographyScheme(typographyScheme, to: textInput)
    //        }
    //    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        hideKeyboardWhenTappedAround()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "cancel-edit"), style: .plain, target: self, action: #selector(handleBackhome))

        
        //        let messageController = MDCTextInputControllerFilled(textInput: message)
        //        message.textView?.delegate = self
        //
        //        messageController.placeholderText = "Message"
        //        message.keyboardToolbar.barStyle = .black
        //        allTextFieldControllers.append(messageController)
        //
        
        setupView()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    @objc
    func handleBackhome() {
        
        if message.text.isEmpty {
            
            self.dismiss(animated: true, completion: nil)
        } else {
            
//            self.PresentDialogOneActionController(title: "Warning", message: "Are you sure want to leave? This will not save your information", actionTitle: "Okay")
            
        }
        
    }
    func setupView() {
        
        view.addSubview(profileVariables.profileView)
        view.addSubview(detailProfileVariables.detailProfileView)
        self.constraintProfileView()
        self.constraintViewBelowProfileView()
        
        self.profileViewSubview()
        self.constraintImageCoverView()
        
        self.setupLoginMainView()
        
    }
    func constraintProfileView() {
        
        profileVariables.profileView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        profileVariables.profileView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        profileVariables.profileView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        profileVariables.profileView.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        profileVariables.profileView.heightAnchor.constraint(equalToConstant: view.frame.height / 2.5).isActive = true
        
        
    }
    func constraintViewBelowProfileView() {
        
        detailProfileVariables.detailProfileView.topAnchor.constraint(equalTo: profileVariables.profileView.bottomAnchor).isActive = true
        detailProfileVariables.detailProfileView.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        detailProfileVariables.detailProfileView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        detailProfileVariables.detailProfileView.backgroundColor = .white
        
        
    }
    
    func profileViewSubview() {
        
        profileVariables.profileView.addSubview(profileVariables.imageUser)
        profileVariables.imageUser.addSubview(profileVariables.opacityDetailEachExplore)
        
        profileVariables.opacityDetailEachExplore.topAnchor.constraint(equalTo: profileVariables.imageUser.topAnchor).isActive = true
        profileVariables.opacityDetailEachExplore.leftAnchor.constraint(equalTo: profileVariables.imageUser.leftAnchor).isActive = true
        profileVariables.opacityDetailEachExplore.rightAnchor.constraint(equalTo: profileVariables.imageUser.rightAnchor).isActive = true
        profileVariables.opacityDetailEachExplore.heightAnchor.constraint(equalTo: profileVariables.profileView.heightAnchor).isActive = true
        
    }
    
    func constraintImageCoverView() {
        
        profileVariables.imageUser.topAnchor.constraint(equalTo: profileVariables.profileView.topAnchor).isActive = true
        profileVariables.imageUser.leftAnchor.constraint(equalTo: profileVariables.profileView.leftAnchor).isActive = true
        profileVariables.imageUser.rightAnchor.constraint(equalTo: profileVariables.profileView.rightAnchor).isActive = true
        profileVariables.imageUser.heightAnchor.constraint(equalTo: profileVariables.profileView.heightAnchor).isActive = true
        
    }
}

extension FeedbackViewController {
    
    func setupLoginMainView () {
        view.addSubview(cardViewInstance.mainCardView)
        cardViewInstance.mainCardView.addSubview(loginRegisterComponent.loginLabel)
        
        if getDeviceModelName.userDeviceIphone5() {
            NSLayoutConstraint.activate([
                cardViewInstance.mainCardView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -50),
                cardViewInstance.mainCardView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                cardViewInstance.mainCardView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
                cardViewInstance.mainCardView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
                cardViewInstance.mainCardView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 1/2.1)
                ])
        } else {
            
            cardViewInstance.mainCardView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -70).isActive = true
            cardViewInstance.mainCardView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            
            cardViewInstance.mainCardView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
            cardViewInstance.mainCardView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
            cardViewInstance.mainCardView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 1/2.5).isActive = true
        }
        
        
        
        loginRegisterComponent.loginLabel.text = "Feedback"
        loginRegisterComponent.loginLabel.topAnchor.constraint(equalTo: cardViewInstance.mainCardView.topAnchor, constant: 10).isActive = true
        
        loginRegisterComponent.loginLabel.centerXAnchor.constraint(equalTo: cardViewInstance.mainCardView.centerXAnchor).isActive = true
        
        self.setupFeedbackField()
    }
    
    func setupFeedbackField() {
        cardViewInstance.mainCardView.addSubview(message)
        cardViewInstance.mainCardView.addSubview(detailProfileVariables.divideView)
        cardViewInstance.mainCardView.addSubview(loginRegisterComponent.loginButton)
        NSLayoutConstraint.activate([
            message.topAnchor.constraint(equalTo: loginRegisterComponent.loginLabel.bottomAnchor, constant: 10),
            message.leadingAnchor.constraint(equalTo: cardViewInstance.mainCardView.leadingAnchor, constant: 10),
            message.trailingAnchor.constraint(equalTo: cardViewInstance.mainCardView.trailingAnchor, constant: -10),
            message.heightAnchor.constraint(equalTo: cardViewInstance.mainCardView.heightAnchor, multiplier: 1/1.7)
            ])
        
        NSLayoutConstraint.activate([
            
            detailProfileVariables.divideView.topAnchor.constraint(equalTo: message.bottomAnchor),
            detailProfileVariables.divideView.leadingAnchor.constraint(equalTo: message.leadingAnchor),
            detailProfileVariables.divideView.trailingAnchor.constraint(equalTo: message.trailingAnchor),
            detailProfileVariables.divideView.heightAnchor.constraint(equalToConstant: 1)
            
            ])
        
        NSLayoutConstraint.activate([
            
            loginRegisterComponent.loginButton.topAnchor.constraint(equalTo: message.bottomAnchor, constant: 10),
            loginRegisterComponent.loginButton.leadingAnchor.constraint(equalTo: message.leadingAnchor),
            loginRegisterComponent.loginButton.trailingAnchor.constraint(equalTo: message.trailingAnchor),
            loginRegisterComponent.loginButton.bottomAnchor.constraint(equalTo: cardViewInstance.mainCardView.bottomAnchor, constant: -10)
            
            ])
        
        loginRegisterComponent.loginButton.addTarget(self, action: #selector(handleSendFeedback), for: .touchUpInside)
        
        
        
    }
    @objc
    func handleSendFeedback() {
        
//        UIComponentHelper.showProgressWith(status: "Sending...", interact: false)
//        let content = FeedbackModel(deviceModel: getDeviceModelName.userDeviceModel, message: message.text, userName: (Auth.auth().currentUser?.displayName)!)
//
//        if !InternetConnection.isConnectedToNetwork() {
//            UIComponentHelper.ProgressDismiss()
//            self.PresentDialogOneActionController(title: "Error", message: "There was an error of submitting a form. Please try again.", actionTitle: "Okay")
//            return
//        }
//        if self.message.text.isEmpty {
//            UIComponentHelper.ProgressDismiss()
//            self.PresentDialogOneActionController(title: "Error", message: "There was an error of submitting a form. Please try again.", actionTitle: "Okay")
//            return
//        }
//        FIRFireStoreService.shared.feedbackFirestore(for: content, in: .feedback) { (done) in
//            if done {
//                UIComponentHelper.ProgressDismiss()
//
//                self.PresentDialogOneActionController(title: "Success", message: "The form has been submitted. Thank for using vKclub.", actionTitle: "Okay")
//                self.message.text = ""
//                return
//
//            } else {
//                UIComponentHelper.ProgressDismiss()
//                self.PresentDialogOneActionController(title: "Error", message: "There was an error of submitting a form. Please try again.", actionTitle: "Okay")
//            }
//        }
    }
}
