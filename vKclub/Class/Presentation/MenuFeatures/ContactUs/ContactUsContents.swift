//
//  ContactUsContents.swift
//  vKclub
//
//  Created by Pisal on 11/8/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import UIKit
import MaterialComponents

class ContactUsContentController: MDCCollectionViewController {
    
    let cellId = "CellId"
    let contents = ["English Speaker: (+855) 78 777 284", "Khmer Speaker: (+855) 96 2222 735"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.register(MDCCollectionViewTextCell.self, forCellWithReuseIdentifier: cellId)
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MDCCollectionViewTextCell
        cell.textLabel?.text = contents[indexPath.item]
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {


        return CGSize(width: view.frame.width, height: view.frame.height / 8)
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            print("Khmer speaker")
            self.presentDialog(row: 0)
        default:
            self.presentDialog(row: 1)
            print("English speaker")
        }
        
    }
    
    func presentDialog(row: Int) {
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad) {
            self.alertMessageDialog(title: "Warning", message: "Sorry! Your device does not support this feature.", actionTitle: "Okay")
            
            return
        }
        
        
        if row == 0 {
            askVerifyDialog(row: "078777284")
            
        } else {
            
            askVerifyDialog(row: "0962222735")
        }
        
        
    }
    func askVerifyDialog(row: String) {
        
        let dialog = MDCAlertController(title: "Make Call", message: "Are you sure want to call \(row)?")
        
        let okayButton = MDCAlertAction(title: "Call") { (action) in
            
            guard let number = URL(string: "tel://" + row ) else { return }
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(number, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
        }
        let cancel = MDCAlertAction(title: "Cancel", handler: nil)
        dialog.addAction(okayButton)
        dialog.addAction(cancel)
        self.present(dialog, animated: true, completion: nil)
        
    }
    
    
    
    
}
