
//
//  InternalCallController.swift
//  vKclub
//
//  Created by HuySophanna on 29/6/17.
//  Copyright © 2017 WiAdvance. All rights reserved.
//

import Foundation
import UIKit
import CoreData


class SipCallViewController: UIViewController {
    @IBOutlet weak var numberTextField: UITextField!
    static let extensionBtn = UIButton(type: .system)
//    var incomingCallInstance: IncomingCallController? = IncomingCallController()
    var dialPhoneNumber: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
  
        
    }
    
    func ChangeExtensionActiveStatus(color: UIColor) {
        InternalCallController.extensionBtn.tintColor = color
    }

    
    
    @IBAction func NumberBtnClicked(_ sender: Any) {
        dialPhoneNumber = dialPhoneNumber + ((sender as AnyObject).titleLabel??.text)!
        numberTextField.text = dialPhoneNumber
        
    }
    
    @IBAction func DelBtnClicked(_ sender: Any) {
        if (dialPhoneNumber.characters.count > 0) {
            let lastIndex = dialPhoneNumber.index(before: dialPhoneNumber.endIndex)
            dialPhoneNumber = dialPhoneNumber.substring(to: lastIndex)
            numberTextField.text = dialPhoneNumber
        }
        if (dialPhoneNumber.characters.count == 0) {
           
        }
    }
    
    @IBAction func CallBtnClicked(_ sender: Any) {
        if ( dialPhoneNumber.count  !=  0 ){
            
            InComeCallController.phoneNumber =  dialPhoneNumber
            self.present(InComeCallController(), animated: false, completion: nil)
            
            InComeCallController.dialPhone = true
            LinphoneManager.makeCall(phoneNumber : dialPhoneNumber)
            
        }
        
        
       
    }
    
}
