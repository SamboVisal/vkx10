//
//  AppDelegate.swift
//  vKclub
//
//  Created by Machintos-HD on 11/1/18.
//  Copyright © 2018 vKirirom.com. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import SDWebImage
import UserNotifications
import IQKeyboardManagerSwift
import PushKit


var IMAGELOAD = false
var incomingCallUser = ""
var USER_RESET_PASS = false
var UserCommitChnage = false
var falseGetdata = false
var isEnableLoadView = true
var notification_num = 0
var usetosave = false
var appactive = false
var connectionStatus = false
var connectionProgress = false
var TimeModCheck = Timer()
var LinphoneConnectionStatusFlag: Bool = true
let roboto_bold = "Roboto-Bold"
let roboto_regular = "Roboto-Regular"
let appDel: AppDelegate = UIApplication.shared.delegate as! AppDelegate
let managedObjectContext = appDel.persistentContainer.viewContext
let db = Firestore.firestore()
var backgroundTaskIdentifier : UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
let userDefaults = UserDefaults.standard
var userExtensionNumber = ""
/// Colors
let navigationBarColor = BaseColor.colorPrimary
let collectionViewBackgroundColor = BaseColor.colorBackground
var linphoneInit : LinphoneManager = LinphoneManager()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , UNUserNotificationCenterDelegate, MessagingDelegate{

    let callKitManager = CallKitCallInit(uuid: UUID(), handle: "")
    
    lazy var providerDelegate: ProviderDelegate = ProviderDelegate(callKitManager: self.callKitManager)
    
    
    func displayIncomingCall(uuid: UUID, handle: String, completion: ((NSError?) -> Void)?) {
        providerDelegate.reportIncomingCall(uuid: uuid, handle: handle, completion: completion)
    }
    
    
    class var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    let personService = UserProfileCoreData()
    var window: UIWindow?
    fileprivate func customNavigationBarTintcolor() {
        // Override point for customization after application launch.
        
        // Adding Tint color to navigationBar
        UINavigationBar.appearance().barTintColor = BaseColor.colorPrimary
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    fileprivate func handleUserTologoutIfDeleteApp() {
        // Handle user delete app and then logout user from device
        if userDefaults.bool(forKey: "hasRunBefore") == false {
            print("The app is launching for the first time. Setting UserDefaults...")
            
            do {
                
                try Auth.auth().signOut()
            } catch {
                
            }
            
            
            // Update the flag indicator
            userDefaults.set(true, forKey: "hasRunBefore")
            userDefaults.synchronize() // This forces the app to update userDefaults
            
            // Run code here for the first launch
            
        } else {
            print("The app has been launched before. Loading UserDefaults...")
            // Run code here for every other launch but the first
        }
    }
    
    fileprivate func helperFunctions() {
        IQKeyboardManager.shared.enable = true
        FirebaseApp.configure()
        
        let voipPushResgistry = PKPushRegistry(queue: DispatchQueue.main)
        
        voipPushResgistry.delegate = self
        
        voipPushResgistry.desiredPushTypes = [PKPushType.voIP]
        
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        helperFunctions()
        customNavigationBarTintcolor()
        handleUserTologoutIfDeleteApp()
        Messaging.messaging().delegate = self
        
        authorizeNotification()
        requestionAuthorizeNotification(application)
        
        // RepositoryClass.shared.deleteAllPhonebookCore()
        readAllPhoneBooksData()
        readAllMapMarkersData()
        
        return true
    }
    
    func readAllPhoneBooksData() {
        FIRFireStoreService.shared.readAllContactPhoneBook(in: FIRCollectionReference.users_dev, returning: UserProfileModel.self, completion: { (contacts, done) in
            
            if done {
                DispatchQueue.main.async(execute: {
                    RepositoryClass.shared.insertPhoneBookCore(userModel: contacts, completion: { (done) in
                        if done {
                            print("Inserted Phone book to core data Successfully")
                        }
                    })
                })
            }else{
                print("Data doesn't added to contact")
            }
        })
    }
    
    func readAllMapMarkersData() {
        FIRFireStoreService.shared.readAllMapData(in: FIRCollectionReference.information, and: FIRCollectionCell.mapsData, and: FIRCollectionReference.markers) { (markers, done) in
            if done {
                DispatchQueue.main.async(execute: {
                    mapDataCache.setObject(markers as AnyObject, forKey: "mapData" as AnyObject)
                    let occupiedMarkers = markers.filter({ (marker) -> Bool in
                        return !marker.images.isEmpty
                    })
                    for marker in occupiedMarkers {
                        DispatchQueue.main.async(execute: {
                            for imageUrl in marker.images {
                                guard let url = URL(string: imageUrl) else{
                                    return
                                }
                                URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                                    
                                    //download hit an error so lets return out
                                    if error != nil {
                                        print(error ?? "")
                                        return
                                    }
                                    
                                    DispatchQueue.main.async(execute: {
                                        if let downloadedImage = UIImage(data: data!) {
                                            mapImageCache.setObject(downloadedImage, forKey: imageUrl as AnyObject)
                                            print("==> image saved to mapImageCache")
                                        }
                                    })
                                }).resume()
                            }
                        })
                    }
                })
                
            }else{
                print("==> Markers data can't query from firebase right now.")
                return
            }
        }
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    fileprivate func authorizeNotification() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound], completionHandler: {(granted, error) in
            if granted {
                print("Notification access granted")
                UserDefaults.standard.set(1, forKey: "setting")
                let toekn = InstanceID.instanceID().token()
                print("Token ==== ", toekn ?? "")
            } else {
                UserDefaults.standard.set(0, forKey: "setting")
                print("User reject notification access")
                
            }
        })
    }
    
    fileprivate func requestionAuthorizeNotification(_ application: UIApplication) {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {data, error in
                    if error != nil{
                        self.showAlertAppDelegate(title: "Error", message: (error?.localizedDescription)! , buttonTitle:"Okay", window:self.window!)
                        
                        
                    }
                    
            })
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
    }
    

    func showAlertAppDelegate(title: String,message : String,buttonTitle: String,window: UIWindow){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertActionStyle.default, handler: nil))
        alert.dismiss(animated: true, completion: nil)
        window.rootViewController?.present(alert, animated: false, completion: nil)
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        appactive = false
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
    }
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Coredata")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}

// handle pushkit
extension AppDelegate: PKPushRegistryDelegate {
    
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        let token = pushCredentials.token.map { String(format: "%02.2hhx", $0) }.joined()
        print("voip token: \(token)")
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
        
        UIComponentHelper.scheduleNotification(_title: "Hello", _body: "World", _inSeconds: 1)
        
        if let user = Auth.auth().currentUser {
            print("User is available \(user)")
            if !LinphoneManager.CheckLinphoneConnectionStatus() {
                if !connectionProgress {
                    LinphoneConfigure.linphoneRegister()
                    
                }
                
            }
        }
        
        
        print("===== Did receive VOIP push =====")
        
        
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
        
    }
    
    
}



// handle notification delegate
extension AppDelegate {
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
        completionHandler(.alert)
        
        usetosave = true
        appactive = true
        if notification.request.content.userInfo["aps"] == nil {
            return
        }
        notification_num += 1
        
        print("notification Willpresent ", notification.request.content.userInfo["aps"] as! NSDictionary)
        let dict = notification.request.content.userInfo["aps"] as! NSDictionary
        let d: [String: Any] = dict["alert"] as! [String : Any]
        print("title message ", d["title"] as! String)
        let title: String = d["title"] as! String
        let body: String = d["body"] as! String
        
        
        let notificationModel = NotificationModel(title: title, body: body)
        
        
        self.saveNotificationContent(model: notificationModel)
        self.saveNotificationContentCoreData(notificationNum: notification_num, body: body, title: title)
        
        
    }
    
    fileprivate func saveNotificationContent(model: NotificationModel) {
        
        FIRFireStoreService.shared.addUserNotification(for: model, in: .users) { (success) in
            if success {
                print("======= Done added notification =====")
            }
        }
        
        
    }
    fileprivate func saveNotificationContentCoreData(notificationNum: Int, body: String, title: String) {
        
        personService.CreatnotificationCoredata(_notification_num: notificationNum, _notification_body: body, _notification_title: title)
        
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if response.notification.request.content.userInfo["aps"] == nil {
            return
        }
        
        // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
        notification_num += 1
        let dict = response.notification.request.content.userInfo["aps"] as! NSDictionary
        print (dict)
        let d : [String : Any] = dict["alert"] as! [String : Any]
        let body : String = d["body"] as! String
        let title : String = d["title"] as! String
        print("Title:\(title) + body:\(body)")
        
        if let username = Auth.auth().currentUser?.displayName {
            self.showAlertAppDelegate(title: "Hello "+username, message: title + ": " + body, buttonTitle:"Okay", window:self.window!)
        } else {
            self.showAlertAppDelegate(title: "Hello There", message: title + ": " + body, buttonTitle:"Okay", window:self.window!)
        }
        let notificationModel = NotificationModel(title: title, body: body)
        
        if usetosave == false {
            self.saveNotificationContent(model: notificationModel)
            self.saveNotificationContentCoreData(notificationNum: notification_num, body: body, title: title)
        } else if usetosave == true {
            if !appactive {
                self.saveNotificationContent(model: notificationModel)
                self.saveNotificationContentCoreData(notificationNum: notification_num, body: body, title: title)
            }
        }
        
        //UserDefaults.standard.setupEnableBadgeView(value: false)
        completionHandler()
    }
    @available(iOS 10.0, *)
    private func application(_ application: UIApplication, didRegister notificationSettings: UNNotificationSettings) {
        
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print(token)
        let data = Data(token.utf8)
        Auth.auth().setAPNSToken(data, type: AuthAPNSTokenType.unknown)
        print(deviceToken.description)
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            print(uuid)
        }
        UserDefaults.standard.setValue(token, forKey: "ApplicationIdentifier")
        UserDefaults.standard.synchronize()
        
        
    }
    
}


