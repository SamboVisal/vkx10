//
//  ExploreFirestore.swift
//  vKclub
//
//  Created by Pisal on 11/17/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import Foundation
import Firebase

extension FIRFireStoreService {
    
    
    
    func readExploreInformation<T: Encodable & Decodable>(in collectionReference: FIRCollectionReference, and collectionCell: FIRCollectionCell, type anotherCollectionReference: FIRCollectionReference,  returning objectType: T.Type, completion: @escaping ([T] ,_ sucess: Bool) -> Void) {
        
        collectionReferences(to: collectionReference).document(collectionCell.rawValue).collection(anotherCollectionReference.rawValue).getDocuments { (snapshot, error) in
            
            guard let snapshot = snapshot else {
                return
            }
            
            do {
                
                var objects = [T]()
                
                for document in snapshot.documents {
                    
                    let object = try document.decode(as: objectType.self)
                    objects.append(object)
                    
                    
                }
                print("Accommodation Data \(objects), and amount of it is \(objects.count)")
                completion(objects, true)
                let source = snapshot.metadata.isFromCache ? "local cache" : "server"
                print("Metadata: Data fetched from \(source)")
            } catch {
                completion( [], false)
                print(error)
            }
        }
    }
}

