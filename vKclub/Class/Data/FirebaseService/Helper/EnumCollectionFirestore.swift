//
//  EnumCollectionFirestore.swift
//  vKclub
//
//  Created by Pisal on 9/17/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import Foundation

// collection
enum FIRCollectionReference: String {
    
    case users
    case information
    case feedback
    case accommodation
    case activity
    case property
    case UserContacts
    case contacts
    case users_dev
    case markers
}

// Document
enum FIRCollectionCell: String {
    case maps_data
    case notifications
    case explore
    case accommodation
    case service
    case property
    case mapsData

}
