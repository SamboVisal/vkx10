//
//  MapFirestore.swift
//  vKclub
//
//  Created by Chhayrith on 12/15/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import Firebase

extension FIRFireStoreService {
    // Mark: - Get data from Firebase without encode and decode
    func readAllMapData(in collectionReference: FIRCollectionReference, and nextCollectionReference: FIRCollectionCell, and anotherCollectionReference: FIRCollectionReference, completion: @escaping ([MapDataModelHelper],_ success: Bool) -> Void) {
        
        collectionReferences(to: collectionReference).document(nextCollectionReference.rawValue).collection(anotherCollectionReference.rawValue).order(by:"title").addSnapshotListener({ (snapshot, error) in
            guard let snapshot = snapshot else {
                completion([], false)
                return
            }
            
            var objects = [MapDataModelHelper]()
            for document in snapshot.documents {
                let data = document.data()
                print("==> Data contain: \(data)")
                let latLng = data["latLng"] as! GeoPoint
                var images: [String] = []
                if data["images"] != nil {
                    if data["images"] is String {
                        images.append(data["images"] as! String)
                    }else if data["images"] is [String] {
                        images = data["images"] as! [String]
                    }
                }
                let model = MapDataModelHelper(title: data["title"] == nil ? "" : data["title"] as! String,
                                               snippet: data["snippet"] == nil ? "" : data["snippet"] as! String,
                                               latitude: latLng.latitude,
                                               longitude: latLng.longitude,
                                               icon: data["icon"] == nil ? "" : data["icon"] as! String,
                                               images: images)
                                               //images: data["images"] == nil ? [] : data["images"] as! [String])
                    
                objects.append(model)
            }
            completion(objects, true)
            let source = snapshot.metadata.isFromCache ? "Local Cache": "Server"
            print("==> Map markers data loaded from \(source) ")
        })
    }
}
