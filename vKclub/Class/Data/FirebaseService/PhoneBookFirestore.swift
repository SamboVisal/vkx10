//
//  PhoneBookFirestore.swift
//  vKclub
//
//  Created by Chhayrith on 12/3/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import Firebase
import UIKit

extension FIRFireStoreService {
    // Read data from ContactList
    func readAllContactPhoneBook<T: Encodable & Decodable>(in collectionReference: FIRCollectionReference, returning objectType: T.Type, completion: @escaping ([T],_ success: Bool) -> Void) {
        
        collectionReferences(to: collectionReference).order(by: "name").addSnapshotListener({ (snapshot, error) in
            guard let snapshot = snapshot else {
                return
            }
            
            do{
                var objects = [T]()
                for document in snapshot.documents {
                    let object = try document.decode(as: objectType.self, includingId: true)
                    objects.append(object)
                }
                
                completion(objects, true)
                let source = snapshot.metadata.isFromCache ? "Local Cache": "Server"
                print("==> Phonebook loaded from \(source) ")
            }catch{
                completion([], false)
                print(error)
            }
        })
    }
}
