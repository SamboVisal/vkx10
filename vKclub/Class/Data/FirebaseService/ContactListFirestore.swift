//
//  ContactListFirestore.swift
//  vKclub
//
//  Created by Chhayrith on 11/30/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import Firebase


extension FIRFireStoreService {
    
    // Read data from ContactList
    func readContactListInfo<T: Encodable & Decodable>(in collectionReference: FIRCollectionReference,and anotherCollectionReference: FIRCollectionReference, returning objectType: T.Type, completion: @escaping ([T],_ success: Bool) -> Void) {
        
        if let userId = Auth.auth().currentUser?.uid {
            collectionReferences(to: collectionReference).document(userId).collection(anotherCollectionReference.rawValue).order(by: "Name").addSnapshotListener { (snapshot, error) in
                
                guard let snapshot = snapshot else {
                    return
                }
                
                do{
                    var objects = [T]()
                    for document in snapshot.documents {
                        let object = try document.decode(as: objectType.self, includingId: true)
                        objects.append(object)
                    }
                    
                    completion(objects, true)
                    let source = snapshot.metadata.isFromCache ? "Local Cache": "Server"
                    print("Contact loaded from \(source) ")
                }catch{
                    completion([], false)
                    print(error)
                }
            }
        }
    }
    
    
    func readSingleContact<T: Encodable & Decodable>(in collectionReference: FIRCollectionReference, id: String, returning objectType: T.Type, completion: @escaping ([T],_ success: Bool) -> Void) {
        
        
        collectionReferences(to: collectionReference).document(id).getDocument { (snapshot, error) in
            
            guard let snapshot = snapshot else {
                return
            }
            
            do{
                var objects = [T]()
                let object =  try snapshot.decode(as: objectType.self)
                objects.append(object)
                completion(objects, true)
            }catch{
                completion([], false)
                print(error)
            }
        }
        
    }
    
    
    // Add One Contact to firebase.
    func addOneContact<T: Encodable & Decodable>(for encodableObject: T,in collectionReference: FIRCollectionReference, and anotherCollectionReference: FIRCollectionReference, completion: @escaping (_ success: Bool) -> Void) {
        do{
            let json = try encodableObject.toJson(excluding: ["id"])
            if let userId = Auth.auth().currentUser?.uid {
                collectionReferences(to: collectionReference).document(userId).collection(anotherCollectionReference.rawValue).addDocument(data: json, completion: {(error) in
                    if let error = error {
                        print(error)
                        completion(false)
                    }else{
                        completion(true)
                    }
                })
            }
        }catch{
            print(error)
        }
    }
    
    func editOneContact<T: Encodable & Decodable>(for encodableObject: T, get contactID: String,in collectionReference: FIRCollectionReference, and anotherCollectionReference: FIRCollectionReference, completion: @escaping (_ success: Bool) -> Void) {
        do{
            let json = try encodableObject.toJson(excluding: ["id"])
            DispatchQueue.main.async(execute: {
                contactCache.setObject(json as AnyObject, forKey: contactID as AnyObject)
            })
            if let userId = Auth.auth().currentUser?.uid {
                collectionReferences(to: collectionReference).document(userId).collection(anotherCollectionReference.rawValue).document(contactID).setData(json, options: SetOptions.merge(), completion: { (error) in
                    
                    if let error = error {
                        print(error)
                        completion(false)
                    }else{
                        completion(true)
                    }
                })
            }
        }catch{
            print(error)
        }
    }
    
    // Delete One Contact
    func deleteOneContact(get contactID: String, in collectionReference: FIRCollectionReference, and anotherCollectionReference: FIRCollectionReference,  completion: @escaping (_ sucess: Bool) -> Void) {
        if let userId = Auth.auth().currentUser?.uid {
            DispatchQueue.main.async(execute: {
                contactCache.removeObject(forKey: contactID as AnyObject)
            })
            collectionReferences(to: collectionReference).document(userId).collection(anotherCollectionReference.rawValue).document(contactID).delete { (error) in
                if let error = error {
                    print(error)
                    completion(false)
                }else{
                    completion(true)
                }
            }
        }
    }
    
    
}
