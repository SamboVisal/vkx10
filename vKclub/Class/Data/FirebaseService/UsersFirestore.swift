//
//  UsersFirestore.swift
//  vKclub
//
//  Created by Pisal on 9/13/2561 BE.
//  Copyright © 2561 BE WiAdvance. All rights reserved.
//

import Foundation
import Firebase
// User firestore
extension FIRFireStoreService {
    
    /// Store user information such as: Email, and password in order to verify when user login to our sever in Firestore
    /**
     - Parameters:
        - encodableObject: The cubes available for allocation
        - collectionReference: The people that require cubes
     */
    func createUserFirestore <T: Encodable>(for encodableObject: T, in collectionReference: FIRCollectionReference,  completion: @escaping FeedbackCompletionHandler) {
        
        do {
            
            let json = try encodableObject.toJson(excluding: ["id"])
            
            guard let id = Auth.auth().currentUser?.uid else {
                return
            }
            collectionReferences(to: collectionReference).document(id).setData(json, options: .merge()) { (error) in
                
                if error == nil {
                    completion(true)
                    print("Done created first user")
                } else {
                    completion(false)
                    print("Failed create user")
                }
                
            }
            
        } catch {
            print(error)
        }
        
    }
    
    
    
    func loginUserFirestore <T: Encodable & Decodable>(for encodableObject: T, in collectionReference: FIRCollectionReference,  returning objectType: T.Type, completion: @escaping FeedbackCompletionHandler) {
        
        do {
            let dataJson = try encodableObject.toJson()
            let uid = collectionReferences(to: .users).document((Auth.auth().currentUser?.uid)!)
            
            uid.getDocument { (data, error) in
                if error == nil {
                    
                    if (data?.data().isEmpty)! {
                        
                        uid.setData(dataJson, completion: { (error) in
                            
                            if let error = error {
                                print("Error set data \(error.localizedDescription)")
                                completion(false)
                            } else {
                                completion(true)
                            }
                        })
                    } else {
                        print("Data is not empty \(data?.data())")
                        completion(true)
                        
                    }
                    
                }
            }
        } catch {
            print(error)
        }
        
        
        
//        do {
//
//            let json = try encodableObject.toJson()
//
//            collectionReferences(to: .users).document((Auth.auth().currentUser?.uid)!).setData(json) { (error) in
//
//                if let error = error {
//                    print("Error set data \(error.localizedDescription)")
//                    completion(false)
//                } else {
//                    completion(true)
//                }
//
//            }
//
//
//        } catch {
//            print(error)
//        }
        
        
//        do {
//
//            let json = try encodableObject.toJson()
//
//
//
//            let d = collectionReferences(to: .users).whereField("email", isEqualTo: json["email"] as! String)
//            let datas = self.collectionReferences(to: collectionReference)
//            d.getDocuments { (data, error) in
//                if let err = error {
//                    print("Error ", err)
//                } else {
//                    if (data?.isEmpty)! {
//
//                        datas.addDocument(data: json, completion: { (err) in
//
//                            if let error = err {
//                                print("Error ", error)
//                                completion(false)
//                            } else {
//                                completion(true)
//                            }
//
//
//                        })
//                    } else {
//                        guard let snapshot = data else  {
//                            return
//                        }
//
//                        do {
//
//
//                            //print("user id ",id)
//                            for document in snapshot.documents {
//                                let object = try document.decode(as: objectType.self)
//                                print("Json data ", object)
//
//                            }
//
//                        }catch {
//                            print(error)
//                        }
//
//                        completion(true)
//                    }
//                }
//
//            }
//
//
//        } catch {
//            print(error)
//        }
        
    }
    
//    func readUser<T: UserModel> (for object: T, in collectionReference: FIRCollectionReference, returning objectType: T.Type, completion: @escaping ([T] ,_ sucess: Bool) -> Void{
//
//
//
//    }
    
    func readSingleUser<T: Encodable & Decodable>(in collectionReference: FIRCollectionReference, and documentID: String, returning objectType: T.Type, completion: @escaping([T], _ success: Bool) -> Void) {
        
        collectionReferences(to: collectionReference).document(documentID).getDocument { (snapshot, error) in
            
            guard let snapshot = snapshot else {
                return
            }
            
            var appendObject = [T]()
            do {
                
                let object = try snapshot.decode(as: objectType.self)
                appendObject.append(object)
                completion(appendObject, true)
            } catch {
                
                completion([], false)
                
                print(error)
            }
            
            
        }
        
        
    }
    
    func readUserProfileFirestore<T: Encodable & Decodable>(for encodableObject: T, in collectionReference: FIRCollectionReference,  returning objectType: T.Type, completion: @escaping ([T] ,_ sucess: Bool) -> Void) {
        
        guard let id = Auth.auth().currentUser?.uid else {
            return
        }
        
        let reference = collectionReferences(to: .users).document(id)
        reference.getDocument { (snapshot, error) in
            
            guard let snap = snapshot else {
                return
            }
            do {
                var objects = [T]()
                let decodeObject = try snap.decode(as: objectType.self)
                objects.append(decodeObject)
                
                print("Json data \(objects)")
                completion(objects, true)
            } catch {
                print(error)
            }
        }
        
//        do {
//            let json = try encodableObject.toJson()
//            print("Json data ",json.count)
//            let d = collectionReferences(to: .users).whereField("email", isEqualTo: json["email"] as! String)
//            d.getDocuments(completion: { (snapshot, error) in
//                guard let snapshot = snapshot else { return }
//                do {
//
//                    var objects = [T]()
//                    for document in snapshot.documents {
//                        let object = try document.decode(as: objectType.self)
//                        print("UpdateJson data ", object)
//                        objects.append(object)
//
//                    }
//                    if objects.count > 0 {
//
//                        completion(objects, true)
//                    } else {
//                        completion(objects, false)
//                    }
//
//                }catch {
//                    print(error)
//                }
//            })
//
//        } catch {
//            print(error)
//        }
    }
    
    // Update user EMAIL, USERNAME
    func updateUserFirestore<T: Encodable & Identifiable>(for encodableObject: T, in collectionReference: FIRCollectionReference) {
        
        do {
            let json = try encodableObject.toJson(excluding: ["id"])
            
            print("created one switch json \(String(describing: json["createdOneSwitch"]))")
            print("Json data updating ", json)
//            guard let id = encodableObject.id else { throw MyError.encodingError }
//            print("Id user is ", id)
            collectionReferences(to: collectionReference).document((Auth.auth().currentUser?.uid)!).setData(json)
            
        } catch {
            print(error)
        }
    }
    
    
//    func feedbackFirestore <T: Encodable & FeedbackID>(for encodableObject: T, in collectionReference: FIRCollectionReference, completion: @escaping FeedbackCompletionHandler) {
//        
//        do {
//            var json = try encodableObject.toJson(excluding: ["id"])
//            //guard let id = encodableObject.id else {throw MyError.encodingError}
//            // print("Feedback id ", id)
//        
//            json["timestamp"] = FieldValue.serverTimestamp()
//          
//            collectionReferences(to: .feedback).addDocument(data: json, completion: { (error) in
//                if let error = error {
//                    print(error)
//                    completion(false)
//                } else {
//                    print(json)
//                    completion(true)
//                }
//            })
//            
//        } catch {
//            print(error)
//        }
//        
//    }
    
}
