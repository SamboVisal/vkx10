//
//  ChhayrithReusableFunction.swift
//  vKclub
//
//  Created by Chhayrith on 12/19/18.
//  Copyright © 2018 WiAdvance. All rights reserved.
//

import UIKit


class ChhayrithReusableFunction {
    static let shared = ChhayrithReusableFunction()
    
    /// 1. Download image via url string. Then save to cache provided.
    /// 2. Load image from from cache provided.
    func loadImageToCacheWithUrlString( urlString: String, cacheName: NSCache<AnyObject, AnyObject>, completion: @escaping (_ success: Bool, _ image: UIImage) -> Void) {
        print("==> URL String \(urlString)")
        
        //check cache for image first
        if let cachedImage = cacheName.object(forKey: urlString as AnyObject) as? UIImage {
            print("==> image load from Cache \(contactProfileImgCache)")
            completion(true, cachedImage)
        }
        
        //otherwise fire off a new download
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            //download hit an error so lets return out
            guard let data = data, error == nil else {
                print(error ?? "")
                return
            }
            
            DispatchQueue.main.async {
                if let downloadedImage = UIImage(data: data) {
                    cacheName.setObject(downloadedImage, forKey: urlString as AnyObject)
                    print("==> image load from Server")
                    completion(true, downloadedImage)
                }
            }
        }).resume()
    }
}
