
Hello There. Welcome to vkClub.

If you want to contribute to vkClub just kindly clone our project and work on it. (^_^)

Remember: 
1. After you clone you might have to install pod. 
-> Terminal: pod install
2. Also you might see some errors:
-> if the error cause by the pod library please kindly search on google or follow these steps
    1. self.bringSubviewToFront(badgeLabel) -> rename it to   self.bringSubview(toFront: badgeLabel)
    2. NSAttributedString.Key   -> change to NSAttributedStringKey
    3. CAMediaTimingFunctionName.easeInEaseOut -> kCAMediaTimingFunctionEaseIn
    4. UITableView.automaticDimension -> UITableViewAutomaticDimension
    5. NSLayoutConstraint.Attribute.width -> NSLayoutAttribute.width
    6. NSLayoutConstraint.Attribute.height -> NSLayoutAttribute.height
    7. UIApplication.didBecomeActiveNotification -> NSNotification.Name.UIApplicationDidBecomeActive
    8. UIApplication.didEnterBackgroundNotification -> NSNotification.Name.UIApplicationDidEnterBackground

-> If the error cause during the compile time, please kindly read the error in the box you will understant. 
Error in compile time may cause by file no path or file duplication.
-> To solve it you might want to go to vkClub project file then go to Build Phases section on the top navigation bar.
-> Then filter the file you want to find in filter box. Then solve it based on your problem.

-> If the error in compile time related to Main.storyboard, delete the derive folder. 
To delete derived folder go to File -> Setting workspace -> Above Advance and then click on  forward arrow key -> then delete all the vkclub folder that you see.

==> If you don't have your ssh key in gitlab <===
To add ssh key to gitlab
1. Create one rsa key from xcode 10 itself after logged to gitlab. Don’t try to create by ourselves in terminal, it’s useless because xcode won’t accept it.
2. Add that rsa key to xcode 9
3. That’s it.

